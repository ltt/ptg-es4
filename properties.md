[comment]: # (written by Yilin Liu)
[comment]: # (created on 10.07.2021)
[comment]: # (last edited by )

Properties 
========
|   |  chemical formula  |molar mass (kg/kmol)<sub>[1]</sub> | density (at STP, kg/m3)<sub>[1]</sub>  | Heat capacity (kJ/(kg*k)<sub>[3,4]</sub> | enthalpie of formation (kJ/mol)<sub>[2]</sub> | standard entropy (kJ/mol)<sub>[2]</sub> |
|---|---|---|---|---|---|---|
|Water    |H<sub>2</sub>O   | 18.02  |  1 |   4.187 | -285.84(l)/-241.82(g)| 69.94(l)/188.72(g)|
|Hydrogen |  H<sub>2 |2.016   |  0.09 | 14.304    | 0 | 130.57 |
|Oxygen   |O<sub>2   | 32  | 1.429  |  0.919  | 0 | 205.03 |
|Carbon Dioxide | CO<sub>2 | 44.01 | 1.95 | 0.0846  | -393.52 | 213.69 |
|Methane | CH<sub>4 | 16.04 | 0.72 | 2.21 | -74.8 | 186.15 |

## Higher heating value (HHV) & lower heating value (LHV)

|   |kJ/mol<sub>[6]</sub>     | | kJ/kg<sub>[6]</sub> | | kJ/Nm3<sub>[6]</sub> | | kWh/kmol<sub>[6]</sub> | | kWh/kg<sub>[5]</sub> | | kWh/Nm3<sub>[5]</sub> | |
|---|:---|:---|:---|:---|:---|:---|:---|:---|:---|:---|:---|:---|
|   | HHV | LHV | HHV | LHV | HHV | LHV | HHV | LHV | HHV | LHV | HHV | LHV |
|Hydrogen | 285.83 | 241.82 | 141700| 120000| 12700| 10800| 79.43 | 67.13 | 39.4 |33.3 | 3.54 | 3 |
|Methane|  890.36 | 802.34  | 55500| 50000| 39800 | 35800| 247.02 | 222.96 | 15.4 | 13.9 | 11.03| 9.94 |

## Sources
[[1] Heintz, Andreas. Thermodynamik. Berlin, Springer Spektrum, 2017.](https://link.springer.com/content/pdf/10.1007%2F978-3-662-49922-1_13.pdf )  
[[2] Langeheinecke et al. Thermodynamik für Ingenieure. Wiesbaden, Vieweg+Teubner Verlag, 2011.](https://link.springer.com/content/pdf/bbm%3A978-3-8348-9903-3%2F1.pdf)  
[[3] Evans, Paul. Specific heat capacity of materials. The Engineering Mindset, 16 Oct. 2016. theengineeringmindset.com/specific-heat-capacity-of-materials/ Accessed 23 Jul. 2021](https://theengineeringmindset.com/specific-heat-capacity-of-materials/)  
[[4] Liste der spezifischen Wärmekapazitäten. chemie.de. www.chemie.de/lexikon/Liste_der_spezifischen_Wärmekapazitäten. Accessed 23 Jul. 2021.](https://www.chemie.de/lexikon/Liste_der_spezifischen_Wärmekapazitäten.html)   
[[5] Meier, Boris. Heiz- und Brennwerte. Hochschule für Technik Rapperswil, IET. 20 Nov 2014.](https://www.iet.hsr.ch/fileadmin/user_upload/iet.hsr.ch/Power-to-Gas/Kurzberichte/10_Heiz-_und_Brennwerte.pdf)   
[[6] Fuels - Higher and Lower Calorific Values. Engineering ToolBox, 2003. www.engineeringtoolbox.com/fuels-higher-calorific-values-d_169. Accessed 23 Jul. 2021.](https://www.engineeringtoolbox.com/fuels-higher-calorific-values-d_169.html)  




