"""Example Components for the COMANDO framework."""

# created by Luka Bornemann
# edited by Yifan Wang
# based on Baumgärter LowcarbonUS


from comando.core import System, Component, BINARY

class VollComponent_adapted(Component):
    """Specification of a Voll Cponent.

    Arguments
    ---------
    - label : str
        Unique string that serves as an identifier of this Demand.
    - nom_ref: The nominal reference size of the component
    - c_ref: coefficient of investment costs
    - c_m: factor of investment costs corresponding to maintenance costs
          TODO: This implies calculations with a year as the time series!
    - M: (1) exponent of investment costs
    - nom_min: (0) lower bound for nominal size
    - nom_max: (None) upper bound for nominal size
    - min_part_load: (0) minimum allowed value for relative output
    - base_eff: the base efficiency of the component
    - fit_params: dict of power, coefficient items for polynomial
        approximation of efficiency
    - in_name: name of input commodity
    - out_name: name of output commodity
    - in_connector_name: name of input connector
    - out_connector_name: name of output connector
    - exists: bool
        Specifies whether the Component is currently installed; if set to
        True, its nominal size will be a user-specified parameter. If set
        to False, the nominal size of the component is a design variable.
    - optional: bool
        Specifies whether the Component may be added or removed.
        If exists is True, setting optional to True allows the Component to
        be sold for a user-specified price.
        If exists is False, setting optional to False requires the
        component to be installed with a minimal nominal size of at least
        `nom_min`, while setting it to True adds the posibility of not
        installing it.


    LB: Added self.add_expression('available', available) to use in inverter sub class
    YW: doupled operational constraints, bi-direction electricity flow
    """

    def __init__(self, label, nom_ref, c_ref, c_m, M=1, nom_min=0,
                 nom_max=None, min_part_load=0, fit_params=None, # (YW: above DC = AC)
                 AC2DC_eff=1, DC2AC_eff=1,
                 in_name_1='AC_input', out_name_1='DC_output',
                 in_name_2='DC_input', out_name_2='AC_output',
                 in_connector_name_1='AC_IN', out_connector_name_1='DC_OUT',
                 in_connector_name_2='DC_IN', out_connector_name_2='AC_OUT',
                 available = 1, exists=False, optional=True ):

        # If no fit_params are given we assume no dependency on relative output
        if not fit_params:
            fit_params = {0: 1}  # results in eff = base_eff

        super().__init__(label)

        # nominal (installed) output power
        nom_name = out_name_1 + '_nom' # YW: nom_AC = nom_DC
        available = 1

        # YW: design constraints -> same as Voll-Components
        if exists:
            if optional:  # Component exits but may be sold
                installed = self.make_parameter(nom_name)
                self.add_le_constraint(nom_min, installed,
                                       name=nom_name + '_min')
                self.add_le_constraint(installed, nom_max,
                                       name=nom_name + '_max')
                keep = self.make_design_variable('keep', BINARY, init_val=1)
                available = keep
                out_nom = keep * installed

                # IDEA: Add a discount factor, that would make more sense!
                # TODO: Is that a bad formulation?
                revenue = c_ref * ((installed * (1 - keep)) / nom_ref) ** M
                # revenue from selling counts as negative investment costs
                self.add_expression('investment_costs', -revenue)
            else:  # Component exists and may NOT be sold
                # TODO: This shows that we might want to follow GAMS, Pyomo,
                #       etc. and introduce bounds on parameters!
                out_nom = self.make_parameter(nom_name)
                self.add_le_constraint(nom_min, out_nom,
                                       name=nom_name + '_min')
                self.add_le_constraint(out_nom, nom_max,
                                       name=nom_name + '_max')

            # investment cost of the component
            inv = c_ref * (out_nom / nom_ref) ** M
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)
        else:
            # TODO: If the component does NOT exist we might add a method to
            #       the component to specify a finite number of possible
            #       nominal sizes.
            if optional:  # Component doesn't exist but may be installed or not
                out_nom = self.make_design_variable(nom_name,
                                                    bounds=(0, nom_max),
                                                    init_val=nom_max)
                exists = self.make_design_variable('exists', BINARY,
                                                   init_val=1)
                available = exists
                self.add_le_constraint(exists * nom_min, out_nom,
                                       name=nom_name + '_min')
                self.add_le_constraint(out_nom, exists * nom_max,
                                       name=nom_name + '_max')
            else:  # Component doesn't exist and must be installed
                out_nom = self.make_design_variable(nom_name,
                                                    bounds=(nom_min, nom_max),
                                                    init_val=nom_max)
            # LB: Added expression to use in inverter subclass
            # YW: comment out
            # self.add_expression('available', available)
            # investment cost of the component
            inv = c_ref * (out_nom / nom_ref) ** M

            # set (investment) costs
            self.add_expression('investment_costs', inv)
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)


        ### YW: operational constraints -> doubled
        # relative output
        out_rel_ac2dc = self.make_operational_variable(out_name_1 + '_rel',  bounds=(0, 1), init_val=1)
        out_rel_dc2ac = self.make_operational_variable(out_name_2 + '_rel', bounds=(0, 1), init_val=1)

        output_ac2dc = out_rel_ac2dc * out_nom
        output_dc2ac = out_rel_dc2ac * out_nom

        # YW: assumed that min part-loads are same
        min_part_load_ac2dc = min_part_load
        min_part_load_dc2ac = min_part_load

        self.add_expression('min_part_load_ac2dc', min_part_load_ac2dc)
        self.add_expression('min_part_load_dc2ac', min_part_load_dc2ac)

        # YW: binary indicating whether the component is operational, and which direction is activated
        operating_ac2dc = self.make_operational_variable('operating_ac2dc', domain=BINARY, init_val=0)
        operating_dc2ac = self.make_operational_variable('operating_dc2ac', domain=BINARY, init_val=1)

        self.add_le_constraint(operating_ac2dc + operating_dc2ac, 1, name='bi-direction_switch')

        if available != 1:  # available is variable
            self.add_le_constraint(operating_ac2dc, available, name='availablility_ac2dc')
            self.add_le_constraint(operating_dc2ac, available, name='availablility_dc2ac')

        self.add_ge_constraint(out_rel_ac2dc, operating_ac2dc * min_part_load_ac2dc, name='min_part_load_ac2dc')
        self.add_le_constraint(out_rel_ac2dc, operating_ac2dc, name='max_part_load_ac2dc')

        self.add_ge_constraint(out_rel_dc2ac, operating_dc2ac * min_part_load_dc2ac, name='min_part_load_dc2ac')
        self.add_le_constraint(out_rel_dc2ac, operating_dc2ac, name='max_part_load_dc2ac')


        # efficiency in terms of relative output
        numerator = 0
        denominator = 0
        for power, coeff in fit_params.items():
            if power is None:
                denominator += coeff
            elif power < 0:
                denominator += coeff * out_rel_ac2dc ** (-power)
            else:
                numerator += coeff * out_rel_ac2dc ** power
        eff_ac2dc = numerator if denominator == 0 else numerator / denominator
        eff_ac2dc *= AC2DC_eff
        self.add_expression('eff_ac2dc', eff_ac2dc)

        eff_dc2ac = eff_ac2dc.subs(out_rel_ac2dc, out_rel_dc2ac)
        eff_dc2ac = eff_dc2ac.subs(AC2DC_eff, DC2AC_eff)

        """
        numerator = 0
        denominator = 0
        for power, coeff in fit_params.items():
            if power is None:
                denominator += coeff
            elif power < 0:
                denominator += coeff * out_rel_dc2ac ** (-power)
            else:
                numerator += coeff * out_rel_dc2ac ** power
        eff_dc2ac = numerator if denominator == 0 else numerator / denominator
        
        eff_dc2ac *= DC2AC_eff
        """

        self.add_expression('eff_dc2ac', eff_dc2ac)

        inp_rel_init_ac2dc = 1 / eff_ac2dc.subs(out_rel_ac2dc, 1)
        inp_rel_init_dc2ac = 1 / eff_dc2ac.subs(out_rel_dc2ac, 1)

        inp_rel_max_ac2dc = 1 / eff_ac2dc.subs(out_rel_ac2dc, min_part_load_ac2dc)
        inp_rel_max_dc2ac = 1 / eff_dc2ac.subs(out_rel_dc2ac, min_part_load_dc2ac)

        inp_rel_ac2dc = self.make_operational_variable(in_name_1 + '_rel', bounds=(0, inp_rel_max_ac2dc),
                                                       init_val=inp_rel_init_ac2dc)
        inp_rel_dc2ac = self.make_operational_variable(in_name_2 + '_rel', bounds=(0, inp_rel_max_dc2ac),
                                                       init_val=inp_rel_init_dc2ac)

        self.add_le_constraint(inp_rel_ac2dc, operating_ac2dc * inp_rel_max_ac2dc, name='input_active_ac2dc')
        self.add_le_constraint(inp_rel_dc2ac, operating_dc2ac * inp_rel_max_dc2ac, name='input_active_dc2ac')

        self.add_eq_constraint(inp_rel_ac2dc * eff_ac2dc, out_rel_ac2dc, name='input_output_relation_ac2dc')
        self.add_eq_constraint(inp_rel_dc2ac * eff_dc2ac, out_rel_dc2ac, name='input_output_relation_dc2ac')

        self.add_expression('ac2dc_input', inp_rel_ac2dc * out_nom)
        self.add_expression('ac2dc_output', output_ac2dc)

        self.add_expression('dc2ac_input', inp_rel_dc2ac * out_nom)
        self.add_expression('dc2ac_output', output_dc2ac)

        # set connectors
        self.add_input(in_connector_name_1, inp_rel_ac2dc * out_nom)
        self.add_output(out_connector_name_1, output_ac2dc)

        self.add_input(in_connector_name_2, inp_rel_dc2ac * out_nom)
        self.add_output(out_connector_name_2, output_dc2ac)

class Inverter_adapted(VollComponent_adapted):
    """Inverter as in Baumgärter LowcarbonUS.

    design variable: nominal heating power to be installed
    operational variable: operational output (heat and elec.) for each timestep
    The investment costs 'c_inv' is a nonlinear function of the nominal thermal
    output 'Qdot_nom':
        c_inv = c_inv,ref * (Qdot_nom/Qdot_ref)^M
    The input 'Qdot_in' can be determined via the efficiency relation:
        Qdot_in =  Qdot_out / eff_th
    The thermal efficiency 'eff_th' is determined via the product of nominal
    thermal efficiency and a polynomial fitting function f(q), with the load
    fraction q = Qdot_out/Qdot_nom, giving the relationship:
        eff_th = f(q) * eff_th_nom
    The electric efficiency 'eff_el' is determined via the product of nominal
    electrical efficiency and f(q), giving the relationship:
        eff_el = f(q) * eff_el_nom
    Thus the output power can be computed as:
        P_out = Qdot_in * eff_el = Qdot_out / eff_th * eff_el

    LB:
    - Added Qdot to __init__ to create class objects with different nominal power values
    - Copied structure from VollComponent:
        - Divided class into AC->DC & DC->AC part
        - Only one direction can be used at a time
        - AC-Side: .IN_AC, .OUT_DC
        - DC-Side: .IN_DC, .OUT_AC
    """

    Qdot_ref = 1  # [kW], reference nominal power
    c_ref = 486.77  # [€], reference cost
    c_m = 0.01  # [-], maintenance coefficient, (fraction of investment cost)
    M = 0.8687  # [-], cost exponent

    Qdot_min = 0  # [kW], minimal nominal power allowed for the model
    Qdot_max = 1300  # [kW], maximal nominal power allowed for the model

    qdot_min = 0  # [-] minimum modeled thermal output part load
    ac2dc_eff_nom = 0.95  # [-], nominal efficiency
    dc2ac_eff_nom = 0.955 # [-], added by YW
    # Our own fit which avoids fractional representation

    #
    fit_params = {0: 1}

    def __init__(self, label, exists=False, optional=True):
        super().__init__(label, self.Qdot_ref, self.c_ref, self.c_m, M=self.M,
                         nom_min=self.Qdot_min, nom_max=self.Qdot_max, min_part_load=self.qdot_min,
                         AC2DC_eff=self.ac2dc_eff_nom, DC2AC_eff=self.dc2ac_eff_nom,
                         fit_params=self.fit_params,
                         in_name_1='AC_in', out_name_1='DC_out',
                         in_name_2='DC_in', out_name_2='AC_out',
                         in_connector_name_1='AC_IN', out_connector_name_1='DC_OUT',
                         in_connector_name_2='DC_IN', out_connector_name_2='AC_OUT',
                         exists=exists, optional=optional)

