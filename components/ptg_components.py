"""
Components for energy system PtG_MSE

- created by YW on 28.06.2022
- last edited by YW on 28.06.2022
"""

from comando.core import Component, BINARY

class VollComponent(Component):
    """Specification of a Voll Component.

    Arguments
    ---------
    - label : str
        Unique string that serves as an identifier of this Demand.
    - nom_ref: The nominal reference size of the component
    - c_ref: coefficient of investment costs
    - c_m: factor of investment costs corresponding to maintenance costs
          TODO: This implies calculations with a year as the time series!
    - M: (1) exponent of investment costs
    - nom_min: (0) lower bound for nominal size
    - nom_max: (None) upper bound for nominal size
    - min_part_load: (0) minimum allowed value for relative output
    - base_eff: the base efficiency of the component
    - fit_params: dict of power, coefficient items for polynomial
        approximation of efficiency
    - in_name: name of input commodity
    - out_name: name of output commodity
    - in_connector_name: name of input connector
    - out_connector_name: name of output connector
    - exists: bool
        Specifies whether the Component is currently installed; if set to
        True, its nominal size will be a user-specified parameter. If set
        to False, the nominal size of the component is a design variable.
    - optional: bool
        Specifies whether the Component may be added or removed.
        If exists is True, setting optional to True allows the Component to
        be sold for a user-specified price.
        If exists is False, setting optional to False requires the
        component to be installed with a minimal nominal size of at least
        `nom_min`, while setting it to True adds the posibility of not
        installing it.
    """

    def __init__(self, label, nom_ref, c_ref, c_m, M=1, nom_min=0, nom_max=None, nom_size=None,
                 min_part_load=0, max_part_load = 1, base_eff=1, fit_params=None,
                 in_name='input', out_name='output', in_connector_name='IN',
                 out_connector_name='OUT', exists=False, optional=True):
        # If no fit_params are given we assume no dependency on relative output
        if not fit_params:
            fit_params = {0: 1}  # results in eff = base_eff

        super().__init__(label)

        # nominal (installed) output power
        nom_name = out_name + '_nom'
        available = 1

        if exists:
            if optional:  # Component exits but may be sold

                installed = self.make_parameter(nom_name)
                self.add_le_constraint(nom_min, installed, name=nom_name + '_min')
                self.add_le_constraint(installed, nom_max, name=nom_name + '_max')

                keep = self.make_design_variable('keep', BINARY, init_val=1)

                available = keep
                out_nom = keep * installed

                # IDEA: Add a discount factor, that would make more sense!
                # TODO: Is that a bad formulation?
                revenue = c_ref * ((installed * (1 - keep)) / nom_ref) ** M
                # revenue from selling counts as negative investment costs
                self.add_expression('investment_costs', -revenue)

            else:  # Component exists and may NOT be sold
                # TODO: This shows that we might want to follow GAMS, Pyomo,
                #       etc. and introduce bounds on parameters!
                # added by YW
                if nom_size is None:
                    out_nom = nom_max
                else:
                    out_nom = nom_size
                    nom_max = nom_size
                    nom_min = nom_size

                out_nom = self.make_parameter(nom_name, nom_max)
                #out_nom = nom_max # comment YW, not ok for CHP
                #self.add_le_constraint(nom_min, out_nom, name=nom_name + '_min')
                #self.add_le_constraint(out_nom, nom_max, name=nom_name + '_max')

            # investment cost of the component
            inv = c_ref * (out_nom / nom_ref) ** M
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)
        else:
            # TODO: If the component does NOT exist we might add a method to
            #       the component to specify a finite number of possible
            #       nominal sizes.
            if optional:  # Component doesn't exist but may be installed or not
                out_nom = self.make_design_variable(nom_name, bounds=(0, nom_max), init_val=nom_max)  # YW change LB to nom_min instead of 0
                exists = self.make_design_variable('exists', BINARY, init_val=1)

                available = exists

                self.add_le_constraint(exists * nom_min, out_nom, name=nom_name + '_min')
                self.add_le_constraint(out_nom, exists * nom_max, name=nom_name + '_max')

            else:  # Component doesn't exist and must be installed
                out_nom = self.make_design_variable(nom_name,  bounds=(nom_min, nom_max), init_val=nom_max)

            # investment cost of the component
            inv = c_ref * (out_nom / nom_ref) ** M

            # set (investment) costs
            self.add_expression('investment_costs', inv)
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)

        # relative output
        out_rel = self.make_operational_variable(out_name + '_rel', bounds=(0, 1), init_val=1) # YW change LB to nom_min instead of 0
        output = out_rel * out_nom

        # If minimal part load is considered, we need to decide whether it's
        # operational or not to determine the lower bound for the part load.
        self.add_expression('min_part_load', min_part_load)

        if min_part_load == 0:
            operating = 1
        else:
            # binary indicating whether the component is operational
            operating = self.make_operational_variable('operating', domain=BINARY, init_val=1)

            if available != 1:  # available is variable
                self.add_le_constraint(operating, available, name='availablility') #operating <= exists

            self.add_ge_constraint(out_rel, operating * min_part_load, name='min_part_load')

        self.add_le_constraint(out_rel, operating,  name='max_part_load')

        # efficiency in terms of relative output
        # fit_params dict with parameters that allow the definition of an
        # efficiency in terms of the part-load fraction p in form of a ratio of
        # polynomials, e.g.:
        #            c_0 + c_1 * p ** 1 + ... + c_n * p ** n
        # eff = --------------------------------------------
        #       c_None + c_-1 * p ** 1 + ... + c_-m * p ** m
        # coefficients of the numerator are the values in fit_params when using
        # the corresponding exponent as key while those of the denominator are
        # the values corresponding to the key with the negative exponent.

        numerator = 0
        denominator = 0

        for power, coeff in fit_params.items():
            if power is None:
                denominator += coeff
            elif power < 0:
                denominator += coeff * out_rel ** (-power)
            else:
                numerator += coeff * out_rel ** power
        eff = numerator if denominator == 0 else numerator/denominator
        eff *= base_eff

        self.add_expression('eff', eff)

        # init_guess = nom_max/eff.subs(out_rel, 1)
        #input = self.make_operational_variable(in_name, bounds=(0, None), init_val=init_guess)
        # self.add_eq_constraint(input * eff, output,
        #                        name='input_output_relation')

        inp_rel_init = 1 / eff.subs(out_rel, 1)

        inp_rel_max_part_lod = 1/eff.subs(out_rel, 1)
        inp_rel_min_part_lod = 1/eff.subs(out_rel, min_part_load)

        # NOTE: Conservarive estimate (big-M)!
        #       If you add more components ensure this is an upper bound!

        inp_rel_max = max(inp_rel_max_part_lod, inp_rel_min_part_lod)

        inp_rel = self.make_operational_variable(in_name + '_rel',
                                                 bounds=(0, inp_rel_max),
                                                 init_val=inp_rel_init)

        # YW add inp_rel_min
        #inp_rel_min = min_part_load/eff.subs(out_rel, min_part_load)

        # YW: edit inp_rel
        #inp_rel = self.make_operational_variable(in_name + '_rel',
        #                                         bounds=(0, inp_rel_max),
        #                                         init_val=inp_rel_init)

        #inp_rel = self.make_operational_variable(in_name + '_rel',
        #                                         bounds=(0, inp_rel_max+0.1),
        #                                        init_val=inp_rel_max)


        self.add_le_constraint(inp_rel, operating * inp_rel_max, name='input_active')

        self.add_eq_constraint(inp_rel * eff, out_rel, name='input_output_relation')

        # self.add_expression('input', input)
        self.add_expression('input', inp_rel * out_nom)
        self.add_expression('output', output)

        # set connectors
        self.add_input(in_connector_name, inp_rel * out_nom)
        self.add_output(out_connector_name, output)



class PtGComponent(Component):
    """Specification of a EnA Component. Used for PEM


    Arguments
    ---------
    - label : str
        Unique string that serves as an identifier of this Demand.
    - nom_ref: The nominal reference size of the component
    - c_ref: coefficient of investment costs
    - c_m: factor of investment costs corresponding to maintenance costs
          TODO: This implies calculations with a year as the time series!
    - M: (1) exponent of investment costs
    - nom_min: (0) lower bound for nominal size
    - nom_max: (None) upper bound for nominal size
    - min_part_load: (0) minimum allowed value for relative input
    - base_eff: the base efficiency of the component
    - fit_params: dict of power, coefficient items for polynomial
        approximation of efficiency
    - in_name: name of input commodity
    - out_name: name of output commodity
    - in_connector_name: name of input connector
    - out_connector_name: name of output connector
    - exists: bool
        Specifies whether the Component is currently installed; if set to
        True, its nominal size will be a user-specified parameter. If set
        to False, the nominal size of the component is a design variable.
    - optional: bool
        Specifies whether the Component may be added or removed.
        If exists is True, setting optional to True allows the Component to
        be sold for a user-specified price.
        If exists is False, setting optional to False requires the
        component to be installed with a minimal nominal size of at least
        `nom_min`, while setting it to True adds the posibility of not
        installing it.

    - last update by YW on 28.06.2022
    """

    def __init__(self, label, nom_ref=1, c_ref=1, M = 1,
                 nom_min = 0, nom_max = None, c_m=0,
                 nom_size=None, min_part_load=0, max_part_load=1,
                 base_eff=1, fit_params=None,
                 in_name='input', out_name='output', in_connector_name='IN',
                 out_connector_name='OUT', exists=False, optional=True):
        # If no fit_params are given we assume no dependency on relative output
        if not fit_params:
            fit_params = {0: 1}  # results in eff = base_eff

        super().__init__(label)

        # nominal (installed) output power
        nom_name = in_name + '_nom'
        available = 1
        if exists:
            if optional:  # Component exits but may be sold
                pass # deleted by YW

            else:  # Component exists and may NOT be sold
                # TODO: This shows that we might want to follow GAMS, Pyomo,
                #       etc. and introduce bounds on parameters!
                # in_nom = self.make_parameter(nom_name, nom_min) changed by YW
                if nom_size is None:
                    in_nom = nom_max
                else:
                    in_nom = nom_size
                    nom_max = nom_size
                    nom_min = nom_size

            # investment cost of the component
            inv = c_ref * (in_nom/nom_ref) ** M
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)
        else:
            # TODO: If the component does NOT exist we might add a method to
            #       the component to specify a finite number of possible
            #       nominal sizes.
            if optional:  # Component doesn't exist but may be installed or not
                in_nom = self.make_design_variable(nom_name, bounds=(0, nom_max), init_val=nom_max)
                exists = self.make_design_variable('exists', BINARY,  init_val=1)

                available = exists
                self.add_le_constraint(exists * nom_min, in_nom, name=nom_name + '_min')
                self.add_le_constraint(in_nom, exists * nom_max, name=nom_name + '_max')

            else:  # Component doesn't exist and must be installed
                in_nom = self.make_design_variable(nom_name, bounds=(nom_min, nom_max), init_val=nom_max)

            # investment cost of the component
            inv = c_ref * (in_nom/nom_ref) ** M

            # set (investment) costs
            self.add_expression('investment_costs', inv)
            # add maintenance costs to fixed costs
            self.add_expression('fixed_costs', c_m * inv)

        # relative input
        in_rel = self.make_operational_variable(in_name + '_rel', bounds=(0, max_part_load), init_val=max_part_load) #YW changed ub
        input = in_rel * in_nom

        # If minimal part load is considered, we need to decide whether it's
        # operational or not to determine the lower bound for the part load.
        self.add_expression('min_part_load', min_part_load)

        if min_part_load == 0:
            operating = 1
        else:
            # binary indicating whether the component is operational
            operating = self.make_operational_variable('operating', domain=BINARY, init_val=1)

            if available != 1:  # available is variable
                self.add_le_constraint(operating, available, name='availablility')

            self.add_ge_constraint(in_rel, operating * min_part_load, name='min_part_load')

        self.add_expression('max_part_load', max_part_load)
        self.add_le_constraint(in_rel, operating * max_part_load,  name='max_part_load')

        numerator = 0
        denominator = 0

        for power, coeff in fit_params.items():
            if power is None:
                denominator += coeff # * operating # YW add operating
            elif power == 0:
                numerator += coeff #* operating # YW add operating
            elif power < 0:
                denominator += coeff * in_rel ** (-power)
            else:
                numerator += coeff * in_rel ** power
        eff = numerator if denominator == 0 else numerator / denominator
        eff *= base_eff
        self.add_expression('eff', eff)

        eff_operating= eff.subs(operating, 1) # YW dummy formulated, wrong

        out_rel_init = max_part_load * eff_operating.subs(in_rel, max_part_load) # YW dummy formulated

        out_rel_max_part_load = max_part_load * eff_operating.subs(in_rel, max_part_load) # YW dummy formulated
        out_rel_min_part_load = min_part_load * eff_operating.subs(in_rel, min_part_load) # YW dummy formulated

        out_rel_max = max(out_rel_max_part_load, out_rel_min_part_load)

        out_rel = self.make_operational_variable(out_name + '_rel', bounds=(0, out_rel_max), init_val=out_rel_init)

        self.add_le_constraint(out_rel, operating * out_rel_max, name='output_active')

        self.add_eq_constraint(in_rel * eff, out_rel, name='input_output_relation')

        # self.add_expression('input', input)
        self.add_expression('input', input)
        self.add_expression('output', out_rel * in_nom)

        # set connectors
        self.add_input(in_connector_name, input)
        self.add_output(out_connector_name, out_rel * in_nom)



class PtGStorage(Component):
    """General storage unit.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage state of charge is modeled using state variables, for which
    time coupling constraints are created after the parametrization of the
    problem.

    Arguments
    ---------
    - label : str
        Unique sting that serves as an identifier of the storage unit.
    - nom_ref: The nominal reference size of the storage unit
    - c_ref: coefficient of investment costs
    - c_m: factor of investment costs corresponding to maintenance costs
          TODO: This implies calculations with a year as the time series!
    - M: (1) exponent of investment costs
    - min_cap: (0) lower bound for nominal size
    - max_cap: (None) upper bound for nominal size
    - charge_eff: (1) the charging efficiency of the storage unit
    - discharge_eff: (1) the discharging efficiency of the storage unit
    - c_loss: (0) the relative self-discharge of the unit (% SOC/h)
    - in_min: (0) minimum charging rate relative to nominal capacity
    - in_max: (1) maximum charging rate relative to nominal capacity
    - out_min: (0) minimum discharging rate relative to nominal capacity
    - out_max: (1) mxaimum discharging rate relative to nominal capacity

    - in_name: name of input commodity
    - out_name: name of output commodity
    - in_connector_name: name of input connector
    - out_connector_name: name of output connector
    - exists: bool
        Specifies whether the Component is currently installed; if set to
        True, its nominal size will be a user-specified parameter. If set
        to False, the nominal size of the component is a design variable.

    """

    def __init__(self, label, nom_ref, c_ref,  M=1, c_m = 0,
                 min_cap=0, max_cap=None, nom_cap=None,
                 charge_eff=1, discharge_eff=1, c_loss=0,
                 c_loss_T=0, theta_min= None, theta_max=None, T_amb = None,
                 in_min=0, in_max=1, out_min=0, out_max=1, in_name='input',
                 out_name='output', in_connector_name='IN',
                 out_connector_name='OUT', exists=False):
        super().__init__(label)

        # nominal (installed) output power
        nom_name = 'capacity_nom'
        if exists:
            if nom_cap is None:
                print("Warning: nominal capacity is not given...")
                cap = max_cap
            else:
                cap = nom_cap
                max_cap = cap
                min_cap = cap
            #cap = self.make_parameter(nom_name)
            #self.add_le_constraint(min_cap, cap, name=nom_name + '_min')
            #self.add_le_constraint(cap, max_cap, name=nom_name + '_max')
            # investment cost of the component
            inv = c_ref * (cap / nom_ref) ** M
        else:
            cap = self.make_design_variable(nom_name, bounds=(0, max_cap), init_val=max_cap)
            exists = self.make_design_variable('exists', BINARY, init_val=1)
            # bound component size
            self.add_le_constraint(exists * min_cap, cap, name=nom_name + '_min')
            self.add_le_constraint(cap, exists * max_cap, name=nom_name + '_max')
            # investment cost of the component
            inv = c_ref * (cap / nom_ref) ** M
            # set (investment) costs
            self.add_expression('investment_costs', inv)
        # add maintenance costs to fixed costs
        self.add_expression('fixed_costs', c_m * inv)

        ## changed by YW
        if exists is True:
            avaiable = 1
        else:
            avaiable = exists
        # define operational variables
        soc = self.make_operational_variable('soc', bounds=(0, max_cap))
        self.add_le_constraint(soc, avaiable * cap, name=nom_name + 'soc_max') #YW changed name

        # input at timestep t
        inp = self.make_operational_variable(in_name, bounds=(0, max_cap))
        # binary indicating whether the storage is charging
        b_in = self.make_operational_variable('b_in', domain=BINARY,
                                              init_val=0)
        # the upper/lower bound for input depends on the nominal output, but
        # since that's a variable  we have to write this bound as a constraint:
        self.add_le_constraint(inp, b_in * cap * in_max, name='input_max')
        self.add_ge_constraint(inp, b_in * cap * in_min, name='input_min')
        # output at timestep t
        out = self.make_operational_variable(out_name, bounds=(0, max_cap))
        # binary indicating whether the storage is discharging
        b_out = self.make_operational_variable('b_out', domain=BINARY,
                                               init_val=1)
        # the upper/lower bound for output depends on the nominal output, but
        # since that's a variable  we have to write this bound as a constraint:
        self.add_le_constraint(out, b_out * cap * out_max, name='output_max')
        self.add_ge_constraint(out, b_out * cap * out_min, name='output_min')

        dissipation = self.add_expression('dissipation',  soc * c_loss)

        if c_loss_T != 0:
            #ES_T_amb = ('T_amb')  # [°C] air temperature
            g = (self.theta_min - T_amb) / (self.theta_max - self.theta_min)

            dissipation_T = self.add_expression('dissipation', cap * c_loss_T * g)
        else:
            dissipation_T = 0
        # define storage constraints and declare state variables
        state_change = inp * charge_eff - out / discharge_eff \
            - dissipation - dissipation_T

        self.declare_state(soc, state_change, 0) ##TODO YW have to check

        # make sure, storage units cannot charge and discharge at the same time
        self.add_le_constraint(b_in + b_out, 1, name='charge_discharge_switch')
        self.add_le_constraint(b_in + b_out, avaiable, name='availablility') # YW added

        self.add_expression('input', inp)
        self.add_expression('output', out)

        # set connectors
        self.add_input(in_connector_name, inp)
        self.add_output(out_connector_name, out)
