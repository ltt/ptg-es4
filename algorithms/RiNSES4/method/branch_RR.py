"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Marvin Volkmer, Yifan Wang

import pprint
pp = pprint.PrettyPrinter(indent=4)

from algorithms.RiNSES4.method.processing import *


def prepare_input_data_RR(options_rinses4 = None, options_rinses4_RR=None):

    origin_input_data = options_rinses4['input_data'].copy(deep=True)
    scenarios = options_rinses4['scenarios'].copy(deep=True)
    timesteps = options_rinses4['timesteps'].copy(deep=True)

    current_itr = list(options_rinses4_RR['options_relax_balance'])[-1]
    no_typical_periods = options_rinses4_RR['options_relax_balance'][current_itr]['no_typical_periods']
    hours_per_period = options_rinses4_RR['options_relax_balance'][current_itr]['hours_per_period']
    no_segments = options_rinses4_RR['options_relax_balance'][current_itr]['no_segments']


    energy_demands = []

    for i in origin_input_data.columns:
        if '_demand' in i:
            energy_demands.append(i)

    ###
    ### TODO check, which can be deleted
    if current_itr == 0:
        origin_input_data.drop(columns=['dt'], inplace=True)
        origin_input_data.drop(columns=['period_weight'], inplace=True)
        #origin_input_data.drop(columns=['eGrid_compensation'], inplace=True)
        #origin_input_data.drop(columns=['gGrid_compensation'], inplace=True)


    import tsam.timeseriesaggregation as tsam
    aggregation = []
    aggregation = tsam.TimeSeriesAggregation(origin_input_data,
                                             resolution=1,
                                             noTypicalPeriods=no_typical_periods,
                                             hoursPerPeriod=hours_per_period,
                                             clusterMethod='k_means',
                                             noSegments=no_segments,
                                             segmentation=True,
                                             #extremePeriodMethod = 'append',
                                             #addPeakMax = energy_demands,
                                             #addPeakMin = energy_demands
                                             )


    typical_periods = aggregation.createTypicalPeriods()

    typical_periods.index.names = ['Typical_Period', 'Segment_Step', 'dt']

    index_matching = ''
    index_matching = aggregation.indexMatching()

    #input_data_relax = origin_input_data.copy(deep=True)
    input_data_relax = origin_input_data.assign(HeatD_demand_max=0.0,
                                                HeatD_demand_min=0.0,
                                                PowerD_demand_max=0.0,
                                                PowerD_demand_min=0.0)

    #input_columns = list(origin_input_data.columns)
    #input_columns.append('HeatD_demand_max')
    #input_columns.append('HeatD_demand_min')
    #input_columns.append('PowerD_demand_max')
    #input_columns.append('PowerD_demand_min')



    #input_data_relax = pd.DataFrame(columns=input_columns, dtype=)
    #data_aggregated_list_max_min = pd.DataFrame(columns=input_columns)


    for i in index_matching.index:

        #input_data_relax = input_data_relax.append(origin_input_data.loc[i], ignore_index=True)

        if i == (1,1):
            typical_p_index = index_matching.loc[i, 'PeriodNum']
            typical_ts_index = index_matching.loc[i, 'SegmentIndex']

            heatD_max = float(origin_input_data.loc[i, 'HeatD_demand'])
            heatD_min = float(origin_input_data.loc[i, 'HeatD_demand'])

            powerD_max = float(origin_input_data.loc[i, 'PowerD_demand'])
            powerD_min = float(origin_input_data.loc[i, 'PowerD_demand'])

            segment = [i]
            #segment_end = 1

        else:

            if index_matching.loc[i, 'PeriodNum'] == typical_p_index and index_matching.loc[i, 'SegmentIndex'] == typical_ts_index:

                if origin_input_data.loc[i, 'HeatD_demand'] > heatD_max:
                    heatD_max = origin_input_data.loc[i, 'HeatD_demand']

                if origin_input_data.loc[i, 'HeatD_demand'] < heatD_min:
                    heatD_min = origin_input_data.loc[i, 'HeatD_demand']


                if origin_input_data.loc[i, 'PowerD_demand'] > powerD_max:
                    powerD_max = origin_input_data.loc[i, 'PowerD_demand']

                if origin_input_data.loc[i, 'PowerD_demand'] < powerD_min:
                    powerD_min = origin_input_data.loc[i, 'PowerD_demand']

                #segment_end = segment_end + 1
                segment.append(i)
            else:
                for j in segment:
                    input_data_relax.loc[j, 'HeatD_demand_max'] = float(heatD_max)
                    input_data_relax.loc[j, 'HeatD_demand_min'] = float(heatD_min)
                    input_data_relax.loc[j, 'PowerD_demand_max'] = float(powerD_max)
                    input_data_relax.loc[j, 'PowerD_demand_min'] = float(powerD_min)


                typical_p_index = index_matching.loc[i, 'PeriodNum']
                typical_ts_index = index_matching.loc[i, 'SegmentIndex']

                heatD_max = origin_input_data.loc[i, 'HeatD_demand']
                heatD_min = origin_input_data.loc[i, 'HeatD_demand']

                powerD_max = origin_input_data.loc[i, 'PowerD_demand']
                powerD_min = origin_input_data.loc[i, 'PowerD_demand']

                segment = [i]

        if i == index_matching.index[-1]:

            for j in segment:
                input_data_relax.loc[j, 'HeatD_demand_max'] = float(heatD_max)
                input_data_relax.loc[j, 'HeatD_demand_min'] = float(heatD_min)
                input_data_relax.loc[j, 'PowerD_demand_max'] = float(powerD_max)
                input_data_relax.loc[j, 'PowerD_demand_min'] = float(powerD_min)




    #for i in aggregation.clusterOrder:
    #    data_aggregated_list.append(typical_periods.query(f'Typical_Period == {i}'))

    #data_aggregated = pd.concat(data_aggregated_list)
    #data_aggregated.reset_index(inplace=True)



    input_data_relax['s'] = 1
    input_data_relax['t'] = range(1, len(input_data_relax.index) + 1)
    input_data_relax['period_weight'] = 1
    input_data_relax['dt'] = 1
    #aggregation.dt_typical_periods = data_aggregated[['Typical_Period', 'Segment_Step', 'dt']]
    #aggregation.dt_typical_periods.drop_duplicates(inplace=True)
    #aggregation.dt_typical_periods.set_index(['Typical_Period', 'Segment_Step'], inplace=True)

    #data_aggregated.drop(columns=['Segment_Step', 'Typical_Period'], inplace=True)
    input_data_relax.set_index(['s', 't'], drop=True, inplace=True)

    input_data_relax.to_csv(f'{options_rinses4_RR["output_folder"]}/input_data_after_tsam_iter{current_itr}_p{no_typical_periods}_ts{no_segments}.csv', sep=';', decimal=',')

    return input_data_relax

def get_lb_of_baron(input_file_dic, baron_gap):

    ###
    ### check if feasible
    ###
    input_file = input_file_dic['sum_file']
    file = open(input_file, 'r')
    lines = file.read().splitlines()
    file.close()

    results_infeasible = False
    for i in lines:
        if "Problem is infeasible" in i:
            results_infeasible = True
            print('The optimization problem is infeasible ...')
            break


    ###
    ### read vaule
    ###
    if not results_infeasible:
        input_file = input_file_dic['tim_file']
        file = open(input_file, 'r')
        lines = file.read().splitlines()
        file.close()

        col = lines[0].split(' ')

        j = 0
        for i in range(0,len(col)):

            if i == 0 and col[0] != 'problem':
                exit('Sth wring in tim.lst file')

            if col[i] == '':
                pass
            else:
                j = j+1

                if j == 6:
                    lu_value = float(col[i])
                elif j == 7:
                    ub_value = float(col[i])#float(lines[0].split(' ')[15])

        if (ub_value - lu_value)/ub_value > baron_gap:
            print(f'In {input_file}: taking lb as lb')
            return False, lu_value

        else:
            print(f'In {input_file}: taking lb instead of ub as lb')
            return True, lu_value# ub_value


    else:
        print(f'In {input_file}: there is no lb.')
        return False, None

def rinses4_RR(options_rinses4 = None, options_RR = None):

    from timeit import default_timer as timer
    ###
    ### prepare
    ###
    res_RR = {}
    #file_path_RR = 'Recoding_time_LB_RR.txt'
    file_path_RR = Path(f'{options_rinses4["output_folder"]}/Recoding_time_LB_RR.txt')

    with open(file_path_RR, 'a') as file_log_RR:
        # Write new content to the file
        file_log_RR.write("Computing time; Real_Time_LB\n")

    file_log_RR.close()

    ###
    ### input data
    ###
    options_RR['options_relax_balance']={
        'branch': 'LB',
        'active_relax_balance': True,
        'len_input_ts': len(options_rinses4['input_data']),
        'len_timespan': options_rinses4['timesteps'].values[0]
    }
    iter_relax_balance = 0

    options_RR['options_relax_balance'], options_RR['options_relax_balance']['active_relax_balance'] = determine_parameter_tsam(options_RR['options_relax_balance'], res_RR, set_up = True)

    time_RR_start = timer()
    change_baron_parameter = False

    while options_RR['options_relax_balance']['active_relax_balance']:


        ###
        ### decide parameter for time series aggregation
        options_RR['options_relax_balance'][iter_relax_balance], options_RR['options_relax_balance']['active_relax_balance'] =\
            determine_parameter_tsam(options_RR['options_relax_balance'], res_RR)

        #options_RR['options_relax_balance'][iter_relax_balance] = {
        #    'no_typical_periods': 3,
        #    'hours_per_period': 24,
        #    'no_segments': 4
        #}

        ###
        ### prepare input data

        options_RR['input_data'] = prepare_input_data_RR(options_rinses4, options_RR)

        options_RR['scenarios'] = options_rinses4['scenarios'].copy(deep=True)
        options_RR['timesteps'] = options_rinses4['timesteps'].copy(deep=True)

        options_RR['storage_model'] = 'relax'

        ES_RR, ES_AD_comp = create_energy_system(test_ES=options_rinses4['test_ES'], sto_model=options_RR['storage_model'])

        comp_storages = ES_AD_comp['stor']
        d_obj, o_obj = make_tac_objective(ES_RR, n=10, i=0.08)

        ###############################################################
        ### relax nonlinear
        ###############################################################
        #TODO

        ###############################################################
        ### relax product balance
        ###############################################################


        for bus_id, connectors in ES_RR.connections.items():

            bus = ES_RR.connections[bus_id]

            #print(bus_id)
            #pp.pprint(connectors)
            #pp.pprint(bus)

            for connector in bus:

                if '_demand' in str(connector.expr):
                    #s = str(connector.component)
                    #component_str = s[s.find("('") + 2:s.find("')")]
                    #print(component_str)

                    component_str = connector.component.label

                    max_demand = connector.component.make_parameter('demand_max')
                    print(max_demand)
                    min_demand = connector.component.make_parameter('demand_min')
                    print(min_demand)
                    ES_RR.add_le_constraint(-sum(c.expr for c in bus if c is not connector), max_demand,
                                            bus_id + '_max')
                    ES_RR.add_ge_constraint(-sum(c.expr for c in bus if c is not connector), min_demand,
                                            bus_id + '_min')
                    ES_RR._constraints_dict.pop(bus_id)
                    print('\t Relaxing product balances:')
                    print(ES_RR._constraints_dict)

        ###############################################################
        ### relax storage
        ###############################################################

        res_RR[iter_relax_balance] = {}
        pp.pprint(comp_storages)

        options_RR['options_relax_balance'][iter_relax_balance]['options_relax_storage']={
            'active_relax_storage': True,
            'nr_of_storage': len(comp_storages)
        }
        iter_relax_storage = 0
        get_lb_value = None

        while iter_relax_storage <= len(comp_storages) and \
                options_RR['options_relax_balance'][iter_relax_balance]['options_relax_storage']['active_relax_storage']:

            print(f'Calculating relax_balance = {iter_relax_balance} and relax_storage = {iter_relax_storage}')

            options_RR['options_relax_balance'][iter_relax_balance]['options_relax_storage'][iter_relax_storage] = {}


            comp_storages.sort(key=get_label)

            if iter_relax_storage == 0:

                for i_stor in comp_storages:

                    print('Relaxing the storage ', i_stor)
                    max_soc = i_stor['soc'].ub #YW should be capacity, correct later
                    min_soc = i_stor['soc'].lb #YW should be capacity, correct later

                    soc_change = i_stor._expressions_dict['declare_state']

                    ES_RR.add_ge_constraint(soc_change, min_soc, f'{i_stor.label}_soc_relax_min')
                    ES_RR.add_le_constraint(soc_change, max_soc, f'{i_stor.label}_soc_relax_max')

                #relaxed_stor = comp_storages
            else:
                from comando import cyclic

                for i_stor in comp_storages[iter_relax_storage-1:iter_relax_storage]:
                    soc_change = i_stor._expressions_dict['declare_state']

                    for j in ES_RR.operational_variables:
                        if j.name == f'{i_stor.label}_soc':

                            ES_RR.declare_state(j, soc_change, cyclic)

                    #try:
                    ES_RR._constraints_dict.pop(f'{i_stor.label}_soc_relax_min')
                    ES_RR._constraints_dict.pop(f'{i_stor.label}_soc_relax_max')
                    #except:
                    #    print('Hier hier hier: ')
                    #    print(i_stor.label)
                    #    print(comp_storages)
                    #    print(ES_RR._constraints_dict)
                    #    print(iter_relax_storage)
                    #    exit()

                    #ES_RR._st
            #from comando import cyclic
            #BAT._states_dict[BAT['soc']] = cyclic, *BAT._states_dict[BAT['soc']][1:]

            output_folder_RR = Path(f"{options_RR['output_folder']}/{iter_relax_balance}_{iter_relax_storage}")
            output_folder_RR.mkdir(parents=True, exist_ok=True)

            P = ES_RR.create_problem(design_objective=d_obj, operational_objective=o_obj, timesteps=options_RR['timesteps'],
                                  scenarios=options_RR['scenarios'], data=options_RR['input_data'], name=f'ptg_rises5_{iter_relax_balance}_{iter_relax_storage}')


            from comando.interfaces.baron import solve

            output_file = Path(f'{output_folder_RR}/ptg_rises5_MINLP_LB.bar')

            if output_file.is_file():
                output_file.unlink()

            ###
            ### post Processing
            ###
            baron_log_file = {
                'bar_file': output_file,
                'tim_file': Path(f'{output_file.parent}/ptg_rises5_MINLP_LB.tim.lst'),
                'sum_file': Path(f'{output_file.parent}/ptg_rises5_MINLP_LB.sum.lst')
            }
            if get_lb_value == None and not change_baron_parameter:
                baron_maxTime = 60*60*24*2
                baron_gap = 0.99
                max_iter = 0
            else:
                baron_maxTime = 60*60*2
                baron_gap = 0.002
                max_iter = -1

            test_without_sove = False
            if not test_without_sove:
                solve(P, f'{output_file.parent}/{output_file.name}', MaxIter= max_iter, MaxTime=baron_maxTime, summary=1, EpsR=baron_gap) #,

            else:
                file_lb = open(rf'{baron_log_file["tim_file"]}', 'w')
                x_str = 'problem    4470 2166 0 3899  2074.25249572     0.100000000000E+52 1 2 0 -1 -3 0 0.06 0.14 '
                file_lb.write(x_str)
                file_lb.close()

                file_lb = open(rf'{baron_log_file["sum_file"]}', 'w')
                file_lb.write(str(112263.47166))
                file_lb.close()


            solution_status, get_lb_value = get_lb_of_baron(baron_log_file, baron_gap=baron_gap)

            if solution_status:

                print(P.objective.value)

                from user_utilities.plot_visualize.store_results import write_results

                options_post = {
                    # 'read_which_pickle': 'results_dict_DO_S3',
                    'input_folder': Path('./output/'),
                    'output_folder': output_folder_RR,
                    'if_pickle': False,
                    'if_csv': True,
                    'plot_setup': 'ESCAPE_2023',
                    'if_plot': False,
                }

                write_results(P, options_post)




            elif not solution_status:

                print('Get lb of baron: ', get_lb_value)

            ###
            ###
            ###
            if get_lb_value != None:
                change_baron_parameter = True
                ###
                ### write lb in real_time file
                file_lb = open(rf'{options_rinses4["output_folder"]}/real_time_lb.txt', 'w')
                file_lb.write(str(get_lb_value))
                file_lb.close()

                time_RR_iteration_end = timer()

                delta_time = time_RR_iteration_end - time_RR_start

                with open(file_path_RR, 'a') as file_log_AD:
                    # Write new content to the file
                    file_log_AD.write(f"{delta_time}; {get_lb_value}\n")
                file_log_AD.close()
            ###
            ### read real time gap
            current_gap = get_rises_gap(options_rises=options_rinses4)

            if get_lb_value == None:
                res_RR[iter_relax_balance][iter_relax_storage] = (None, None)

            else:
                if iter_relax_storage == 0:

                    res_RR[iter_relax_balance][iter_relax_storage] = (get_lb_value, current_gap)

                elif res_RR[iter_relax_balance][iter_relax_storage - 1][0] == None:

                    res_RR[iter_relax_balance][iter_relax_storage] = (get_lb_value, current_gap)

                elif res_RR[iter_relax_balance][iter_relax_storage - 1][0] > get_lb_value:

                    res_RR[iter_relax_balance][iter_relax_storage] = (res_RR[iter_relax_balance][iter_relax_storage - 1][0], res_RR[iter_relax_balance][iter_relax_storage - 1][1])

                elif res_RR[iter_relax_balance][iter_relax_storage - 1][0] <= get_lb_value:

                    res_RR[iter_relax_balance][iter_relax_storage] = (get_lb_value, current_gap)

            ### check optimality gap
            iter_relax_storage = iter_relax_storage + 1
            ##
            ## check if continue iteration
            if current_gap <= options_rinses4['rises5_gap']:

                options_RR['options_relax_balance']['active_relax_balance'] = False
                options_RR['options_relax_balance']['active_relax_storage'] = False
            else:
                #options_RR['options_relax_balance']['active_relax_balance'] = True
                options_RR['options_relax_balance']['active_relax_storage'] = True

        iter_relax_balance = iter_relax_balance +1






        #options_RR['options_relax_balance']['active_relax_balance'] = False
        print('Current results of RR branch: ', res_RR)
    return res_RR


