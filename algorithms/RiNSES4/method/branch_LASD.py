"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Marvin Volkmer

from pathlib import Path
from algorithms.RiNSES4.method.processing import *
import pandas as pd
import tsam.timeseriesaggregation as tsam

def get_ub_of_baron(input_file_dic, baron_gap=0.01):
    ###
    ### check if feasible
    ###
    input_file = input_file_dic['sum_file']
    file = open(input_file, 'r')
    lines = file.read().splitlines()
    file.close()

    results_infeasible = False
    for i in lines:
        if "Problem is infeasible" in i:
            results_infeasible = True
            print('The optimization problem is infeasible ...')
            break


    ###
    ### read vaule
    ###
    if not results_infeasible:
        input_file = input_file_dic['tim_file']
        file = open(input_file, 'r')
        lines = file.read().splitlines()
        file.close()

        col = lines[0].split(' ')

        j = 0
        for i in range(0,len(col)):

            if i == 0 and col[0] != 'problem':
                exit('Sth wrong in tim.lst file')

            if col[i] == '':
                pass
            else:
                j = j+1

                if j == 6:
                    lu_value = float(col[i])
                elif j == 7:
                    ub_value = float(col[i])#float(lines[0].split(' ')[15])

        #if (ub_value - lu_value)/ub_value > baron_gap:
        #    print(f'In {input_file}: taking lb as lb')
        #    return False, lu_value

        #else:
        print(f'In {input_file}: taking ub as ub')
        return True, ub_value
    else:
        print(f'In {input_file}: there is no ub.')
        return False, None

def prepare_input_data_LASD(options_rinses4 = None, options_rinses4_LASD= None):

    origin_input_data = options_rinses4['input_data'].copy(deep=True)
    # scenarios = options_rises5['scenarios'].copy(deep=True)
    # timesteps = options_rises5['timesteps'].copy(deep=True)

    current_itr = list(options_rinses4_LASD['options_aggregation'])[-1]
    no_typical_periods = options_rinses4_LASD['options_aggregation'][current_itr]['no_typical_periods']
    hours_per_period = options_rinses4_LASD['options_aggregation'][current_itr]['hours_per_period']
    no_segments = options_rinses4_LASD['options_aggregation'][current_itr]['no_segments']

    #if current_itr == 0:
    origin_input_data.drop(columns=['dt'], inplace=True)
    #origin_input_data.drop(columns=['period_weight'], inplace=True)
    # origin_input_data.drop(columns=['eGrid_compensation'], inplace=True)
    #origin_input_data.drop(columns=['gGrid_compensation'], inplace=True)

    import tsam.timeseriesaggregation as tsam

    aggregation = tsam.TimeSeriesAggregation(origin_input_data,
                                             resolution=4, # YW 1
                                             noTypicalPeriods=no_typical_periods,
                                             hoursPerPeriod=hours_per_period *4,
                                             clusterMethod='k_means',
                                             noSegments=no_segments,
                                             segmentation=True)

    data_aggregated_list = []
    typical_periods = aggregation.createTypicalPeriods()
    typical_periods.index.names = ['Typical_Period', 'Segment_Step', 'dt']
    for i in aggregation.clusterOrder:
        data_aggregated_list.append(typical_periods.query(f'Typical_Period == {i}'))

    input_data_aggregate = pd.concat(data_aggregated_list)
    input_data_aggregate.reset_index(inplace=True)

    input_data_aggregate['s'] = 1
    input_data_aggregate['t'] = range(1, len(input_data_aggregate.index) + 1)
    input_data_aggregate['period_weight'] = 1

    aggregation.dt_typical_periods = input_data_aggregate[['Typical_Period', 'Segment_Step', 'dt']]
    aggregation.dt_typical_periods.drop_duplicates(inplace=True)
    aggregation.dt_typical_periods.set_index(['Typical_Period', 'Segment_Step'], inplace=True)

    input_data_aggregate.drop(columns=['Segment_Step', 'Typical_Period'], inplace=True)
    input_data_aggregate.set_index(['s', 't'], drop=True, inplace=True)

    #input_data_aggregate.reset_index(inplace=True)
    #input_data_aggregate.set_index(['s', 't'], inplace=True)

    scenarios = input_data_aggregate['period_weight'].groupby('s').first().rename('pi')

    input_data_aggregate['dt'] = input_data_aggregate['dt'] * 4 #YW hard coded
    timesteps = input_data_aggregate['dt']

    return input_data_aggregate, scenarios,  timesteps

def rinses4_LASD(options_rinses4 = None, options_LASD = None):

    import time
    from timeit import default_timer as timer

    ###
    ### UB using relaxation and decomposition
    ###
    if options_rinses4 is None or options_LASD is None:
        #TODO
        pass

    res_LASD = {}
    file_path_LASD = Path(f'{options_rinses4["output_folder"]}/Recoding_time_UB_LASD.txt')

    with open(file_path_LASD, 'a') as file_log_LASD:
        # Write new content to the file
        file_log_LASD.write("Computing time; Real_Time_UB\n")

    file_log_LASD.close()
    #options_LASD['input_data'], options_LASD['scenarios'], options_LASD['timesteps'] = prepare_input_data_LASD(options_rises5, options_LASD)

    ES_LASD, ES_LASD_comp = create_energy_system(test_ES=options_rinses4['test_ES'], sto_model='superposition')
    d_obj, o_obj = make_tac_objective(ES_LASD, n = 10, i=0.08)

    origin_input_data = options_rinses4['input_data']
    origin_timesteps = options_rinses4['timesteps']
    origin_scenarios = options_rinses4['scenarios']

    LASD_obj = {}
    options_LASD['options_aggregation'] = {
        'branch': 'UB',
        'active_aggregation': True,
        'len_input_ts': len(options_rinses4['input_data']),
        'len_timespan': options_rinses4['timesteps'].values[0]
    }
    iter_LASD_design = 0

    options_LASD['options_aggregation'], options_LASD['options_aggregation']['active_aggregation'] = \
        determine_parameter_tsam(options_LASD['options_aggregation'], options_LASD, set_up=True)

    time_LASD_start = timer()

    while options_LASD['options_aggregation']['active_aggregation']:

        ###
        ### decide parameter for tasm
        ###
        options_LASD['options_aggregation'][iter_LASD_design], options_LASD['options_aggregation'][
            'active_aggregation'] = determine_parameter_tsam(options_LASD['options_aggregation'], res_LASD)

        #hours_per_period = options_LASD['options_aggregation'][iter_LASD_design]['hours_per_period']
        #no_typical_periods = options_LASD['options_aggregation'][iter_LASD_design]['no_typical_periods']
        #no_segments = options_LASD['options_aggregation'][iter_LASD_design]['no_segments']

        #from algorithm.RiSES5.sub_utilities.pre_processing import create_aggregated_block
        #input_data, aggregation = create_aggregated_block(origin_input_data,
        #                                                no_typical_periods,
        #                                                hours_per_period,
        #                                                no_segments)

        #input_data.reset_index(inplace=True)
        #input_data.set_index(['s', 't'], inplace=True)

        #scenarios = input_data['period_weight'].groupby('s').first().rename('pi')
        #timesteps = input_data['dt']

        input_data, scenarios, timesteps = prepare_input_data_LASD(options_rinses4, options_LASD)

        for i in ES_LASD_comp['stor']:

            stor_para_1 = f'{i.label}_no_segments'
            input_data[stor_para_1] = int(len(timesteps)/ scenarios.size)  #timesteps.values.sum()  YW need to check

            stor_para_2 = f'{i.label}_dt'
            input_data[stor_para_2] = input_data['dt']


        #options['output_folder'].mkdir(parents=True, exist_ok=True)
        output_folder_LASD_design = Path(f"{options_LASD['output_folder']}/LASD_design_iter_{iter_LASD_design}")
        output_folder_LASD_design.mkdir(parents=True, exist_ok=True)

        input_data.to_csv(f"{output_folder_LASD_design}/input_data_after_tsam.csv", sep=';', decimal=',')


        ###
        ### calculate Design problem
        P_LASD_design = ES_LASD.create_problem(design_objective=d_obj, operational_objective=o_obj, timesteps=timesteps,
                              scenarios=scenarios, data=input_data, name='ptg_LASD_design')

        from comando.linearization import linearize
        from comando.interfaces.gurobi import to_gurobi

        # timestamp1 = datetime.datetime.now()
        P_LASD_design_lin = linearize(P_LASD_design, 3, 'convex_combination')

        #print(f'Linearization of problem {P.name} has {P_LASD_design_lin.num_vars} '
        #      f'variables and {P_lin.num_cons} constraints!')
        # timestamp2 = datetime.datetime.now()

        # diff = timestamp2 - timestamp1

        # print(f"-----This took {diff.seconds} seconds for linearization.")

        m = to_gurobi(P_LASD_design_lin)

        # timestamp3 = datetime.datetime.now()
        # diff = timestamp3 - timestamp2

        # print(f"-----This took {diff.seconds} seconds to write model.")

        m.Params.timelimit = 60*60*6
        m.Params.logfile = f'{output_folder_LASD_design}/LASD_design.log'
        m.Params.mipgap = 0.01
        #if iter_LASD_design == 0:
         #   m.Params.SolutionLimit = 1
        #else:
        #    m.Params.SolutionLimit = 'MAXINT'
        # m.Params.threLASDs =
        # m.Params.cutpasses = 1  # Comments YW add gap
        print('Solving LASD Design using gurobi:')
        m.solve()
        # timestamp4 = datetime.datetime.now()
        # diff = timestamp4 - timestamp3
        # print(f"-----This took {diff.seconds} seconds to solve the problem.")


        m.write(f'{output_folder_LASD_design}/LASD_design.lp')  # out.mst out.lp

        ###
        ### calculate fix design size
        tmp_obj_design_part = P_LASD_design.design_objective.value ## YW important

        desagg = P_LASD_design_lin.design#.T # YW can also be P_lin
        desagg.to_csv(f'{output_folder_LASD_design}/LASD_design_' + str(iter_LASD_design) + '_with_all.csv', sep=';', decimal=',')

        LASD_obj[iter_LASD_design] = {}

        LASD_obj[iter_LASD_design]['d_obj'] = tmp_obj_design_part

        P_lin_used = False ##YW need to check

        if P_lin_used:
            desagg = desagg.loc[:, ~desagg.columns.str.startswith('Lambda')]
            desagg = desagg.loc[:, ~desagg.columns.str.startswith('Simplex')]
            desagg = desagg.loc[:, ~desagg.columns.str.startswith('glover')]

            desagg.to_csv(f'{output_folder_LASD_design}/AO_design_aggregated_' + str(iter_LASD_design) + '_without_lambda.csv', sep=';', decimal=',')


        comp = {}

        for i in ES_LASD.components:

            ##YW this formulation dummy
            lable_str1 = i.label + '_exists'
            lable_str2 = i.label + '_capacity_nom'
            lable_str3 = i.label + '_Qdot_out_nom'
            lable_str4 = i.label + '_P_in_nom'
            lable_str5 = i.label + '_P_PV_N'

            comp[i.label] ={}
            for j in ES_LASD.design_variables:

                if lable_str1 == j.name:
                    comp[i.label]['exists'] = ES_LASD[lable_str1].value
                    comp[i.label]['optional'] = False

                if lable_str2 == j.name or lable_str3 == j.name or lable_str4 == j.name or lable_str5 == j.name:
                    comp[i.label]['nom_size'] = j.value


        res_LASD[iter_LASD_design] = {}
        iter_LASD_operation = 0

        ES_LASD_operation, comp_storages = create_energy_system(test_ES=options_rinses4['test_ES'], sto_model='standard', ES_options = comp)

        d_obj_operation, o_obj_operation = make_tac_objective(ES_LASD_operation, n=10, i=0.08)
        P_LASD_operation = ES_LASD_operation.create_problem(design_objective=d_obj_operation, operational_objective=o_obj_operation, timesteps=origin_timesteps,
                              scenarios=origin_scenarios, data=origin_input_data, name='ptg_LASD_operation')

        operation_solution = options_LASD['operation_solution']

        output_folder_LASD_operation = Path(f"{output_folder_LASD_design}/{operation_solution}")

        output_folder_LASD_operation.mkdir(parents=True, exist_ok=True)

        LASD_obj[iter_LASD_design][iter_LASD_operation] = {}

        LASD_obj[iter_LASD_design][iter_LASD_operation]['operation_solution'] = operation_solution

        tmp_obj_operation_part = []
        tmp_obj_total = []

        if operation_solution == 'baron':
            print("Using baron for LASD operation...\n")
            from comando.interfaces.baron import solve
            #from pathlib import Path

            output_file = Path(f'{output_folder_LASD_operation}/P_LASD_operation.bar')

            if output_file.is_file():
                output_file.unlink()

            baron_log_files = {
                'bar_file': output_file,
                'tim_file': Path(f'{output_file.parent}/P_LASD_operation.tim.lst'),
                'sum_file': Path(f'{output_file.parent}/P_LASD_operation.sum.lst')
            }

            if iter_LASD_design == 0:
                baron_maxTime = 60 * 60 * 24 * 2
                baron_gap = 0.99
                max_iter = 0
                FirstLoc_value = 1
            else:
                baron_maxTime = 60 * 60 * 12
                baron_gap = 0.002
                max_iter = -1
                FirstLoc_value = 0
            solve(P_LASD_operation, f'{output_file.parent}/{output_file.name}', MaxIter= max_iter,
                  MaxTime=baron_maxTime, summary=1,  EpsR=baron_gap,  FirstLoc=FirstLoc_value)

            solution_status, get_ub_value_operation = get_ub_of_baron(baron_log_files)


            #### if baron cannot solve the problem
            ###
            if solution_status:
                tmp_obj_operation_part.append(P_LASD_operation.objective.value)
                tmp_obj_total.append(P_LASD_operation.objective.value)

                real_time_ub_value = P_LASD_operation.objective.value # + tmp_obj_design_part

                from algorithms.RiNSES4.method.post_processing import write_results

                options_post = {
                    # 'reLASD_which_pickle': 'results_dict_DO_S3',
                    'input_folder': '',
                    'output_folder': output_folder_LASD_operation,
                    'if_pickle': False,
                    'if_csv': True,
                    'plot_setup': 'ESCAPE_2023',
                    'if_plot': False,
                }

                write_results(P_LASD_operation, options_post)

            elif not solution_status:
                print('UB is infeasible')

                #message['LASD_operation'] = {i, 'infeasible'}
        elif operation_solution == 'DeLoop':
            #iter_LASD_operation = 0
            #tmp_obj_operation_part = []

            output_folder_LASD_operation_DeLoop = Path(f"{output_folder_LASD_operation}/LASD_operation_iter_{iter_LASD_operation}")
            output_folder_LASD_operation_DeLoop.mkdir(parents=True, exist_ok=True)


            from algorithms.RiNSES4.method.DeLoop.algorithm_deloop import DeLoop

            #DeLoop_options = {
            #    # unnecessary to change
            #    'problem_class': 'MILP',
            #    'solution_path': 'pyomo',
            #    'peak_power': False,
            #    't_aut': 2500,

                # can be changed
            #    'check_steps': False,
            #    'lower_bound': 'pre_calculated',

            #    'DeLoop_gap': 0.02,
            #    'DeLoop_step1': 'manual'
            #}

            #TODO Call DeLoop
            options_rises5_DeLoop = options_LASD['options_DeLoop']
            options_rises5_DeLoop['output_path'] = output_folder_LASD_operation_DeLoop
            #options_rises5_DeLoop['upper_bound_Design'] = LASD_design_obj[iter_LASD_design]
            P_deloop = DeLoop(P=P_LASD_operation, ES=ES_LASD_operation, data=origin_input_data, method_options=options_rises5_DeLoop)

            #YW: need to correct: tmp_obj_operation_part.append(P_deloop.objective.values)
            tmp_obj_operation_part.append(P_LASD_operation.objective.value)
            tmp_obj_total.append(P_LASD_operation.objective.value + tmp_obj_design_part)

            real_time_ub_value = P_LASD_operation.objective.value + tmp_obj_design_part
            print('real_time_ub_value is:', real_time_ub_value)

        if solution_status:
            print('Find new UB and wrting in the real_time_ub')
            file_ub = open(rf'{options_rinses4["output_folder"]}/real_time_ub.txt', 'w')
            file_ub.write(str(real_time_ub_value))
            file_ub.close()

            time_LASD_iteration_end = timer()

            delta_time = time_LASD_iteration_end -time_LASD_start

            with open(file_path_LASD, 'a') as file_log_LASD:
                # Write new content to the file
                file_log_LASD.write(f"{delta_time}; {real_time_ub_value}\n")
            file_log_LASD.close()


        LASD_obj[iter_LASD_design][iter_LASD_operation]['o_obj'] = tmp_obj_operation_part
        LASD_obj[iter_LASD_design][iter_LASD_operation]['total_obj'] = tmp_obj_total

        ###
        ### check gap
        ###
        from math import inf

        current_gap = get_rises_gap(options_rises=options_rinses4)


        if get_ub_value_operation == None:
            res_LASD[iter_LASD_design][iter_LASD_operation] = (None, None)

        else:
            if iter_LASD_operation == 0:
                res_LASD[iter_LASD_design][iter_LASD_operation] = (real_time_ub_value, current_gap)

            elif res_LASD[iter_LASD_design][iter_LASD_operation - 1][0] == None:

                res_LASD[iter_LASD_design][iter_LASD_operation] = (real_time_ub_value, current_gap)

            elif res_LASD[iter_LASD_design][iter_LASD_operation - 1][0] <= real_time_ub_value:

                res_LASD[iter_LASD_design][iter_LASD_operation] = (res_LASD[iter_LASD_design][iter_LASD_operation - 1][0],
                                                                  res_LASD[iter_LASD_design][iter_LASD_operation - 1][1])

            elif res_LASD[iter_LASD_design][iter_LASD_operation - 1][0] > real_time_ub_value:

                res_LASD[iter_LASD_design][iter_LASD_operation] = (real_time_ub_value, current_gap)


            if current_gap <= options_rinses4['rinses4_gap']:
                trigger_aggregation = False
            else:
                pass

        if operation_solution == 'DeLoop':

            iter_LASD_operation = iter_LASD_operation + 1

            print('DeLoop is not included in this version. Please contact authors for the latest version. ')

        iter_LASD_design = iter_LASD_design +1


    iter_LASD_design = iter_LASD_design+1




        ###
        ### calculate operatational part
        # from algorithm.RiSES5.method import aggregation_operation

        #ub_ao = AggregateAndOperate()


    return P_LASD_operation

