"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Marvin Volkmer


from pathlib import Path


def create_energy_system(test_ES = None, sto_model = None, ES_options=None):

    ###
    ### import library
    from comando import System
    from components.generic_components import Grid, Demand

    from algorithms.RiNSES4.components.components_PVBAT import PVModule



    ###
    ### define conversion components
    ###
    comp_choice = ES_options

    if sto_model == 'standard':
        from algorithms.RiNSES4.components.storage_standard import Battery
    elif sto_model == 'superposition':
        from algorithms.RiNSES4.components.storage_kotzur import Battery
    elif sto_model == 'relax':
        from algorithms.RiNSES4.components.storage_relaxtion import Battery
    ###
    ### define energy system
    ES = System('ES')

    ### define components
    eGrid = Grid('eGrid', consumption_limit=0, feedin_limit=None)

    HeatD = Demand('HeatD')
    PowerD = Demand('PowerD')

    if ES_options is None:
        PV = PVModule('PV', exists=False, optional=True)  # [kW]


        BAT = Battery('BAT', exists=False)

    else:
        try:
            PV = PVModule('PV', exists=comp_choice['PV']['exists'], optional=False, P_N=comp_choice['PV']['nom_size'])  # [kW]
        except:
            PV = PVModule('PV', exists=True, optional=False, P_N=0)


        #from algorithm.RiSES5.components.storage_standard import Battery,

        try:
            BAT = Battery('BAT', exists=comp_choice['BAT']['exists'], nom_cap=comp_choice['BAT']['nom_size'])
        except:
            BAT = Battery('BAT', exists=comp_choice['BAT']['exists'], nom_cap=0)


    if test_ES == 'oneS':
        ### with PV
        comp_all = [eGrid, PV, BAT, PowerD]
        comp_extern = [PowerD, eGrid]
        comp_conv = [PV]
        comp_stor = [BAT]

        conn = {
            'e_Bus_1': [eGrid.FEEDIN, PV.OUT, PowerD.IN, BAT.IN, BAT.OUT],
        }
    else:
        exit('Other ESs are not defined yet. ')

    if sto_model == 'superposition':

        from comando import superposition

        for c in comp_stor:
            c._states_dict[c['soc_intra']] = superposition, *c._states_dict[c['soc_intra']][0:]
            c._states_dict[c['soc_inter']] = superposition, *c._states_dict[c['soc_inter']][1:]

    elif sto_model == 'standard':

        from comando import cyclic

        try:
            for c in comp_stor:
                c._states_dict[c['soc']] = cyclic, *c._states_dict[c['soc']][1:]

        except:
            pass

    for c in comp_all:
        ES.add(c)

    for bus_id, c in conn.items():
        ES.connect(bus_id, c)

    comp = {
        'all': comp_all,
        'conv': comp_conv,
        'stor': comp_stor,
        'extern': comp_extern

    }
    return ES, comp


def load_inputs(input_file):

    import pandas as pd

    input_2020 = pd.read_csv(input_file, sep=';', decimal=',')#.head(168)#168) #(144)
    input_2020.set_index(['s', 't'], inplace=True)

    scenarios = input_2020['period_weight'].groupby('s').first().rename('pi')
    timesteps = input_2020['dt']

    return input_2020, scenarios, timesteps

def make_tac_objective(system, ic_labels=None, fc_labels=None, vc_labels=None,
                       n=10, i=0.08):
    """Create the objective components corresponding to total annualized cost.

    Arguments
    ---------
    ic_labels : iterable of `str`
        labels for the investment cost expressions
    fc_labels : iterable of `str`
        labels for the fixed cost expressions
    vc_labels : iterable of `str`
        labels for the variable cost expressions
    n : `int`
        number of repetitions of the time-period specified by `timesteps`.
    i : `float`
        interest_rate

    Returns
    -------
    Expressions for design_objective and operational_objective forming part
    of the total annualized costs.
    """
    if ic_labels is None:
        ic_labels = ['investment_costs']
    if fc_labels is None:
        fc_labels = ['fixed_costs']
    if vc_labels is None:
        vc_labels = ['variable_costs']
    ic = sum(system.aggregate_component_expressions(ic_label)
             for ic_label in ic_labels)
    fc = sum(system.aggregate_component_expressions(fc_label)
             for fc_label in fc_labels)
    vc = sum(system.aggregate_component_expressions(vc_label)
             for vc_label in vc_labels)
    af = ((1 + i) ** n * i) / ((1 + i) ** n - 1)  # annuity factor

    #fc = 0 #Yifan
    return af*ic + fc, vc



def create_aggregated_block(data_historic_raw, no_typical_periods, hours_per_period, no_segments):
    ## TODO useless for RiSES5 -> Maybe for real-time
    import tsam.timeseriesaggregation as tsam
    import pandas as pd

    ###  #TODO YW -> check redundent input
    ###
    # data_historic = data_historic_raw.iloc[:, 6].to_frame()  # Converted to DataFrame for use in tsam, select el_pc
    data_historic_raw.drop(columns=['dt'], inplace=True)

    aggregation = tsam.TimeSeriesAggregation(data_historic_raw,
                                             resolution=1,
                                             noTypicalPeriods=no_typical_periods,
                                             hoursPerPeriod=hours_per_period,
                                             clusterMethod='k_means',
                                             noSegments=no_segments,
                                             segmentation=True)

    data_aggregated_list = []
    typical_periods = aggregation.createTypicalPeriods()
    typical_periods.index.names = ['Typical_Period', 'Segment_Step', 'dt']
    for i in aggregation.clusterOrder:
        data_aggregated_list.append(typical_periods.query(f'Typical_Period == {i}'))

    data_aggregated = pd.concat(data_aggregated_list)
    data_aggregated.reset_index(inplace=True)
    data_aggregated['s'] = 1
    data_aggregated['t'] = range(1, len(data_aggregated.index) + 1)

    aggregation.dt_typical_periods = data_aggregated[['Typical_Period', 'Segment_Step', 'dt']]
    aggregation.dt_typical_periods.drop_duplicates(inplace=True)
    aggregation.dt_typical_periods.set_index(['Typical_Period', 'Segment_Step'], inplace=True)

    data_aggregated.drop(columns=['Segment_Step', 'Typical_Period'], inplace=True)
    data_aggregated.set_index(['s', 't'], drop=True, inplace=True)

    return data_aggregated, aggregation



def get_rises_gap(options_rises = None):

    from math import inf

    ub_file = Path(options_rises['output_folder']/options_rises['real_time_ub'])
    lb_file = Path(options_rises['output_folder']/options_rises['real_time_lb'])

    rt_ub_value = inf
    rt_lb_value = inf

    ###
    ### get ub
    ###
    try:
        #Path(ub_file).is_file():
        f = open(ub_file, "r")
        rt_ub_value = float(f.readline())
        f.close()
    except:
        rt_ub_value = inf

    ###
    ### get lb
    ###
    try:
    #if Path(lb_file).is_file():
        f = open(lb_file, "r")
        rt_lb_value = float(f.readline())
        f.close()
    except:
        rt_lb_value = inf


    if rt_ub_value != inf and rt_lb_value != inf:
        gap = (rt_ub_value - rt_lb_value) / rt_ub_value

        print(f'Current rises_gap = {gap}.')
    else:
        print('These is no valid gap yet!')
        gap = inf

    return gap


def determine_parameter_tsam(options_tsam= None, res=None, set_up = False):


    #next_parameter = {
    #    'hours_per_period': 24
    #}

    len_input_ts = options_tsam['len_input_ts']
    len_timespan = options_tsam['len_timespan']

    if options_tsam['branch'] == 'LB':

        increment_nr_typical_periods = 1 # TODO Yifan[need to define ]
        increment_nr_segments_in_period = 5 #23 # TODO Yifan[need to define ]

        start_nr_typical_periods = 1 # TODO Yifan[need to define ]
        start_nr_segments_in_period = 1 # TODO Yifan[need to define ]

    elif options_tsam['branch'] == 'UB':
        increment_nr_typical_periods = 1  # TODO Yifan[need to define ]
        increment_nr_segments_in_period = 1  # TODO Yifan[need to define ]

        start_nr_typical_periods = 1  # TODO Yifan[need to define ]
        start_nr_segments_in_period = 6  # TODO Yifan[need to define ]

    next_status = True

    if set_up:
        print('Setting up parameters for tsam: ')
        ts_span = 24 /len_timespan # 6 #24

        max_no_typical_periods = int(len_input_ts / ts_span)

        i = 1
        choice_pair = []

        while i<= max_no_typical_periods:
            if (len_input_ts % (i*ts_span) == 0):
                #
                choice_pair.append((int(len_input_ts/(i*ts_span)), int(ts_span*i)))
            i = i+1

        options_tsam['choice_pair'] = choice_pair
        #options_tsam['increment_no_typical_periods'] = 1
        #options_tsam['increment_no_segments_in_period'] = 6
        print('tsam choice_pair [number of periods, number of segments]: ', choice_pair)
        return options_tsam, next_status

    else:
        next_parameter = {}
        #next_parameter['choice_pair'] = options_tsam['choice_pair']

        if len(res) == 0:

            try:
                current_choice_pair = 0
                next_parameter['current_choice_pair'] = current_choice_pair

                next_parameter['max_nr_typical_periods'] = options_tsam['choice_pair'][next_parameter['current_choice_pair']][0]
                next_parameter['hours_per_period'] = options_tsam['choice_pair'][next_parameter['current_choice_pair']][1]

                next_parameter['no_typical_periods'] = start_nr_typical_periods
                next_parameter['no_segments'] = start_nr_segments_in_period

            except:
                Warning('Sth wrong!')
                current_choice_pair = 0
                next_parameter['current_choice_pair'] = current_choice_pair

                next_parameter['max_nr_typical_periods'] = 1
                next_parameter['hours_per_period'] = 6#4
                next_parameter['no_typical_periods'] = 1
                next_parameter['no_segments'] = 6#4


        else:

            iter_current = list(res.keys())[-1]

            current_choice_pair = options_tsam[iter_current]['current_choice_pair']
            current_gap = res[iter_current][list(res[iter_current])[-1]][1]
            current_nr_seg = options_tsam[iter_current]['no_segments']
            current_nr_tp = options_tsam[iter_current]['no_typical_periods']
            current_hr_per_period = options_tsam[iter_current]['hours_per_period']
            current_max_nr_tp = options_tsam[iter_current]['max_nr_typical_periods']

            if options_tsam['branch'] == 'LB':
                increment_nr_typical_periods = current_max_nr_tp -start_nr_typical_periods  # TODO Yifan[need to define ]
                increment_nr_segments_in_period = current_hr_per_period - start_nr_segments_in_period  # 23 # TODO Yifan[need to define ]

            elif options_tsam['branch'] == 'UB':
                increment_nr_typical_periods = current_max_nr_tp -start_nr_typical_periods  # TODO Yifan[need to define ]
                increment_nr_segments_in_period = 6  # 23 # TODO Yifan[need to define ]

            if current_nr_seg < current_hr_per_period and current_nr_tp <= current_max_nr_tp:

                next_parameter['current_choice_pair'] = options_tsam[iter_current]['current_choice_pair']
                next_parameter['max_nr_typical_periods'] = options_tsam[iter_current]['max_nr_typical_periods']
                next_parameter['hours_per_period'] = options_tsam[iter_current]['hours_per_period']

                next_parameter['no_typical_periods'] = options_tsam[iter_current]['no_typical_periods']
                next_parameter['no_segments'] = current_nr_seg + increment_nr_segments_in_period

            elif current_nr_seg >= current_hr_per_period and current_nr_tp < current_max_nr_tp:
                next_parameter['current_choice_pair'] = options_tsam[iter_current]['current_choice_pair']
                next_parameter['max_nr_typical_periods'] = options_tsam[iter_current]['max_nr_typical_periods']
                next_parameter['hours_per_period'] = options_tsam[iter_current]['hours_per_period']

                next_parameter['no_typical_periods'] = current_nr_tp + increment_nr_typical_periods
                next_parameter['no_segments'] = start_nr_segments_in_period

            elif current_nr_seg == current_hr_per_period and current_nr_tp == current_max_nr_tp:

                current_choice_pair = current_choice_pair + 1

                if current_choice_pair < len(options_tsam['choice_pair']):
                    next_parameter['current_choice_pair'] = current_choice_pair

                    next_parameter['max_nr_typical_periods'] = options_tsam['choice_pair'][next_parameter['current_choice_pair']][0]
                    next_parameter['hours_per_period'] = options_tsam['choice_pair'][next_parameter['current_choice_pair']][1]
                    next_parameter['no_typical_periods'] = start_nr_typical_periods
                    next_parameter['no_segments'] = start_nr_segments_in_period


            """
                for i in list(options_tsam):
    
                    if i != 'active_relax_balance' and i != 'active_relax_storage' and i != 'len_input_ts':
    
                        if options_tsam[i]['no_typical_periods'] == current_nr_tp and options_tsam[i]['no_segments'] == current_nr_seg - increment_no_segments_in_period:
                            iter_last_seg = i
                        elif options_tsam[i]['no_typical_periods'] == current_nr_tp - increment_no_typical_periods and options_tsam[i]['no_segments'] == current_nr_seg:
                            iter_last_tp = i
    
                if iter_last_seg == '':
                    for i in list(options_tsam):
    
                        if i != 'active_relax_balance' and i != 'active_relax_storage' and i != 'len_input_ts':
    
                            if options_tsam[i]['no_typical_periods'] == current_nr_tp-increment_no_typical_periods and options_tsam[i]['no_segments'] == current_nr_seg - increment_no_segments_in_period:
                                iter_last_seg = i
    
                if iter_last_tp == '':
                    for i in list(options_tsam):
                        if i != 'active_relax_balance' and i != 'active_relax_storage' and i != 'len_input_ts':
                            if options_tsam[i]['no_typical_periods'] == current_nr_tp - increment_no_typical_periods and options_tsam[i]['no_segments'] == current_nr_seg:
                                iter_last_tp = i
    
                print(options_tsam)
    
                last_tp_gap = res[iter_last_tp][list(res[iter_current])[-1]][1]
                last_tp_tp = options_tsam[iter_last_tp]['no_typical_periods']
                last_tp_seg = options_tsam[iter_last_tp]['no_segments']
    
                last_seg_gap = res[iter_last_seg][list(res[iter_current])[-1]][1]
                last_seg_tp = options_tsam[iter_last_seg]['no_typical_periods']
                last_seg_seg = options_tsam[iter_last_seg]['no_segments']
    
    
    
                # Calculate finite backward differences of optimality gap for n_periods and n_segments in A&O
                try:
                    delta_eps_p = (last_tp_gap - current_gap) / (
                            current_nr_tp * current_nr_seg - last_tp_tp *last_tp_seg)
                except Exception:
                    delta_eps_p = 0
    
        
                try:
                    delta_eps_s = (last_seg_gap - current_gap) / (
                            current_nr_tp * current_nr_seg - last_seg_tp * last_seg_seg)
                except Exception:
                    delta_eps_s = 0
    
        
        
                if abs(delta_eps_p) >= abs(delta_eps_s):
                    next_parameter['no_typical_periods'] = current_nr_tp + increment_no_typical_periods
                    next_parameter['no_segments'] = current_nr_seg
    
                    print(
                        f"Next run has {next_parameter['no_typical_periods']} typical periods and again {next_parameter['no_segments']} segments per period")
                else:
    
                    next_parameter['no_typical_periods'] = current_nr_tp
                    next_parameter['no_segments'] = current_nr_seg + increment_no_segments_in_period
    
                    print(
                        f"Next run A&O has {next_parameter['no_segments']} segments per period and again {next_parameter['no_typical_periods']} typical periods")
            """

            #next_parameter['no_typical_periods'] = current_nr_tp + options_tsam['increment_no_typical_periods']
            #next_parameter['no_segments'] = current_nr_seg + options_tsam['increment_no_segments_in_period']

            #if current_nr_tp == 300:# and current_nr_seg == next_parameter['hours_per_period']:
            #    exit('Reach the original optimization problem')

            #if current_nr_tp  ==  current_hr_per_period, options_tsam['increment_no_segments_in_period']\

            #        * current_nr_seg == len_input_ts:

            #    next_parameter['no_typical_periods'] = current_nr_tp
            #    next_parameter['no_segments'] = current_nr_seg + options_tsam['increment_no_segments_in_period']

            #    if next_parameter['no_segments'] > next_parameter['hours_per_period']:

            #        next_parameter['hours_per_period'] = next_parameter['hours_per_period'] + 24
            #        next_parameter['no_segments'] = 4
            #        next_parameter['no_typical_periods'] = 2

        #if next_parameter['hours_per_period'] * next_parameter['no_typical_periods'] > len_input_ts:
        #    next_parameter['hours_per_period'] = 1
        #    next_parameter['no_typical_periods'] = len_input_ts
        if current_choice_pair == len(options_tsam['choice_pair']) - 1:

            if next_parameter['no_segments'] == next_parameter['hours_per_period'] and next_parameter['no_typical_periods'] == next_parameter['max_nr_typical_periods']:

                next_status = False

        print(
            f"Next optimization: \n\t [nr of periods, nr of segments] [{options_tsam['choice_pair'][next_parameter['current_choice_pair']]}]\n \t [nr of typical periods, nr of typical segments]: [{next_parameter['no_typical_periods']}, {next_parameter['no_segments']}]")

    return next_parameter, next_status


def get_label(comando_comp):
    return comando_comp.label

if __name__ == '__main__':



    test = True

    if test:
        options_tsam={
            'len_input_ts': 48,
            0:{
            'no_typical_periods': 3,
            'hours_per_period': 24,
            'no_segments': 3},

            1: {
                'no_typical_periods': 3,
                'hours_per_period': 24,
                'no_segments': 4},
            2: {
                'no_typical_periods': 4,
                'hours_per_period': 24,
                'no_segments': 4},
        }
        res_1= {}
        res= {
            0: {
                0: (None, None),
                1: (64364, 0.87),
                2: (64364, 0.87)
            },
            1: {
                0: (None, None),
                1: (64364, 0.87),
                2: (96911, 0.73)
            },
            2: {
                0: (None, None),
                1: (64364, 0.87),
                2: (106911, 0.72)
            },
        }

    x = determine_parameter_tsam(options_tsam, res_1)

    y = determine_parameter_tsam(options_tsam, res)