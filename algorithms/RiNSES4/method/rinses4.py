"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang


def rinses4(options = None):


    options_rinses4 = options['rinses4']
    options_rinses4_LASD = options['LASD']
    options_rinses4_RR = options['RR']

    res_rinses4 = {
        'RR':{},
        'LASD':{}
    }
    if options_rinses4['which_branch'] == 'LASD':

        ###
        ### calculate UB
        ###
        from algorithms.RiNSES4.method.branch_LASD import rinses4_LASD
        print("+++++ RiNSES4: Computing UB ")
        res_rinses4['LASD'] = rinses4_LASD(options_rinses4, options_rinses4_LASD)

        #real_gap = get_rises_gap(obj_AD, obj_RR)

        return res_rinses4['LASD']
    elif options_rinses4['which_branch'] == 'RR':
        from algorithms.RiNSES4.method.branch_RR import rinses4_RR

        print("+++++ RiNSES4: Computing LB ")
        res_rinses4['RR'] = rinses4_RR(options_rinses4, options_rinses4_RR)

        return res_rinses4['RR']
    elif options_rinses4['which_branch'] == 'parallel':
        print('RiNSES4 parallel is under construction... ')

        from algorithms.RiNSES4.method.branch_RR import rinses4_RR
        from algorithms.RiNSES4.method.branch_LASD import rinses4_LASD

        # res_rises5['RR'] = rises5_RR(options_rises5, options_rises5_RR)
        # res_rises5['AD'] = rises5_AD(options_rises5, options_rises5_AD)

        from multiprocessing import Process
        p1 = Process(target=rinses4_RR(options_rinses4, options_rinses4_RR))
        p1.start()
        p2 = Process(target=rinses4_LASD(options_rinses4, options_rinses4_LASD))
        p2.start()
        p1.join()
        p2.join()

    return res_rinses4
