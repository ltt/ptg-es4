"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang


def write_results(P=None, options=None):

    if P == None:
        exit('Problem not defined!')

    if options == None:
        from pathlib import Path
        options = {
            # 'read_which_pickle': 'results_dict_DO_S3',
            'input_folder': Path('./'),
            'output_folder': Path('./'),
            'if_pickle': False,
            'if_csv': True,
            'plot_setup': 'ESCAPE_2023',
            'if_plot': False,
        }

    obj = {}
    dv_dummy = {}
    ov_dummy = {}
    state_dummy = {}
    parameters_dummy = {}
    ts_index = P.timesteps

    try:
        for dv in P.design_variables:
            dv_dummy[dv.name] = dv.value
    except:
        print('The set of decision variable are empty.')
        pass

    for ov in P.operational_variables:
        ov_dummy[ov.name] = ov.value

    state_dict = {}
    ov_storage = P.states.keys()

    for state in ov_storage:
        str = state.name
        str = str[:-4]
        str = str + '_exists'

        try:
            if P[str].value == 1:
                state_dict[state] = state
        except:
            state_dict[state] = state

    for i in state_dict:
        state_dummy[i.name] = i.value

    dv_dummy = dict(sorted(dv_dummy.items()))
    ov_dummy = dict(sorted(ov_dummy.items()))
    state_dummy = dict(sorted(state_dummy.items()))

    results_dict = {
        #'ts_index':{1: ts_index.keys()},
        #'dt': {1: ts_index.values},
        'design variables': {1: dv_dummy},
        'operational variables':  {1:  ov_dummy},
        'storage variables':  {1: state_dummy},
        #'parameters': parameters_dummy
    }

    import time
    timestr = time.strftime("%Y%m%d-%H%M%S")

    if options['if_pickle']:
        import pickle
        with open(f'results_dict_{P.name}_{timestr}.pickle', 'wb') as f:
            pickle.dump(results_dict, f)

    #results_dict_all = {1: results_dict}
    ###
    ### write csv files
    ###
    import pandas as pd
    from pathlib import Path

    for i in results_dict.keys():

        # the paramters and design variables are currently empty, they should be included later
        if i != "parameters" and i != "design variables":  ## currently empty

            ### csv files for each subproblem
            temp_list = []

            for j in results_dict[i].keys():
                files_1 = pd.DataFrame(results_dict[i][j])
                #files_1.to_csv(f'.\\{options["output_folder"]}\\{i}_{j}.csv', sep=';', decimal=',',
                #               float_format='%.2f')

                # merge the subproblem dataframes to a single one
                temp_list.append(files_1.copy())

            # if i != "parameters" and i != "design variables":
            merged_df = pd.concat(temp_list)

            merged_df['t'] = list(range(1, len(merged_df) + 1))
            merged_df['s'] = [1] * len(merged_df)
            merged_df.set_index(['s', 't'], inplace=True)

            import os.path
            #csv_to_write = os.path.join(options["output_folder"], f'{i}_all.csv')
            #pathlib.Path('').append_suffix()
            merged_df.to_csv(f'{options["output_folder"]}/{i}_all.csv', sep=';', decimal=',',
                             float_format='%.2f')

            ## remove Lambda, Simplex
            if i == "operational variables":
                # iterating the columns
                column_list = merged_df.columns.copy()

                for col in column_list:
                    if "Lambda" in col or "Simplex" in col:
                        merged_df = merged_df.drop(col, axis=1)
                merged_df.to_csv(f'{options["output_folder"]}/{i}_all_except_lambda_simplex.csv', sep=';',
                                 decimal=',',
                                 float_format='%.2f')

        elif i == "design variables":
            temp_list = []

            temp_list = pd.DataFrame(results_dict[i])

            temp_list.to_csv(f'{options["output_folder"]}/{i}_all.csv', sep=';', decimal=',',
                             float_format='%.2f')
    print("Done: Results are written in csv files!")

    return 0
