"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang


import pandas as pd
from pathlib import Path
import pprint
pp = pprint.PrettyPrinter(indent=4)

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)



if __name__ == '__main__':

    import time
    current_timestr = time.strftime("%m%d-%H%M")

    from algorithms.RiNSES4.method.processing import determine_parameter_tsam

    # test to set-up nr of tsam
    #x = determine_parameter_tsam()

    ###
    ### set up the case study
    ###

    options_rinses4 = {
        'minlp_solution': 'baron', #[baron, gurobi, RiNSES4]
        'parallel': False,  # [True, False]
        'debugging': True,  # [True, False]

        'rinses_gap': 0.05,

        'input_folder': Path('./input/'),
        'input_file': Path('./input/input_2024_FOCAPD_2190.csv'),
        'test_ES': 'oneS',
        'storage_model': 'standard', #[standard, superposition, relax]

        'real_time_ub': 'real_time_ub.txt',
        'real_time_lb': 'real_time_lb.txt'
    }

    if options_rinses4['minlp_solution'] == 'RiNSES4':
        options_rinses4['output_folder'] =  Path(f'./output/{options_rinses4["minlp_solution"]}')
    else:
        options_rinses4['output_folder'] = Path(f'./output/{options_rinses4["minlp_solution"]}_{current_timestr}')

    if options_rinses4['minlp_solution'] == 'RiNSES4':
        options_rinses4['output_folder'] =  Path(f'./output/{options_rinses4["minlp_solution"]}')

        options_LASD = {
            'output_folder': Path(f'{options_rinses4["output_folder"]}/LASD')

        }
        options_RR = {
            'output_folder': Path(f'{options_rinses4["output_folder"]}/RR')
        }

        from algorithms.RiNSES4.method.processing import get_rises_gap


    ###
    ### prepare the structure
    ###
    options_rinses4['output_folder'].mkdir(parents=True, exist_ok=True)

    #options_rinses4['output_folder'].stat().st_mode
    #options_rinses4['output_folder'].chmod(0o0777)

    if options_rinses4['minlp_solution'] == 'RiNSES4':
        pass

    from algorithms.RiNSES4.method.processing import *

    ###
    ### load input data
    ###
    input_data, scenarios, timesteps = load_inputs(options_rinses4['input_file'])
    options_rinses4['input_data'] = input_data
    options_rinses4['scenarios'] = scenarios
    options_rinses4['timesteps'] = timesteps


    if options_rinses4['minlp_solution'] == 'baron' or options_rinses4['minlp_solution'] == 'gurobi':

        ###
        ### just for debugging
        ES, comp_storages = create_energy_system(test_ES=options_rinses4['test_ES'], sto_model=options_rinses4['storage_model'])
        d_obj, o_obj = make_tac_objective(ES, n=10, i=0.08)

        if options_rinses4['storage_model'] == 'superposition':
            input_data['BAT_no_segments'] = int(timesteps.values.sum() / scenarios.size)
            input_data['BAT_dt'] = input_data['dt']


        ### solve the MINLP problem using baron
        # solver baron
        print("Using baron solves MINLP ...\n")
        from comando.interfaces.baron import solve

        P = ES.create_problem(design_objective=d_obj, operational_objective=o_obj, timesteps=timesteps,
                              scenarios=scenarios, data=input_data, name='ptg_MINLP_baron')

        print(f'Problem {P.name} has {P.num_vars} '
              f'variables and {P.num_cons} constraints!')

        if options_rinses4['minlp_solution'] == 'baron':
            output_file = Path(f'{options_rinses4["output_folder"]}/ptg_rises5_MINLP.bar')

            if output_file.is_file():
                output_file.unlink()

            import multiprocessing

            solve(P, f'{output_file.parent}/{output_file.name}', threads = multiprocessing.cpu_count(), MaxTime=3600 * 12, EpsR=0.05) #FirstLoc=1
        elif options_rinses4['minlp_solution'] == 'gurobi':

            print("Using Gurobi solves MINLP (for checking) ...\n")

            pass

        options_post = {
            # 'read_which_pickle': 'results_dict_DO_S3',
            'input_folder': Path('./output/'),
            'output_folder': options_rinses4['output_folder'],
            'if_pickle': False,
            'if_csv': True,
            'plot_setup': 'ESCAPE_2023', #"['ESCAPE_2023', 'FOCAPD_2024'] #TODO
            'if_plot': False,
        }

        from algorithms.RiNSES4.method.post_processing import write_results
        #TODO: will be moved in a generic folder in the future
        write_results(P, options_post)


    elif options_rinses4['minlp_solution'] == 'RiSES5':


        ###
        ###
        ###

        pass
