<!-- This file is part of the PtG-ES4 project which is released under the MIT 
license. See file LICENSE.txt for full license details.

AUTHOR: Yifan Wang -->
# PtG-ES4 Algorithms: RiNSES4

## Description
The RiNSES4 method is generally applicable to two-stage, time-dependent systhesis problems with
coupled decision variables and constraints in complex energy systems.
And this work will be presented at the [FOCAPD2024](https://focapd.cache.org) conference.

![](RiNSES4.PNG)

## Structure 
```
ptg-es4/algorithms:
|-- RiNSES4
    |-- components models
    |-- method
    |-- case_study_UB.py
    |-- case_study_LB.py
    |-- case_study_baron.py

    |-- input
    |-- output
    |-- env_RiNSES4_py38.yml
```

## Referencing
```bibtex
@article{Wang2024b,
title = {RiNSES4: Rigorous Nonlinear Synthesis of Energy Systems for Seasonal Energy Supply and Storage},
journal = {Systems and Control Transactions},
volume = {3},
pages = {604--611},
year = {2024},
doi = {https://doi.org/10.69997/sct.105466},
url = {https://psecommunity.org/LAPSE:2024.1583},
author = {Yifan Wang, Marvin Volkmer, Dörthe Franzisca Hagedorn, Christiane Reinert and Niklas {von der Assen}},
}
```


## Getting started

### Intall packages
For a list of all available features refer to the file env_bridgeESPS_py38.yml.
In order to install all available extensions (recommended) run:
```bash
mkdir venv_folder

conda env create --prefix=venv_folder --file=correct_path\env_RiNSES4_py38.yml

conda activate venv_folder
```
### Run case study

```bash
# run LASD branch
python case_study_UB.py

# run RR branch
python case_study_LB.py

# run benchmark
python case_study_baron.py

```

Please note that this method is being further developed. If you are interested in the latest version of the code, please contact the authors directly.
