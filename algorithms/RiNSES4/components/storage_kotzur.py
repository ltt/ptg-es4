"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang

from comando.core import Component, BINARY

class KotzurStorage(Component):

    def __init__(self, label, nom_ref, c_ref, M=1, c_m=0,
                 min_cap=0, max_cap=None, nom_cap=None,
                 charge_eff=1, discharge_eff=1, c_loss=0,
                 c_loss_T=0, theta_min=None, theta_max=None, T_amb=None,
                 in_min=0, in_max=1, out_min=0, out_max=1,
                 in_name='input', out_name='output',
                 in_connector_name='IN', out_connector_name='OUT',
                 exists=False):

        super().__init__(label)

        nom_name = 'capacity_nom'

        if exists:
            if nom_cap is None:
                print("Warning: nominal capacity is not given...")
                nom_cap = max_cap
            else:
                #nom_cap = nom_cap
                min_cap = nom_cap
                max_cap = nom_cap
                inv = c_ref * (nom_cap / nom_ref) ** M
        else:
            nom_cap = self.make_design_variable(nom_name, bounds=(0, max_cap), init_val=0)
            exists = self.make_design_variable('exists', BINARY, init_val=1)

            # bound component size
            self.add_le_constraint(exists * min_cap, nom_cap, name=nom_name + '_min')
            self.add_le_constraint(nom_cap, exists * max_cap, name=nom_name + '_max')

            # investment cost of the component
            inv = c_ref * (nom_cap / nom_ref) ** M
        self.add_expression('investment_costs', inv)

        # add maintenance costs to fixed costs
        self.add_expression('fixed_costs', c_m * inv)

        ##########################################################################
        ###
        ### operational model
        ### based on Kotzur
        ##########################################################################

        if exists is True:
            avaiable = 1
        else:
            avaiable = exists

        ### based on TD MA

        """
        ###
        ### create vars for detailed block:
        ###
        soc_detail = self.make_operational_variable(f'soc_intra_detailed', bounds=(0, max_cap))

        # INPUT
        inp_detail = self.make_operational_variable(f'{in_name}_intra_detailed', bounds=(0, max_cap))
        b_in_detail = self.make_operational_variable(f'b_in_intra_detailed', domain=BINARY, init_val=0)

        self.add_le_constraint(inp_detail, b_in_detail * cap * in_max, name=f'input_max_intra_detailed')
        self.add_ge_constraint(inp_detail, b_in_detail * cap * in_min, name=f'input_min_intra_detailed')

        # OUTPUT
        out_detail = self.make_operational_variable(f'{out_name}_intra_detailed', bounds=(0, max_cap))
        b_out_detail = self.make_operational_variable(f'b_out_intra_detailed', domain=BINARY, init_val=1)

        self.add_le_constraint(out_detail, b_out_detail * cap * out_max, name=f'output_max_intra_detailed')
        self.add_ge_constraint(out_detail, b_out_detail * cap * out_min, name=f'output_min_intra_detailed')


        dissipation_intra = self.add_expression(f'dissipation_intra_detailed', soc_detail * c_loss)

        state_change_detail = inp_detail * charge_eff - out_detail / discharge_eff - dissipation_intra
        self.declare_state(soc_detail, state_change_detail, 0)

        # make sure, storage units cannot charge and discharge at the same time
        self.add_le_constraint(b_in_detail + b_out_detail, 1, name=f'charge_discharge_switch_intra_detailed')

        self.add_expression(f'input_intra_detailed', inp_detail)
        self.add_expression(f'output_intra_detailed', out_detail)

        self.add_input(f'{in_connector_name}_detailed', inp_detail)
        self.add_output(f'{out_connector_name}_detailed', out_detail)
        """

        ###
        ### create vars for aggregated block:
        ###
        o_dt = self.make_parameter('o_dt', 1) #[hr]
        dt = self.make_parameter('dt', 1)
        no_segments = self.make_parameter('no_segments', 24)
        ###
        ### intra
        soc_intra = self.make_operational_variable('soc_intra', bounds=(-max_cap, max_cap))

        self.add_le_constraint(soc_intra, nom_cap*avaiable, name = 'soc_intra_max_limit')
        self.add_ge_constraint(soc_intra, -nom_cap*avaiable, name='soc_intra_min_limit')
        ### input
        inp_intra = self.make_operational_variable(in_name, bounds=(0, max_cap))
        b_in_intra = self.make_operational_variable(f'b_in_intra', domain=BINARY, init_val=0)

        #self.add_le_constraint(inp_intra, nom_cap, name='inp_intra_max_limit')


        self.add_le_constraint(inp_intra, b_in_intra * nom_cap * in_max, name='input_max')
        self.add_ge_constraint(inp_intra, b_in_intra * nom_cap * in_min, name='input_min')

        # output at timestep t
        out_intra = self.make_operational_variable(out_name, bounds=(0, max_cap))
        b_out_intra = self.make_operational_variable('b_out_intra', domain=BINARY, init_val=1)

        #self.add_le_constraint(out_intra, nom_cap, name='out_intra_max_limit')


        self.add_le_constraint(out_intra, b_out_intra * nom_cap * out_max, name='output_max_speed')
        self.add_ge_constraint(out_intra, b_out_intra * nom_cap * out_min, name='output_min_speed')


        self.add_le_constraint(b_in_intra + b_out_intra, 1, name='charge_discharge_switch')



        #dissipation_intra = self.add_expression('dissipation_intra', soc_intra * c_loss)
        dissipation_intra = self.add_expression('dissipation_intar',
                                                soc_intra * ((1 - c_loss * o_dt) ** dt - 1))

        if c_loss_T != 0:
            #ES_T_amb = ('T_amb')  # [°C] air temperature
            g = (self.theta_min - T_amb) / (self.theta_max - self.theta_min)

            dissipation_T_intra = self.add_expression('dissipation_T_intra', max_cap * c_loss_T * g)
        else:
            dissipation_T_intra = 0

        state_change_intra = dt * (inp_intra * charge_eff - out_intra / discharge_eff) \
                       - dissipation_intra - dissipation_T_intra

        state_change_intra_init = self.make_parameter('state_change_intra_init', 0)
        self.declare_state_superposition(soc_intra, state_change_intra, state_change_intra_init)

        ###
        ### inter
        soc_inter = self.make_operational_variable('soc_inter', bounds=(0, max_cap))

        self.add_le_constraint(soc_inter, avaiable*nom_cap, name='soc_inter_max_limit')


        dissipation_inter = self.add_expression('dissipation_inter',
                                                soc_inter * ((1 - c_loss * o_dt)**dt - 1)) ##YW not correct




        #soc_intra_max = self.make_operational_variable('soc_intra_max', bounds=(-max_cap, max_cap))
        #soc_intra_min = self.make_operational_variable('soc_intra_min', bounds=(-max_cap, max_cap))

        #self.add_le_constraint(soc_intra, soc_intra_max, name='auxiliary_max')
        #self.add_ge_constraint(soc_intra, soc_intra_min, name='auxiliary_min')


        #state_change_intra_min_max = self.make_parameter('state_change_intra_min_max', 0)
        #self.declare_state_superposition(soc_intra_max, state_change_intra_min_max, 0)
        #self.declare_state_superposition(soc_intra_min, state_change_intra_min_max, 0)

        #self.add_le_constraint(soc_inter + soc_intra_max, nom_cap, name='soc_max')
        #self.add_ge_constraint(soc_inter * (1-c_loss*delta_to)**no_segments + soc_intra_min, 0, name='soc_min')

        self.add_le_constraint(soc_inter + soc_intra, avaiable*nom_cap, name='soc_max')
        self.add_ge_constraint(soc_inter * (1-c_loss*o_dt)**dt + soc_intra, 0, name='soc_min')


        soc_intra_last = self.make_operational_variable('soc_intra_last', bounds=(-max_cap, max_cap))
        self.add_le_constraint(soc_intra_last, nom_cap, name = 'soc_intra_last_max_limit')
        self.add_ge_constraint(soc_intra_last, -nom_cap, name='soc_intra_last_min_limit')

        state_change_inter = dissipation_inter + soc_intra_last

        self.declare_state_superposition(soc_inter, state_change_inter, 0)

        total_soc = self.make_operational_variable('total_soc', bounds=(0, max_cap))
        self.add_le_constraint(total_soc, nom_cap*avaiable, name='total_soc_max')
        #state_change_intra = inp_intra * charge_eff - out_intra / discharge_eff \
        #                     - dissipation_intra - dissipation_T_intra

        self.add_eq_constraint(total_soc, soc_intra+soc_inter, name='total_soc')

        #self.declare_state(soc_inter, state_change_inter, 0)


        self.add_input(in_connector_name, inp_intra)
        self.add_output(out_connector_name, out_intra)


class Battery(KotzurStorage):
    """Battery unit (BAT) parametrized as in Baumgärtner Lowcarbon2019.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 2116.1  # [€], reference cost
    M = 0.8382  # [-], cost exponent
    c_m = 0.025  # [-], maintenance coefficient, (fraction of investment cost)
    # bounds of component size
    min_cap = 40  # [kW], minimal capacity allowed for the model
    max_cap = 600000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.920  # [-], charging efficiency
    discharge_eff = 0.920  # [-], discharging efficiency
    c_loss = 0.000042  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.36  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.36  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT',
                         exists=exists)