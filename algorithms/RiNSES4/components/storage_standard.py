"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Marvin Volkmer

from components.ptg_components import PtGStorage

class Battery(PtGStorage):
    """Battery unit (BAT) parametrized as in Baumgärtner Lowcarbon2019.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 2116.1  # [€], reference cost
    M = 0.8382  # [-], cost exponent
    c_m = 0.025  # [-], maintenance coefficient, (fraction of investment cost)
    # bounds of component size
    min_cap = 40  # [kW], minimal capacity allowed for the model
    max_cap = 600000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.920  # [-], charging efficiency
    discharge_eff = 0.920  # [-], discharging efficiency
    c_loss = 0.000042  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.36  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.36  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)