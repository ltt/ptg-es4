"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang

from comando import Component, BINARY


class RelaxStorage(Component):
    """General storage unit.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage state of charge is modeled using state variables, for which
    time coupling constraints are created after the parametrization of the
    problem.

    Arguments
    ---------
    - label : str
        Unique sting that serves as an identifier of the storage unit.
    - nom_ref: The nominal reference size of the storage unit
    - c_ref: coefficient of investment costs
    - c_m: factor of investment costs corresponding to maintenance costs
          TODO: This implies calculations with a year as the time series!
    - M: (1) exponent of investment costs
    - min_cap: (0) lower bound for nominal size
    - max_cap: (None) upper bound for nominal size
    - charge_eff: (1) the charging efficiency of the storage unit
    - discharge_eff: (1) the discharging efficiency of the storage unit
    - c_loss: (0) the relative self-discharge of the unit (% SOC/h)
    - in_min: (0) minimum charging rate relative to nominal capacity
    - in_max: (1) maximum charging rate relative to nominal capacity
    - out_min: (0) minimum discharging rate relative to nominal capacity
    - out_max: (1) mxaimum discharging rate relative to nominal capacity

    - in_name: name of input commodity
    - out_name: name of output commodity
    - in_connector_name: name of input connector
    - out_connector_name: name of output connector
    - exists: bool
        Specifies whether the Component is currently installed; if set to
        True, its nominal size will be a user-specified parameter. If set
        to False, the nominal size of the component is a design variable.

    """

    def __init__(self, label, nom_ref, c_ref,  M=1, c_m = 0,
                 min_cap=0, max_cap=None, nom_cap=None,
                 charge_eff=1, discharge_eff=1, c_loss=0,
                 c_loss_T=0, theta_min= None, theta_max=None, T_amb = None,
                 in_min=0, in_max=1, out_min=0, out_max=1, in_name='input',
                 out_name='output', in_connector_name='IN',
                 out_connector_name='OUT', exists=False):
        super().__init__(label)

        # nominal (installed) output power
        nom_name = 'capacity_nom'
        if exists:
            if nom_cap is None:
                print("Warning: nominal capacity is not given...")
                cap = max_cap
            else:
                cap = nom_cap
                max_cap = cap
                min_cap = cap
            #cap = self.make_parameter(nom_name)
            #self.add_le_constraint(min_cap, cap, name=nom_name + '_min')
            #self.add_le_constraint(cap, max_cap, name=nom_name + '_max')
            # investment cost of the component
            inv = c_ref * (cap / nom_ref) ** M



        else:
            cap = self.make_design_variable(nom_name, bounds=(0, max_cap), init_val=max_cap)
            exists = self.make_design_variable('exists', BINARY, init_val=1)
            # bound component size
            self.add_le_constraint(exists * min_cap, cap, name=nom_name + '_min')
            self.add_le_constraint(cap, exists * max_cap, name=nom_name + '_max')
            # investment cost of the component
            inv = c_ref * (cap / nom_ref) ** M
           # set (investment) costs
        self.add_expression('investment_costs', inv)
        # add maintenance costs to fixed costs
        self.add_expression('fixed_costs', c_m * inv)

        ## changed by YW
        if exists is True:
            avaiable = 1
        else:
            avaiable = exists
        # define operational variables
        soc = self.make_operational_variable('soc', bounds=(0, max_cap))
        self.add_le_constraint(soc, avaiable * cap, name=nom_name + 'soc_max') #YW changed name

        # input at timestep t
        inp = self.make_operational_variable(in_name, bounds=(0, max_cap))
        # binary indicating whether the storage is charging
        b_in = self.make_operational_variable('b_in', domain=BINARY,
                                              init_val=0)
        # the upper/lower bound for input depends on the nominal output, but
        # since that's a variable  we have to write this bound as a constraint:
        self.add_le_constraint(inp, b_in * cap * in_max, name='input_max')
        self.add_ge_constraint(inp, b_in * cap * in_min, name='input_min')
        # output at timestep t
        out = self.make_operational_variable(out_name, bounds=(0, max_cap))
        # binary indicating whether the storage is discharging
        b_out = self.make_operational_variable('b_out', domain=BINARY,
                                               init_val=1)
        # the upper/lower bound for output depends on the nominal output, but
        # since that's a variable  we have to write this bound as a constraint:
        self.add_le_constraint(out, b_out * cap * out_max, name='output_max')
        self.add_ge_constraint(out, b_out * cap * out_min, name='output_min')

        dissipation = self.add_expression('dissipation',  soc * c_loss)

        if c_loss_T != 0:
            #ES_T_amb = ('T_amb')  # [°C] air temperature
            g = (self.theta_min - T_amb) / (self.theta_max - self.theta_min)

            dissipation_T = self.add_expression('dissipation', cap * c_loss_T * g)
        else:
            dissipation_T = 0
        # define storage constraints and declare state variables
        state_change = inp * charge_eff - out / discharge_eff \
            - dissipation - dissipation_T

        # YW state self.declare_state(soc, state_change, 0) ##TODO YW have to check
        self.add_expression('declare_state', state_change)  #add YW

        # make sure, storage units cannot charge and discharge at the same time
        self.add_le_constraint(b_in + b_out, 1, name='charge_discharge_switch')
        self.add_le_constraint(b_in + b_out, avaiable, name='availablility') # YW added

        self.add_expression('input', inp)
        self.add_expression('output', out)

        # set connectors
        self.add_input(in_connector_name, inp)
        self.add_output(out_connector_name, out)


###################################################################
####
#### models for "method"
####
###################################################################


class Battery(RelaxStorage):
    """Battery unit (BAT) parametrized as in Baumgärtner Lowcarbon2019.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 2116.1  # [€], reference cost
    M = 0.8382  # [-], cost exponent
    c_m = 0.025  # [-], maintenance coefficient, (fraction of investment cost)
    # bounds of component size
    min_cap = 40  # [kW], minimal capacity allowed for the model
    max_cap = 600000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.920  # [-], charging efficiency
    discharge_eff = 0.920  # [-], discharging efficiency
    c_loss = 0.000042  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.36  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.36  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)
