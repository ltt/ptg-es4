"""Case study for the RiNSES method"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang


import pandas as pd
from pathlib import Path
import pprint
pp = pprint.PrettyPrinter(indent=4)

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)



if __name__ == '__main__':

    import time
    current_timestr = time.strftime("%m%d-%H%M")

    ###
    ### set up the case study
    ###

    options_rinses4 = {
        'minlp_solution': 'RiNSES4', #[baron, gurobi, RiNSES4]
        'which_branch': 'RR',  # [RR, LASD, parallel]
        'debugging': False,  # [True, False]

        'rises5_gap': 0.05,

        'input_folder': Path('./input/'),
        'input_file': Path('./input/input_2024_FOCAPD_2190.csv'),
        'test_ES': 'oneS',
        'storage_model': 'standard',  #[standard, superposition, relax]

        'real_time_ub': 'real_time_ub.txt',
        'real_time_lb': 'real_time_lb.txt'
    }

    if options_rinses4['minlp_solution'] == 'RiNSES4':
        options_rinses4['output_folder'] = Path(f'./output/{options_rinses4["minlp_solution"]}')
    else:
        options_rinses4['output_folder'] = Path(f'./output/{options_rinses4["minlp_solution"]}_{current_timestr}')

    if options_rinses4['minlp_solution'] == 'RiNSES4':

        options_LASD = {
            'output_folder': Path(f'{options_rinses4["output_folder"]}/AD'),
            'operation_solution': 'gurobi',#[DeLoop, Baron]
            'options_DeLoop': {
                """
                # problem_class: [MILP, MINLP],currently only MILP possible
                # solution_path: [to_gurobi, to_baron, tp_pyomo], currently only to_pyomo possible
                # peak_power: [True, False], consider peak_power as in the DeLoop paper, but not used in this work
                # t_aut: a dummy value, see Eq. (7) in DeLoop Paper, not used in this work
    
                # check_steps: [True, False], for debugging
                # lower_bound: [pre_given, pre_calculated, parallel] status of the LB: a certain value, read from a log-file or parallel calculated
                # DeLoop_gap: a pre-defined gap of the the method
                # DeLoop_step1: [manual, automatic], how to set the number of subproblems
                """
                # unnecessary to change
                'problem_class': 'MINLP',
                'solution_path': 'baron',  # 'pyomo',
                'peak_power': False,
                't_aut': 2500,

                # can be changed
                'check_steps': False,
                'lower_bound': 'pre_given',  # 'pre_calculated',
                'lower_bound_value': 470000,

                'DeLoop_gap': 0.02,
                'DeLoop_step1': 'manual'
            },
        }
        options_RR = {
            'output_folder': Path(f'{options_rinses4["output_folder"]}/RR_{current_timestr}'),
        }

    from algorithms.RiNSES4.method.processing import get_rises_gap

    ###
    ### prepare the structure
    ###
    if options_rinses4['minlp_solution'] == 'RiNSES4':
        try:
            #YW options_rises5['output_folder'].stat().st_mode
            #YW options_rises5['output_folder'].chmod(0o0777)

            options_RR['output_folder'].stat().st_mode
            options_RR['output_folder'].chmod(0o0777)

            import shutil

            #YW shutil.rmtree(options_rises5['output_folder'])
            shutil.rmtree(options_RR['output_folder'])
        except:
            pass

        if options_rinses4['which_branch'] == 'LASD':
            options_LASD['output_folder'].mkdir(parents=True, exist_ok=True)
        elif options_rinses4['which_branch'] == 'RR':
            options_RR['output_folder'].mkdir(parents=True, exist_ok=True)
    else:

        options_rinses4['output_folder'].mkdir(parents=True, exist_ok=True)

    from algorithms.RiNSES4.method.processing import *

    ###
    ### load input data
    ###
    input_data, scenarios, timesteps = load_inputs(options_rinses4['input_file'])
    options_rinses4['input_data'] = input_data
    options_rinses4['scenarios'] = scenarios
    options_rinses4['timesteps'] = timesteps


    if options_rinses4['minlp_solution'] == 'baron' or options_rinses4['minlp_solution'] == 'gurobi':

      pass


    elif options_rinses4['minlp_solution'] == 'RiNSES4':


        ###
        ###
        ###
        from algorithms.RiNSES4.method.rinses4 import *

        options={
            'rinses4': options_rinses4,
            'RR': options_RR,
            'LASD': options_LASD
        }

        res_rinses4 = rinses4(options = options)

    else:
        print(f"The MINLP solution {options_rinses4['minlp_solution']} is not defined yet. Please check your setup!")