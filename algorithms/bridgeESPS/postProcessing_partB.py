"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann,  Yifan Wang


import pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
    'text.usetex': True,
    'text.latex.preamble': [
    # r'\usepackage{amsmath}',
    # r'\usepackage{amssymb}',
    r'\usepackage{units}',
    # r'\usepackage{siunitx},'
    ],
     # 'font.size': 11,
     'font.family': 'lmodern',

})

def set_size(use=True, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    width_pt = 422.52347
    height_pt = 635.5
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2


    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    if use==True:
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])
    else:
        fig_height_in =fig_width_in * golden_ratio*1.3 * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)

def plot_CH4(options=None):

    objects = []
    # timestr = '0410-1355'
    #hourss = 24
    #timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]
    true_idx = [1, 2, 3, 4, 5, 10]  # YW,7,10]
    x_hours = data[1]['results_CH4']['time']
    x_ticks = [1, 4, 8, 12, 16, 20, 24]
    y_ticks = [0, 250, 500]

    fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=set_size(use=True, fraction=1, subplots=(3, 2)))
    i = 0
    for ax in [ax1, ax2, ax3, ax4, ax5, ax6]:
        # print(i)
        df = data[true_idx[i]]['results_CH4']
        if ax == ax1:
            # ax.plot('time', 'energy_feed_out', 's-', data=df, label='PE', color='tab:orange')
            ax.plot('time', data[true_idx[i]]['CH4_out'], 'o-', data=df, label='ES', color='tab:blue')
        else:
            ax.plot('time', 'energy_feed_out', 's-', data=df, label='PS', color='tab:orange')
            ax.plot('time', data[true_idx[i]]['CH4_out'], 'o-', data=df, label='ES', color='tab:blue')
        ax.set_ylabel(r'$ \dot{E}^\mathrm{out}_\mathrm{MP,CH_{4}}$ in kW')
        ax.set_ylim(0, 550)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_xticks(x_ticks)
        ax.set_yticks(y_ticks)
        if i == 0:
            ax.set_title(f'iteration {true_idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {true_idx[i]}')
        # get handles and labels
        # handles, labels = plt.gca().get_legend_handles_labels()
        # order = [1,0]
        # ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc='best', ncol=1, prop={'size': 8}, frameon=False)
        ax.legend(loc='best', ncol=1, prop={'size': 8},
                  frameon=False)
        # ax.grid()
        if ax == ax5 or ax == ax6:
            ax.set_xlabel(r'Time in hr')
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()
    #plt.savefig(f"{options['output_folder']}\\partB_CH4.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_CH4.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()

def plot_CH4_app(options=None):

    objects = []
    # timestr = '0410-1355'
    #hourss = 24
    #timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]
    true_idx = [1, 4]  # YW,7,10]
    x_hours = data[1]['results_CH4']['time']
    x_ticks = [1, 4, 8, 12, 16, 20, 24]
    y_ticks = [0, 250, 500]

    fig, ((ax1), (ax3)) = plt.subplots(2, 1, figsize=set_size(use=True, fraction=1, subplots=(2, 1)))
    i = 0
    for ax in [ax1, ax3]:
        # print(i)
        df = data[true_idx[i]]['results_CH4']
        if ax == ax1:
            # ax.plot('time', 'energy_feed_out', 's-', data=df, label='PE', color='tab:orange')
            ax.plot('time', data[true_idx[i]]['CH4_out'], 'o-', data=df, label='ES', color='tab:blue')
        else:
            ax.plot('time', 'energy_feed_out', 's-', data=df, label='PS', color='tab:orange')
            ax.plot('time', data[true_idx[i]]['CH4_out'], 'o-', data=df, label='ES', color='tab:blue')
        ax.set_ylabel(r'$ \dot{E}^\mathrm{out}_\mathrm{MP,CH_{4}}$ in kW')
        ax.set_ylim(0, 550)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_xticks(x_ticks)
        ax.set_yticks(y_ticks)
        if i == 0:
            ax.set_title(f'iteration {true_idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {true_idx[i]}')
        # get handles and labels
        # handles, labels = plt.gca().get_legend_handles_labels()
        # order = [1,0]
        # ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc='best', ncol=1, prop={'size': 8}, frameon=False)
        ax.legend(loc='best', ncol=1, prop={'size': 8},
                  frameon=False)
        # ax.grid()
        if ax == ax3: #or ax == ax4:
            ax.set_xlabel(r'Time in hr')
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()
    #plt.savefig(f"{options['output_folder']}\\partB_CH4.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_CH4.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()

def plot_temp(options=None):

    objects = []
    # timestr = '0410-1355'
    #hourss = 24
    #timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]
    true_idx = [1, 5]  # 10
    x_hours = data[1]['results_CH4']['time']
    x_ticks = [1, 4, 8, 12, 16, 20, 24]

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=set_size(use=True, fraction=0.45, subplots=(2, 1)))
    i = 0
    for ax in [ax1, ax2]:

        df = data[true_idx[i]]['results_CH4']
        ax.plot('time', 'T_R101_in', 'o-', data=df, label='R1')
        ax.plot('time', 'T_R102_in', 's-', data=df, label='R2')
        ax.plot('time', 'T_R103_in', '^-', data=df, label='R3')
        ax.plot('time', 'T_R104_in', 'D-', data=df, label='R4')
        ax.set_ylabel(r'Temperature in °C')
        ax.set_ylim(230, 370)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_xticks(x_ticks)
        if i == 0:
            ax.set_title(f'iteration {true_idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {true_idx[i]}')
        ax.legend(loc='upper left', ncol=2, prop={'size': 6}, frameon=False)
        # ax.grid()
        if ax == ax2:
            ax.set_xlabel(r'Time in hr')
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()

    #plt.savefig(f"{options['output_folder']}\\partB_temp.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_temp.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()

def plot_pressure(options=None):
    objects = []
    # timestr = '0410-1355'
    # hourss = 24
    # timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]
    true_idx = [1, 5]  # YW 10
    x_hours = data[1]['results_CH4']['time']
    x_ticks = [1, 4, 8, 12, 16, 20, 24]
    y_ticks = [1, 5, 10, 15, 20]

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=set_size(use=True, fraction=0.45, subplots=(2, 1)))
    i = 0
    for ax in [ax1, ax2]:

        df = data[true_idx[i]]['results_CH4']
        ax.plot('time', 'pressure_R101_in', 'o-', data=df, label='R101')
        ax.set_ylabel(r'Pressure in bar')
        ax.set_ylim(1, 20)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_xticks(x_ticks)
        ax.set_yticks(y_ticks)
        if i == 0:
            ax.set_title(f'iteration {true_idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {true_idx[i]}')
        # ax.grid()
        if ax == ax2:
            ax.set_xlabel(r'Time in hr')
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()

    #plt.savefig(f"{options['output_folder']}\\partB_pressure.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_pressure.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()

def plot_ED(options=None):
    objects = []
    # timestr = '0410-1355'
    # hourss = 24
    # timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]

    x = np.arange(1, len(data) + 1, 1)
    x_ticks = [1, 4, 8, 12, 16, 20, 24]
    # fig.suptitle('Strombedarf')
    ### Reactor temperatures - einzeln
    idx = [1, 3, 10]
    idx = [1, 5]
    i = 0
    # fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=set_size(use=True, fraction=1, subplots=(3, 1)))
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=set_size(use=True, fraction=0.5, subplots=(2, 1)))
    # for ax in [ax1,ax2,ax3]:
    for ax in [ax1, ax2]:
        df = data[idx[i]]['results_CH4']
        ED_DAC = data[idx[i]]['ED_DAC']
        ED_PEM = data[idx[i]]['ED_PEM']
        ax.bar('time', 'electricity_demand', data=df, label='MP', bottom=ED_PEM + ED_DAC)
        ax.bar('time', ED_DAC, data=df, label='DAC', bottom=ED_PEM)
        ax.bar('time', ED_PEM, data=df, label='PEM')

        ax.set_ylabel(r'$\dot{E}^\mathrm{in}_{k,\mathrm{electricity},t,i}$ in kW')
        if ax == ax2:
            ax.set_xlabel(r'Time in hr')
        if i == 0:
            ax.set_title(f'iteration {idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {idx[i]}')
        ax.set_xlim([0, 25])
        ax.set_xticks(x_ticks)
        ax.set_ylim([0, 1500])
        ax.set_yticks([0, 500, 1000, 1500])
        ax.legend(loc='upper left', ncol=1, prop={'size': 8}, frameon=False)
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()

    #plt.savefig(f"{options['output_folder']}\\partB_Ein.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_Ein.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()

def plot_Stor(options=None):

    mpl.rcParams.update({
        #     "pgf.texsystem": "pdflatex",
        #     'font.family': 'serif',
        #     'text.usetex': True,
        #     'pgf.rcfonts': False,
        'text.usetex': True,
        'text.latex.preamble': "\n".join([
        r'\usepackage{amsmath}',
        r'\usepackage{amssymb}',
        r'\usepackage{units}',
        r'\usepackage[version=4]{mhchem}',
        # r'\usepackage{siunitx},'
        ]),
        # 'font.size': 11,
        'font.family': 'lmodern',


    })

    objects = []
    # timestr = '0410-1355'
    # hourss = 24
    # timestr = '0506-1320'
    with open(f"{options['input_folder']}\\results_{options['hours']}_{options['timestr']}.pkl", 'rb') as f:
        objects.append(pickle.load(f))

    data = objects[0]
    x = np.arange(1, len(data) + 1, 1)
    x_ticks = [1, 4, 8, 12, 16, 20, 24]
    # fig.suptitle('Strombedarf')
    ### Reactor temperatures - einzeln

    idx = [1, 3, 5]
    idx = [1, 5]  # , 5]

    i = 0
    # fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=set_size(use=True, fraction=1, subplots=(3, 1)))
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=set_size(use=True, fraction=0.5, subplots=(2, 1)))
    # for ax in [ax1,ax2,ax3]:
    for ax in [ax1, ax2]:  # , ax3]:
        df = data[idx[i]]['results_CH4']
        SOC_CH4 = data[idx[i]]['CH4_SOC'] / 3000 * 100
        SOC_H2 = data[idx[i]]['H2_SOC'] / 4000 * 100
        SOC_CO2 = data[idx[i]]['CO2_SOC'] / 400 * 100
        # SOC_BAT = data[idx[i]]['BAT_SOC']/4000*100
        # SOC_HEAT = data[idx[i]]['HEAT_SOC']/2000*100
        ax.plot('time', SOC_CH4, 'o-', data=df, label=r'\ce{CH4}')
        ax.plot('time', SOC_CO2, 's-', data=df, label=r'\ce{CO2}')
        ax.plot('time', SOC_H2, '^-', data=df, label=r'\ce{H2}')
        # ax.plot('time', SOC_BAT, 'D-', data=df, label=r'battery')
        # ax.plot('time', SOC_HEAT, '*-', data=df, label=r'thermal storage')

        ax.set_ylabel(r'State of charge in $\%$')
        if ax == ax2:
            ax.set_xlabel(r'Time in hr')
        if i == 0:
            ax.set_title(f'iteration {idx[i]} / benchmark')
        else:
            ax.set_title(f'iteration {idx[i]}')
        ax.set_xlim([0, 25])
        ax.set_ylim([0, 150])
        ax.set_xticks(x_ticks)
        ax.set_yticks([0, 50, 100])
        ax.legend(loc='upper left', ncol=3, prop={'size': 8}, frameon=False)
        i = i + 1

    fig.set_tight_layout(True)
    plt.tight_layout()

    #plt.savefig(f"{options['output_folder']}\\partB_SOC.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"{options['output_folder']}\\partB_SOC.pdf", format='pdf', bbox_inches='tight')

    if options['show_fig']:
        plt.show()


def plot_all(options):

    ###
    ### plot results of part B
    ###
    from pathlib import Path
    import matplotlib as mpl

    mpl.rcParams['lines.markersize'] = 4

    plot_CH4(options)
    # plot_CH4_app(options) # for plots in appendix 4

    plot_temp(options)
    plot_pressure(options)

    plot_ED(options)
    plot_Stor(options)

if __name__ == "__main__":

    ###
    ### plot results of part B
    ###
    from pathlib import Path
    import matplotlib as mpl

    mpl.rcParams['lines.markersize'] = 4

    ### define file paths
    options = {
        'timestr':  '1117-1533',# '1023-1449', #'1023-1411',#'1023-1542',#'1023-1449', #'1023-1116',
        'hours': 24, #timesteps.size
        'input_folder': Path('./output'),
        'output_folder': Path('./pictures_partB'),
        'show_fig': True
    }

    options['output_folder'].mkdir(parents=True, exist_ok=True)


    plot_CH4(options)

    #plot_CH4_app(options) # for plots in appendix 4

    plot_temp(options)
    plot_pressure(options)

    plot_ED(options)
    plot_Stor(options)
