"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang


from comando.core import Component

class PVModule(Component):
    """
    Component model of a photovoltaic module based on bachelor thesis by Hagen Seele, 2016
    Some equations differ from the thesis and are taken from "Skript Ringlabor Solarthermie 2021" by EBC (see folder 'literature')
    Output parameter:
        - Pt_PV: Electrical output power
    Input parameter:
        - T_U: Ambient temperature
        - Gdir_hor: Direct irradiation on horizontal surface
        - Gdir_diff: Diffuse irradiation on horizontal surface
        - v_w: Wind speed
        - N_day: Day of the year [1..365]
        - MEZ: Time of the day in CET [0..24h]
    General parameter:
        - lambda_betr: Längengrad / longitude of PV module; default: 6 - Aachen [°]
        - phi: Breitengrad / latitude; default: 51 - Aachen [°]
        - beta_M: Neigung PV-Modul bzgl. Ebene / module angle of inclination relative to surface; default: 40 [°]
        - alpha_M: Azimutwinkel Modul / azimuth angle module; default: 0 - south orientation [°]
        - sMod: mathematical model for calculation irradiation intensity
            - 'iso': Isotropes Strahlungsmodell, leicht geneigte Fläche
            - 'diffus': Diffuses Strahlungsmodell, stark geneigte Fläche, Perez [DEFAULT]
        - P_N: installed power [kWp]

    For convenient handling of the input parameters, the calculation of the specific power pt_PV has been moved into
    the class function calculate_pt() which has to be run separately

    Usage in case study (see component_PV_test.py):
    - read csv data file and store in dataFrame
    - run calculate_pt(dataFrame) --> returns specific power pt_PV based on weather data
    - add pv_PV to existing dataFrame
    - assign DataFrame to optimization problem

    Function visualize() provides 3 subplots:
    - Chronological and load duration curve of PV_OUT
    - Ambient temperature and wind speed over time
    - Module radiation GM over time
    """

    def __init__(self, label, alpha_M=0, beta_M=40, phi=51, lambda_Betr=6, P_N=500, sMod='diffus', M=1, nom_min=0,
                 nom_max=None, min_part_load=0, base_eff=1, fit_params=None,
                 in_name='input', out_name='output', in_connector_name='IN',
                 out_connector_name='OUT', exists=False, optional=True):

        super().__init__(label)
        self.alpha_M = alpha_M
        self.beta_M = beta_M
        self.phi = phi
        self.lambda_Betr = lambda_Betr
        self.sMod = sMod

        # General parameter
        # YW: parameter for design
        Pel_ref = .001  # [MW], reference nominal power
        c_ref = 4254.3 / 1000  # [k€], reference cost
        c_m = 0.01  # [-], maintenance coefficient, (fraction of investment cost)
        M = 0.9502  # [-], cost exponent
        P_PV_N = P_N  # [kW]  installed power
        self.add_expression('P_nom', P_PV_N)
        Pel_nom = P_PV_N * 10 ** (-3)  # [MW]

        ### output power calculation
        pt_PV = self.make_parameter('pt_PV')  #YW TODO add check pt_PV exists
        # P_PV_N = (self.make_design_variable('P_nom'))
        self.Pt_PV = (self.make_operational_variable('P_out'))  # output power [kW]

        # YW: 3.5?????
        self.add_le_constraint(self.Pt_PV, pt_PV * P_PV_N, '3.5')  # 3.5 # YW eq

        # investment cost of the component, adopted from IES_components
        inv = c_ref * (Pel_nom / Pel_ref) ** M #YW have to update

        # fixed cost of the component
        fc = c_m * inv
        self.add_expression('investment_costs', inv)
        self.add_expression('fixed_costs', fc)

        # set connectors

        self.add_output('OUT', self.Pt_PV)

    def calculate_pt(self, data=None):

        ### Calculation irradiation intensity
        """
        Calculation:
        - GM = f(Gdir_hor,Gdiff_hor,Ceta,sigmaZ,F1,betaM,F2,roh_ref)
        - Gdir_hor, Gdiff_hor = Messwerte
        - F1 = f(f1_1,f1_2,Delta,f1_3, sigmaZ)
        - F2 = f(f2_1, f2_2, Delta, f2_3, sigmaZ)
        - fij = f(epsilon)
        - epsilon = f(Gdir_normal, Gdiff_hor, SigmaZ)
        - Gdir_normal = f(Gdir_hor, Sigma, CetaZ)
        - Delta = f(SigmaZ, Gdiff_hor, Gext_normal)
        - G_ext_normal = f(G_SK, N)
        - SigmaZ = f(betaS, delta, phi, omega)
        - Ceta = f(betaM, betaS, alphaS, alphaM)
        - delta = f(B=f(N)) Deklination, N = Tag im Jahr
        - Wahre Ortszeit WOZ = f(MEZ, L_K, E_t)
        - E_t = f(B)
        - L_K = f(lambda_Betrachter, lambda_Zeitzone) Längengradkorrektur
        - omega = f(WOZ) Stundenwinkel
        - alpha_S = f(phi,betaS,delta) Azimutwinkel Sonne
        -

        Input:
        - N: Day of the year
        - Gdir_hor: Direct irradiation on horizontal surface
        - Gdiff_hor: Diffuse irradiation on horizontal surface
        - Lambda: Längengrad / longitude
        - betaM: Neigung PV-Modul bzgl. Ebene / module angle of inclination relative to surface
        - alphaM: Azimutwinkel Modul / azimuth angle
        - phi: Breitengrad / latitude
        """

        import math
        import sympy
        import numpy as np

        if data is None:
            raise ValueError('No input data for PV calculations are given')

        # Parameter
        N_day = data.N_day.values  # 1...365 day of the year, HIER AKTUELLEN TAG EINGEBEN
        MEZ = data.MEZ.values  # Mitteleuropäische Zeit/CET [1..24 h] HIER AKTUELLE ZEIT EINGEBEN

        # N_day = self.make_parameter('N_day')
        # MEZ = self.make_parameter('MEZ')

        # alpha_M = 0  # [°] Azimutwinkel Modul bzgl. Südausrichtung; 0° = Südausrichtung, BELIEBIG ANGENOMMEN
        # beta_M = 40  # [°] Neigungswinkel Modul, BELIEBIG ANGENOMMEN
        Gdir_hor = data.PV_Gdir_hor_name.values  # [W/m²] direct irradiation on horizontal surface, MESSWERT
        Gdiff_hor = data.PV_Gdiff_hor_name.values  # [W/m²] diffuse irradiation on horizontal surface, MESSWERT

        lambda_Zeitzone = 15  # [°] Längengrad mitteleuropäische Zeitzone / longitude CET
        # lambda_Betr = 6  # [°] Längengrad Aachen / longitude Aachen
        # phi = 51  # [°] Breitengrad Aachen / latitude Aachen
        rho_refl = 0.2  # Reflexionskoeffizient für unbekannte Umgebungen / coefficient of reflections for unknown environment
        G_SK = 1367  # [W/m²] Solarkonstante / solar constant

        # Calculations
        def degToRad(deg):
            rad = deg * (np.pi) / 180
            return rad

        B = 360 * (N_day - 1) / 365  # A.6

        delta = (180 / np.pi) * (
                0.006918 - 0.399912 * np.cos(np.deg2rad(B)) + 0.070258 * np.sin(np.deg2rad(B))
                - 0.006758 * np.cos(2 * np.deg2rad(B)) + 0.000907 * np.sin(2 * np.deg2rad(B))
                - 0.002697 * np.cos(3 * np.deg2rad(B)) + 0.001480 * np.sin(
            3 * np.deg2rad(B)))  # A.5, declination angle [°]

        E_t = 3.82 * (0.000075 + 0.001868 * np.cos(np.deg2rad(B)) - 0.032077 * np.sin(np.deg2rad(B))
                      - 0.014615 * np.cos(2 * np.deg2rad(B)) - 0.040849 * np.sin(
                    2 * np.deg2rad(B)))  # A.8, correction factor ellipctical orbit [h]
        L_K = 1 / 15 * (self.lambda_Betr - lambda_Zeitzone)  # A.9, [h] correction factor  longitude
        WOZ = MEZ + L_K + E_t  # A.7,  wahre Ortszeit [h]
        omega = 15 * (WOZ - 12)  # A.10 [°] Stundenwinkel / hour angle

        sigma_Z = np.arccos(
            np.sin(np.deg2rad(delta)) * np.sin(np.deg2rad(self.phi)) + np.cos(
                np.deg2rad(delta)) * np.cos(
                np.deg2rad(self.phi)) * np.cos(np.deg2rad(omega)))  # A.11,  crown angle [rad]
        beta_S = sigma_Z  # A.11, elevation [rad]

        alpha_Sun = np.sign(omega) * np.arccos(
            (np.sin(np.deg2rad(self.phi)) * np.cos(sigma_Z) - np.sin(np.deg2rad(delta)))
            / (np.sin(sigma_Z) * np.cos(np.deg2rad(self.phi))))

        ceta = np.arccos(np.cos(sigma_Z) * np.cos(np.deg2rad(self.beta_M)) + np.sin(sigma_Z) * np.sin(
            np.deg2rad(self.beta_M)) * np.cos(
            alpha_Sun - np.deg2rad(
                self.alpha_M)))  # [rad] A.13 mistake in bachelor thesis, taken from script "Ringlabor 2021: Solarthermie" - EBC

        # Wähle Strahlungsmodell
        # 'iso': Isotropes Strahlungsmodell, leicht geneigte Fläche
        # 'diffus': Diffuses Strahlungsmodell, stark geneigte Fläche, Perez

        import numpy as np
        from pandas import DataFrame

        # sMod = 'diffus'

        if self.sMod == 'iso':

            F_MH = 0.5 * (1 + np.cos(np.deg2rad(self.beta_M)))  # A.15, form factor module - horizon [-]
            F_MB = 0.5 * (1 - np.cos(np.deg2rad(self.beta_M)))  # A.16 form factor module - ground [-]

            self.GM = Gdir_hor * (np.cos(ceta) / np.cos(sigma_Z)) + Gdiff_hor * F_MH + (
                    Gdiff_hor + Gdir_hor) * rho_refl * F_MB  # A.14, module surface irradiation [W/m²]
            self.add_expression('GM', self.GM)
        else:

            # Factor y is necessary to prevent negative irradiation values
            y = np.maximum(0, np.cos(ceta))  # taken from script "Ringlabor 2021: Solarthermie" - EBC
            # Factor z is necessary to prevent dividing through very small values of cos(sigma_Z)
            z = np.maximum(np.cos(np.deg2rad(85)),
                           np.cos(sigma_Z))  # taken from script "Ringlabor 2021: Solarthermie" - EBC

            # Replaced cos(ceta) & cos(sigma_Z) by y & z
            Gdir_norm = Gdir_hor * (y / z)  # A.21, direct surface on orthogonal inclined surface [W/m²]

            # clip zero values to prevent dividing through 0 in next step
            Gdiff_hor = Gdiff_hor.astype('float64')
            Gdiff_hor[Gdiff_hor == 0] = 0.000001


            eps_klar = (1 + (Gdir_norm / Gdiff_hor) + 5.535 * 10 ** (-6) * (np.deg2rad(sigma_Z) / 1) ** 3) / (
                    1 + 5.535 * 10 ** (-6) * (np.deg2rad(sigma_Z) / 1) ** 3)  # A.20, cloudlessness parameter [-]

            # very small values of Gdiff_hor leads to wrong eps_klar results -> replace by 1
            eps_klar[Gdiff_hor == 0.000001] = 1

            Gext_norm = G_SK * (
                    1 + 0.033 * np.cos(np.deg2rad(360 * N_day / 365)))  # A.23 extraterrestrial radiation [W/m²]

            # cos(sigma_Z) replaced by z to prevent dividing through very small values
            Delta = (1 / z) * (Gdiff_hor / Gext_norm)  # A.22, brightness [-]

            f11 = np.zeros_like(eps_klar)
            f12 = np.zeros_like(eps_klar)
            f13 = np.zeros_like(eps_klar)
            f21 = np.zeros_like(eps_klar)
            f22 = np.zeros_like(eps_klar)
            f23 = np.zeros_like(eps_klar)

            ### YW changed
            for i in range(0, len(eps_klar)):
                if eps_klar[i] <= 1.056:  # <= 1.056:
                    f11[i] = 0.041
                    f12[i] = 0.621
                    f13[i] = -0.105
                    f21[i] = -0.04
                    f22[i] = 0.074
                    f23[i] = -0.031
                elif eps_klar[i] <= 1.253:  # 1.056 < eps_klar <= 1.253:
                    f11[i] = 0.054
                    f12[i] = 0.966
                    f13[i] = -0.166
                    f21[i] = -0.016
                    f22[i] = 0.114
                    f23[i] = -0.045
                elif eps_klar[i] <= 1.586:  # 1.253 < eps_klar <= 1.586:
                    f11[i] = 0.227
                    f12[i] = 0.866
                    f13[i] = -0.25
                    f21[i] = 0.069
                    f22[i] = -0.002
                    f23[i] = -0.062
                elif eps_klar[i] <= 2.134:  # 1.586 < eps_klar <= 2.134:
                    f11[i] = 0.486
                    f12[i] = 0.67
                    f13[i] = -0.373
                    f21[i] = 0.148
                    f22[i] = -0.137
                    f23[i] = -0.056
                elif eps_klar[i] <= 3.23:  # 2.134 < eps_klar <= 3.23:
                    f11[i] = 0.819
                    f12[i] = 0.106
                    f13[i] = -0.465
                    f21[i] = 0.268
                    f22[i] = -0.497
                    f23[i] = -0.029
                elif eps_klar[i] <= 5.98:  # 3.23 < eps_klar <= 5.98:
                    f11[i] = 1.02
                    f12[i] = -0.26
                    f13[i] = -0.514
                    f21[i] = 0.306
                    f22[i] = -0.804
                    f23[i] = 0.046
                elif eps_klar[i] <= 10.08:  # 5.98 < eps_klar <= 10.08:
                    f11[i] = 1.009
                    f12[i] = -0.708
                    f13[i] = -0.433
                    f21[i] = 0.287
                    f22[i] = -1.286
                    f23[i] = 0.166
                elif eps_klar[i] > 10.08:  # eps_klar > 10.08:
                    f11[i] = 0.936
                    f12[i] = -1.121
                    f13[i] = -0.352
                    f21[i] = 0.226
                    f22[i] = -2.449
                    f23[i] = 0.383
        """
        ### YW changed




        F1 = 0
        if ((f11 + f12 * Delta + f13 * (np.pi * np.deg2rad(sigma_Z) / 180)).compare(0)) > -1:
            F1 = f11 + f12 * Delta + f13 * (np.pi * np.deg2rad(sigma_Z) / 180) # A.18, brightness coefficient [-]
        """
        F1 = np.maximum(0, (f11 + f12 * Delta + f13 * (np.pi * np.deg2rad(sigma_Z)) / 180))
        F2 = f21 + f22 * Delta + f23 * (np.pi * np.deg2rad(sigma_Z) / 180)  # A.19, brightness coefficient [-]

        # cos(ceta) & cos(sigma_Z) replaced by y & z to prevent negative irradiation values and deviding through very small values
        GM = (Gdir_hor * (y / z) + Gdiff_hor *
              ((1 - F1) * (1 + np.cos(np.deg2rad(self.beta_M))) / 2
               + F1 * (y / z) + F2 * np.sin(np.deg2rad(self.beta_M)))
              + (Gdir_hor + Gdiff_hor) * rho_refl * ((1 - np.cos(np.deg2rad(self.beta_M))) / 2))
        # A.17, module surface irradiation [W/m²], adopted from script "Ringlabor 2021: Solarthermie" - EBC

        self.GM = GM.clip(min=0)  # clip GM to prevent negative values
        # self.add_expression('GM', self.GM)

        ### Calculation module temperature
        """Input:
        - GM: Module surface irradiation [W/m²]
        - eta_m = f(T_M_STC): Module efficiency at standard test conditions
        - T_U: Ambient temperature
        - v_w: Wind speed

        Output: T_M: module temperature [K]

        iterative calculation:
        - T_M = f(GM,alpha,alpha_K,alpha_S, etha_M,T_U)
        - alpha_S = f(T_M,T_U,sigma, epsilon)
        - alpha_K = f(T_M, T_U, v_w)
        """

        # Parameter
        self.T_U = data.PV_T_U.values + 273.15  # YW changed, Ambient temperature [K]
        etha_M_TMSC = 0.15  # Module efficiency at standard test conditions, Wert beliebig angenommen
        self.v_w = data.PV_v_w.values  # Wind speed [m/s]

        alpha_Glas = 0.8  # [0.7...0.9] radiation absorption coefficient glass cover
        sigma = 5.670367 * 10 ** (-8)  # stefan-bolzmann constant [W/m^2K^4]
        eps_Glas = 0.88  # radiation emission coefficient glass cover

        # Define iterative function based on 2012 Eicker, Solare Technologien für Gebäude, Kap. 2.4.2.2
        def EstimateModuleTemp(T_M_assumed, GM, T_U, v_w, etha_M_TMSC=etha_M_TMSC, alpha_Glas=alpha_Glas,
                               sigma=sigma, eps_Glas=eps_Glas):
            h_r = sigma * eps_Glas * (T_M_assumed ** 2 + T_U ** 2) * (
                    T_M_assumed + T_U)  # radiation exchange with environment [W/m^2K]
            h_c_wind = 4.214 + 3.575 * v_w  # eq. 5.43 Eicker [W/m^2K]
            h_c_free = 1.78 * (T_M_assumed - T_U) ** (1 / 3)  # free convection [W/m^2K]
            h_c = (h_c_wind ** 3 + h_c_free ** 3) ** (1 / 3)  # combined heat transfer coefficient [W/m^2K]
            T_M_temp = ((alpha_Glas - etha_M_TMSC) * GM) / (h_r + h_c) + T_U
            return T_M_temp

        T_M_temp = 273.15 + 50  # initial guess
        n_iter = 3  # iteration steps, 3 seems to be a good compromise between solving time and accuracy
        # iterate through function
        for i in range(0, n_iter):
            T_M_temp = EstimateModuleTemp(T_M_temp, self.GM, self.T_U, self.v_w)

        T_M = T_M_temp  # module temperature [K]
        # self.add_expression('T_M', T_M)

        # calculation module output power
        eta_WR = 0.97  # efficiency power inverter
        gamma = -(0.45 / 100)  # [%/°C] temperature coefficient
        GM_STC = 1000  # [W/m²] radiation at standard test conditions
        TM_STC = 25 + 273.15  # [K] module temperature at standard test conditions

        pt_PV = eta_WR * (self.GM / GM_STC) * (1 + gamma * (T_M - TM_STC))  # 3.4, specific power output [-]
        pt_PV = pt_PV[np.newaxis]  # change pt_PV array from 1D to 2D
        pt_PV = pt_PV.transpose()  # transpose pt_PV array
        PV_pt_PV = DataFrame(data=pt_PV)  # create array of type DataFrame
        PV_pt_PV = PV_pt_PV.fillna(0)  # fill all NaN with zeros

        ## YW added by YW
        PV_pt_PV[PV_pt_PV < 0.001] = 0
        PV_pt_PV[PV_pt_PV > 1] = 1

        return PV_pt_PV  # return DataFrame
        # self.add_expression('pt_PV', pt_PV)

    def visualize(self):

        time = range(len(self.Pt_PV.value))

        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(3, sharex=True)
        ax[0].plot(time, self.Pt_PV.value, label="Chronological curve")
        ax[0].plot(time, sorted(self.Pt_PV.value, reverse=True), label="Load duration curve")
        ax[0].set_ylabel(ylabel=r"$P_{t}^{OUT}$ in kW")
        ax[0].set_title('PV-module: Electrical output over time')
        ax[0].grid(True)
        ax[0].legend()

        ax[2].plot(time, self.GM)
        ax[2].set_ylabel(ylabel=r"$G^{M}$ in W/m²")
        ax[2].set_xlabel('time in h')
        ax[2].grid(True)

        color = 'tab:red'
        ax[1].plot(time, (self.T_U - 273.15), color=color)
        ax[1].set_ylabel(ylabel=r"Ambient temperature in °C ", color=color)
        ax[1].tick_params(axis='y', labelcolor=color)
        ax[1].set_title('Environmental conditions')
        ax[1].grid(True)

        color = 'tab:blue'
        ax1 = ax[1].twinx()
        ax1.plot(time, self.v_w, color=color)
        ax1.set_ylabel(ylabel=r"Wind speed in m/s", color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        fig.tight_layout()
        plt.show()

        return