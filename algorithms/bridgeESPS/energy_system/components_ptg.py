"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann

from components.ptg_components import VollComponent, PtGComponent, PtGStorage


#### Boiler
class VollBoiler(VollComponent):
    """Boiler parameterized as in Baumgärtner2019 LowcarbonUS.

    design variable: nominal heating power to be installed
    operational variable: heat output during a given time step

    The investment costs 'c_inv' is a nonlinear function of the nominal output 'Qdot_nom':
        c_inv = c_inv,ref * (Qdot_nom/Qdot_ref)^M

    The input 'Qdot_in' can be determined via the efficiency relation:
        Qdot_in =  Qdot_out / eff

    The efficiency 'eff' is determined via the product of the nominal
    efficiency and a polynomial fitting function of the load fraction 'q':
        q = Qdot_out/Qdot_nom
    """

    Qdot_ref = 1  # [kW], reference nominal power
    c_ref = 2701.6  # [€], reference cost
    c_m = 0.015  # [-], maintenance coefficient, (fraction of investment cost)
    M = 0.4502  # [-], cost exponent

    Qdot_min = 100  # [kW], minimal nominal power allowed for the model
    Qdot_max = 500 #14000  # [kW], maximal nominal power allowed for the model
    qdot_min = 0.2  # [-] minimum modeled thermal output part load
    eff_nom = 0.9  # [-], nominal efficiency

    # Voll Thesis
    fit_params = {0: -0.07557,
                  1: 1.39731,
                  2: -7.0013,
                  3: 21.75378,
                  None: 0.03487,
                  -1: 0.67774,
                  -2: -5.34196,
                  -3: 20.66646}
    # fit_params = {0: 1}
    # Fitting curve 2 based on Voll Thesis_edit by YW June 09 2022
    fit_params = {0: 0,
                  1: 2.67984355332151,
                  2: -18.337824329997364,
                  3: 84.34555518446167,
                  None: 0.731658799528522,
                  -1: -0.790002441829007,
                  -2: -10.39149306331708,
                  -3: 79.13741111340339}


    def __init__(self, label, exists=False, optional=False, nom_size=None):
        super().__init__(label, nom_ref=self.Qdot_ref, c_ref=self.c_ref, c_m=self.c_m, M=self.M,
                         nom_min=self.Qdot_min, nom_max=self.Qdot_max, nom_size=nom_size,
                         min_part_load=self.qdot_min, base_eff=self.eff_nom,
                         fit_params=self.fit_params, in_name='Qdot_in',
                         out_name='Qdot_out', exists=exists, optional=optional)

#### CHP engine
class VollCHP(VollComponent):
    """Combined-heat-and-power (CHP) engine parameterized as in Voll2014 -> Baumgärtner2019 LowcarbonUS.

    design variable: nominal heating power to be installed
    operational variable: operational output (heat and elec.) for each timestep

    The investment costs 'c_inv' is a nonlinear function of the nominal thermal
    output 'Qdot_nom':
        c_inv = c_inv,ref * (Qdot_nom/Qdot_ref)^M

    The input 'Qdot_in' can be determined via the efficiency relation:
        Qdot_in =  Qdot_out / eff_th

    The thermal efficiency 'eff_th' is determined via the product of nominal
    thermal efficiency and a polynomial fitting function f(q), with the load
    fraction q = Qdot_out/Qdot_nom, giving the relationship:
        eff_th = f(q) * eff_th_nom

    The electric efficiency 'eff_el' is determined via the product of nominal
    electrical efficiency and f(q), giving the relationship:
        eff_el = f(q) * eff_el_nom

    Thus the output power can be computed as:
        P_out = Qdot_in * eff_el = Qdot_out / eff_th * eff_el
    """

    # investment model parameters
    Qdot_ref = 1  # [kW], reference nominal power
    c_ref = 9332.6  # [€], reference cost
    c_m = 0.1  # [-], maintenance coefficient, (fraction of investment cost)
    M = 0.539  # [-], cost exponent

    Qdot_min = 500  # [kW], minimal nominal power allowed for the model
    Qdot_max = 3200  # [kW], maximal nominal power allowed for the model
    qdot_min = 0.5  # [-] minimum output part load

    eff_nom = 0.87  # [-] nominal efficiency ## added by YW

    # coefficients for polynomial fits of thermal and electrical efficiencies
    # Voll Thesis
    fit_params = {0: 0.0487, 1: 1.1856, 2: -0.2434}
    fit_params = {0: 0, 1: 1.287523019758233, 2: -0.287523019758233}
    # fit_params = {0: 1}

    def __init__(self, label, exists=True, optional=False, nom_size=None):
        # NOTE: Since the CHP module hase two outputs (heat and power) we use
        #       one of them (heat) as the base commodity for which the part
        #       load restrictions are imposed and then add the other one, along
        #       with the related constraints and connector.
        #       Also, since the base efficiency is assumed to depend on the
        #       nominal size, we remove the standard input output relation and
        #       create a custom one.
        super().__init__(label, nom_ref=self.Qdot_ref, c_ref=self.c_ref, c_m=self.c_m, M=self.M,
                         nom_min=self.Qdot_min, nom_max=self.Qdot_max, nom_size=nom_size,
                         base_eff=self.eff_nom, fit_params=self.fit_params,
                         min_part_load=self.qdot_min, in_name='Qdot_in',
                         out_name='Qdot_out', out_connector_name='HEAT_OUT',
                         exists=exists, optional=optional)

        # label = 'CHP'; exists = False; optional = True
        # self = VollComponent(label, Qdot_ref, c_ref, c_m, M=M,
        #                  nom_min=Qdot_min, nom_max=Qdot_max,
        #                  min_part_load=qdot_min, in_name='Qdot_in',
        #                  out_name='Qdot_out', out_connector_name='HEAT_OUT',
        #                  exists=exists, optional=optional)

        # Get previously created variables to impose additional constraints
        #Qdot_nom = self['Qdot_out_nom']
        #out_rel = self['Qdot_out_rel']
        #operating = self['operating']
        # in_rel = self['Qdot_in_rel']
        # in_name = self['Qdot_in']

        # Get previously created variables to impose additional constraints
        if exists:
            if not optional:
                Qdot_nom = nom_size
        else:
            Qdot_nom = self['Qdot_out_nom']  ## have to check YW

        out_rel = self['Qdot_out_rel']
        operating = self['operating']


        # FIXME: This still uses the incorrect fit_params from Voll's thesis!
        # operation-dependent efficiencies
        f = sum(coeff * out_rel ** power for power, coeff
                in VollCHP.fit_params.items())

        # size-dependent base efficiencies for custom input output relation
        eff_th_nom = 0.498 - 3.55e-5 * Qdot_nom  # [-], nominal thermal eff.
        self.add_expression('eff_th_nom', eff_th_nom)
        eff_el_nom = 0.372 + 3.55e-5 * Qdot_nom  # [-], nominal electrical eff.
        self.add_expression('eff_el_nom', eff_el_nom)

        eff_th = f * eff_th_nom  # depends on q and Qdot_nom
        self.add_expression('eff_th', eff_th)
        eff_el = f * eff_el_nom
        self.add_expression('eff_el', eff_el)

        # Updating input_output_relation with new efficiency
        ior = 'input_output_relation'
        con = self._constraints_dict[ior]
        # self.constraints_dict[ior] = type(con)(con.lhs * eff_th, con.rhs) # Bug Fixing YW
        # self._constraints_dict[ior] = type(con)(con.lhs * eff_th, con.rhs) # also works YW

        inp_rel_cal = out_rel / eff_th
        self.add_expression('inp_rel_cal', inp_rel_cal)

        inp_rel_init = inp_rel_cal.subs(out_rel, 1).subs(Qdot_nom, self.Qdot_max)

        inp_rel_max = max(
            inp_rel_cal.subs(out_rel, 1).subs(Qdot_nom, self.Qdot_max),
            inp_rel_cal.subs(out_rel, self.qdot_min).subs(Qdot_nom, self.Qdot_max),
            inp_rel_cal.subs(Qdot_nom, self.Qdot_max).subs(out_rel, 1),
            inp_rel_cal.subs(Qdot_nom, self.Qdot_max).subs(out_rel, self.qdot_min))

        # NOTE: Conservarive estimate (big-M)!
        #       If you add more components ensure this is an upper bound!

        inp_rel = self.make_operational_variable('Qdot_in_rel', bounds=(0, inp_rel_max), init_val=inp_rel_init)

        self.add_eq_constraint(inp_rel * eff_th, out_rel, name='input_output_relation')
        self.add_le_constraint(inp_rel, self['operating'] * inp_rel_max, name='input_active')

        # Adding second output
        power_output = inp_rel * eff_el * Qdot_nom
        self.expressions_dict['power_output'] = power_output
        self.add_output('POWER_OUT', power_output)


class PtGPEM(PtGComponent):
    """
    TODO: investment

    """
    c_lambda = 1295  # [€/kWel], cost exponent for 15a

    # change c_lambda # Lifetime 23a
    n = 15  # assumed lifetime in years
    i = 0.08  # interest rate
    af_15a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)

    c_lambda_annual = c_lambda * af_15a

    n = 10
    af_10a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)
    c_ref = c_lambda_annual / af_10a

    P_ref = 1
    M = 1
    c_m = 0.035  # [-], maintenance coefficient, (fraction of investment cost)

    rated_power_min = 0 #[kW_el]
    rated_power_max = 2000 #[kW_el]

    ### operational part
    min_load = 0.2
    max_load = 1.2
    # from Bullter2017 below
    # eff_LHV_nom = Vdot_H2 * LHV_H2 / P_el
    # eff_HHV_nom = Vdot_H2 * HHV_H2 / P_el
    LHV_H2 = 3.00 # [KWh/Nm^3]
    HHV_H2 = 3.54 # [KWh/Nm^3]

    eff_LHV_nom = 0.53 # [-]
    # from Bullter2017 above
    eff_HHV_nom = eff_LHV_nom / LHV_H2 * HHV_H2

    ## YW edit
    fit_params = {0: 0 * 0.76 /eff_HHV_nom,
                  1: 1.2299336955660698 * 0.76 /eff_HHV_nom,
                  2: -43.28094452962493 * 0.76 /eff_HHV_nom,
                  3: 905.836146970993 * 0.76 /eff_HHV_nom,
                  4: 500.05876426748597 * 0.76 /eff_HHV_nom,
                  None: 0.34016019684846976 * 0.34016019684846976,
                  -1: -2.3299508685948505 ,
                  -2: 10.897542272900568,
                  -3: 686.683654469744,
                  -4: 1041.502274738038}


    def __init__(self, label, exists=False, optional=False, nom_size=None): #YW
        super().__init__(label, nom_ref = self.P_ref, c_ref = self.c_ref, M = self.M,
                         nom_min=self.rated_power_min, nom_max=self.rated_power_max,  c_m = self.c_m,
                         nom_size=nom_size, min_part_load=self.min_load, max_part_load=self.max_load,
                         base_eff=self.eff_HHV_nom, fit_params=self.fit_params,
                         in_name='P_el', out_name='H2_out', exists=exists, optional=optional)


class FasihiDAC(VollComponent):
    """DAC parameterized as in Fasihi2019.

    design:
    CAPEX: 730€/tco2 * a
    matainance: 4% * CAPEX
    life time 20 years

    operational variable: carbon dioxide mass flow 'CO2_out' for each time step
    el.demand = 250 kWh_el/t_CO2 = 0.25 kWh_el/kg_CO2
    th.demand = 1750 kWh_th/t_CO2 = 1.75 kWh_th/kg_CO2

    The Carbon Dioxide Mass Flow 'CO2_out' can be determined via the efficiency relation:
        'CO2_out=power_in/Energy_needed_per_kg_of_CO2'
     """

    n = 20  #[a] assumed lifetime in years
    i = 0.08  # interest rate

    paper_CAPEX = 730 * n # [€/t_CO2]

    af_20a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)

    CAPEX_annual = paper_CAPEX * af_20a

    n = 10
    af_10a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)
    c_ref = CAPEX_annual / af_10a

    CO2_ref = 360000 #[t_CO2/a]
    CO2_ref = 41000 #CO2_ref/(360*24) #[kg_CO2/h] YW assumed

    P_ref = 1
    M = 1
    c_m = 0.04  # [-], maintenance coefficient, (fraction of investment cost)

    rated_CO2_min = 0  # [t_CO2/h]
    rated_CO2_max = CO2_ref  # [t_CO2/h] ~41.67t

    ### operational part
    min_load = 0
    max_load = 1

    el_needed_per_t_of_CO2 = 250  # [kWh/t_CO2] Fasihi 2019
    th_needed_per_t_of_CO2 = 1750  # [kWh/t_CO2] Fasihi 2019

    el_needed_per_kg_of_CO2 = 0.250  # [kWh/kg_CO2] Fasihi 2019
    th_needed_per_kg_of_CO2 = 1.750  # [kWh/kg_CO2] Fasihi 2019

    # all energy support comes from electricity

    el_needed_per_kg_of_CO2 = (el_needed_per_kg_of_CO2 + th_needed_per_kg_of_CO2)  # [kWh/t_CO2]
    eff_nom = 1/el_needed_per_kg_of_CO2 # [t_CO2/kWh]

    fit_params = {0: 1}

    def __init__(self, label, exists=False, optional=False, nom_size=None):  # YW
        super().__init__(label, nom_ref=self.CO2_ref, c_ref=self.c_ref, M=self.M,
                         nom_min=self.rated_CO2_min, nom_max=self.rated_CO2_max, c_m=self.c_m,
                         nom_size=nom_size, min_part_load=self.min_load, max_part_load=self.max_load,
                         base_eff=self.eff_nom, fit_params=self.fit_params,
                         in_name='P_el', out_name='CO2_out', exists=exists, optional=optional)


class GorreMethanation(VollComponent):

    n = 20  # assumed lifetime in years
    i = 0.08  # interest rate
    af_20a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)

    c_lambda = 450 # [€/kW_SNG] Gorre 2020 for 20a
    CAPEX_annual = c_lambda * af_20a

    n = 10
    af_10a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)
    c_ref = CAPEX_annual / af_10a

    CH4_ref = 1 #[kW_SNG]
    M = 1
    c_m = 0.033  # [-], maintenance coefficient, (fraction of investment cost)

    rated_CH4_min = 100  # [kW_SNG]
    rated_CH4_max = 500#7500  # [kW_SNG]

    ### operational part
    min_load = 0
    max_load = 1

    eff_HHV_nom = 0.77# [-] ideal reaction

    ## based on PA_BJM
    fit_params = {0: 1}

    ## chemical reaction
    MW_H2 = 2.01588  # [kg/kmol]
    MW_CH4 = 16.043  # [kg/kmol]
    MW_CO2 = 44.01  # [kg/kmol]

    hhv_CH4_kWh_per_kg = 15.4  # [kWh/kg]
    hhv_CH4_MJ_per_kg = 55.5  # # [MJ/kg]
    hhv_CH4_kJ_per_mol = 890.36  # [kJ/mol] higher heating value of methan

    hhv_H2_kWh_per_kg = 39.4  # [kWh/kg]
    hhv_H2_MJ_per_kg = 141.7  # # [MJ/kg]
    hhv_H2_kJ_per_mol = 285.83  # [kJ/mol] higher heating value of hydrogen

    # 1MJ = 0,2777778 kWh
    hhv_H2_kWh_per_kmol = hhv_H2_kWh_per_kg * MW_H2  # [kWh/kmol] * 0.277778 / 1000
    hhv_CH4_kWh_per_kmol = hhv_CH4_kWh_per_kg * MW_CH4  # [kWh/kmol] 0.277778 / 1000

    def __init__(self, label, exists=False, optional=False, nom_size=None):  # YW for operation
        super().__init__(label, nom_ref=self.CH4_ref, c_ref=self.c_ref, M=self.M,
                         nom_min=self.rated_CH4_min, nom_max=self.rated_CH4_max, c_m=self.c_m,
                         nom_size=nom_size, min_part_load=self.min_load, max_part_load=self.max_load,
                         base_eff=self.eff_HHV_nom, fit_params=self.fit_params,
                         in_name='H2_in', out_name='CH4_out',
                         in_connector_name = 'H2_IN',
                         exists=exists, optional=optional)


        CO2_input = self.get_expression('input') / (self.hhv_H2_kWh_per_kmol * 4) * self.MW_CO2 #[kg/h]

        self.add_expression('CO2_input', CO2_input)  #[kg/h]
        self.add_input('CO2_IN', CO2_input)  #[kg/h]


class Battery(PtGStorage):
    """Battery unit (BAT) parametrized as in Baumgärtner Lowcarbon2019.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 2116.1  # [€], reference cost
    M = 0.8382  # [-], cost exponent
    c_m = 0.025  # [-], maintenance coefficient, (fraction of investment cost)
    # bounds of component size
    min_cap = 40  # [kW], minimal capacity allowed for the model
    max_cap = 6000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.920  # [-], charging efficiency
    discharge_eff = 0.920  # [-], discharging efficiency
    c_loss = 0.000042  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.36  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.36  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)

#### HotWaterStorage
class HotWaterStorage(PtGStorage):
    """Heating storage unit (STH) parametrized as in Sass2020, Beccali2013.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 83.8  # [€], reference cost
    M = 0.8663  # [-], cost exponent
    c_m = 0.02  # [-], maintenance coefficient, (fraction of investment cost)

    # bounds of component size
    min_cap = 0  # [kW], minimal capacity allowed for the model (Standard 200)
    max_cap = 280000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.95  # [-], charging efficiency
    discharge_eff = 0.95  # [-], discharging efficiency
    c_loss = 0.005  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 1  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 1  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)


class GasStorage(PtGStorage):
    """Gas storage unit (STH) parametrized as in Beccali2013.

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters from beccali2013 and Gabrielli 2018
    nom_ref = 1  # [kg], reference nominal capacity
    c_ref = 471.67  # [€/kg], reference cost
    M =  0.0896  # [-]


    hhv_H2_kWh_per_kg = 39.4  # [kWh/kg]
    c_ref = c_ref * (1/hhv_H2_kWh_per_kg)**M  # [€/kWh], reference cost
    c_m = 0.03  # [-], maintenance coefficient, (fraction of investment cost)

    # bounds of component size
    min_cap = 0  # [kW], minimal capacity allowed for the model (Standard 200)
    max_cap = 150000  # [kW], maximum capacity allowed for the model
    #nom_cap = 140000  # [kW], maximum capacity allowed for the model

   # efficiencies
    charge_eff = 1  # [-], charging efficiency
    discharge_eff = 1  # [-], discharging efficiency
    c_loss = 0  # [1/h], self-discharge loss
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.25  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.25  # [1/h], maximum discharging rate relative to nominal power

    def __init__(self, label, exists=False, nom_cap=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)


class GabrielliHotWaterStorage(PtGStorage):
    """Heating storage unit (STH) parametrized as in Gabrielli 2018

    design variable: nominal capacity to be installed
    operational variable: charge/discharge rate in operation
    The investment costs 'c_inv' is a nonlinear function of the capacity
    'C_nom':
    c_inv = c_inv,ref * C_nom^M
    The storage volume is modeled using state variables, for which time
    coupling constraints are created post-parametrization.
    """

    # cost parameters
    nom_ref = 1  # [kWh], reference nominal capacity
    c_ref = 83.8  # [€], reference cost
    M = 0.8663  # [-], cost exponent
    c_m = 0.02  # [-], maintenance coefficient, (fraction of investment cost)

    # bounds of component size
    min_cap = 0  # [kW], minimal capacity allowed for the model (Standard 200)
    max_cap = 280000  # [kW], maximum capacity allowed for the model
    # efficiencies
    charge_eff = 0.95  # [-], charging efficiency
    discharge_eff = 0.95  # [-], discharging efficiency
    c_loss = 0.005  # [1/h], self-discharge loss
    c_loss_T = 0.001 #[-] only for Heat storage
    # operational parameters
    in_min = 0  # [1/h], minimum charging rate relative to nominal power
    in_max = 0.25  # [1/h], maximum charging rate relative to nominal power
    out_min = 0  # [1/h], minimum discharging rate relative to nominal power
    out_max = 0.25  # [1/h], maximum discharging rate relative to nominal power

    theta_min = 65 #[°C]
    theta_max = 90 #[°C]

    def __init__(self, label, exists=True, nom_cap=None, T_amb=None):
        super().__init__(label, nom_ref=self.nom_ref, c_ref=self.c_ref, M=self.M, c_m=self.c_m,
                         min_cap=self.min_cap, max_cap=self.max_cap, nom_cap=nom_cap,
                         charge_eff=self.charge_eff, discharge_eff=self.discharge_eff, c_loss=self.c_loss,
                         c_loss_T=self.c_loss_T, theta_min=self.theta_min, theta_max=self.theta_max, T_amb=T_amb,
                         in_min=self.in_min, in_max=self.in_max, out_min=self.out_min, out_max=self.out_max,
                         in_name='input', out_name='output',
                         in_connector_name='IN', out_connector_name='OUT', exists=exists)



