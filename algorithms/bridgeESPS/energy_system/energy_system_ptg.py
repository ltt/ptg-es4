"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann


def define_energy_system():

    ###
    ### import libraires
    ###
    from components.generic_components import Demand, Grid
    from algorithms.bridgeESPS.energy_system.components_renewable import PVModule
    from algorithms.bridgeESPS.energy_system.components_ptg import PtGPEM, FasihiDAC, VollCHP, VollBoiler, \
            Battery, GabrielliHotWaterStorage, GasStorage
    from algorithms.bridgeESPS.energy_system.components_calibrated import TREMP

    from comando import System


    ###
    ### define energy system
    ###
    ES = System('ES')
    T_amb = ES.make_parameter('T_amb') # [°C]

    ###
    ### define energy system components
    eGrid = Grid('eGrid', feedin_limit=0)
    gGrid = Grid('gGrid', consumption_limit=0, feedin_limit=None)

    PV = PVModule('PV', exists=True, optional=False, P_N=200) # [kg_CO2/h]

    heatD = Demand('heatD')
    electricityD = Demand('electricityD')

    PEM = PtGPEM('PEM', exists=True, optional=False, nom_size=500)
    DAC = FasihiDAC('DAC', exists=True, optional=False, nom_size=400)
    MP = TREMP('MP', exists=False, optional=True)

    boiler1 = VollBoiler('boiler1', exists=True, optional=False, nom_size=250)
    CHP1 = VollCHP('CHP1', exists=True, optional=False, nom_size=200)

    BAT = Battery('BAT', exists=True, nom_cap=4000)
    heatStorage = GabrielliHotWaterStorage('heatStorage', exists=True, nom_cap=2000, T_amb=T_amb)

    H2Storage = GasStorage('H2Storage', exists=True, nom_cap=4000)  # [kWh] and [kW]
    CH4Storage = GasStorage('CH4Storage', exists=True, nom_cap=3000)  # [kWh] and [kW]
    CO2Storage = GasStorage('CO2Storage', exists=True, nom_cap=400)  # [kg] and [kg/h]


    ###
    ### connect energy system components
    components = [PV, eGrid, gGrid,
                  electricityD, heatD,
                  PEM, DAC, MP, boiler1, CHP1,
                  BAT, heatStorage, H2Storage, CO2Storage, CH4Storage
                  ]

    connections = {
        "Power_Bus": [
            PV.OUT,
            eGrid.CONSUMPTION,
            # eGrid.FEEDIN,
            electricityD.IN,
            DAC.IN,
            PEM.IN,
            CHP1.POWER_OUT,
            BAT.IN,
            BAT.OUT,
        ],
        "CO2_Bus": [
            DAC.OUT,
            MP.CO2_IN,
            CO2Storage.IN,
            CO2Storage.OUT
        ],
        "CH4_Bus": [
            # gGrid.CONSUMPTION,
            gGrid.FEEDIN,
            CHP1.IN,
            MP.CH4_OUT,
            boiler1.IN,
            CH4Storage.IN,
            CH4Storage.OUT,
        ],
        "H2_Bus": [
            PEM.OUT,
            MP.H2_IN,
            H2Storage.IN,
            H2Storage.OUT
        ],
        "Heat_Bus": [
            heatD.IN,
            CHP1.HEAT_OUT,
            boiler1.OUT,
            heatStorage.IN,
            heatStorage.OUT,
        ]
    }

    for c in components:
        ES.add(c)

    for bus_id, connectors in connections.items():
        ES.connect(bus_id, connectors)
        bus = ES.connections[bus_id]

    ###
    ### add cyclic constraints for storage components
    from comando import cyclic

    storages = [
        BAT,
        heatStorage,
        H2Storage,
        CH4Storage,
        CO2Storage
    ]

    for comp in storages:
        comp._states_dict[comp['soc']] = \
            cyclic, *comp._states_dict[comp['soc']][1:]

    for i in ['variable_costs']:
        ES.add_expression(i, ES.aggregate_component_expressions(i))

    return ES

def load_input_timeseries(options=None):

    #import pandas as pd
    from pandas import read_csv

    data = read_csv(f"{options['input_folder']}\\{options['input_file']}", sep=';', decimal=',')
    data.set_index(['s', 't'], inplace=True)
    scenarios = data['period_weight'].groupby('s').first().rename('pi')
    timesteps = data['dt']

    return data, scenarios, timesteps

if __name__ == '__main__':


    from pathlib import Path

    ES = define_energy_system()

    options = {
        'input_folder': Path("../input"),
        'input_file': 'TimeSeries_paper3.csv',
    }

    data, scenarios, timesteps = load_input_timeseries(options=options)

    o_obj = ES.get_expression('variable_costs')

    P = ES.create_problem(0, o_obj, timesteps, scenarios, name='case_study_02')
    P.data = data


    from comando.linearization import linearize

    P_lin = linearize(P, 3, 'convex_combination')

    from comando.interfaces.pyomo import to_pyomo

    m = to_pyomo(P_lin)

    options = {'mipgap': 0.1/10000, 'timelimit': 60, 'threads': 8}

    res = m.solve('gurobi', options=options, tee=True, keepfiles=True, logfile='pyomo.log')
