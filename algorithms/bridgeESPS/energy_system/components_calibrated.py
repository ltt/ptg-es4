"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann,  Yifan Wang

from components.ptg_components import VollComponent

class TREMP(VollComponent):

    """
    Methanation parameterized
        - Operational as in BetscherRohdeSpoek2020
        - O&M as in Gorre2020
        - investment as in Gorre2020

    constant: rated_power of the CH4-reactor (has to be turned into a design variable)
    operational variable: operational output (heat and HHV of the methan flow) for each timestep
    The investment costs 'c_inv' still have to be calculated and are set equal to 0 for this model
    The methan molar flow 'methan_molar_flow_out' can be determined via the the total molar input flow and a factor
    determined by the equilibrium constant for the given operating point:
        'methan_molar_flow_out = input_molar_flow * 0.171045793'
    Any other molar flow and the heat output is also determined by a conversion factor
    93,3% of the entire CH4 Output
    """

    # investment model parameters
    # rated_power_ref = 1  # [kW], reference nominal power measured by the higher heating value of methane in the output flow
    # c_ref = 1  # [€], reference cost
    # c_m = 1  # [-], maintenance coefficient, (fraction of investment cost)
    # M = 0  # [-], cost exponent

    n = 20  # assumed lifetime in years
    i = 0.08  # interest rate
    af_20a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)

    c_lambda = 450 # [€/kW_SNG] Gorre 2020 for 20a
    CAPEX_annual = c_lambda * af_20a

    n = 10
    af_10a = ((1 + i) ** n * i) / ((1 + i) ** n - 1)
    c_ref = CAPEX_annual / af_10a

    CH4_ref = 1 #[kW_SNG]
    M = 1
    c_m = 0.033

    #YW c_phi = 0.3  # [-] Gorre 2020
    rated_CH4_min = 500   # [kW], minimum nominal power measured by the higher heating value of methane in the output flow
    rated_CH4_max = 500  # [kW], maximum nominal power measured by the higher heating value of methane in the output flow

    ### operational part
    min_load = 0.1  # [-] YW for TREMP minimum output part load
    max_load = 1

    # LB changed
    eff_HHV_nom = 0.77

    # coefficients for polynomial fits of thermal and electrical efficiencies
    fit_params = {0: 1}

    ## chemical reaction
    MW_H2 = 2.01588 # [kg/kmol]
    MW_CH4 = 16.043 # [kg/kmol]
    MW_CO2 = 44.01  # [kg/kmol]

    hhv_CH4_kWh_per_kg = 15.4 # [kWh/kg]
    hhv_CH4_MJ_per_kg = 55.5 # # [MJ/kg]
    hhv_CH4_kJ_per_mol = 890.36  # [kJ/mol] higher heating value of methan

    hhv_H2_kWh_per_kg = 39.4  # [kWh/kg]
    hhv_H2_MJ_per_kg = 141.7  # # [MJ/kg]
    hhv_H2_kJ_per_mol = 285.83  # [kJ/mol] higher heating value of hydrogen

    # 1MJ = 0,2777778 kWh
    hhv_H2_kWh_per_kmol = hhv_H2_kWh_per_kg * MW_H2 # [kWh/kmol] * 0.277778 / 1000
    hhv_CH4_kWh_per_kmol = hhv_CH4_kWh_per_kg * MW_CH4 # [kWh/kmol] 0.277778 / 1000

    #molar_conversion = 0.2138   # [-] methane output flow in mol/s per hydrogen input flow in mol /s
    H2_conversion = 0.0682 * 1000 # [-] higher heating value of hydrogen in the output flow per higher heating value of hydrogen in the input flow
    heat_conversion = 0.1607 * 1000 # [-] # Note YW, could be wrong, heat output per higher heating value of hydrogen in the input flow

    def __init__(self, label, exists=False, optional=True, nom_size=None):
        super().__init__(label, nom_ref=self.CH4_ref, c_ref=self.c_ref, M=self.M,
                         nom_min=self.rated_CH4_min, nom_max=self.rated_CH4_max, c_m=self.c_m,
                         nom_size=nom_size, min_part_load=self.min_load, max_part_load=self.max_load,
                         base_eff=self.eff_HHV_nom, fit_params=self.fit_params,
                         in_name='H2_input', out_name='CH4_output',
                         in_connector_name='H2_IN', out_connector_name='CH4_OUT',
                         exists=exists, optional=optional)

        # heat_output = self.get_expression('input') / self.hhv_H2_kWh_per_kmol * self.heat_conversion # [kW]
        # self.add_expression('heat_output', heat_output)
        # #self.expressions_dict['heat_output'] = heat_output
        # self.add_output('HEAT_OUT', heat_output)

        # LB added
        out_name = 'CH4_output'

        cooling_demand_fit = [ 3.73495446e-01, -8.66236113e-07]
        heat_demand_fit = [3.48574287e-02, 1.03037085e-07]
        elec_demand_fit = [ 3.44191587e-02, -2.58367406e-08]

        cooling_demand = self[out_name + '_nom'] * (cooling_demand_fit[1] + cooling_demand_fit[0]*self[out_name +'_rel'])
        heat_demand = self[out_name + '_nom'] * (heat_demand_fit[1] + heat_demand_fit[0]*self[out_name +'_rel'])
        elec_demand = self[out_name + '_nom'] * (elec_demand_fit[1] + elec_demand_fit[0]*self[out_name +'_rel'])

        heat_output = cooling_demand - heat_demand # [kW]
        self.add_expression('heat_output', heat_output)
        self.add_output('HEAT_OUT', heat_output)

        hhv_H2_kJ_per_mol = 285.83  # [kJ/mol] higher heating value of hydrogen

        # LB added
        H2_input_mol = self.get_expression('input') / hhv_H2_kJ_per_mol
        CO2_input_mol = H2_input_mol / 4
        CO2_input_g = CO2_input_mol * self.MW_CO2
        CO2_input_kg = CO2_input_g * 1e-3
        CO2_input_kg_h = CO2_input_kg * 3600

        self.add_expression('CO2_input', CO2_input_kg_h)
        self.add_input('CO2_IN', CO2_input_kg_h)