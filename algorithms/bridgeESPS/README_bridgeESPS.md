<!-- This file is part of the PtG-ES4 project which is released under the MIT 
license. See file LICENSE.txt for full license details.

AUTHOR: Yifan Wang -->
# PtG-ES4 Algorithms

## Description
A method to bridge energy and process system optimization: 
Identifying the feasible operating space for a methantation process in power-to-gas energy systems.

![](bridgeESPS.PNG)


## Structure 
```
ptg-es4/algorithms:
|-- bridgeESPS
    |-- energy systems
        |-- components models
        |-- system models
    |-- process systems
        |-- methantation
            |-- property models
            |-- reaction models
            |-- flowsheet

            |-- initialization
            |-- results_step2
            |-- results_partB
    
    |-- method
    |-- case_study_partA.py
    |-- case_study_partB.py

    |-- input
    |-- output
    |-- pictures_partB
    |-- env_bridgeESPS_py38.yml

    |-- postProcessing_partB.py
```

## Referencing
```bibtex
@article{WANG2024108582,
title = {A method to bridge energy and process system optimization: Identifying the feasible operating space for a methanation process in power-to-gas energy systems},
journal = {Computers & Chemical Engineering},
volume = {182},
pages = {108582},
year = {2024},
issn = {0098-1354},
doi = {https://doi.org/10.1016/j.compchemeng.2023.108582},
url = {https://www.sciencedirect.com/science/article/pii/S0098135423004520},
author = {Yifan Wang and Luka Bornemann and Christiane Reinert and Niklas {von der Assen}},
}
```

## Getting started

### Intall packages
For a list of all available features refer to the file env_bridgeESPS_py38.yml.
In order to install all available extensions (recommended) run:
```bash
mkdir venv_folder

conda env create --prefix=venv_folder --file=correct_path\env_bridgeESPS_py38.yml

conda activate venv_folder
```
### Run case study

```bash
# run part A
python case_study_partA.py

# plot results of partA
cd process_system/methantaion
python postProcessing_partA.py

# run part B
python case_study_partB.py

# plot results of partB
python postProcessing_partB.py
```

Please note that the original energy demand data used in 'partB' in our publication, 
is not accessible as open source and, thus, has been substituted with assumed data 
in this repository. 
