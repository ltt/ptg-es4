"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann

###
### import libraires
###
import numpy as np
from pathlib import Path
from timeit import default_timer as timer


###
### import process system
###
from algorithms.bridgeESPS.process_system.methanation.process_methanation import initialize_fs, prepare_optimization

options_PS = {
    'init_folder': Path('./process_system/methanation/results_step2'),  # initialization
}

mCH4 = initialize_fs(options=options_PS)
mCH4 = prepare_optimization(mCH4)



###
### import energy system
###

from algorithms.bridgeESPS.energy_system.energy_system_ptg import *

ES = define_energy_system()

###
### load input data
###
options_ES = {
    'input_folder': Path("./input"),
    'input_file': 'TimeSeries_paper3_edited.csv', #'TimeSeries_paper3.csv',
}

data, scenarios, timesteps = load_input_timeseries(options = options_ES)

###
### call the method
###
from algorithms.bridgeESPS.method.method_partB import bridgingESPS_partB

###
### General Parameters
###

### used in the main script
save_time = {}
save_res = {}
constr_satisfied = False
max_iteration = 10

### used in the method
options_partB = {
    'which_solver': 'gurobi',
    'options_solver': {'mipgap': 0.1 / 100,
                      'timelimit': 3600 * 2,
                      'threads': 0,
                      'nonConvex': 1 },
    'iter': 1,
    'method_gap': 0.005,
    'method_scale_beta': 1000,
    'method_scale_zeta': 0,
    'ramping_con': False,
    #'save_res': {},
    'use_esti': True, # use C^{Prozess,est} 'give_costs'
    'start_pen_iter': 3, # # In which iteration start to use Bestrafungskosten C^{Pen}

    'input_data': { 'data': data,
                    'timesteps': timesteps,
                    'scenarios': scenarios},

    'SNG_init': np.zeros(timesteps.size),
    'SNG_prev': None,
    'SNG_costs': None,
    'output_folder': Path("./output"),
}

options_partB['output_folder'].mkdir(parents=True, exist_ok=True)

comps_atES = []
PS = []
for i in ES.components:
    if i.label == 'MP':
        comps_atES.append(i)

        PS.append({
            'label_ES': 'MP',
            'label_PS': 'methanation',
            'main_folder': Path('./process_system/methanation'),
            'init_folder': Path('./process_system/methanation/initialization'),
            'init_file': 'init_fs_500_95_TREMP_paper.json.gz', #'init_fs_500_95_TREMP_new_energy2.json.gz',
            'step2_folder':Path('./process_system/methanation/results_step2'),
            'output_folder': Path('./process_system/methanation/results_partB'),
            'flowsheet_model': mCH4,

            'which_solver': 'ipopt',
            'options_solver': {'tol': 0.1/100, #YW 0.01,
                               'max_iter': 1500,
                               'halt_on_ampl_error': 'no'
                                # 'gamma_theta': 1e-2
                                # 'acceptable_tol': 0.08,
                                # 'acceptable_constr_viol_tol': 0.01
                               },
        })

###
### 1. iteration
###
start_time = timer()
res = bridgingESPS_partB(ES, comps_atES, PS, options = options_partB)

res['time'] = timer() - start_time

SNG_prev = options_partB['SNG_init']
GAP = res['GAP']
# SNG_costs = res['SNG_costs']
prev_res = res['results_CH4']
SNG_costs = res['SNG_costs_arr']

save_time[options_partB['iter']] = timer() - start_time
save_res[options_partB['iter']] = res


###
### >=2. iteration
###

options_partB['iter'] = options_partB['iter'] + 1

#while options_partB['iter'] <=max_iteration and not(constr_satisfied and options_partB['iter'] > options_partB['start_pen_iter']):
while options_partB['iter'] <= max_iteration:
    print("Operational optimization did not converge!")
    print("GAP is: ", GAP)
    if options_partB['use_esti'] == True:
        print(f"Update SNG costs from {options_partB['SNG_prev']} to {SNG_costs}")
    else:
        print(f"Update SNG costs from {0} to {0}")
    print('NEXT ITERATION: ', options_partB['iter'])

    ###
    ### update options for the method
    ###
    options_partB['prev_res'] = save_res
    options_partB['ramping_con'] = True
    options_partB['SNG_prev'] = SNG_costs
    options_partB['SNG_costs'] = SNG_costs

    print(options_partB)

    res = bridgingESPS_partB(ES, comps_atES, PS, options = options_partB)

    res['time'] = timer() - save_res[options_partB['iter']-1]['time']
    save_res[options_partB['iter']] = res

    save_time[options_partB['iter']] = timer() - save_time[options_partB['iter']-1]
    GAP = res['GAP']
    # SNG_costs = res['SNG_costs']
    prev_res = res['results_CH4']
    SNG_costs = res['SNG_costs_arr']

    options_partB['iter'] = options_partB['iter'] + 1
    ### Check if V^{Prozess,ES} == V^{Prozess}
    if not 'false' in prev_res['cond_sat'].tolist():
        constr_satisfied = True

import pickle
import time
### Save as .pkl
timestr = time.strftime("%m%d-%H%M")
with open(f"{options_partB['output_folder']}\\results_{timesteps.size}_{timestr}.pkl", 'wb') as f:
    pickle.dump(save_res, f)

data_ges = []


###################################################################################
#                                                                                 #
#                            post-processing of part B                            #
#                                                                                 #
###################################################################################

###
### This script can also be performed separately.

from algorithms.bridgeESPS.postProcessing_partB import plot_all

options = {
    'timestr': timestr,
    'hours': 24,
    'input_folder': options_partB['output_folder'],
    'output_folder': Path('./pictures_partB'),
    'show_fig': True
}
options['output_folder'].mkdir(parents=True, exist_ok=True)

plot_all(options)
