"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann

if __name__ == '__main__':

    from pathlib import Path
    from algorithms.bridgeESPS.process_system.methanation.process_methanation import *

    ###
    ### step 1 process system modeling
    ###

    ### set up flowsheet optimization
    options = {
        'init_folder': Path('./process_system/methanation/initialization')
    }

    ### initialize and prepare flowsheet optimization
    m = initialize_fs(options=options)

    m = prepare_optimization(m)

    ###
    ### step 2 model calibration
    ###

    ###
    ### prepare operational performance map
    from algorithms.bridgeESPS.process_system.methanation.utility_functions_partA import calc_part_load, \
         calc_multi_wg

    ###
    ###

    options = {
        'title': 'paper',
        'interval': [500, 20, 20],
        'init_folder': Path('./process_system/methanation/initialization'),
        'output_folder': Path('./process_system/methanation/results_step2')
    }

    calc_part_load(m, options=options)

    options = {
        'title': 'paper',
        'interval': [500, 20, 20],#[30,20,10],
        'var': 'P',
        'init_folder': Path('./process_system/methanation/initialization'),
        'output_folder': Path('./process_system/methanation/results_step2')
    }
    options['output_folder'].mkdir(parents=True, exist_ok=True)

    calc_multi_wg(m, options =options)

    ###
    ### step 3 energy system modeling
    ###

    print('Step 3: This part is built directly into Part B.')

    ###################################################################################
    #                                                                                 #
    #                            post-processing of part A                            #
    #                                                                                 #
    ###################################################################################

    ###
    ### This script can also be performed separately.

    from algorithms.bridgeESPS.process_system.methanation.postProcessing_partA import *

    options = {
        'filename': 'part_load_purity_95_TREMP_500_20_paper.xlsx',
        'input_folder': Path('./process_system/methanation/results_step2'),
        'output_folder': Path('./process_system/methanation/pictures/step2')
    }
    options['output_folder'].mkdir(parents=True, exist_ok=True)

    plot_part_load(options=options, titel='paper', relative=False)

