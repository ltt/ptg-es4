"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang


# property data
mw_H2 = 2.016E-3  # kg/mol
HHV_H2_m = 141.800e3  # kJ/kg
LHV_H2_m = 119.972e3  # kJ/kg
HHV_H2_v = 12.745e3  # kJ/m³
LHV_H2_v = 10.783e3  # kJ/m³
HHV_H2_mol = 141.800e3 * mw_H2  # kJ/mol
LHV_H2_mol = 119.972e3 * mw_H2  # kJ/mol #241

mw_CH4 = 16.043E-3  # kg/mol
HHV_CH4_m = 55.498e3  # kJ/kg
LHV_CH4_m = 50.013e3  # kJ/kg
HHV_CH4_v = 39.819e3  # kJ/m³
LHV_CH4_v = 35.883e3  # kJ/m³
HHV_CH4_mol = 55.498e3 * mw_CH4  # kJ/mol
LHV_CH4_mol = 50.013e3 * mw_CH4  # kJ/mol #802



def bridgingESPS_partB(ES, comps_atES, PS, options = None):

    import logging
    import math

    if len(comps_atES) > 1:
        logger = logging.getLogger('ES')
        logger.error('more than one component defined')

    from timeit import default_timer as timer
    import numpy as np

    ###
    ### const parameter
    ###
    k_el_n = 0.2
    k_cool_n = 5 / 1000
    k_heat_n = 2 * k_cool_n
    k_cool = k_cool_n
    k_heat = k_heat_n


    t_start = timer()
    grad = []
    pen_index = []
    tot_index = np.arange(0, len(options['SNG_init']))

    if options['iter'] >= options['start_pen_iter']:
        iter_idx = np.arange(2, len(options['prev_res']) + 1)
        for idx in iter_idx:
            dummy = [i for i, e in enumerate(options['prev_res'][idx]['results_CH4']['cond_sat']) if e == 'false']
            if bool(dummy):
                pen_index.extend(dummy)
    try:
        pen_index = list(set(pen_index))
    except:
        pen_index = []

    # Create "Problem 5" Problem as dummy --> will be overwritten afterwards
    m_costs_ES_h = -1 * comps_atES[0].connectors['CH4_OUT'].expr * 1
    o_obj = m_costs_ES_h + ES.get_expression('variable_costs')
    P = ES.create_problem(0, o_obj, options['input_data']['timesteps'], data=options['input_data']['data'], name='ptg_es')

    #
    CH4_out = P['MP_CH4_output_rel'] * 500

    from algorithms.bridgeESPS.process_system.methanation.utility_functions_partA import calc_gradient_sheet

    ### Eqs. 14(a) and 14(b) etimation term
    if options['use_esti'] == True:
        if options['iter'] == 1:
            CH4_costs = 500 * 1 / (options['input_data']['timesteps'].size) * sum(
                P['MP_CH4_output_rel'][1][idx + 1] * options['SNG_init'][idx] for idx in tot_index)
        else:
            filename = f"{PS[0]['step2_folder']}\\part_load_purity_95_TREMP_500_20_paper.xlsx"
            grad_CD, grad_HD, grad_ED = calc_gradient_sheet(filename, options['prev_res'][options['iter'] - 1]['results_CH4']['energy_feed_out'],
                                                            k_el_n, k_heat_n, k_cool_n)  ## Eq. (3.29)
            # CH4_costs = 1/hours*500*(P['CH4Reactor_CH4_output_rel'][1][1]*SNG_init[0])
            CH4_costs = 0
            for idx in tot_index:
                E_op = 500 * P['MP_CH4_output_rel'][1][idx + 1]
                k_ges = options['SNG_init'][idx]
                # k_prev = SNG_init[idx-1]
                E_it = options['prev_res'][options['iter'] - 1]['results_CH4']['energy_feed_out'][idx]
                # E_it_prev = prev_res[iter-1]['results_CH4']['energy_feed_out'][idx-1]
                # dE = E_it - E_it_prev
                dk = grad_CD[idx] * k_cool / k_cool_n + grad_ED[idx] * options['input_data']['data']['eGrid_price'][
                    1, idx + 1] / k_el_n
                # dk = grad_CD[idx]*k_cool/k_cool_n + grad_HD[idx]*k_heat/k_heat_n + grad_ED[idx]*k_el[1,idx+1]/k_el_n
                # dk = grad_CD[idx] * k_cool + grad_HD[idx] * k_heat + grad_ED[idx] * k_el[1, idx + 1]
                # dk = (grad_CD[idx] * k_cool + grad_HD[idx] * k_heat + grad_ED[idx] * k_el[1, idx + 1]) * 1/E_it
                CH4_costs = CH4_costs + 1 / options['input_data']['timesteps'].size * E_op * (k_ges + (E_op - E_it) * dk)
                grad.append(dk)
                # if abs(dE) <= 0.001:
                #     CH4_costs = CH4_costs + 1 / hours * (
                #             E_op * (k))
                #     grad.append(0)
                #
                # else:
                #     CH4_costs = CH4_costs + 1/hours*(
                #         E_op*(k + (E_op - E_it)*dk/dE)
                #     )
                #     grad.append(dk/dE)

    else:
        SNG_init = np.zeros(len(options['input_data']['data']['timesteps']))
        CH4_costs = 500 * 1 / len(options['input_data']['data']['timesteps']) * sum(
            P['MP_CH4_output_rel'][1][idx + 1] * SNG_init[idx] for idx in tot_index)

    ### Eq. (5)
    ES_costs = ES.get_expression('variable_costs')

    # Skalierungsparameter
    quad_pen_inf = 0
    quad_pen_prev = 0
    #scale_beta = 1000
    #scale_zeta = 0  # YW0

    import copy
    ub_copy = copy.deepcopy(P['MP_CH4_output_rel'].ub)

    # quad_pen = 1/hours*(P['CH4Reactor_CH4_output_rel'][1][1]*P['CH4Reactor_CH4_output_nom'] - 50)**2
    # P['CH4Reactor_CH4_output_rel'].ub = copy.deepcopy(ub_copy)

    # Overwrite cost function of P & linearize
    P.operational_objective = ES_costs  # + CH4_costs + quad_pen

    from comando.linearization import linearize
    P_lin = linearize(P, 3, 'convex_combination')

    ### Step 7 Eq. (15) Penalty term
    if options['iter'] >= options['start_pen_iter'] and bool(pen_index):
        for idx in pen_index:
            quad_pen_inf = quad_pen_inf + (options['method_scale_beta'] * 1 / options['input_data']['timesteps'].size) * (P['MP_CH4_output_rel'][1][idx + 1] * 500
                                                                      - options['prev_res'][options['iter'] - 1]['results_CH4'][
                                                                          'energy_feed_out'][idx]) ** 2
            if not (idx - 1) in pen_index:
                try:
                    quad_pen_prev = quad_pen_prev + (options['method_scale_zeta'] * 1 / options['input_data']['timesteps'].size) * (
                                P['MP_CH4_output_rel'][1][idx + 0] * 500
                                - options['prev_res'][options['iter'] - 1]['results_CH4']['energy_feed_out'][idx - 1]) ** 2
                except:
                    pass
            # P['CH4Reactor_CH4_output_rel'][1][idx + 1].ub = prev_res['energy_feed_out'][idx]/500
            # ub_copy._set_values(idx, prev_res['energy_feed_out'][idx]/500)

    ### Add additional cost terms to optimization cost function --> MILP ---> MIQP
    P_lin.operational_objective = P_lin.operational_objective + quad_pen_inf + quad_pen_prev + CH4_costs
    print("===================================================")
    print("PENALTY INDICES")
    print(pen_index)
    print("ENERGY SYSTEM OBJECTIVE")
    print(P_lin.objective)
    print("===================================================")
    # P_lin['CH4Reactor_CH4_output_rel'].ub = ub_copy

    ## Create pyomo problem
    from comando.interfaces.pyomo import to_pyomo
    mES = to_pyomo(P_lin)
    # mES = to_pyomo(P)
    # sol_ES = pyo.SolverFactory('gurobi')
    """
    options_gurobi = {'mipgap': 0.1 / 10000,
                      'timelimit': 3600 * 2,
                      'threads': 0,
                      'nonConvex': 1
                      }
    options_baron = {'epsr': 0.05,
                     'maxtime': 1000,
                     'threads': 16,
                     }
    """
    # results_ES = mES.solve('baron', options=options_baron, tee=True, logfile=f'{output_file}/baron.log', keepfiles=False)

    ### Solve pyomo problem
    results_ES = mES.solve(options['which_solver'], options=options['options_solver'], tee=True, logfile=f'{options["output_folder"]}/gurobi.log',
                           keepfiles=False)


    gap_MILP = (results_ES['Problem']._list[0]['Upper bound'] - results_ES['Problem']._list[0]['Lower bound']) / \
               results_ES['Problem']._list[0]['Upper bound']
    t_end_ES = timer()


    ### Get results
    from algorithms.bridgeESPS.method.utility_functions_partB import save_results_in_csv ##YW not finished yet
    v_costs_ES = sum(ES.get_expression('variable_costs').value)

    try:
        # m_costs_ES = sum(m_costs_ES_h.value)
        m_costs_ES = (CH4_costs * options['input_data']['timesteps'].size).value
    except TypeError:
        m_costs_ES = 0

    #v_costs_P_lin = P_lin['eGrid_consumption'] * P_lin['eGrid_price']  # YW - P_lin['Electricity_feedin']*P_lin['Electricity_compensation']

    #v_costs_P_lin = sum(v_costs_P_lin.value)
    objective_P_lin = P_lin.objective.value
    objective_P = P.objective.value

    CH4_SOC = P_lin['CH4Storage_soc'].value
    H2_SOC = P_lin['H2Storage_soc'].value
    CO2_SOC = P_lin['CO2Storage_soc'].value
    BAT_SOC = P_lin['BAT_soc'].value
    HEAT_SOC = P_lin['heatStorage_soc'].value
    data_input = save_results_in_csv(ES, options['output_folder'], options['input_data']['timesteps'].size, options['iter'])
    # H2_in, CO2_in, CH4_out = get_methanation_input(hours, iter)
    H2_in = data_input['MP H2_IN']
    CO2_in = data_input['MP CO2_IN']
    CH4_out = -1 * data_input['MP CH4_OUT']
    ED_DAC = data_input['DAC IN']
    ED_PEM = data_input['PEM IN']

    ###  Step 5 integrated process system optimization
    """
    from algorithm.bridgeESPS.process_system.methanation.process_methanation import initialize_fs, prepare_optimization

    options_PS = {
        'init_folder': PS[0]['init_folder']
    }

    mCH4 = initialize_fs(options=options_PS)
    mCH4 = prepare_optimization(mCH4)

    PS[0].update({'flowsheet_model': mCH4})
    """
    #from algorithm.bridgeESPS.process_system.methanation.utility_functions_partA import flowsheet_opt_seq

    results_CH4 = flowsheet_opt_seq(PS[0], CO2_in, H2_in, CH4_out,
                                    options['input_data']['data']['eGrid_price'],
                                    options['iter'],
                                    options['input_data']['data']['h'],
                                    options['ramping_con'],
                                    )

    # TODO: objective_CH4 = variable_costs + sum(operational_costs)
    m_costs_CH4 = sum(results_CH4['oper_cost'])

    objective_CH4 = v_costs_ES + m_costs_CH4
    objective_ES = v_costs_ES + m_costs_ES
    penalty = objective_P_lin - objective_P - m_costs_ES

    penalty_CH4 = (options['method_scale_beta'] * 1 / options['input_data']['timesteps'].size) * sum((P_lin['MP_CH4_output_rel'][1][idx + 1].value * 500
                                                  - results_CH4['energy_feed_out'][idx]) ** 2 for idx in tot_index)

    print("=======================================================================")
    # TODO: Var costs aus Plin bestimmen -> direkt berechnen?
    print("ES Meth costs: ", m_costs_ES)
    print("CH4 Meth costs: ", m_costs_CH4)
    print("ES var costs: ", v_costs_ES)
    print("P_lin Objective: ", objective_P_lin)
    print("P Objective: ", objective_P)
    print("Penalty Term ES: ", penalty)
    print("Penalty Term CH4: ", penalty_CH4)
    print("ES Objective: ", v_costs_ES + m_costs_ES)
    print("CH4 Objective: ", objective_CH4)
    print("=======================================================================")
    # TODO: Calc GAP = oCH4 - oES / oES
    GAP = abs((objective_CH4 - objective_ES) / objective_ES)
    # TODO: Update specific SNG costs
    # SNG_costs_res = [x for x in results_CH4['sng_cost'] if math.isnan(x) == False and x != 0]
    # SNG_costs_arr = results_CH4['oper_cost'] / CH4_out.tolist()

    ### Eq. (3.28) Calc "tatsächliche Prozesskosten" C^{Prozess}_t
    SNG_costs_arr = results_CH4['oper_cost'] / results_CH4['energy_feed_out']
    SNG_costs_res = [x for x in SNG_costs_arr if math.isnan(x) == False and x != 0]
    SNG_costs = sum(SNG_costs_res) / len(SNG_costs_res)
    t_end_PE = timer()

    time_solve = [t_end_ES - t_start, t_end_PE - t_end_ES]

    ### Save results --> .pkl files
    results_dict = {'GAP': GAP,
                    'MILP_gap': gap_MILP * 100,
                    'solving_time': time_solve,
                    'scale_prev': options['method_scale_zeta'],
                    'scale_inf': options['method_scale_beta'],
                    'SNG_costs': SNG_costs,
                    'SNG_costs_arr': SNG_costs_arr,
                    'v_costs_ES': v_costs_ES,
                    'm_costs_ES': m_costs_ES,
                    'm_costs_CH4': m_costs_CH4,
                    'penalty_ES': penalty,
                    'penalty_CH4': penalty_CH4,
                    'objective_P_lin': objective_P_lin,
                    'objective_ES': v_costs_ES + m_costs_ES,
                    'objective_CH4': objective_CH4,
                    'results_CH4': results_CH4,
                    'grad': grad,
                    'infeasible_index': pen_index,
                    'H2_in': H2_in[1],
                    'CO2_in': CO2_in[1],
                    'CH4_out': CH4_out[1],
                    'ED_DAC': ED_DAC[1],
                    'ED_PEM': ED_PEM[1],
                    'CH4_SOC': CH4_SOC,
                    'H2_SOC': H2_SOC,
                    'CO2_SOC': CO2_SOC,
                    'BAT_SOC': BAT_SOC,
                    'HEAT_SOC': HEAT_SOC
                    }

    return results_dict


def flowsheet_opt_seq(options, CO2_in, H2_in, CH4_out, k_el, iter, time, ramping_con=True):
    #TODO: Beobachtung: Wenn init point < Zielpunkt -> schlägt fehl --> Vorher abchecken und ansonsten direkt neu laden

    import numpy as np
    from pyomo.environ import (Constraint,
                               Var,
                               ConcreteModel,
                               Expression,
                               Objective,
                               SolverFactory,
                               TransformationFactory,
                               value)
    import pandas as pd
    import numpy as np
    from idaes.core.util import model_serializer as ms
    from pyomo.opt import TerminationCondition, SolverStatus
    from pandas import read_csv

    initialization_points = range(20,520,20)
    opt = SolverFactory(options['which_solver'])

    opt.options = options['options_solver']
    """
    opt.options = {'tol': 0.1/100, #YW 0.01,
                   'max_iter': 500,
                   "halt_on_ampl_error": 'no'
                   # 'gamma_theta': 1e-2
                   # 'acceptable_tol': 0.08,
                   # 'acceptable_constr_viol_tol': 0.01
                   }
    """

    m = options['flowsheet_model']

    cond_sat = []
    energy_feed_in = []
    energy_out = []
    co2_in = []
    h2_co2_ratio = []
    rec = []
    cooling_demand = []
    heating_demand = []
    hd_101 = []
    hd_102 = []
    hd_103 = []
    hd_104 = []
    hd_105 = []
    hd_106 = []
    hd_107 = []
    electricity_demand = []
    ed_1 = []
    ed_2 = []
    pressure_R101_in = []
    T_R101_in = []
    T_R102_in = []
    T_R103_in = []
    T_R104_in = []
    T_R101_out = []
    T_R102_out = []
    T_R103_out = []
    T_R104_out = []
    T_HEX103_out = []
    T_HEX105_out = []
    T_HEX107_out = []
    T_C101_out = []
    T_C102_out = []
    etha_chem = []
    etha_ener = []
    oper_cost = []
    sng_cost = []

    end_value = len(CH4_out)
    start_value = 1
    i = start_value
    count_calc = 0
    init_point = value(m.fs.energy_out)
    P_C101_lb = 1e5
    P_C101_ub = 20e5
    T_R101_lb = 250 + 273.15
    T_R101_ub = 600 + 273.15
    T_R102_lb = 250 + 273.15
    T_R102_ub = 600 + 273.15
    T_R103_lb = 250 + 273.15
    T_R103_ub = 600 + 273.15
    T_R104_lb = 250 + 273.15
    T_R104_ub = 600 + 273.15
    eps = 3e-3
    # Load starting initialization
    # ms.from_json(m, fname=f'..\initialization\init_fs_{500}_95_TREMP_new_pressure.json.gz')
    ### Lade Nennbetriebspunkt
    ms.from_json(m, fname=f'{options["init_folder"]}\\{options["init_file"]}')

    while i <= end_value and count_calc <=5:
        print(f'ITERATION: {iter}, TIMESTEP: {i}, COUNTER: {count_calc}, INIT_POINT: {init_point} ')
        if CH4_out[1,i] == 0 or H2_in[1,i] == 0:
            print("CH4 OUT: ", CH4_out[1,i])
            energy_out.append(0)
            energy_feed_in.append(0)
            co2_in.append(0)
            h2_co2_ratio.append(0)
            rec.append(0)
            cooling_demand.append(0)
            hd_101.append(0)
            hd_102.append(0)
            hd_103.append(0)
            hd_104.append(0)
            hd_105.append(0)
            hd_106.append(0)
            hd_107.append(0)
            heating_demand.append(0)
            electricity_demand.append(0)
            ed_1.append(0)
            ed_2.append(0)
            pressure_R101_in.append(0)
            # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
            T_R101_in.append(0)
            T_R102_in.append(0)
            T_R103_in.append(0)
            T_R104_in.append(0)
            T_R101_out.append(0)
            T_R102_out.append(0)
            T_R103_out.append(0)
            T_R104_out.append(0)
            T_HEX103_out.append(0)
            T_HEX105_out.append(0)
            T_HEX107_out.append(0)
            T_C101_out.append(0)
            T_C102_out.append(0)
            etha_chem.append(0)
            etha_ener.append(0)
            oper_cost.append(0)
            sng_cost.append(0)
            i = i + 1
        else:
            # Avoid to start at an initialization point below the desired energy output
            if CH4_out[1,i] > init_point:
                nearest_point = min(initialization_points,
                                    key=lambda x: abs(x - (CH4_out[1, i] + 20 * (count_calc + 1))))
                # ms.from_json(m, fname=f'..\initialization\init_fs_{nearest_point}_95_isothermal.json.gz')
                # ms.from_json(m, fname=f'..\initialization\init_fs_{nearest_point:03}_95_TREMP_new_pressure.json.gz')
                ms.from_json(m, fname=f'{options["step2_folder"]}\\init_fs_{nearest_point:03}_95_TREMP_paper.json.gz')
                print("Initilization point lies below target energy output")
                print(f"LOAD INITILIZATION AT {nearest_point}")
            # Unfix parameters
            if count_calc > 1 and ramping_con == True:
                #TODO: Bevor direkt auf max umschwingen, vorher noch 1 iteration mit min(OPEX) aber energy_out nicht fixen
                #TODO: sondern über ub/lb stark eingrenzen, sodass Toleranz 3e-3 erfüllt ist
                ### Relaxation of Eq.3.20c --> Eq. (3.23,3.24)
                if count_calc == 2:
                    print("COULDN'T SOLVE DESIRED CH4 OUTPUT --- TRY RELAXATION W/ FACTOR E")
                    opt.options = options['options_solver']
                    """
                    opt.options = {'tol': 0.1/100, #YW0.01,
                                   'max_iter': 1500,
                                   "halt_on_ampl_error": 'no'
                                   # 'gamma_theta': 1e-2
                                   # 'acceptable_tol': 0.08,
                                   # 'acceptable_constr_viol_tol': 0.01
                                   }
                    """
                    del m.fs.objective
                    m.fs.objective = Objective(expr=m.fs.operating_cost_neu)
                    m.fs.energy_out.unfix()
                    m.fs.energy_out.setub(CH4_out[1,i]*(1+eps))
                    m.fs.energy_out.setlb(CH4_out[1,i]*(1-eps))
                    m.fs.k_el.unfix()
                    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].unfix()
                    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].unfix()
                    m.fs.k_el.fix(k_el[1, i])
                    mark_var = 'false'
                else:
                    ### Change objective Eq. (3.25)
                    print("COULDN'T SOLVE DESIRED CH4 OUTPUT --- CHANGE OBJECTIVE TO MAX OUTPUT")
                    opt.options = options['options_solver']
                    """
                    opt.options = {'tol': 0.1/100, #YW0.01,
                                   'max_iter': 1500,
                                   "halt_on_ampl_error": 'no'
                                   # 'gamma_theta': 1e-2
                                   # 'acceptable_tol': 0.08,
                                   # 'acceptable_constr_viol_tol': 0.01
                                   }
                    """
                    del m.fs.objective
                    # m.fs.objective = Objective(expr=-m.fs.energy_out)
                    m.fs.objective = Objective(expr=(m.fs.energy_out-CH4_out[1,i])**2)
                    m.fs.energy_out.unfix()
                    # m.fs.energy_out.setub(CH4_out[1,i])
                    m.fs.energy_out.setub(500)
                    m.fs.energy_out.setlb(20)
                    m.fs.k_el.unfix()
                    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].unfix()
                    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].unfix()
                    m.fs.k_el.fix(k_el[1, i])
                    mark_var = 'false'
            else:
                opt.options = options['options_solver']
                """
                opt.options = {'tol': 0.1/100, #YW0.01,
                               'max_iter': 500,
                               "halt_on_ampl_error": 'no'
                               # 'gamma_theta': 1e-2
                               # 'acceptable_tol': 0.08,
                               # 'acceptable_constr_viol_tol': 0.01
                               }
                """
                del m.fs.objective
                m.fs.objective = Objective(expr=m.fs.operating_cost_neu)
                m.fs.energy_out.unfix()
                m.fs.k_el.unfix()
                m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].unfix()
                m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].unfix()

                # Fix Input / Output data
                m.fs.energy_out.setub(500)
                m.fs.energy_out.setlb(20)
                m.fs.energy_out.fix(CH4_out[1,i])
                m.fs.k_el.fix(k_el[1,i])
                molH2 = H2_in[1,i] / LHV_H2_mol
                molCO2 = molH2/4
                print("KEL: ", m.fs.k_el.value)
                print("CH4 OUT: ", m.fs.energy_out.value)
                mark_var = 'true'
            # m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].fix(molCO2)
            # m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].fix(molH2)

            # Overwrite upper & lower variable bounds
            # TODO: Speichern der letzten Eingangstemp + Druck
            # TODO: Anschließnd neue bounds definieren zB max(p_prev - dp, p_min) < p < min(p_prev + dp, pmax)
            ### Ramping constraints
            if ramping_con == True:

                if i >= 2: ## iteration >= 2
                    ## ramping constraints parameters of pressure
                    p_max = 20e5
                    p_min = 1e5
                    dp = 1e5

                    P_C101_lb = max(p_min, pressure_R101_in[i-2]*1e5 - dp)
                    P_C101_ub = min(p_max, pressure_R101_in[i-2]*1e5 + dp)
                    m.fs.C101.outlet.pressure.setlb(P_C101_lb)
                    m.fs.C101.outlet.pressure.setub(P_C101_ub)

                    ## ramping constraints parameters of temperature
                    T_min = 250 + 273.15
                    T_max = 600 + 273.15
                    dT = 10

                    T_R101_lb = max(T_min, T_R101_in[i-2] + 273.15 - dT)
                    T_R101_ub = min(T_max, T_R101_in[i-2] + 273.15  + dT)
                    m.fs.R101.inlet.temperature.setlb(T_R101_lb)
                    m.fs.R101.inlet.temperature.setub(T_R101_ub)



                    T_R102_lb = max(T_min, T_R102_in[i-2] + 273.15  - dT)
                    T_R102_ub = min(T_max, T_R102_in[i-2] + 273.15  + dT)
                    m.fs.R102.inlet.temperature.setlb(T_R102_lb)
                    m.fs.R102.inlet.temperature.setub(T_R102_ub)


                    T_R103_lb = max(T_min, T_R103_in[i-2] + 273.15  - dT)
                    T_R103_ub = min(T_max, T_R103_in[i-2] + 273.15  + dT)
                    m.fs.R103.inlet.temperature.setlb(T_R103_lb)
                    m.fs.R103.inlet.temperature.setub(T_R103_ub)


                    T_R104_lb = max(T_min, T_R104_in[i-2] + 273.15  - dT)
                    T_R104_ub = min(T_max, T_R104_in[i-2] + 273.15  + dT)
                    m.fs.R104.inlet.temperature.setlb(T_R104_lb)
                    m.fs.R104.inlet.temperature.setub(T_R104_ub)

                P_C101_lb = m.fs.C101.control_volume.properties_out[0.0].pressure.lb
                P_C101_ub = m.fs.C101.control_volume.properties_out[0.0].pressure.ub
                T_R101_lb = m.fs.R101.control_volume.properties[0.0,0.0].temperature.lb
                T_R101_ub = m.fs.R101.control_volume.properties[0.0,0.0].temperature.ub
                T_R102_lb = m.fs.R102.control_volume.properties[0.0,0.0].temperature.lb
                T_R102_ub = m.fs.R102.control_volume.properties[0.0,0.0].temperature.ub
                T_R103_lb = m.fs.R103.control_volume.properties[0.0,0.0].temperature.lb
                T_R103_ub = m.fs.R103.control_volume.properties[0.0,0.0].temperature.ub
                T_R104_lb = m.fs.R104.control_volume.properties[0.0, 0.0].temperature.lb
                T_R104_ub = m.fs.R104.control_volume.properties[0.0, 0.0].temperature.ub
                print(f"T R101: [{T_R101_lb - 273.15}, {T_R101_ub - 273.15}]°C; T R102: [{T_R102_lb - 273.15}, {T_R102_ub - 273.15}]°C")
                # print(f"T R102: [{T_R102_lb - 273.15}, {T_R102_ub - 273.15}]°C")
                print(f"T R103: [{T_R103_lb - 273.15}, {T_R103_ub - 273.15}]°C; T R104: [{T_R104_lb - 273.15}, {T_R104_ub - 273.15}]°C")
                # print(f"T R104: [{T_R104_lb - 273.15}, {T_R104_ub - 273.15}]°C")
                print(f"P C101: [{P_C101_lb/1e5}, {P_C101_ub/1e5}]")
            try:
                solve_status = opt.solve(m, tee=True)
                # assert solve_status.solver.termination_condition == TerminationCondition.optimal
                # assert solve_status.solver.status == SolverStatus.ok

                if solve_status.solver.termination_condition.value != 'optimal':
                    # find nearest initialization point

                    nearest_point = min(initialization_points, key=lambda x:abs(x-(CH4_out[1,i] + 20*(count_calc+1))))
                    # ms.from_json(m,fname=f'..\initialization\init_fs_{nearest_point}_95_isothermal.json.gz')
                    # ms.from_json(m, fname=f'..\initialization\init_fs_{nearest_point:03}_95_TREMP_new_pressure.json.gz')
                    ms.from_json(m, fname=f"{options['step2_folder']}\\init_fs_{nearest_point:03}_95_TREMP_paper.json.gz")
                    count_calc = count_calc + 1
                    init_point = nearest_point
                    continue
                else:
                    # Save results
                    if mark_var == 'false' and abs((value(m.fs.energy_out) - CH4_out[1,i])/CH4_out[1,i]) <= eps: ## Eq. (3.26)
                        mark_var = 'true'
                    cond_sat.append(mark_var)
                    energy_out.append(value(m.fs.energy_out))
                    energy_feed_in.append(value(m.fs.energy_in))
                    mol_co2 = value(m.fs.Feed_CO2.properties[0.0].flow_mol)
                    m_co2 = mol_co2 * 44.01 * 1e-3 * 3600 #kg/h
                    co2_in.append(m_co2)
                    h2_co2_ratio.append(value(m.fs.H2_CO2_ratio))
                    rec.append(value(m.fs.S101.split_fraction[0, "recycle_R101"]) * 100)
                    cooling_demand.append(value(m.fs.cooling_demand) / 1000)
                    hd_101.append(value(m.fs.HEX101.heat_duty[0]) / 1000)
                    hd_102.append(value(m.fs.HEX102.heat_duty[0]) / 1000)
                    hd_103.append(value(m.fs.HEX103.heat_duty[0]) / 1000)
                    hd_104.append(value(m.fs.HEX104.heat_duty[0]) / 1000)
                    hd_105.append(value(m.fs.HEX105.heat_duty[0]) / 1000)
                    hd_106.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
                    hd_107.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
                    heating_demand.append(value(m.fs.heating_demand) / 1000)
                    electricity_demand.append(value(m.fs.electricity_demand) / 1000)
                    ed_1.append(value(m.fs.C101.work_mechanical[0]) / 1000)
                    ed_2.append(value(m.fs.C102.work_mechanical[0]) / 1000)
                    pressure_R101_in.append(value(m.fs.C101.outlet.pressure[0]) / 1e5)
                    # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
                    T_R101_in.append(value(m.fs.R101.inlet.temperature[0]) - 273.15)
                    T_R102_in.append(value(m.fs.R102.inlet.temperature[0]) - 273.15)
                    T_R103_in.append(value(m.fs.R103.inlet.temperature[0]) - 273.15)
                    T_R104_in.append(value(m.fs.R104.inlet.temperature[0]) - 273.15)
                    T_R101_out.append(value(m.fs.R101.outlet.temperature[0]) - 273.15)
                    T_R102_out.append(value(m.fs.R102.outlet.temperature[0]) - 273.15)
                    T_R103_out.append(value(m.fs.R103.outlet.temperature[0]) - 273.15)
                    T_R104_out.append(value(m.fs.R104.outlet.temperature[0]) - 273.15)
                    T_HEX103_out.append(value(m.fs.HEX103.outlet.temperature[0]) - 273.15)
                    T_HEX105_out.append(value(m.fs.HEX105.outlet.temperature[0]) - 273.15)
                    T_HEX107_out.append(value(m.fs.HEX107.outlet.temperature[0]) - 273.15)
                    T_C101_out.append(value(m.fs.C101.outlet.temperature[0]) - 273.15)
                    T_C102_out.append(value(m.fs.C102.outlet.temperature[0]) - 273.15)
                    etha_chem.append(value(m.fs.etha_chem))
                    etha_ener.append(value(m.fs.etha_energy))
                    oper_cost.append(value(m.fs.operating_cost_neu))
                    sng_cost.append(value(m.fs.sng_cost) * 100)
                    init_point = value(m.fs.energy_out)
                    i = i + 1
                    count_calc = 0

            except ValueError:
                # find nearest initialization point
                nearest_point = min(initialization_points, key=lambda x: abs(x - (CH4_out[1,i]+ 20*(count_calc+1))))
                # ms.from_json(m, fname=f'..\initialization\init_fs_{nearest_point}_95_isothermal.json.gz')
                # ms.from_json(m, fname=f"..\initialization\init_fs_{nearest_point:03}_95_TREMP_new_pressure.json.gz")
                ms.from_json(m, fname=f'{options["step2_folder"]}\\\init_fs_{nearest_point:03}_95_TREMP_paper.json.gz')
                count_calc = count_calc + 1
                init_point = nearest_point
                continue

    name_list = [
        'time',
        'cond_sat',
        'energy_feed_out',
        'energy_feed_in',
        'CO2_in',
        'H2/CO2',
        'rec',
        'cooling_demand',
        'hd_101',
        'hd_102',
        'hd_103',
        'hd_104',
        'hd_105',
        'hd_106',
        'hd_107',
        'heating_demand',
        'electricity_demand',
        'ed_1',
        'ed_2',
        'pressure_R101_in',
        # 'pressure_R102_in',
        'T_R101_in',
        'T_R102_in',
        'T_R103_in',
        'T_R104_in',
        'T_R101_out',
        'T_R102_out',
        'T_R103_out',
        'T_R104_out',
        'T_HEX103_out',
        'T_HEX105_out',
        'T_HEX107_out',
        'T_C101_out',
        'T_C102_out',
        'etha_chem',
        'etha_ener',
        'oper_cost',
        'sng_cost'
    ]

    df = pd.DataFrame(list(zip(
        time,
        cond_sat,
        energy_out,
        energy_feed_in,
        co2_in,
        h2_co2_ratio,
        rec,
        cooling_demand,
        hd_101,
        hd_102,
        hd_103,
        hd_104,
        hd_105,
        hd_106,
        hd_107,
        heating_demand,
        electricity_demand,
        ed_1,
        ed_2,
        pressure_R101_in,
        # pressure_R102_in,
        T_R101_in,
        T_R102_in,
        T_R103_in,
        T_R104_in,
        T_R101_out,
        T_R102_out,
        T_R103_out,
        T_R104_out,
        T_HEX103_out,
        T_HEX105_out,
        T_HEX107_out,
        T_C101_out,
        T_C102_out,
        etha_chem,
        etha_ener,
        oper_cost,
        sng_cost
    )), columns=name_list)

    import time
    timestr = time.strftime("%m%d-%H%M")

    df.to_csv(f'{options["output_folder"]}\\results_flowsheet_iter-{iter}_{timestr}.csv')
    df.to_excel(f'{options["output_folder"]}\\results_flowsheet_iter-{iter}_{timestr}.xlsx')

    return df
