"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def save_results_in_csv(ES, output_folder, hours, iter):
    data_ges = []
    for bus in ES.connections:
        data = pd.DataFrame({f'{c.component.label} {c.name}': c.expr.value for c in ES.connections[bus]})
        data.to_csv(f'{output_folder}\\plot_final_' + str(hours) + bus + '.csv', sep=';', decimal = ',')
        data_ges.append(data)

    data_ges = pd.concat(data_ges, axis=1)
    data_ges.to_csv(f'{output_folder}\\plot_final_ges_' + str(hours) + '_' + str(iter) +  '.csv', sep=';', decimal=',')
    return data_ges

def get_methanation_input(hours, output_folder, iter):
    data = pd.read_csv(f'{output_folder}\\plot_final_ges_' + str(hours) + '_' + str(iter) +  '.csv', sep=';', decimal=',')
    H2_in = data['MP H2_IN']
    CO2_in = data['MP CO2_IN']
    CH4_out = -1 * data['MP CH4_OUT']

    return H2_in, CO2_in, CH4_out

def get_electricty_demand(hours, output_folder, iter):
    data = pd.read_csv(f'{output_folder}\\plot_final_ges_' + str(hours) + '_' + str(iter) +  '.csv', sep=';', decimal=',')
    ED_DAC = data['MP H2_IN']
    ED_PEM = data['MP CO2_IN']

    return ED_DAC, ED_PEM

#######
####### plot results
#######
def plot_costs(timestr, output_folder, data):
    iter_plot = range(1, len(data)+1)
    SNG_plot = [0]
    GAP_plot = [0]
    M_costES_plot = [0]
    M_costCH4_plot = [0]
    ObjectiveES_plot = [0]
    ObjectiveCH4_plot = [0]
    for i in iter_plot:
        SNG_plot.append(data[i]['SNG_costs'])
        M_costES_plot.append(data[i]['m_costs_ES'])
        M_costCH4_plot.append(data[i]['m_costs_CH4'])
        ObjectiveES_plot.append(data[i]['objective_ES'])
        ObjectiveCH4_plot.append(data[i]['objective_CH4'])
    SNG_plot.pop(-1)
    ObjectiveES_plot.pop(0)
    ObjectiveCH4_plot.pop(0)
    M_costES_plot.pop(0)
    M_costCH4_plot.pop(0)

    SNG_plot_arr = np.asarray(SNG_plot)
    ObjectiveES_plot_arr = np.asarray(ObjectiveES_plot)
    ObjectiveCH4_plot_arr = np.asarray(ObjectiveCH4_plot)
    M_costES_plot_arr = np.asarray(M_costES_plot)
    M_costCH4_plot_arr = np.asarray(M_costCH4_plot)


    fig, (ax1, ax2, ax3,ax4, ax5) = plt.subplots(5,1)
    ax1.plot(iter_plot,SNG_plot, '-o')
    ax1.set_title('SNG costs')
    ax1.set_ylabel('Spez. Kosten [€/kWh SNG]')
    ax2.plot(iter_plot,ObjectiveES_plot,'-o',label='Geschätzt - ES')
    ax2.plot(iter_plot, ObjectiveCH4_plot,'-o', label='Tatsächlich - Prozess')
    ax2.set_title('Gesamtkosten')
    ax2.set_ylabel('€')
    ax3.plot(iter_plot,M_costES_plot,'-o',label='Geschätzt - ES')
    ax3.plot(iter_plot, M_costCH4_plot,'-o', label='Tatsächlich - Prozess')
    ax3.set_title('Methanisierungskosten')
    ax3.set_ylabel('€')
    ax4.plot(iter_plot, ObjectiveCH4_plot_arr/ObjectiveCH4_plot_arr[0]*100,'-o', label='Tatsächlich - Prozess')
    ax4.set_title('Gesamtkosten - Tatsächliche Kosten')
    ax4.set_ylim(99,100)
    ax4.set_ylabel('[%]')
    ax4.grid()
    ax5.plot(iter_plot, M_costCH4_plot_arr/M_costCH4_plot_arr[0]*100,'-o', label='Tatsächlich - Prozess')
    ax5.set_title('Methanisierungskosten - Tatsächliche Kosten')
    ax5.set_ylabel('[%]')
    ax5.set_ylim(85, 100)
    ax5.set_xlabel('Iteration')
    ax5.grid()
    ax1.grid()
    ax2.legend()
    ax2.grid()
    ax3.legend()
    ax3.grid()
    manager = plt.get_current_fig_manager()
    try:
        manager.window.showMaximized()
    except:
        manager.window.state('zoomed')
    fig.set_tight_layout(True)
    plt.tight_layout()
    plt.savefig(f'{output_folder}\\Kosten_{timestr}.png', bbox_inches='tight')
    plt.show()

def plot_temp_in(timestr, output_folder,  data):
    fz = 8
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    fig.suptitle('Reaktortemperaturen - Eingang')
    ### Reactor temperatures - einzeln
    df=data[1]['results_CH4']
    t = np.arange(len(df['T_R101_in']))
    ax1.plot('time', 'T_R101_in', '.-', data=df, label='R101')
    ax1.plot('time', 'T_R102_in', '.-', data=df, label='R102')
    ax1.plot('time', 'T_R103_in', '.-', data=df, label='R103')
    ax1.plot('time', 'T_R104_in', '.-', data=df, label='R104')
    ax1.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax1.set_title('Iteration 1')
    ax1.legend(loc='best', ncol=4, prop={'size': 8})
    ax1.grid()


    df=data[2]['results_CH4']
    t = np.arange(len(df['T_R101_in']))
    ax2.plot('time', 'T_R101_in', '.-', data=df, label='R101')
    ax2.plot('time', 'T_R102_in', '.-', data=df, label='R102')
    ax2.plot('time', 'T_R103_in', '.-', data=df, label='R103')
    ax2.plot('time', 'T_R104_in', '.-', data=df, label='R104')
    ax2.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax2.set_title('Iteration 2')
    ax2.legend(loc='best', ncol=4, prop={'size': 8})
    ax2.grid()


    df=data[3]['results_CH4']
    t = np.arange(len(df['T_R101_in']))
    ax3.plot('time', 'T_R101_in', '.-', data=df, label='R101')
    ax3.plot('time', 'T_R102_in', '.-', data=df, label='R102')
    ax3.plot('time', 'T_R103_in', '.-', data=df, label='R103')
    ax3.plot('time', 'T_R104_in', '.-', data=df, label='R104')
    ax3.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax3.set_xlabel(r'Zeit t')
    ax3.set_title('Iteration 3')
    ax3.legend(loc='best', ncol=4, prop={'size': 8})
    ax3.grid()
    fig.set_tight_layout(True)
    plt.tight_layout()
    plt.savefig(f'{output_folder}\\Reaktorentemperaturen_Eingang_{timestr}.png', bbox_inches='tight')
    plt.show()


def plot_temp_out(timestr,  output_folder,  data):
    fz = 8
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    fig.suptitle('Reaktortemperaturen - Ausgang')
    ### Reactor temperatures - einzeln
    df = data[1]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax1.plot('time', 'T_R101_out', '.-', data=df, label='R101')
    ax1.plot('time', 'T_R102_out', '.-', data=df, label='R102')
    ax1.plot('time', 'T_R103_out', '.-', data=df, label='R103')
    ax1.plot('time', 'T_R104_out', '.-', data=df, label='R104')
    ax1.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax1.set_title('Iteration 1')
    ax1.legend(loc='best', ncol=4, prop={'size': 8})
    ax1.grid()


    df = data[2]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax2.plot('time', 'T_R101_out', '.-', data=df, label='R101')
    ax2.plot('time', 'T_R102_out', '.-', data=df, label='R102')
    ax2.plot('time', 'T_R103_out', '.-', data=df, label='R103')
    ax2.plot('time', 'T_R104_out', '.-', data=df, label='R104')
    ax2.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax2.set_title('Iteration 2')
    ax2.legend(loc='best', ncol=4, prop={'size': 8})
    ax2.grid()


    df = data[3]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax3.plot('time', 'T_R101_out', '.-', data=df, label='R101')
    ax3.plot('time', 'T_R102_out', '.-', data=df, label='R102')
    ax3.plot('time', 'T_R103_out', '.-', data=df, label='R103')
    ax3.plot('time', 'T_R104_out', '.-', data=df, label='R104')
    ax3.set_ylabel('Temperatur [°C]', fontsize=fz)
    ax3.set_xlabel(r'Zeit t')
    ax3.set_title('Iteration 3')
    ax3.legend(loc='best', ncol=4, prop={'size': 8})
    ax3.grid()
    fig.set_tight_layout(True)
    plt.tight_layout()
    plt.savefig(f'{output_folder}\\Reaktorentemperaturen_Ausgang_{timestr}.png', bbox_inches='tight')
    plt.show()

def plot_pressure(timestr,  output_folder, data):
    fz = 8
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    fig.suptitle('Betriebsdruck')
    ### Reactor temperatures - einzeln
    df = data[1]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax1.plot('time', 'pressure_R101_in', '.-', data=df, label='R101')
    ax1.set_ylabel('Druck [bar]', fontsize=fz)
    ax1.set_title('Iteration 1')
    ax1.legend(loc='best', ncol=4, prop={'size': 8})
    ax1.grid()


    df = data[2]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax2.plot('time', 'pressure_R101_in', '.-', data=df, label='R101')
    ax2.set_ylabel('Druck [bar]', fontsize=fz)
    ax2.set_title('Iteration 2')
    ax2.legend(loc='best', ncol=4, prop={'size': 8})
    ax2.grid()


    df = data[3]['results_CH4']
    t = np.arange(len(df['T_R101_out']))
    ax3.plot('time', 'pressure_R101_in', '.-', data=df, label='R101')
    ax3.set_ylabel('Druck [bar]', fontsize=fz)
    ax3.set_xlabel(r'Zeit t')
    ax3.set_title('Iteration 3')
    ax3.legend(loc='best', ncol=4, prop={'size': 8})
    ax3.grid()
    fig.set_tight_layout(True)
    plt.tight_layout()
    plt.savefig(f'{output_folder}\\Druck_{timestr}.png', bbox_inches='tight')
    plt.show()
def plot_CH4(timestr,  output_folder, data):
    fz = 8

    x_hours = data[1]['results_CH4']['time']
    Tot = len(data)
    Cols = 2
    Rows = Tot // Cols
    Rows += Tot % Cols

    Position = range(1,Tot +1)
    fig = plt.figure(1)
    for k in range(Tot):
        ax = fig.add_subplot(Rows, Cols, Position[k])
        df = data[k+1]['results_CH4']
        ax.plot('time', 'energy_feed_out', '.-', data=df, label='Tats. Output')
        ax.plot('time', data[k+1]['CH4_out'], '.-', data=df, label='Gewünschter Output')
        ax.set_ylabel(r'$ \dot{E}_{CH4}$ [kWh]', fontsize=fz)
        ax.set_ylim(0,500)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_title(f'Iteration {k+1}')
        ax.legend(loc='best', ncol=4, prop={'size': 8})
        ax.grid()
    ax.set_xlabel(r'Zeit [h]')

    manager = plt.get_current_fig_manager()
    try:
        manager.window.showMaximized()
    except:
        manager.window.state('zoomed')

    fig.set_tight_layout(True)
    plt.tight_layout()

    plt.savefig(f'{output_folder}\\CH4output_{timestr}.png')
    plt.show()


def plot_ED(timestr,  output_folder, data):
    fz = 8
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    fig.suptitle('Strombedarf')
    ### Reactor temperatures - einzeln
    df = data[1]['results_CH4']
    ED_DAC = data[1]['ED_DAC']
    ED_PEM = data[1]['ED_PEM']

    ax1.bar('time', 'electricity_demand', data=df, label='Methanisierung', bottom=ED_PEM + ED_DAC)

    ax1.bar('time', ED_DAC,  data=df, label='DAC', bottom=ED_PEM)
    ax1.bar('time', ED_PEM, data=df, label='PEM')



    ax1.set_ylabel(r'$ W_{el}$ [kWh]', fontsize=fz)
    ax1.set_title('Iteration 1')
    ax1.legend(loc='best', ncol=3, prop={'size': 8})
    ax1.grid()

    df = data[2]['results_CH4']
    ED_DAC = data[2]['ED_DAC']
    ED_PEM = data[2]['ED_PEM']
    ax2.bar('time', 'electricity_demand', data=df, label='Methanisierung', bottom=ED_PEM+ED_DAC)
    ax2.bar('time', ED_DAC,  data=df, label='DAC', bottom=ED_PEM)
    ax2.bar('time', ED_PEM,  data=df, label='PEM')
    ax2.set_ylabel(r'$ W_{el}$ [kWh]', fontsize=fz)
    ax2.set_title('Iteration 2')
    ax2.legend(loc='best', ncol=3, prop={'size': 8})
    ax2.grid()

    df = data[3]['results_CH4']
    ED_DAC = data[3]['ED_DAC']
    ED_PEM = data[3]['ED_PEM']

    ax3.bar('time', 'electricity_demand',  data=df, label='Methanisierung', bottom=ED_PEM+ED_DAC)
    ax3.bar('time', ED_DAC,  data=df, label='DAC', bottom=ED_PEM)
    ax3.bar('time', ED_PEM,  data=df, label='PEM')
    ax3.set_ylabel(r'$ W_{el}$ [kWh]', fontsize=fz)
    ax3.set_title('Iteration 3')
    ax3.legend(loc='best', ncol=3, prop={'size': 8})
    ax3.grid()


    fig.set_tight_layout(True)
    plt.tight_layout()
    plt.savefig(f'{output_folder}\\Strombedarf_{timestr}.png', bbox_inches='tight')
    plt.show()
def plot_SNG_costs(timestr, output_folder, data):
    fz = 8

    x_hours = data[1]['results_CH4']['time']
    Tot = len(data)
    Cols = 2
    Rows = Tot // Cols
    Rows += Tot % Cols

    Position = range(1,Tot +1)
    fig = plt.figure(1)
    for k in range(Tot):
        ax = fig.add_subplot(Rows, Cols, Position[k])
        df = data[k+1]['results_CH4']
        c = data[k+1]['SNG_costs_arr']*100
        ax.plot('time', c, '.-', data=df, label='Tats. Output')
        ax.set_ylabel(r'$ k_{SNG}$ [ct/kWh]', fontsize=fz)
        ax.set_ylim(0,1.5)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_title(f'Iteration {k+1}')
        ax.legend(loc='best', ncol=4, prop={'size': 8})
        ax.grid()
    ax.set_xlabel(r'Zeit [h]')

    manager = plt.get_current_fig_manager()
    try:
        manager.window.showMaximized()
    except:
        manager.window.state('zoomed')

    fig.set_tight_layout(True)
    plt.tight_layout()
    # plt.savefig(f'./output/CH4output_{timestr}.png', bbox_inches='tight')
    plt.savefig(f'{output_folder}\\SNG_costs_{timestr}.png')
    plt.show()
def plot_gradient(timestr, output_folder, data):
    fz = 8

    x_hours = data[1]['results_CH4']['time']
    Tot = len(data)
    Cols = 2
    Rows = Tot // Cols
    Rows += Tot % Cols

    Position = range(1,Tot +1)
    fig = plt.figure(1)
    for k in range(Tot):
        ax = fig.add_subplot(Rows, Cols, Position[k])
        df = data[k+1]['results_CH4']
        if k == 0:
            c = np.zeros_like(df['time'])
        else:
            c = np.array(data[k+1]['grad'])*100
        ax.plot('time', c, '.-', data=df, label='Tats. Output')
        ax.set_ylabel(r'$ \Delta k_{SNG} [ct/kWh^2]$', fontsize=fz)
        # ax.set_ylim(0,0.01)
        ax.set_xlim(x_hours.tolist()[0], x_hours.tolist()[-1])
        ax.set_title(f'Iteration {k+1}')
        ax.legend(loc='best', ncol=4, prop={'size': 8})
        ax.grid()
    ax.set_xlabel(r'Zeit [h]')

    manager = plt.get_current_fig_manager()
    manager = plt.get_current_fig_manager()
    try:
        manager.window.showMaximized()
    except:
        manager.window.state('zoomed')

    fig.set_tight_layout(True)
    plt.tight_layout()
    # plt.savefig(f'./output/CH4output_{timestr}.png', bbox_inches='tight')
    plt.savefig(f'{output_folder}\\grad_{timestr}.png')
    plt.show()

def plot_all(timestr, output_folder, data):

    plot_costs(timestr, output_folder, data)
    plot_CH4(timestr, output_folder, data)
    plot_SNG_costs(timestr, output_folder, data)
    plot_gradient(timestr, output_folder, data)
    plot_temp_in(timestr, output_folder, data)
    plot_temp_out(timestr, output_folder, data)
    plot_pressure(timestr, output_folder, data)
    plot_ED(timestr, output_folder, data)