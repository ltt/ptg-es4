"""Case study for bridging energy and process system optimization"""
#
#  - Appendix A.3
#  - Figures A.16(a), A.16(b), A.16(c), A.16(c)
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann


###
### import libraries
###
import os
from pyomo.environ import (Constraint,
                           Var,
                           ConcreteModel,
                           Expression,
                           Objective,
                           SolverFactory,
                           TransformationFactory,
                           value, RangeSet, Set)

from idaes.core import FlowsheetBlock, EnergyBalanceType

from pyomo.network import Arc, SequentialDecomposition
from pyomo.environ import units as pyunits

###
### properties, process units and flowsheet
from idaes.models.properties.modular_properties import (GenericParameterBlock, GenericReactionParameterBlock)

#### Importing required thermo and reaction package
import properties_methanation
import reaction_property_Falbo_TREMP_validate


from idaes.models.unit_models import (PFR, Feed, Product)

###
### tools
from idaes.core.util.model_statistics import degrees_of_freedom

def function(unit):
    unit.initialize(outlvl=idaeslog.INFO)

import idaes.logger as idaeslog

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

###
### setups for latex input
################################################################
###
### This part requires a latex environment on your machine.
### Otherwise you can deactivate this setup.
mpl.rcParams.update({

    'text.usetex': True,
    'text.latex.preamble': "\n".join([
        r'\usepackage{amsmath}',
        r'\usepackage{amssymb}',
        r'\usepackage{units}',
#        r'\usepackage[version=4]{mhchem}',
#        # r'\usepackage{siunitx},'
    ]),
#    # 'font.size': 11,
    'font.family': 'lmodern',

})
################################################################
###
### parameters for case study
###

### prices
k_cool = 5/1000 # €/kWh 10°C coolings water
k_elec = 0.2 # €/kWh electricity

# substance data
mw_H2 = 2.016E-3 # kg/mol
HHV_H2_m = 141.800e3 # kJ/kg
LHV_H2_m = 119.972e3 # kJ/kg
HHV_H2_v = 12.745e3 # kJ/m³
LHV_H2_v = 10.783e3 # kJ/m³
HHV_H2_mol = 141.800e3*mw_H2 # kJ/mol
LHV_H2_mol = 119.972e3*mw_H2 # kJ/mol #241

mw_CH4 = 16.043E-3 # kg/mol
HHV_CH4_m = 55.498e3 # kJ/kg
LHV_CH4_m = 50.013e3 # kJ/kg
HHV_CH4_v = 39.819e3 # kJ/m³
LHV_CH4_v = 35.883e3 # kJ/m³
HHV_CH4_mol = 55.498e3*mw_CH4 # kJ/mol
LHV_CH4_mol = 50.013e3*mw_CH4 # kJ/mol #802

def set_size(use=True, fraction=1, subplots=(1, 1)):

    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    width_pt = 422.52347
    height_pt = 635.5
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2


    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    if use==True:
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])
    else:
        fig_height_in =fig_width_in * golden_ratio*1.3 * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)

def plot_discretization(plot_data={}, if_tuning=False):

    fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

    j = 0
    for i in plot_data.keys():
        if j == 0:
            ax.plot(plot_data[i]['x'], plot_data[i]['y'], 'o-', label=i)
        if j == 1:
            ax.plot(plot_data[i]['x'], plot_data[i]['y'], 's-', label=i)
        j+=1

    #ax.set_title(plot_data[i]['name'])

    ax.set_xlabel(r'Spatial dimension')
    ax.set_ylabel(r'Temperature in °C')

    ax.set_xlim([0, 1])
    ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1])

    ###
    ### with tuning
    if plot_data[i]['name'] == 'R1' and if_tuning:
        ax.set_ylim([520, 930])
        ax.set_yticks([520, 725, 930])
    elif plot_data[i]['name'] == 'R2' and if_tuning:
        ax.set_ylim([520, 700])
        ax.set_yticks([520, 610, 700])
    ###
    ### without tuning
    elif plot_data[i]['name'] == 'R1' and not if_tuning:
        ax.set_ylim([250, 260])
        ax.set_yticks([250, 255, 260])
    elif plot_data[i]['name'] == 'R2' and not if_tuning:
        ax.set_ylim([250, 410])
        ax.set_yticks([250, 330, 410])
    elif plot_data[i]['name'] == 'R3':
        ax.set_ylim([270, 460])
        ax.set_yticks([270, 360, 450])
    elif plot_data[i]['name'] == 'R4':
        ax.set_ylim([320, 380])
        ax.set_yticks([320, 350, 380])

    if plot_data[i]['name'] == 'R1' or plot_data[i]['name'] == 'R2':
        ax.legend(loc='upper left')
    elif plot_data[i]['name'] == 'R3' or plot_data[i]['name'] == 'R4':
        ax.legend(loc='lower right')

    fig.set_tight_layout(True)
    plt.tight_layout()
    #plt.savefig(f"./pictures_appendix/appendix3/Appendix_T_dis_{plot_data[i]['name']}_{if_tuning}.svg", format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f"./pictures_appendix/appendix3/Appendix_T_dis_{plot_data[i]['name']}_{if_tuning}.pdf", format='pdf', bbox_inches='tight')

def get_init(options = None, finite_elements_setup = None, if_tuning= False):

    ####
    #### construct the flowsheet
    ####
    m = ConcreteModel()
    m.fs = FlowsheetBlock(dynamic = False)

    ###
    ### add the properties
    m.fs.thermo_params_vapor = GenericParameterBlock(**properties_methanation.thermo_configuration_vapor)
    m.fs.thermo_params_VLE = GenericParameterBlock(**properties_methanation.thermo_configuration_VLE)

    m.fs.reaction_properties_TREMP = reaction_property_Falbo_TREMP_validate.MethanationReactionParameterBlock(
        property_package=m.fs.thermo_params_vapor)

    ###
    ### define flowsheet
    print('finite_elements is set to be ', finite_elements_setup)

    m.fs.Feed = Feed(property_package=m.fs.thermo_params_vapor)
    m.fs.Product = Product(property_package=m.fs.thermo_params_vapor)

    m.fs.R101 = PFR(property_package = m.fs.thermo_params_vapor,
                    reaction_package = m.fs.reaction_properties_TREMP,
                    has_heat_of_reaction =False,
                    #has_rate_reactions = True,
                    has_equilibrium_reactions = False,
                    has_heat_transfer = False,
                    finite_elements = finite_elements_setup,
                    transformation_method="dae.finite_difference",
                    transformation_scheme="BACKWARD",
                    has_holdup = False,
                    has_pressure_change = False)


    m.fs.s02 = Arc(source=m.fs.Feed.outlet, destination=m.fs.R101.inlet)
    m.fs.s14 = Arc(source=m.fs.R101.outlet, destination=m.fs.Product.inlet)

    TransformationFactory("network.expand_arcs").apply_to(m)

    print("Initial DOF of whole process is", degrees_of_freedom(m))

    ### define feed
    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2"].fix(options['nH2'])
    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CO2"].fix(options['nCO2']) ## for Fig(2)

    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].fix(options['nH2O'])
    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CH4"].fix(options['nCH4'])

    m.fs.Feed.temperature[0].fix(options['Temperature'])
    m.fs.Feed.pressure.fix(options['Pressure'])

    print("DOF after CO2,H2 feed initilization ", degrees_of_freedom(m))

    ### define process units

    ## reactor
    m.fs.R101.volume.fix(options['Vr'])
    m.fs.R101.length.fix(options['L'])


    print("MISSING DOF ", degrees_of_freedom(m))

    ############################# initizalization ####################################################

    seq = SequentialDecomposition()
    seq.options.select_tear_method = "heuristic"
    seq.options.tear_method = "Wegstein"
    seq.options.iterLim = 5

    # Using the SD tool
    G = seq.create_graph(m)
    heuristic_tear_set = seq.tear_set_arcs(G, method="heuristic")
    order = seq.calculation_order(G)

    for o in heuristic_tear_set:
        print("Tear set: ",o.name)
    for o in order:
        print(o[0].name)

    opt = SolverFactory('ipopt', tee=True)
    opt.options = {"halt_on_ampl_error": 'no',
                   "tol": 0.01}

    ##R101
    if if_tuning:
        if finite_elements_setup ==1:
            ### ##if not, another local optimum
            if options['which_reactor'] == 'R101':
                m.fs.R101.outlet.temperature[0].setlb(550)

    init_file_name = f'init_check_kinetik.json.gz'

    # Import idaes model serializer to store initialized model
    from idaes.core.util import model_serializer as ms

    #if not os.path.exists(init_file_name):
    seq.run(m, function)

    solve_status = opt.solve(m, tee=True, keepfiles=False)

    ms.to_json(m, fname=init_file_name)

    #m.fs.report()
    m.fs.R101.report()

    ###
    ### compare the results with the main case study
    if if_tuning:

        print("=" * 10, 'doing tuning:', "=" * 10)

        ###
        ### a random objective function
        m.fs.R101.conversion_CO2 = Expression(expr=1 - (m.fs.R101.outlet.flow_mol_phase_comp[0, "Vap", "CO2"] /
                                                        m.fs.R101.inlet.flow_mol_phase_comp[0, "Vap", "CO2"]))

        m.fs.objective = Objective(expr=-m.fs.R101.conversion_CO2)

        ###
        ### for R101
        if options['which_reactor'] == 'R101':
            if finite_elements_setup == 1:
                m.fs.tOut_con_1 = Constraint(expr=m.fs.R101.outlet.temperature[0] >= 600)
                m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].unfix()

                m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].setlb(0.32454) #0.32454
                #m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].setub(0.32518)
            if finite_elements_setup == 20:
                m.fs.tOut_con_1 = Constraint(expr=m.fs.R101.outlet.temperature[0] >= 600)
                m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].unfix()
                m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].setlb(0.32516)# 0.32511
                m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].setub(0.32518)

        ###
        ### R102
        if options['which_reactor'] == 'R102':
            m.fs.tOut_con_1 = Constraint(expr=m.fs.R101.outlet.temperature[0] >= 650)
            m.fs.tOut_con_2 = Constraint(expr=m.fs.R101.outlet.temperature[0] <= 700)

            m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].unfix()
            m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].fix(0.6010)
            m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CH4"].unfix()

        results = opt.solve(m, tee=True, keepfiles=False)

    m.fs.R101.report()
    n_finite = []
    n_temp = []

    for i in m.fs.R101.control_volume.length_domain:
        n_finite.append(i)
        n_temp.append(value(m.fs.R101.control_volume.properties[0, i].temperature-273.15))

        print(f'R101 [0,{i}]: ', value(m.fs.R101.control_volume.properties[0, i].temperature))

    return n_finite, n_temp

############################# VALIDATE ####################################################

if __name__ == '__main__':

    ############################# Constant T ####################################################
    ###
    ### call calculate, temperature constant
    ###
    finite_elements_set = [1, 20]

    ###
    ### plot which reactor?
    R101 = True
    R102 = True
    R103 = True
    R104 = True

    plot_data = {}
    ###
    ### constant temperature
    if R101:
        ###
        ### calculate CO2 conversion of implemented Falbo et al. Kinetic

        options = {
            'which_reactor': 'R101',
            'Vr': 1.2,  # [m]
            'L': 3.3,  # [m^3]
            'nCO2':0.85462,
            'nH2': 3.4199,
            'nH2O': 2.4588, #,
            'nCH4': 1.2294,
            'Temperature': 524.18,
            'Pressure': 1.5739e+06
        }
        options_2 = {
            'which_reactor': 'R101',
            'Vr': 1.2,  # [m]
            'L': 3.3,  # [m^3]
            'nCO2': 0.85031,
            'nH2': 3.4053,
            'nH2O': 2.4240,  # ,
            'nCH4': 1.2120,
            'Temperature': 523.16,
            'Pressure': 1.5745e+06
        }

        options_3 = {
            'which_reactor': 'R101',
            'Vr': 1.2,  # [m]
            'L': 3.3,  # [m^3]
            'nCO2': 0.85054,
            'nH2': 3.4060,
            'nH2O': 2.4256,  # ,
            'nCH4': 1.2128,
            'Temperature': 523.16,
            'Pressure': 1.5745e+06
        }

        plot_data = {}
        for i in finite_elements_set:

            x_dis, T_out = get_init(options=options_3, finite_elements_setup=i, if_tuning=False)
            plot_data[i] = {
                'name': 'R1',
                'x': x_dis,
                'y': T_out}
        plot_discretization(plot_data=plot_data, if_tuning=False)


    if R102:
        ###
        ### calculate CO2 conversion of implemented Falbo et al. Kinetic

        options = {
            'which_reactor': 'R102',
            'Vr': 2.3,  # [m]
            'L': 4.2,  # [m^3]
            'nCO2': 0.16115,
            'nH2': 0.39923,
            'nH2O': 0.91372,
            'nCH4': 0.45687,
            'Temperature': 532.42,
            'Pressure': 1.5110e+06
        }
        options_3 = {
            'which_reactor': 'R102',
            'Vr': 2.3,  # [m]
            'L': 4.2,  # [m^3]
            'nCO2': 0.10826,
            'nH2':  0.43406,
            'nH2O': 0.90296,
            'nCH4': 0.45149,
            'Temperature': 529.11,
            'Pressure': 1.5111e+06
        }

        plot_data = {}
        for i in finite_elements_set:
            x_dis, T_out = get_init(options=options_3, finite_elements_setup=i, if_tuning=False)

            plot_data[i] = {
                'name': 'R2',
                'x': x_dis,
                'y': T_out,
            }
        plot_discretization(plot_data=plot_data, if_tuning=False)

    if R103:
        ###
        ### calculate CO2 conversion of implemented Falbo et al. Kinetic

        options = {
            'which_reactor': 'R103',
            'Vr': 0.19,  # [m]
            'L': 1.8,  # [m^3]
            'nCO2': 0.046245,
            'nH2': 0.18535,
            'nH2O': 0.003,
            'nCH4': 0.51368,
            'Temperature':551.32,
            'Pressure':1.4505e+06
        }

        plot_data = {}
        for i in finite_elements_set:
            x_dis, T_out = get_init(options=options, finite_elements_setup=i, if_tuning=False)
            plot_data[i] = {
                'name': 'R3',
                'x': x_dis,
                'y': T_out}
        plot_discretization(plot_data=plot_data)

    if R104:
        options = {
            'which_reactor': 'R104',
            'Vr': 0.19,  # [m]
            'L': 1.8,  # [m^3]
            'nCO2': 0.070994,
            'nH2': 0.038619,
            'nH2O': 0.0020065,
            'nCH4':0.54702,
            'Temperature': 603.21,
            'Pressure': 1.3925e+06
        }

        plot_data = {}
        for i in finite_elements_set:
            x_dis, T_out  = get_init(options=options, finite_elements_setup=i, if_tuning=False)

            plot_data[i]= {
                'name': 'R4',
                'x': x_dis,
                'y':T_out,
            }

        plot_discretization(plot_data=plot_data)

    plt.show()