"""Case study for bridging energy and process system optimization"""
#
#  - Appendix A.3
#  - Figures A.15(a), A.15(b)
#
# This file is part of the ptg-es4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Yifan Wang, Luka Bornemann

###
### import libraries
###
import os
from pyomo.environ import (Constraint,
                           Var,
                           ConcreteModel,
                           Expression,
                           Objective,
                           SolverFactory,
                           TransformationFactory,
                           value, RangeSet, Set)

from idaes.core import FlowsheetBlock, EnergyBalanceType

from pyomo.network import Arc, SequentialDecomposition
from pyomo.environ import units as pyunits

###
### properties, process units and flowsheet
from idaes.models.properties.modular_properties import (GenericParameterBlock, GenericReactionParameterBlock)

#### Importing required thermo and reaction package
import algorithms.bridgeESPS.process_system.methanation.properties_methanation as thermo_props
import algorithms.bridgeESPS.process_system.methanation.reaction_property_Falbo_validate as reaction_props

from idaes.models.unit_models import (PFR, Feed, Product)

###
### tools
from idaes.core.util.model_statistics import degrees_of_freedom

def function(unit):
    unit.initialize(outlvl=idaeslog.INFO)

import idaes.logger as idaeslog

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

###
### setups for latex input
################################################################
###
### This part requires a latex environment on your machine.
### Otherwise you can deactivate this setup.
mpl.rcParams.update({
    #     "pgf.texsystem": "pdflatex",
    #     'font.family': 'serif',
    #     'text.usetex': True,
    #     'pgf.rcfonts': False,
    'text.usetex': True,
    'text.latex.preamble': "\n".join([
        r'\usepackage{amsmath}',
        r'\usepackage{amssymb}',
        r'\usepackage{units}',
        r'\usepackage[version=4]{mhchem}',
        # r'\usepackage{siunitx},'
    ]),
    # 'font.size': 11,
    'font.family': 'lmodern',

})
################################################################
mpl.rcParams.update(mpl.rcParamsDefault)

###
### parameters for case study
###

### prices
k_cool = 5/1000 # €/kWh 10°C coolings water
k_elec = 0.2 # €/kWh electricity

# substance data
mw_H2 = 2.016E-3 # kg/mol
HHV_H2_m = 141.800e3 # kJ/kg
LHV_H2_m = 119.972e3 # kJ/kg
HHV_H2_v = 12.745e3 # kJ/m³
LHV_H2_v = 10.783e3 # kJ/m³
HHV_H2_mol = 141.800e3*mw_H2 # kJ/mol
LHV_H2_mol = 119.972e3*mw_H2 # kJ/mol #241

mw_CH4 = 16.043E-3 # kg/mol
HHV_CH4_m = 55.498e3 # kJ/kg
LHV_CH4_m = 50.013e3 # kJ/kg
HHV_CH4_v = 39.819e3 # kJ/m³
LHV_CH4_v = 35.883e3 # kJ/m³
HHV_CH4_mol = 55.498e3*mw_CH4 # kJ/mol
LHV_CH4_mol = 50.013e3*mw_CH4 # kJ/mol #802


def calc_n_based_on_L(L):
    import math

    L_orig = 0.23
    D_orig = 0.011
    L_D = L_orig / D_orig
    V_orig = L_orig * 0.25*math.pi*D_orig**2
    gCat_orig = 0.375e-3
    rhoCat= gCat_orig/V_orig
    # L = 15
    D = L/L_D
    Vr = L * 0.25*math.pi*D**2
    rho = 1/(22.4e-3)

    gCat = Vr * rhoCat * 1000
    GHSV = 5e-3/3600 # m³/s/gCat
    Vl = GHSV * gCat
    nH2 = Vl * rho / (1.25)
    nCO2 = nH2/4
    return nH2, nCO2, Vr, L

def get_init(options = None):
    ####
    #### construct the flowsheet
    ####
    m = ConcreteModel()
    m.fs = FlowsheetBlock(dynamic = False)

    ###
    ### add the properties
    m.fs.thermo_params_vapor = GenericParameterBlock(**thermo_props.thermo_configuration_vapor)
    m.fs.thermo_params_VLE = GenericParameterBlock(**thermo_props.thermo_configuration_VLE)

    m.fs.reaction_properties_TREMP = reaction_props.MethanationReactionParameterBlock(
        property_package=m.fs.thermo_params_vapor)

    ###
    ### define flowsheet

    m.fs.Feed = Feed(property_package = m.fs.thermo_params_vapor)
    m.fs.Product = Product(property_package = m.fs.thermo_params_vapor)


    finite_elements = 20

    m.fs.R101 = PFR(property_package = m.fs.thermo_params_vapor,
                    reaction_package = m.fs.reaction_properties_TREMP,
                    has_heat_of_reaction =False,
                    #has_rate_reactions = True,
                    has_equilibrium_reactions = False,
                    has_heat_transfer = True,
                    finite_elements = finite_elements,
                    #transformation_method="dae.finite_difference",
                    #transformation_scheme="BACKWARD",
                    has_holdup = False,
                    has_pressure_change = False)


    m.fs.s02 = Arc(source=m.fs.Feed.outlet, destination=m.fs.R101.inlet)
    m.fs.s14 = Arc(source=m.fs.R101.outlet, destination=m.fs.Product.inlet)

    TransformationFactory("network.expand_arcs").apply_to(m)

    print("Initial DOF of whole process is", degrees_of_freedom(m))

    ### define feed
    nH2, nCO2, Vr, L = calc_n_based_on_L(options['L'])  #LB: 15

    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2"].fix(nH2)

    if options['H2_CO2']:
        m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CO2"].fix(nH2/options['H2_CO2'])  # mol/s
    else:
        m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CO2"].fix(nCO2) ## for Fig(2)

    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "H2O"].fix(1e-15)
    m.fs.Feed.flow_mol_phase_comp[0, "Vap", "CH4"].fix(1e-15)

    m.fs.Feed.temperature[0].fix(273.15 + 310)
    m.fs.Feed.pressure.fix(1.01325e5)

    print("DOF after CO2,H2 feed initilization ", degrees_of_freedom(m))

    ### define process units

    ## reactor
    if options['Vr']:
        m.fs.R101.volume.fix(options['Vr'])  # for thermodynamic equilibrium
    else:
        m.fs.R101.volume.fix(Vr)  # Vr

    m.fs.R101.length.fix(L)   #L

    # temperature inside the reactor
    def T_rule(m, i, adiabatic=False):

        if adiabatic == True:
            return m.fs.R101.control_volume.heat[0, i] == 0
        else:
            return m.fs.R101.control_volume.properties[0, i].temperature == m.fs.R101.control_volume.properties[0, 0].temperature

    range = np.around(np.linspace(1/finite_elements,1.0, num=finite_elements,dtype=float), decimals=2)
    m.Range = Set(initialize=range)

    m.T_con = Constraint(m.Range, rule=T_rule)

    Dp = 0.0032*pyunits.m # m , particle diameter ; used in [Fablo,2018]
    eps = 0.6 # bed voidage - hollow cylinder [Elvers 1992]
    rho_particle = 1014 # kg/m³ , particle density
    dvisc_air = 36.7E-6*pyunits.Pa*pyunits.s # Pa*s, Dyn. Viskosität Luft bei 20bar, 500°C

    ## define conversion
    m.fs.R101.conversion_CO2 = Expression(expr= 1 - (m.fs.R101.outlet.flow_mol_phase_comp[0, "Vap", "CO2"]/
                                            m.fs.R101.inlet.flow_mol_phase_comp[0, "Vap", "CO2"]))

    print("MISSING DOF ", degrees_of_freedom(m))

    ############################# initizalization ####################################################

    seq = SequentialDecomposition()
    #seq.options.select_tear_method = "heuristic"
    #seq.options.tear_method = "Wegstein"
    seq.options.iterLim = 5

    # Using the SD tool
    G = seq.create_graph(m)
    #heuristic_tear_set = seq.tear_set_arcs(G, method="heuristic")
    order = seq.calculation_order(G)


    opt = SolverFactory('ipopt', tee=True)
    opt.options = {"halt_on_ampl_error": 'no',
                   "tol": 0.01}

    init_file_name = f'init_check_kinetik.json.gz'

    # Import idaes model serializer to store initialized model
    from idaes.core.util import model_serializer as ms

    #if not os.path.exists(init_file_name):
    seq.run(m, function)
    solve_status = opt.solve(m, tee=True, keepfiles=False)

    ms.to_json(m, fname=init_file_name)

    m.fs.report()
    m.fs.R101.report()

    print("CO2-Conversion is:", value(m.fs.R101.conversion_CO2))

    return m, opt


############################# VALIDATE ####################################################

###
### Fig. A.15(a)
###
def calc_T_conversion(m, opt, temperature):

    conversion_T = []
    r_T = []

    for T in temperature:
        T = T + 273.15
        m.fs.Feed.temperature[0].unfix()
        m.fs.Feed.temperature[0].fix(T)


        solve_status = opt.solve(m, tee=True)

        conversion_T.append(value(m.fs.R101.conversion_CO2) * 100)
        r_T.append(value(m.fs.R101.control_volume.reactions[0, 1].reaction_rate["R1"]))

        m.fs.R101.report()

    print('conversion_T: ', conversion_T)

    return conversion_T

###
### Fig. A.15(b)
###
def calc_p_conversion(m, opt, pressure):

    ###
    ### T1 = 290 °C
    ### T2 = 310 °C
    conversion_p_T1 = []
    conversion_p_T2 = []

    for P in pressure:
        P = P * 1.01325e5
        T = 290 + 273.15
        m.fs.Feed.temperature[0].unfix()
        m.fs.Feed.temperature[0].fix(T)
        m.fs.Feed.pressure[0].unfix()
        m.fs.Feed.pressure[0].fix(P)

        solve_status = opt.solve(m, tee=True)
        conversion_p_T1.append(value(m.fs.R101.conversion_CO2) * 100)
        m.fs.R101.report()

    for P in pressure:
        P = P * 1.01325e5
        T = 310 + 273.15
        m.fs.Feed.temperature[0].unfix()
        m.fs.Feed.temperature[0].fix(T)
        m.fs.Feed.pressure[0].unfix()
        m.fs.Feed.pressure[0].fix(P)
        solve_status = opt.solve(m, tee=True)
        conversion_p_T2.append(value(m.fs.R101.conversion_CO2) * 100)
        m.fs.R101.report()


    print('conversion_T1: ', conversion_p_T1)
    print('conversion_T2: ', conversion_p_T2)

    return conversion_p_T1, conversion_p_T2

def set_size(use=True, fraction=1, subplots=(1, 1)):

    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    width_pt = 422.52347
    height_pt = 635.5
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2


    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    if use==True:
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])
    else:
        fig_height_in =fig_width_in * golden_ratio*1.3 * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)

def plot_T(temperature, conversion_T_equi, conversion_T_kinetic, conversion_T_paper):

    ###
    ### plot figures
    ###

    fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.7, subplots=(1, 1)))

    ax.plot(temperature, conversion_T_equi, 'k--',label=r'Thermodynamic equilibrium')
    ax.plot(temperature, conversion_T_kinetic, 'k-', label=r'Simulation')
    ax.plot(temperature, conversion_T_paper,'ko',label=r'Literature')

    ax.set_xlabel(r'Temperature in °C')
    ax.set_ylabel(r'$\mathrm{CO_2}$ conversion in $\%$')
    ax.set_title(r'p=1 ata; GHSV=5 L(STP)/h/$g_{cat}$;$\lambda_{\mathrm{H_2}/\mathrm{CO_2}}$ = 4')

    ax.axis([250, 410, 0, 100])

    major_ticks_x = np.linspace(temperature[0], temperature[-1], len(temperature))
    minor_ticks_x = np.linspace(temperature[0], temperature[-1], len(temperature)*2-1) #17
    major_ticks_y = np.linspace(0, 100, 11)

    ax.legend(loc='lower right')

    ax.set_xticks(major_ticks_x)
    ax.set_yticks(major_ticks_y)
    ax.set_xticks(minor_ticks_x, minor=True)

    ax.set_xticks([250, 290, 330, 370, 410])
    ax.set_yticks([0, 20, 40, 60, 80, 100])

    # axes[0].set_yticks(minor_ticks_top, minor=True)
    ax.grid(which="major", alpha=0.6)
    ax.grid(which="minor", alpha=0.3)
    ax.grid(visible=True)
    fig.set_tight_layout(True)
    plt.tight_layout()
    #plt.savefig(f'./pictures_appendix/appendix3/Appendix_T_conversion.svg', format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f'./pictures_appendix/appendix3/Appendix_T_conversion.pdf', format='pdf', bbox_inches='tight')
    # plt.savefig('./pictures/T_conversion.png', bbox_inches='tight')
    plt.show()

def plot_P(pressure,conversion_P_equi_T290, conversion_P_equi_T310, conversion_P_T290, conversion_P_T310, conversion_P_paper_T290, conversion_P_paper_T310):

    fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.7, subplots=(1, 1)))

    ax.plot(pressure, conversion_P_equi_T310, 'k--', label=r'Thermodynamic equilibrium')
    ax.plot(pressure, conversion_P_equi_T290, 'k--')
    ax.plot(pressure, conversion_P_T310, 'k-', label=r'Simulation')
    ax.plot(pressure, conversion_P_T290, 'k-')
    ax.plot(pressure, conversion_P_paper_T310, 'ko', label=r'Literature')
    ax.plot(pressure, conversion_P_paper_T290, 'ko')

    ax.set_xlabel('Pressure in ata')
    ax.set_ylabel(r'$\mathrm{CO_2}$ conversion in $\%$')
    ax.set_title(r'GHSV=5 L(STP)/h/$g_{cat}$;$\lambda_{\mathrm{H_2}/\mathrm{CO_2}}$ = 3.9')
    ax.axis([1, 7, 0, 100])
    ax.legend(loc='lower right')

    major_ticks_x = np.linspace(pressure[0], pressure[-1], len(pressure))
    minor_ticks_x = np.linspace(pressure[0], pressure[-1], len(pressure) * 4 - 3)

    major_ticks_y = np.linspace(0, 100, 11)

    ax.set_xticks(major_ticks_x)
    ax.set_yticks(major_ticks_y)
    ax.set_xticks(minor_ticks_x, minor=True)
    ax.set_yticks([0, 20, 40, 60, 80, 100])
    # axes[0].set_yticks(minor_ticks_top, minor=True)
    ax.grid(which="major", alpha=0.6)
    ax.grid(which="minor", alpha=0.3)
    ax.grid(visible=True)
    fig.set_tight_layout(True)

    plt.tight_layout()
    #plt.savefig(f'./pictures_appendix/appendix3/Appendix_P_conversion.svg', format='svg', dpi=1200, bbox_inches='tight')
    plt.savefig(f'./pictures_appendix/appendix3/Appendix_P_conversion.pdf', format='pdf', bbox_inches='tight')
    # plt.savefig('./pictures_appendix/P_conversion.png', bbox_inches='tight')
    plt.show()


if __name__ == '__main__':

    ############################# Constant T ####################################################
    ###
    ### call calculate, temperature constant
    ###

    const_T = True
    ###
    ### constant temperature
    if const_T:
        temperature = [250, 270, 290, 310, 330, 350, 370, 390, 410]

        ###
        ### calculate CO2 conversion at thermodynamic equilibrium
        conversion_T_equi = []
        options = {
            'L': 15,  # [m]
            'Vr': 500  ,  # [m^3]
            'H2_CO2':None
        }
        m, opt = get_init(options=options)
        conversion_T_equi = calc_T_conversion(m, opt, temperature)
        # conversion_T_equi = [97.49872442838394, 96.64352130080371, 95.58773433317216, 94.30782984059095, 92.78215479991462, 90.9920815099481, 88.92317229542041, 86.56638929974807, 83.919289448402]

        ###
        ### calculate CO2 conversion of implemented Falbo et al. Kinetic
        conversion_T_kinetic = []
        options = {
            'L': 15,  # [m]
            'Vr': None  ,  # [m^3]
            'H2_CO2':None
        }

        m, opt = get_init(options=options)
        conversion_T_kinetic = calc_T_conversion(m, opt, temperature)
        # conversion_T_kinetic = [14.606092023060935, 25.658890815048018, 41.09232688437795, 59.10877194868546, 75.31171752868386, 85.00106109485519, 87.59976291894623, 86.38142225649553, 83.90317167243622]

        ###
        ### CO2 conversion from paper
        conversion_T_paper = [14.5, 25.8, 40.9, 60, 77, 86, 86.8, 86.5, 83.9]

        plot_T(temperature, conversion_T_equi, conversion_T_kinetic, conversion_T_paper)

    ############################# Constant P ####################################################
    ###
    ### call calculate, pressure constant
    ###

    const_p = True
    ###
    ### constant pressure
    if const_p:
        pressure = [1,2,3,4,5,6,7] #[bar]

        ###
        ### calculate CO2 conversion at thermodynamic equilibrium
        conversion_P_T290_equi = []
        conversion_P_T310_equi =[]

        options = {
            'L': 15,  # [m]
            'Vr': 500,  # [m^3]
            'H2_CO2':3.9 #[mol/mol]
        }
        m, opt = get_init(options=options)
        conversion_P_T290_equi, conversion_P_T310_equi = calc_p_conversion(m, opt, pressure)
        #conversion_P_T290_equi = [93.58390099890474, 94.59583470062013, 95.0686046011451, 95.35929990524377, 95.56202638400397, 95.71416430340064, 95.83398148913743]
        #conversion_P_T310_equi = [92.34744904000904, 93.65752630491767, 94.27237136712502, 94.6515337657328, 94.91654320116368, 95.11578109889594, 95.2729320401492]

        ###
        ### calculate CO2 conversion of implemented Falbo et al. Kinetic
        conversion_P_T290_kinetic = []
        conversion_P_T310_kinetic = []

        options = {
            'L': 15,  # [m]
            'Vr': None,  # [m^3]
            'H2_CO2':None
        }

        m, opt = get_init(options=options)
        conversion_P_T290_kinetic, conversion_P_T310_kinetic = calc_p_conversion(m, opt, pressure)
        #conversion_P_T290_kinetic = [41.09232688437409, 55.62886407100414, 62.52923143203324, 66.29936378298004, 68.57213578840467, 70.03699303025964, 71.02516894079479]
        #conversion_P_T310_kinetic = [59.10877194868366, 73.21951449693456, 78.68668621164096, 81.37699201928254, 82.89945637998896, 83.83707671956682, 84.4454615908945]
        ###
        ### CO2 conversion from paper
        conversion_P_T290_paper = [40.5, 56, 62, 66.7, 69.2, 70, 70.7]
        conversion_P_T310_paper = [59, 72.5, 79.3, 81.1, 82.9, 83.9, 85]

        plot_P(pressure, conversion_P_T290_equi, conversion_P_T310_equi, conversion_P_T290_kinetic, conversion_P_T310_kinetic, conversion_P_T290_paper, conversion_P_T310_paper)