"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang

###
### import libraries
###

###
### basic enviroment
from pyomo.environ import (Constraint,
                           Var,
                           ConcreteModel,
                           Expression,
                           Objective,
                           SolverFactory,
                           TransformationFactory,
                           value, Set)

from pyomo.network import Arc, SequentialDecomposition
from pyomo.environ import units as pyunits
from idaes.core.util.constants import Constants as const
from idaes.core.util import scaling as iscale
###
### properties, process units and flowsheet
from idaes.models.properties.modular_properties import (GenericParameterBlock, GenericReactionParameterBlock)
from idaes.models.properties import iapws95

#### Importing required thermo and reaction package
import algorithms.bridgeESPS.process_system.methanation.properties_methanation as thermo_props

import algorithms.bridgeESPS.process_system.methanation.reaction_property_Falbo_TREMP as reaction_props

from idaes.models.properties.iapws95 import htpx

from idaes.models.unit_models import (PressureChanger,
                                      Mixer,  MomentumMixingType,
                                      Separator,
                                      Heater,
                                      GibbsReactor, CSTR, EquilibriumReactor, PFR,
                                      Flash,
                                      HeatExchanger,
                                      Feed, Translator,
                                      Product)

from idaes.models.unit_models.pressure_changer import ThermodynamicAssumption

from idaes.core import FlowsheetBlock

from idaes.core.util.model_statistics import degrees_of_freedom, report_statistics
import numpy as np
import math

def dof_comp(comp):
    DOF = degrees_of_freedom(comp)
    print(f'The initial DOF of {comp.name} is {format(DOF)}')
    return

###
### tools
import idaes.logger as idaeslog

###
### parameters of the case-study
###

### prices
k_cool = 5/1000 # [€/kWh] 10°C cooling water
k_elec = 0.2 # [€/kWh} electricity

### substance data
mw_H2 = 2.016E-3 # [kg/mol]
HHV_H2_m = 141.800e3 # [kJ/kg]
LHV_H2_m = 119.972e3 # [kJ/kg]
HHV_H2_v = 12.745e3 # [kJ/m³]
LHV_H2_v = 10.783e3 # [kJ/m³]
HHV_H2_mol = 141.800e3*mw_H2 # [kJ/mol]
LHV_H2_mol = 119.972e3*mw_H2 # [kJ/mol] #241

mw_CH4 = 16.043E-3 # [kg/mol]
HHV_CH4_m = 55.498e3 # [kJ/kg]
LHV_CH4_m = 50.013e3 # [kJ/kg]
HHV_CH4_v = 39.819e3 # [kJ/m³]
LHV_CH4_v = 35.883e3 # [kJ/m³]
HHV_CH4_mol = 55.498e3*mw_CH4 # [kJ/mol]
LHV_CH4_mol = 50.013e3*mw_CH4 # [kJ/mol] #802

rho_n_Luft = 1.293*pyunits.kg/pyunits.m**3 # [kg/m³]


def initialize_fs(options=None):

    ###
    ### Constructing the Flowsheet
    ###
    m = ConcreteModel()
    m.fs = FlowsheetBlock(dynamic=False) #default={"dynamic": False}

    ### Adding the properties
    ###
    m.fs.thermo_params_vapor = GenericParameterBlock(**thermo_props.thermo_configuration_vapor)
    m.fs.thermo_params_VLE = GenericParameterBlock(**thermo_props.thermo_configuration_VLE)

    m.fs.reaction_properties = reaction_props.HDAReactionParameterBlock(property_package=m.fs.thermo_params_vapor)

    m.fs.reaction_properties_TREMP = reaction_props.HDAReactionParameterBlock(property_package=m.fs.thermo_params_vapor)

    # Adding Unit models
    m.fs.properties_steam = iapws95.Iapws95ParameterBlock(phase_presentation=iapws95.PhaseType.MIX, state_vars=iapws95.StateVars.PH)

    ###
    ### define flowsheet
    ###
    m.fs.Feed_CO2 = Feed(property_package = m.fs.thermo_params_vapor)
    m.fs.Feed_H2 = Feed(property_package =  m.fs.thermo_params_vapor)

    m.fs.product = Product(property_package = m.fs.thermo_params_VLE)

    ###
    ### add process units

    m.fs.M101 = Mixer(property_package = m.fs.thermo_params_vapor,
                      inlet_list = ["CO2_feed", "H2_feed"],
                      momentum_mixing_type = MomentumMixingType.minimize)


    m.fs.C101 = PressureChanger(dynamic = False,
                                property_package = m.fs.thermo_params_vapor,
                                compressor = True,
                                thermodynamic_assumption = ThermodynamicAssumption.isentropic)

    HeatingModel = 'Heater'
    m.HeatingModel = HeatingModel
    if HeatingModel == 'Heater':

        m.fs.HEX101 = Heater(property_package = m.fs.thermo_params_vapor,
                             has_pressure_change = False,
                             has_phase_equilibrium = False)

        m.fs.HEX102 = Heater(property_package=m.fs.thermo_params_vapor,
                             has_pressure_change=True,
                             has_phase_equilibrium=False)

        m.fs.HEX103 = Heater(property_package=m.fs.thermo_params_VLE,
                             has_pressure_change=True,
                             has_phase_equilibrium=False)

        m.fs.HEX104 = Heater(property_package=m.fs.thermo_params_vapor,
                             has_pressure_change=False,
                             has_phase_equilibrium=False)

        m.fs.HEX105 = Heater(property_package=m.fs.thermo_params_VLE,
                             has_pressure_change=True,
                             has_phase_equilibrium=False)

        m.fs.HEX106 = Heater(property_package=m.fs.thermo_params_vapor,
                             has_pressure_change=False,
                             has_phase_equilibrium=False)

        m.fs.HEX107 = Heater(property_package=m.fs.thermo_params_VLE,
                             has_pressure_change=True,
                             has_phase_equilibrium=False)

    else:
        print("Check MA_Luka branch.")


    h_in_HEX_20 = htpx((20 + 273.15) * pyunits.K, P=101325 * pyunits.Pa)

    m.fs.M102 = Mixer(property_package = m.fs.thermo_params_vapor,
                      inlet_list = ["educt_feed", "recycle_R101_compr"])

    # Set discretization of reactor models
    finite_elements_1 = 1
    finite_elements_2 = 1
    finite_elements_3 = 1
    finite_elements_4 = 1

    m.fs.R101 = PFR(property_package = m.fs.thermo_params_vapor,
                    reaction_package = m.fs.reaction_properties_TREMP,
                    # "has_heat_of_reaction": False,
                    # "has_rate_reactions": False,
                    has_equilibrium_reactions = False,
                    has_heat_transfer = False,
                    finite_elements = finite_elements_1,
                    has_pressure_change = False)

    m.fs.R102 = PFR(property_package=m.fs.thermo_params_vapor,
                    reaction_package=m.fs.reaction_properties_TREMP,
                    # "has_heat_of_reaction": False,
                    # "has_rate_reactions": False,
                    has_equilibrium_reactions=False,
                    has_heat_transfer=False,
                    finite_elements=finite_elements_2,
                    has_pressure_change=False)

    m.fs.R103 = PFR(property_package=m.fs.thermo_params_vapor,
                    reaction_package=m.fs.reaction_properties_TREMP,
                    # "has_heat_of_reaction": False,
                    # "has_rate_reactions": False,
                    has_equilibrium_reactions=False,
                    has_heat_transfer=False,
                    finite_elements=finite_elements_3,
                    has_pressure_change=False)

    m.fs.R104 = PFR(property_package=m.fs.thermo_params_vapor,
                    reaction_package=m.fs.reaction_properties_TREMP,
                    # "has_heat_of_reaction": False,
                    # "has_rate_reactions": False,
                    has_equilibrium_reactions=False,
                    has_heat_transfer=False,
                    finite_elements=finite_elements_4,
                    has_pressure_change=False)

    m.fs.S101 = Separator(property_package= m.fs.thermo_params_vapor,
                          ideal_separation = False,
                          outlet_list = ["product", "recycle_R101"])

    #
    m.fs.C102 = PressureChanger(property_package = m.fs.thermo_params_vapor,
                                compressor = True,
                                thermodynamic_assumption = ThermodynamicAssumption.isentropic)


    m.fs.F101 = Flash(property_package = m.fs.thermo_params_VLE, has_heat_transfer = False, has_pressure_change=False)

    ###
    ### change property model
    m.fs.translator1 = Translator(inlet_property_package=m.fs.thermo_params_vapor,
                                  outlet_property_package=m.fs.thermo_params_VLE)

    m.fs.translator2 = Translator(inlet_property_package=m.fs.thermo_params_VLE,
                                  outlet_property_package=m.fs.thermo_params_vapor)

    m.fs.translator3 = Translator(inlet_property_package=m.fs.thermo_params_vapor,
                                  outlet_property_package=m.fs.thermo_params_VLE)

    m.fs.translator4= Translator(inlet_property_package=m.fs.thermo_params_VLE,
                                  outlet_property_package=m.fs.thermo_params_vapor)
    m.fs.translator5 = Translator(inlet_property_package=m.fs.thermo_params_vapor,
                                  outlet_property_package=m.fs.thermo_params_VLE)

    for t in [m.fs.translator1, m.fs.translator2, m.fs.translator3, m.fs.translator4, m.fs.translator5]:

        ### vaper phase
        t.eq_H2_flow_vap = Constraint(
            expr=t.outlet.flow_mol_phase_comp[0, "Vap", "H2"] ==
            t.inlet.flow_mol_phase_comp[0, "Vap", "H2"])

        t.eq_CO2_flow_vap = Constraint(
            expr=t.outlet.flow_mol_phase_comp[0, "Vap", "CO2"] ==
            t.inlet.flow_mol_phase_comp[0, "Vap", "CO2"])

        t.eq_H2O_flow_vap = Constraint(
            expr=t.outlet.flow_mol_phase_comp[0, "Vap", "H2O"] ==
            t.inlet.flow_mol_phase_comp[0, "Vap", "H2O"])

        t.eq_CH4_flow_vap = Constraint(
            expr=t.outlet.flow_mol_phase_comp[0, "Vap", "CH4"] ==
            t.inlet.flow_mol_phase_comp[0, "Vap", "CH4"])

        ###Liquid phase

        ### General
        t.eq_temperature = Constraint(
            expr=t.outlet.temperature[0] ==
            t.inlet.temperature[0])

        t.eq_pressure = Constraint(
            expr=t.outlet.pressure[0] ==
            t.inlet.pressure[0])

    for t in [m.fs.translator1, m.fs.translator3, m.fs.translator5]:
        t.eq_H2O_flow_liq = Constraint(expr=t.outlet.flow_mol_phase_comp[0, "Liq", "H2O"] == 1e-5)

    if HeatingModel == 'Heater':
        m.fs.s01 = Arc(source=m.fs.Feed_H2.outlet, destination=m.fs.M101.H2_feed)
        m.fs.s02 = Arc(source=m.fs.Feed_CO2.outlet, destination=m.fs.M101.CO2_feed)
        m.fs.s03 = Arc(source=m.fs.M101.outlet, destination=m.fs.C101.inlet)
        m.fs.s04 = Arc(source=m.fs.C101.outlet, destination=m.fs.HEX101.inlet)
        m.fs.s05 = Arc(source=m.fs.HEX101.outlet, destination=m.fs.M102.educt_feed)
        m.fs.s06 = Arc(source=m.fs.M102.outlet, destination=m.fs.R101.inlet)
        m.fs.s07 = Arc(source=m.fs.R101.outlet, destination=m.fs.HEX102.inlet)
        m.fs.s08 = Arc(source=m.fs.HEX102.outlet, destination=m.fs.S101.inlet)
        m.fs.s09 = Arc(source=m.fs.S101.recycle_R101, destination=m.fs.C102.inlet)
        m.fs.s10 = Arc(source=m.fs.C102.outlet, destination=m.fs.M102.recycle_R101_compr)
        m.fs.s11 = Arc(source=m.fs.S101.product, destination=m.fs.R102.inlet)
        m.fs.s12 = Arc(source=m.fs.R102.outlet, destination=m.fs.translator1.inlet)

        m.fs.s131 = Arc(source=m.fs.translator1.outlet, destination=m.fs.HEX103.inlet)
        m.fs.s132 = Arc(source=m.fs.HEX103.outlet, destination=m.fs.translator2.inlet)

        m.fs.s15 = Arc(source=m.fs.translator2.outlet, destination=m.fs.HEX104.inlet)
        m.fs.s16 = Arc(source=m.fs.HEX104.outlet, destination=m.fs.R103.inlet)
        m.fs.s17 = Arc(source=m.fs.R103.outlet, destination=m.fs.translator3.inlet)
        m.fs.s18 = Arc(source=m.fs.translator3.outlet, destination=m.fs.HEX105.inlet)
        m.fs.s181 = Arc(source=m.fs.HEX105.outlet, destination=m.fs.translator4.inlet)
        m.fs.s19 = Arc(source=m.fs.translator4.outlet, destination=m.fs.HEX106.inlet)
        m.fs.s20 = Arc(source=m.fs.HEX106.outlet, destination=m.fs.R104.inlet)
        m.fs.s21 = Arc(source=m.fs.R104.outlet, destination=m.fs.translator5.inlet)
        m.fs.s22 = Arc(source=m.fs.translator5.outlet, destination=m.fs.HEX107.inlet)
        m.fs.s23 = Arc(source=m.fs.HEX107.outlet, destination=m.fs.F101.inlet)
        m.fs.s24 = Arc(source=m.fs.F101.vap_outlet, destination=m.fs.product.inlet)
    TransformationFactory("network.expand_arcs").apply_to(m)

    print("Initial DOF of whole process is", degrees_of_freedom(m))

    ####
    #### visualization
    ####
    # m.fs.visualize('methanation_flowsheet', loop_forever=True)

    ####
    ####  Fixing feed conditions
    ####
    # CO2 feed
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].fix(2.5 / 4)  # mol/s
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "H2"].fix(1e-5)
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "H2O"].fix(1e-5)
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CH4"].fix(1e-5)

    m.fs.Feed_CO2.temperature[0].fix(298.15)
    m.fs.Feed_CO2.pressure.fix(1e5)

    # H2 feed
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "CO2"].fix(1e-5)
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].fix(2.5)
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2O"].fix(1e-5)
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "CH4"].fix(1e-5)

    m.fs.Feed_H2.temperature[0].fix(298.15)
    m.fs.Feed_H2.pressure.fix(1e5)

    T_101 = 250 + 273.15
    T_102 = 250 + 273.15
    T_103 = 250 + 273.15
    T_104 = 250 + 273.15
    m.fs.R101.volume.fix(1.2)
    m.fs.R101.length.fix(3.3)
    m.fs.R102.volume.fix(2.3)
    m.fs.R102.length.fix(4.2)
    m.fs.R103.volume.fix(0.19)
    m.fs.R103.length.fix(1.8)
    m.fs.R104.volume.fix(0.19)
    m.fs.R104.length.fix(1.8)

    print("DOF after CO2,H2 feed initilization ", degrees_of_freedom(m))

    ###
    ### Fixing unit model specifications

    ### Compressor
    m.fs.C101.outlet.pressure.fix(10e5)  # 8e5 = 8bar
    m.fs.C101.efficiency_isentropic.fix(0.9)

    m.fs.C102.outlet.pressure.fix(10e5)
    m.fs.C102.efficiency_isentropic.fix(0.9)

    ### Heaters
    if HeatingModel == 'Heater':
        T_inlet = 250  # °C
        m.fs.HEX101.outlet.temperature[:].fix(T_101)
        m.fs.HEX102.outlet.temperature[:].fix(T_102)
        m.fs.HEX102.deltaP.fix(0)
        # m.fs.HEX103.outlet.temperature[:].fix(80 + 273.15)
        m.fs.HEX103.outlet.temperature[:].fix(30 + 273.15)
        m.fs.HEX103.deltaP.fix(0)
        m.fs.HEX104.outlet.temperature[:].fix(T_103)

        m.fs.HEX105.outlet.temperature[:].fix(30 + 273.15)
        m.fs.HEX105.deltaP.fix(0)
        m.fs.HEX106.outlet.temperature[:].fix(T_104)
        m.fs.HEX107.outlet.temperature[:].fix(30 + 273.15)
        m.fs.HEX107.deltaP.fix(0)

    m.fs.S101.split_fraction[0, "recycle_R101"].fix(0.7)  # 50% Recycling ratio

    ### Expressions
    m.fs.k_cool = Var(initialize=k_cool, bounds=(0, 1))
    m.fs.k_heat = Var(initialize=2 * k_cool, bounds=(0, 1))
    m.fs.k_cool.fix(k_cool)
    m.fs.k_heat.fix(2 * k_cool)

    if HeatingModel == 'Heater':
        m.fs.cooling_demand = Expression(expr=-m.fs.HEX101.heat_duty[0] - m.fs.HEX102.heat_duty[0]
                                              - m.fs.HEX103.heat_duty[0] - m.fs.HEX105.heat_duty[0]
                                              - m.fs.HEX107.heat_duty[0])  # J/s = W
        m.fs.heating_demand = Expression(expr=m.fs.HEX104.heat_duty[0] + m.fs.HEX106.heat_duty[0])
        m.fs.heating_costs = Expression(expr=1 / 1000 * 1 * (m.fs.k_heat * m.fs.heating_demand))


    m.fs.cooling_costs = Expression(expr=1 / 1000 * 1 * (m.fs.k_cool * m.fs.cooling_demand))  # €/h

    m.fs.k_el = Var(initialize=0.2, bounds=(0, 1))
    m.fs.k_el.fix(k_elec)
    m.fs.electricity_demand = Expression(expr=m.fs.C101.work_mechanical[0] + m.fs.C102.work_mechanical[0])
    m.fs.electricity_costs = Expression(expr=1 / 1000 * 1 * (m.fs.k_el * m.fs.electricity_demand))  # €/h
    # relative Dichte
    T_n = 273.15 * pyunits.K
    P_n = 1.013E5 * pyunits.Pa

    m.fs.Z = Expression(expr=m.fs.product.properties[0].pressure / m.fs.product.properties[0].dens_mol /
                             m.fs.product.properties[0].temperature / const.gas_constant)
    m.fs.zustZahl = Expression(
        expr=T_n * m.fs.product.properties[0].pressure / (m.fs.product.properties[0].temperature *
                                                          P_n * m.fs.Z))

    m.fs.density_mass_n = Expression(expr=P_n * m.fs.product.properties[0].mw / const.gas_constant / T_n)
    m.fs.density_rel = Expression(expr=m.fs.density_mass_n / rho_n_Luft)


    m.fs.operating_cost = Expression(expr=m.fs.cooling_costs + m.fs.electricity_costs + m.fs.heating_costs)  # €/h


    ############################# SIMULATION ####################################################

    print("Missing DOF ", degrees_of_freedom(m))

    seq = SequentialDecomposition()
    seq.options.select_tear_method = "heuristic"
    seq.options.tear_method = "Wegstein"
    seq.options.iterLim = 10

    # Using the SD tool
    G = seq.create_graph(m)
    heuristic_tear_set = seq.tear_set_arcs(G, method="heuristic")
    order = seq.calculation_order(G)

    for o in heuristic_tear_set:
        print("Tear set: ", o.name)
    for o in order:
        print(o[0].name)

        # Set tear_guesses

    tear_guesses_s06 = {
        "flow_mol_phase_comp": {
            (0, "Vap", "CH4"): 1.15,  # 20
            (0, "Vap", "H2O"): 2.3,  # 40
            (0, "Vap", "H2"): 3.71,  # 119
            (0, "Vap", "CO2"): 0.93},  # 30
        "temperature": {0: 523},
        "pressure": {0: 10e5}}

    tear_guesses_s13 = {
        "flow_mol_phase_comp": {
            (0, "Vap", "CH4"): 7.5,
            (0, "Vap", "H2O"): 15,
            (0, "Vap", "H2"): 10,
            (0, "Vap", "CO2"): 2.5,
            (0, "Liq", "H2O"): 0},

        "temperature": {0: 700},
        "pressure": {0: 5e5}}

    tear_guesses_s08 = {
        "flow_mol_phase_comp": {
            (0, "Vap", "CO2"): 0.29873,
            (0, "Vap", "H2"): 1.1948,
            (0, "Vap", "CH4"): 0.95135,
            (0, "Vap", "H2O"): 1.9026,
            (0, "Liq", "H2O"): 0},
        "temperature": {0: 573.15},
        "pressure": {0: 30e5}}
    tear_guesses_s132 = {
        "flow_mol_phase_comp": {
            (0, "Vap", "CO2"): 0.044204,
            (0, "Vap", "H2"): 0.17678,
            (0, "Vap", "CH4"): 0.45583,
            (0, "Vap", "H2O"): 0.012321,
            (0, "Liq", "H2O"): 0.89932},
        "temperature": {0: 353.15},
        "pressure": {0: 30e5}}

    seq.set_guesses_for(m.fs.R101.inlet, tear_guesses_s06)
    # seq.set_guesses_for(m.fs.translator1.inlet, tear_guesses_s13)
    # seq.set_guesses_for(m.fs.S101.inlet, tear_guesses_s08)
    # seq.set_guesses_for(m.fs.F102.inlet, tear_guesses_s132)

    def function(unit):
        unit.initialize(outlvl=idaeslog.INFO)

    # Import python path
    import os

    # Import idaes model serializer to store initialized model
    from idaes.core.util import model_serializer as ms

    opt = SolverFactory('ipopt', tee=True)
    opt.options = {'tol': 0.001,
                   'max_iter': 5000,
                   "constr_viol_tol": 1e-2,
                   "compl_inf_tol": 1e-2,
                   }
    ###
    ### check
    ###
    file_exists = True
    # file_exists = os.path.exists(init_file_name)
    if not file_exists:  # should be 'if not' to load previous initialization, but only 'if' for testing purpose
        seq.run(m, function)

        # Solve the simulation using ipopt
        # Note: If the degrees of freedom = 0, we have a square problem
        solve_status = opt.solve(m, tee=True, keepfiles=False, logfile="reaction_test.log")

        # assert solve_status.solver.termination_condition == TerminationCondition.optimal
        # assert solve_status.solver.status == SolverStatus.ok
        print("Termination condition: ", solve_status.solver.termination_condition)
        print(solve_status.solver.status)

        init_file_name = './initialization/just4test.json.gz'
        ms.to_json(m, fname=init_file_name)
    else:
        # Load previous optimiation results for initialization
        init_file_name = f'{options["init_folder"]}/init_fs_500_95_TREMP_paper.json.gz'
        ms.from_json(m, fname=init_file_name)


    #### can be calculate
    # solve_status = opt.solve(m, tee=True)
    # init_file_name_test = './initialization/new_version_test.json.gz'
    # ms.to_json(m, fname=init_file_name_test)


    m.fs.report()
    m.fs.R101.report()
    m.fs.R102.report()
    m.fs.R103.report()
    m.fs.R104.report()
    y_H2 = []
    y_CO2 = []
    y_H2O = []
    y_CH4 = []

    for R in [m.fs.R101.outlet.flow_mol_phase_comp,
              m.fs.R102.outlet.flow_mol_phase_comp,
              m.fs.R103.outlet.flow_mol_phase_comp,
              m.fs.R104.outlet.flow_mol_phase_comp,
              m.fs.product.flow_mol_phase_comp
              ]:
        y_H2.append(value(R[0, "Vap", "H2"]) / (
                value(R[0, "Vap", "H2"]) +
                value(R[0, "Vap", "H2O"]) +
                value(R[0, "Vap", "CO2"]) +
                value(R[0, "Vap", "CH4"])
        ))
        y_H2O.append(value(R[0, "Vap", "H2O"]) / (
                value(R[0, "Vap", "H2"]) +
                value(R[0, "Vap", "H2O"]) +
                value(R[0, "Vap", "CO2"]) +
                value(R[0, "Vap", "CH4"])
        ))
        y_CO2.append(value(R[0, "Vap", "CO2"]) / (
                value(R[0, "Vap", "H2"]) +
                value(R[0, "Vap", "H2O"]) +
                value(R[0, "Vap", "CO2"]) +
                value(R[0, "Vap", "CH4"])
        ))
        y_CH4.append(value(R[0, "Vap", "CH4"]) / (
                value(R[0, "Vap", "H2"]) +
                value(R[0, "Vap", "H2O"]) +
                value(R[0, "Vap", "CO2"]) +
                value(R[0, "Vap", "CH4"])
        ))
    # Rec = value(m.fs.S101.split_fraction[0, "recycle_R101"])/value(m.fs.S101.split_fraction[0, "product"])
    # print("REC: ", Rec)
    print("Recycling: ", value(m.fs.S101.split_fraction[0, "recycle_R101"]))
    print("y_H2: ", y_H2)
    print("y_H2O: ", y_H2O)
    print("y_CH4: ", y_CH4)
    print("y_CO2: ", y_CO2)
    print("Operating costs: ", value(m.fs.operating_cost))
    print("Reaktionsgeschw. R101: ", value(m.fs.R101.control_volume.reactions[0, 1].reaction_rate["R1"]))
    print("Reaktionsgeschw. R102: ", value(m.fs.R102.control_volume.reactions[0, 1].reaction_rate["R1"]))
    print("Reaktionsgeschw. R103: ", value(m.fs.R103.control_volume.reactions[0, 1].reaction_rate["R1"]))
    print("Reaktionsgeschw. R104: ", value(m.fs.R104.control_volume.reactions[0, 1].reaction_rate["R1"]))

    return m

def prepare_optimization(m):
    #####################################################################################################
    # process system optimization #
    ###################################################################################################

    # Unfix parameter
    if m.HeatingModel == 'Heater':
        m.fs.HEX101.outlet.temperature.unfix() # temperatures are constrained by reactor bounds below
        m.fs.HEX102.outlet.temperature.unfix()
        m.fs.HEX103.outlet.temperature.unfix()
        m.fs.HEX104.outlet.temperature[0].unfix()
        m.fs.HEX105.outlet.temperature.unfix()
        m.fs.HEX106.outlet.temperature.unfix()
        m.fs.HEX107.outlet.temperature.unfix()

        m.fs.HEX103.outlet.temperature[0].setlb(30 + 273.15)
        m.fs.HEX105.outlet.temperature[0].setlb(30 + 273.15)
        m.fs.HEX107.outlet.temperature[0].setlb(30 + 273.15)

        m.fs.HEX101.heat_duty.setub(0) # only cooling
        m.fs.HEX102.heat_duty.setub(0)
        m.fs.HEX103.heat_duty.setub(0)
        m.fs.HEX104.heat_duty.setlb(0)
        m.fs.HEX105.heat_duty.setub(0)
        m.fs.HEX106.heat_duty.setlb(0)
        m.fs.HEX107.heat_duty.setub(0)

        m.fs.HEX102.deltaP.unfix()
        m.fs.HEX103.deltaP.unfix()
        m.fs.HEX105.deltaP.unfix()
        m.fs.HEX107.deltaP.unfix()

        pressure_drop = 4 /100
        m.fs.HEX102_Pcon = Constraint(expr= m.fs.HEX102.deltaP[0] ==
                                            (-pressure_drop * m.fs.HEX102.control_volume.properties_in[0.0].pressure))
        m.fs.HEX103_Pcon = Constraint(expr=m.fs.HEX103.deltaP[0] ==
                                           (-pressure_drop * m.fs.HEX103.control_volume.properties_in[0.0].pressure))
        m.fs.HEX105_Pcon = Constraint(expr=m.fs.HEX105.deltaP[0] ==
                                           (-pressure_drop * m.fs.HEX105.control_volume.properties_in[0.0].pressure))
        m.fs.HEX107_Pcon = Constraint(expr=m.fs.HEX107.deltaP[0] ==
                                           (-pressure_drop * m.fs.HEX107.control_volume.properties_in[0.0].pressure))

        # m.fs.HEX104.area.fix(0.10170) # von vorheriger Optimierung als Ergebnis von 500kW mit max(CH4 Purity)
        # m.fs.HEX104.area.unfix()

    elif m.HeatingModel == 'HeatExchanger':
        #TODO: GGf. Vapor Fraction begrenzen
        #TODO: Ggf. Wassertemp begrenzen

        ### Area wurde über Optimierung min(OPEX + HEX.AREA) für >0.95, p [1,30], rec [0,0.7]
        m.fs.HEX101.hot_outlet.temperature[0].unfix()
        m.fs.HEX101.cold_inlet.flow_mol[0].unfix()
        m.fs.HEX101.cold_inlet.flow_mol[0].setlb(0)
        m.fs.HEX101.cold_inlet.flow_mol[0].setub(40)
        m.fs.HEX101.cold_outlet.enth_mol[0].setlb(htpx(T=(60 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX101.cold_outlet.enth_mol[0].setub(htpx(T=(85 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX101.area.unfix()
        # m.fs.HEX101.area.setlb(0)
        # m.fs.HEX101.area.setub(20)
        m.fs.HEX101.area.fix(0.2906589114009462)

        m.fs.HEX102.hot_outlet.temperature[0].unfix()
        m.fs.HEX102.cold_outlet.enth_mol[0].setlb(htpx(T=(60 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX102.cold_outlet.enth_mol[0].setub(htpx(T=(85 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX102.cold_outlet.temperature[0].setlb((60 + 273.15))
        # m.fs.HEX102.cold_outlet.temperature[0].setub((99 + 273.15))
        m.fs.HEX102.cold_inlet.flow_mol[0].unfix()
        m.fs.HEX102.cold_inlet.flow_mol[0].setlb(0)
        m.fs.HEX102.cold_inlet.flow_mol[0].setub(40)
        # m.fs.HEX102.area.unfix()
        # m.fs.HEX102.area.setlb(0)
        # m.fs.HEX102.area.setub(20)
        m.fs.HEX102.area.fix(1.469342190024854)

        m.fs.HEX103.hot_outlet.temperature[0].unfix()
        m.fs.HEX103.hot_outlet.temperature[0].setub(200+273.15)
        m.fs.HEX103.cold_outlet.enth_mol.setlb(htpx((60 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        m.fs.HEX103.cold_outlet.enth_mol.setub(htpx((85 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX103.cold_outlet.temperature[0].setlb((60 + 273.15))
        # m.fs.HEX103.cold_outlet.temperature[0].setub((80 + 273.15))
        m.fs.HEX103.cold_inlet.flow_mol[0].unfix()
        m.fs.HEX103.cold_inlet.flow_mol[0].setlb(0)
        m.fs.HEX103.cold_inlet.flow_mol[0].setub(40)
        # m.fs.HEX103.area.unfix()
        # m.fs.HEX103.area.setlb(0)
        # m.fs.HEX103.area.setub(10)
        m.fs.HEX103.area.fix(0.8096002718009929)

        m.fs.HEX104.cold_outlet.temperature[0].unfix()

        m.fs.HEX105.hot_outlet.temperature[0].unfix()
        m.fs.HEX105.hot_outlet.temperature[0].setub(200+273.15)
        m.fs.HEX105.cold_outlet.enth_mol.setlb(htpx((60 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        m.fs.HEX105.cold_outlet.enth_mol.setub(htpx((85 + 273.15) * pyunits.K, P=101325 * pyunits.Pa))
        # m.fs.HEX105.cold_outlet.temperature[0].setlb((60 + 273.15))
        # m.fs.HEX105.cold_outlet.temperature[0].setub((80 + 273.15))
        m.fs.HEX105.cold_inlet.flow_mol[0].unfix()
        m.fs.HEX105.cold_inlet.flow_mol[0].setlb(0)
        m.fs.HEX105.cold_inlet.flow_mol[0].setub(20)
        # m.fs.HEX105.area.unfix()
        # m.fs.HEX105.area.setlb(0)
        # m.fs.HEX105.area.setub(10)
        m.fs.HEX105.area.fix(0.219867535226013)

        m.fs.HEX101.heat_duty.setlb(0) # only cooling
        m.fs.HEX102.heat_duty.setlb(0)
        m.fs.HEX103.heat_duty.setlb(0)
        m.fs.HEX104.heat_duty.setlb(0)
        m.fs.HEX105.heat_duty.setlb(0)

        m.fs.HEX104.area.fix(0.10170) # von vorheriger Optimierung als Ergebnis von 500kW mit min(Betriebskosten) bei p=30bar
        # m.fs.HEX104.area.unfix()

    m.fs.C101.outlet.pressure.unfix()
    m.fs.C102.outlet.pressure.unfix()

    m.fs.S101.split_fraction[0, "recycle_R101"].unfix()

    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].unfix()
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].unfix()

    # pressure drop
    Dp = 0.0032*pyunits.m # m , particle diameter ; used in [Fablo,2018]
    eps = 0.6 # bed voidage - hollow cylinder [Elvers 1992]
    rho_particle = 1014 # kg/m³ , particle density
    dvisc_air = 36.7E-6*pyunits.Pa*pyunits.s # Pa*s, Dyn. Viskosität Luft bei 20bar, 500°C
    # Set Reactor constraints
    for R in [m.fs.R101, m.fs.R102, m.fs.R103, m.fs.R104]:
        # R.deltaP.unfix()
        R.inlet.temperature.unfix()
        R.outlet.temperature.unfix()
        R.inlet.temperature.setlb(250 + 273.15)
        R.outlet.temperature.setub(550 + 273.15)

    range = np.around(np.linspace(0.0,1.0, num=11,dtype=float), decimals=2)
    m.Range = Set(initialize=range)
    f1 = Expression(expr=150*dvisc_air*(1-eps)**2/(eps**3*Dp**2))

    def p_ruleR101(m, i):
        return m.fs.R101.deltaP[0,i] == -(f1*(m.fs.R101.control_volume.properties[0,i].flow_mol/
                                  (m.fs.R101.control_volume.properties[0,i].dens_mol*m.fs.R101.area))
                                   +(1.75*m.fs.R101.control_volume.properties[0,i].dens_mass*(1-eps)/eps**3/Dp)*
                                       (m.fs.R101.control_volume.properties[0, i].flow_mol /
                                         (m.fs.R101.control_volume.properties[0, i].dens_mol * m.fs.R101.area))**2)
    def p_ruleR102(m, i):
        return m.fs.R102.deltaP[0,i] == -(f1*(m.fs.R102.control_volume.properties[0,i].flow_mol/
                                  (m.fs.R102.control_volume.properties[0,i].dens_mol*m.fs.R102.area))
                                   +(1.75*m.fs.R102.control_volume.properties[0,i].dens_mass*(1-eps)/eps**3/Dp)*
                                       (m.fs.R102.control_volume.properties[0, i].flow_mol /
                                         (m.fs.R102.control_volume.properties[0, i].dens_mol * m.fs.R102.area))**2)
    def p_ruleR103(m, i):
        return m.fs.R103.deltaP[0,i] == -(f1*(m.fs.R103.control_volume.properties[0,0].flow_mol/
                                  (m.fs.R103.control_volume.properties[0,0].dens_mol*m.fs.R103.area))
                                   +(1.75*m.fs.R103.control_volume.properties[0,0].dens_mass*(1-eps)/eps**3/Dp)*
                                       (m.fs.R103.control_volume.properties[0, 0].flow_mol /
                                         (m.fs.R103.control_volume.properties[0, 0].dens_mol * m.fs.R103.area))**2)

    # m.dP_conR101 = Constraint(m.Range, rule=p_ruleR101)
    # m.dP_conR102 = Constraint(m.Range, rule=p_ruleR102)
    # m.dP_conR103 = Constraint(m.Range, rule=p_ruleR103)
    # m.fs.R101.deltaP.fix(0) #-70 Pa/m
    # m.fs.R102.deltaP.fix(0) #-5 Pa/m
    # m.fs.R103.deltaP.fix(0) # -15 Pa/m
    # m.fs.R104.deltaP.fix(0) # -10 Pa/m


    # Pressure constraints
    m.fs.C101.outlet.pressure.setlb(1e5)
    m.fs.C101.outlet.pressure.setub(20e5)

    m.fs.C102.outlet.pressure.setlb(1e5)
    m.fs.C102.outlet.pressure.setub(20e5)

    # Equal pressure
    m.fs.pressure_cons = Constraint(expr=m.fs.C102.outlet.pressure[0] == m.fs.C101.outlet.pressure[0])

    # Feed constraints
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].setlb(1e-5)
    m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"].setub(10)


    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].setlb(1e-5)
    m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"].setub(10)
    # 4 H2 + 1 CO2 ->..
    # m.fs.Feed_cons = Constraint(expr=m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"] ==
    #                                  4 * m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"])
    m.fs.H2_CO2_ratio = Var(initialize=4, bounds=(3.8, 4.2))
    m.fs.H2_CO2_ratio_con = Constraint(expr=m.fs.H2_CO2_ratio*m.fs.Feed_CO2.flow_mol_phase_comp[0, "Vap", "CO2"]
                                       == m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"])

    # Product quality
    # m.fs.CH4_purity = Var(initialize=0.75, bounds=(0, 1))
    m.fs.CH4_purity = Expression(expr=
                            (m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"]/ (
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2O"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CO2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"])))

    # m.fs.F101.H2_purity = Var(initialize=0.75, bounds=(0, 1))
    m.fs.H2_purity = Expression(expr=
                            (m.fs.product.flow_mol_phase_comp[0, "Vap", "H2"]/ (
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2O"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CO2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"])))

    # m.fs.F101.CO2_purity = Var(initialize=0.75, bounds=(0, 1))
    m.fs.CO2_purity = Expression(expr=
                            (m.fs.product.flow_mol_phase_comp[0, "Vap", "CO2"]/ (
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "H2O"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CO2"] +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"])))

    # Higher and Lower heating value calculations to use e.g. as constraints regarding product quality
    # m.fs.F101.LHV_v = Var(initialize=10, bounds=(0, 100))
    m.fs.LHV_v = Expression(
        expr=  (m.fs.H2_purity * LHV_H2_v + m.fs.CH4_purity * LHV_CH4_v)/3600) # kWh/m³

    # m.fs.F101.HHV_v = Var(initialize=10, bounds=(0, 100))
    m.fs.HHV_v = Expression(
        expr= (m.fs.H2_purity * HHV_H2_v + m.fs.CH4_purity * HHV_CH4_v)/3600) # kWh/m³

    # m.fs.F101.LHV_mol = Var(initialize=10, bounds=(0, 100))
    m.fs.LHV_mol = Expression(
        expr= (m.fs.H2_purity * LHV_H2_mol + m.fs.CH4_purity * LHV_CH4_mol)) # kJ/mol

    # m.fs.F101.HHV_mol = Var(initialize=10, bounds=(0, 100))
    m.fs.HHV_mol = Expression(
        expr=(m.fs.H2_purity * HHV_H2_mol + m.fs.CH4_purity * HHV_CH4_mol)) # kJ/mol

    # Wobbe-Index
    m.fs.Wobbe_o = Expression(expr=m.fs.HHV_v/m.fs.density_rel**0.5)
    m.fs.Wobbe_u = Expression(expr=m.fs.LHV_v/m.fs.density_rel**0.5)
    #TODO: Include H2?

    # Efficiency calculations
    # m.fs.etha = Var(initialize=0.75, bounds=(0, 1))
    m.fs.etha_chem = Expression(
        expr=(m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"] * HHV_CH4_mol) /(
                            m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"] * HHV_H2_mol))

    m.fs.etha_energy = Expression(
        expr=(m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"] * HHV_CH4_mol) /
             (m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"] * HHV_H2_mol + m.fs.electricity_demand/1000
                           ))

    #TODO: Check if HHV or LHV
    m.fs.energy_in = Expression(expr=m.fs.Feed_H2.flow_mol_phase_comp[0, "Vap", "H2"] * HHV_H2_mol)
    # Energy of product stream
    m.fs.energy_out = Var(initialize=500, bounds=(0, 1000))
    m.fs.energy_out_cons = Constraint(
        expr=(m.fs.energy_out == m.fs.product.flow_mol_phase_comp[0, "Vap", "H2"] * HHV_H2_mol +
                            m.fs.product.flow_mol_phase_comp[0, "Vap", "CH4"] * HHV_CH4_mol))

    # SNG costs
    # del m.fs.operating_cost
    m.fs.operating_cost_neu = Expression(expr=m.fs.electricity_costs + 1/1000 * 1 *m.fs.k_cool*(m.fs.cooling_demand - m.fs.heating_demand)) # €/h
    # m.fs.operating_cost_obj = Expression(expr=m.fs.cooling_costs + m.fs.electricity_costs + m.fs.heating_costs)
    m.fs.sng_cost = Expression(expr=m.fs.operating_cost_neu/m.fs.energy_out*1)

    # Product quality constraints
    # m.fs.H2_purity_con2 = Constraint(expr= m.fs.H2_purity <= 0.20)
    m.fs.CH4_purity_con2 = Constraint(expr= m.fs.CH4_purity >= 0.95) # max: 0.9653757706392263 bei 30bar
    m.fs.energy_out.fix(500)

    # Recycling stream definition
    # m.fs.Rec = Var(initialize=2, bounds=(0, 4))
    # m.fs.Rec_cons = Constraint(expr= m.fs.Rec*(1-m.fs.S101.split_fraction[0, "recycle_R101"]) == (m.fs.S101.split_fraction[0, "recycle_R101"]))
    m.fs.S101.split_fraction[0, "recycle_R101"].setlb(0.)
    m.fs.S101.split_fraction[0, "recycle_R101"].setub(0.75)

    # Investment costs
    k_inv = 1 # €/m³
    m.fs.inv_cost = Expression(expr=k_inv * (m.fs.R101.volume + m.fs.R102.volume + m.fs.R103.volume))

    # Objective
    m.fs.objective = Objective(expr=m.fs.operating_cost_neu)

    opt = SolverFactory('ipopt')

    m.fs.HEX101.report()
    m.fs.HEX102.report()
    m.fs.HEX103.report()
    m.fs.HEX104.report()
    m.fs.HEX105.report()

    m.fs.F101.report()
    m.fs.R101.report()
    m.fs.R102.report()
    m.fs.R103.report()
    m.fs.R104.report()
    m.fs.report()
    y_H2 = []
    y_CO2 = []
    y_H2O = []
    y_CH4 = []

    for R in [m.fs.R101.outlet.flow_mol_phase_comp,
              m.fs.R102.outlet.flow_mol_phase_comp,
              m.fs.R103.outlet.flow_mol_phase_comp,
              m.fs.R104.outlet.flow_mol_phase_comp,
              m.fs.product.flow_mol_phase_comp]:

        y_H2.append(value(R[0, "Vap", "H2"])/ (
                            value(R[0, "Vap", "H2"]) +
                            value(R[0, "Vap", "H2O"]) +
                            value(R[0, "Vap", "CO2"]) +
                            value(R[0, "Vap", "CH4"])
                            ))
        y_H2O.append(value(R[0, "Vap", "H2O"])/ (
                            value(R[0, "Vap", "H2"]) +
                            value(R[0, "Vap", "H2O"]) +
                            value(R[0, "Vap", "CO2"]) +
                            value(R[0, "Vap", "CH4"])
                            ))
        y_CO2.append(value(R[0, "Vap", "CO2"])/ (
                            value(R[0, "Vap", "H2"]) +
                            value(R[0, "Vap", "H2O"]) +
                            value(R[0, "Vap", "CO2"]) +
                            value(R[0, "Vap", "CH4"])
                            ))
        y_CH4.append(value(R[0, "Vap", "CH4"])/ (
                            value(R[0, "Vap", "H2"]) +
                            value(R[0, "Vap", "H2O"]) +
                            value(R[0, "Vap", "CO2"]) +
                            value(R[0, "Vap", "CH4"])
                            ))


    print("Recycling ratio: {0:4.3f} %".format(value(m.fs.S101.split_fraction[0, "recycle_R101"])*100))
    print("H2/CO2 ratio: {0:4.3f}".format(value(m.fs.H2_CO2_ratio)))
    print("etha chem: {0:4.4f} %".format(value(m.fs.etha_chem)*100))
    print("etha energy: {0:3.4f} %".format(value(m.fs.etha_energy)*100))
    print("HHV_v: {0:2.4f} kWh/m³ [8.4,13.1]".format(value(m.fs.HHV_v)))
    print("LHV_v: {0:2.4f} kWh/m³".format(value(m.fs.LHV_v)))
    print("oberer Wobbe-Index: {0:2.4f} kWh/m³ [13.6,15.4]".format(value(m.fs.Wobbe_o)))
    print("relative Dichte: {0:2.3f} [0.55,0.75]".format(value(m.fs.density_rel)))
    print("y_H2: ",(y_H2))
    print("y_H2O: ",(y_H2O))
    print("y_CH4: ",(y_CH4))
    print("y_CO2: ",(y_CO2))
    print("Cooling demand [kW]: {0:4.4f}".format(value(m.fs.cooling_demand)/1000))
    print("Electricity demand [kW]: {0:4.4f}".format(value(m.fs.electricity_demand)/1000))
    print("Operating costs [€/h]: {0:4.4f}".format(value(m.fs.operating_cost)))
    print("SNG costs [ct/kWh]: {0:4.4f}".format(value(m.fs.sng_cost)*100))
    print("Cooling costs [€/h]: {0:4.4f} ".format(value(m.fs.cooling_costs)))
    print("Electricity costs [€/h]: {0:4.4f}".format(value(m.fs.electricity_costs)))
    print("Reactor investment costs [€]: {0:4.4f}".format(value(m.fs.inv_cost)))
    print("Reaktionsgeschw. R101: {0:4.4f}".format(value(m.fs.R101.control_volume.reactions[0,1].reaction_rate["R1"])))
    print("Reaktionsgeschw. R102: {0:4.4f}".format(value(m.fs.R102.control_volume.reactions[0,1].reaction_rate["R1"])))
    print("Reaktionsgeschw. R103: {0:4.4f}".format(value(m.fs.R103.control_volume.reactions[0,1].reaction_rate["R1"])))

    return m

###
### The following code is for test purposes only
if __name__ == '__main__':

    m = initialize_fs()
    m = prepare_optimization(m)