"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang

import numpy as np
from pyomo.environ import (Constraint,
                           Var,
                           ConcreteModel,
                           Expression,
                           Objective,
                           SolverFactory,
                           TransformationFactory,
                           value)
import pandas as pd
import numpy as np
from idaes.core.util import model_serializer as ms
from pyomo.opt import TerminationCondition, SolverStatus
from pandas import read_csv

# Stoffdaten
mw_H2 = 2.016E-3  # kg/mol
HHV_H2_m = 141.800e3  # kJ/kg
LHV_H2_m = 119.972e3  # kJ/kg
HHV_H2_v = 12.745e3  # kJ/m³
LHV_H2_v = 10.783e3  # kJ/m³
HHV_H2_mol = 141.800e3 * mw_H2  # kJ/mol
LHV_H2_mol = 119.972e3 * mw_H2  # kJ/mol #241

mw_CH4 = 16.043E-3  # kg/mol
HHV_CH4_m = 55.498e3  # kJ/kg
LHV_CH4_m = 50.013e3  # kJ/kg
HHV_CH4_v = 39.819e3  # kJ/m³
LHV_CH4_v = 35.883e3  # kJ/m³
HHV_CH4_mol = 55.498e3 * mw_CH4  # kJ/mol
LHV_CH4_mol = 50.013e3 * mw_CH4  # kJ/mol #802


def calc_part_load(m, options=None):


    # title=None, interval=[500,0,10]
    start_value = options['interval'][0]
    end_value = options['interval'][1]
    step_size = options['interval'][2]

    opt = SolverFactory('ipopt')
    opt.options = {'tol': 0.1/100, #YW0.01,
                   'max_iter': 1000,
                   "halt_on_ampl_error": 'no'
                   # 'gamma_theta': 1e-2
                   # 'acceptable_tol': 0.08,
                   #  "constr_viol_tol": 1e-2,
                   # "compl_inf_tol": 1e-2
                   }

    energy_feed_out = range
    energy_feed_in = []
    energy_out = []
    h2_co2_ratio = []
    rec = []
    cooling_demand = []
    heating_demand = []
    hd_101 = []
    hd_102 = []
    hd_103 = []
    hd_104 = []
    hd_105 = []
    hd_106 = []
    hd_107 = []
    electricity_demand = []
    ed_1 = []
    ed_2 = []
    pressure_R101_in = []
    T_R101_in = []
    T_R102_in = []
    T_R103_in = []
    T_R104_in = []
    T_R101_out = []
    T_R102_out = []
    T_R103_out = []
    T_R104_out = []
    T_HEX103_out = []
    T_HEX105_out = []
    T_HEX107_out = []
    T_C101_out = []
    T_C102_out = []
    etha_chem = []
    etha_ener = []
    oper_cost = []
    sng_cost = []
    model_dict= {}
    # r_1 = []
    # r_2 = []
    # r_3 = []
    terminal_cond = []

    e = start_value
    count_calc = 0
    while e >= end_value and count_calc <3:

        print("Energy: ", e)
        # ms.from_json(m,fname=f'../initialization/init_fs_500_95_TREMP_new_{name}.json.gz')
        # if e == 20:
        #     ms.from_json(m, fname=f'../initialization/init_fs_060_95_TREMP_new_{name}.json.gz')
        m.fs.energy_out.unfix()
        m.fs.energy_out.fix(e)
        m.fs.HEX103.outlet.temperature[0].setub(30.1 + 273.15)
        m.fs.HEX105.outlet.temperature[0].setub(30.1 + 273.15)
        m.fs.HEX107.outlet.temperature[0].setub(30.1 + 273.15)

        # m.fs.energy_out.setlb(e-1)
        # m.fs.energy_out.setub(e+1)
        try:

            solve_status = opt.solve(m, tee=True, logfile="reaction_test.log")
            # assert solve_status.solver.termination_condition == TerminationCondition.optimal
            # assert solve_status.solver.status == SolverStatus.ok
            #init_file_name = f'{options["output_folder"]}\\init_fs_{0:03.0f}_{1:0.2g}_TREMP_paper.json.gz'.format(value(m.fs.energy_out),
            #                                                                              100*value(m.fs.CH4_purity), options['title'])

            init_file_name = '{0}\\init_fs_{1:03.0f}_{2:0.2g}_TREMP_{3}.json.gz'.format(options["output_folder"], value(m.fs.energy_out),
                                                                                          100*value(m.fs.CH4_purity), options['title'])
            print(init_file_name)
            energy_out.append(value(m.fs.energy_out))
            terminal_cond.append(solve_status.solver.termination_condition.value)
            energy_feed_in.append(value(m.fs.energy_in))
            h2_co2_ratio.append(value(m.fs.H2_CO2_ratio))
            rec.append(value(m.fs.S101.split_fraction[0, "recycle_R101"])*100)
            cooling_demand.append(value(m.fs.cooling_demand)/1000)
            hd_101.append(value(m.fs.HEX101.heat_duty[0])/1000)
            hd_102.append(value(m.fs.HEX102.heat_duty[0])/1000)
            hd_103.append(value(m.fs.HEX103.heat_duty[0])/1000)
            hd_104.append(value(m.fs.HEX104.heat_duty[0])/1000)
            hd_105.append(value(m.fs.HEX105.heat_duty[0])/1000)
            hd_106.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
            hd_107.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
            heating_demand.append(value(m.fs.heating_demand) / 1000)
            electricity_demand.append(value(m.fs.electricity_demand)/1000)
            ed_1.append(value(m.fs.C101.work_mechanical[0])/1000)
            ed_2.append(value(m.fs.C102.work_mechanical[0])/1000)
            pressure_R101_in.append(value(m.fs.C101.outlet.pressure[0])/1e5)
            # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
            T_R101_in.append(value(m.fs.R101.inlet.temperature[0])- 273.15)
            T_R102_in.append(value(m.fs.R102.inlet.temperature[0])- 273.15)
            T_R103_in.append(value(m.fs.R103.inlet.temperature[0])- 273.15)
            T_R104_in.append(value(m.fs.R104.inlet.temperature[0]) - 273.15)
            T_R101_out.append(value(m.fs.R101.outlet.temperature[0]) - 273.15)
            T_R102_out.append(value(m.fs.R102.outlet.temperature[0]) - 273.15)
            T_R103_out.append(value(m.fs.R103.outlet.temperature[0]) - 273.15)
            T_R104_out.append(value(m.fs.R104.outlet.temperature[0]) - 273.15)
            T_HEX103_out.append(value(m.fs.HEX103.outlet.temperature[0]) - 273.15)
            T_HEX105_out.append(value(m.fs.HEX105.outlet.temperature[0]) - 273.15)
            T_HEX107_out.append(value(m.fs.HEX107.outlet.temperature[0]) - 273.15)
            T_C101_out.append(value(m.fs.C101.outlet.temperature[0]) - 273.15)
            T_C102_out.append(value(m.fs.C102.outlet.temperature[0]) - 273.15)
            etha_chem.append(value(m.fs.etha_chem))
            etha_ener.append(value(m.fs.etha_energy))
            oper_cost.append(value(m.fs.operating_cost_neu))
            sng_cost.append(value(m.fs.sng_cost) * 100)
            model_dict[e] = m
            # r_1.append(value(m.fs.R101.control_volume.reactions[0, 1].reaction_rate["R1"]))
            # r_2.append(value(m.fs.R102.control_volume.reactions[0, 1].reaction_rate["R1"]))
            # r_3.append(value(m.fs.R103.control_volume.reactions[0, 1].reaction_rate["R1"]))

            if solve_status.solver.termination_condition.value != 'optimal':
                ms.from_json(m,fname=f'{options["init_folder"]}\\init_fs_500_95_TREMP_{options["title"]}.json.gz')
                # ms.from_json(m, fname=f'../initialization/init_fs_{e+20*(2+count_calc)}_95_TREMP_new_{name}.json.gz')
                count_calc = count_calc + 1
                continue
            else:
                ms.to_json(m, fname=init_file_name)
                e = e - step_size
                count_calc = 0
        except ValueError:
            ms.from_json(m, fname=f'{options["init_folder"]}\\init_fs_500_95_TREMP_{options["title"]}.json.gz')
            # ms.from_json(m,fname=f'../initialization/init_fs_{e+20*(2+count_calc)}_95_TREMP_new_{name}.json.gz')
            count_calc = count_calc + 1
            continue

    name_list = [
        'energy_feed_out',
        'opt',
        'energy_feed_in',
        'H2/CO2',
        'rec',
        'cooling_demand',
        'hd_101',
        'hd_102',
        'hd_103',
        'hd_104',
        'hd_105',
        'hd_106',
        'hd_107',
        'heating_demand',
        'electricity_demand',
        'ed_1',
        'ed_2',
        'pressure_R101_in',
        # 'pressure_R102_in',
        'T_R101_in',
        'T_R102_in',
        'T_R103_in',
        'T_R104_in',
        'T_R101_out',
        'T_R102_out',
        'T_R103_out',
        'T_R104_out',
        'T_HEX103_out',
        'T_HEX105_out',
        'T_HEX107_out',
        'T_C101_out',
        'T_C102_out',
        'etha_chem',
        'etha_ener',
        'oper_cost',
        'sng_cost'
        # 'r_1',
        # 'r_2',
        # 'r_3'
    ]

    df = pd.DataFrame(list(zip(
        energy_out,
        terminal_cond,
        energy_feed_in,
        h2_co2_ratio,
        rec,
        cooling_demand,
        hd_101,
        hd_102,
        hd_103,
        hd_104,
        hd_105,
        hd_106,
        hd_107,
        heating_demand,
        electricity_demand,
        ed_1,
        ed_2,
        pressure_R101_in,
        # pressure_R102_in,
        T_R101_in,
        T_R102_in,
        T_R103_in,
        T_R104_in,
        T_R101_out,
        T_R102_out,
        T_R103_out,
        T_R104_out,
        T_HEX103_out,
        T_HEX105_out,
        T_HEX107_out,
        T_C101_out,
        T_C102_out,
        etha_chem,
        etha_ener,
        oper_cost,
        sng_cost
        # r_1,
        # r_2,
        # r_3
    )), columns=name_list)

    df.to_csv('{0}\\part_load_purity_{1:0.2g}.csv'.format(options["output_folder"], 100 * value(m.fs.CH4_purity)))
    df.to_excel('{0}\\part_load_purity_{1:0.2g}_TREMP_{2:0.3g}_{3:0.2g}_{4}.xlsx'.format(options["output_folder"],100 * value(m.fs.CH4_purity),
                                                                                              start_value, end_value, options['title']))
    #import pickle
    #import time
    # with open(f'../export/part_load_purity_95_TREMP_new_{name}.pkl', 'wb') as f:
    #     pickle.dump(model_dict, f)
    print('Calc_part_load is done.')

    return

def calc_multi_wg(m, options= None):

    start_value = options['interval'][0]
    end_value = options['interval'][1]
    step_size = options['interval'][2]

    opt = SolverFactory('ipopt')
    opt.options = {'tol': 0.1/100, #YW0.01,
                   'max_iter': 5000,
                   "halt_on_ampl_error": 'no'
                   }

    count_calc = 0

    #P = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20])
    P = np.array([20])
    #P = np.array([19, 20])


    T = np.arange(250, 350, 5)

    for p in P:
    # for t in T:
        e = start_value
        energy_feed_out = range
        energy_feed_in = []
        energy_out = []
        h2_co2_ratio = []
        rec = []
        cooling_demand = []
        heating_demand = []
        hd_101 = []
        hd_102 = []
        hd_103 = []
        hd_104 = []
        hd_105 = []
        hd_106 = []
        hd_107 = []
        electricity_demand = []
        ed_1 = []
        ed_2 = []
        pressure_R101_in = []
        T_R101_in = []
        T_R102_in = []
        T_R103_in = []
        T_R104_in = []
        T_R101_out = []
        T_R102_out = []
        T_R103_out = []
        T_R104_out = []
        T_HEX103_out = []
        T_HEX105_out = []
        T_HEX107_out = []
        etha_chem = []
        etha_ener = []
        oper_cost = []
        sng_cost = []

        terminal_cond = []

        while e >= end_value:
            if count_calc == 2:
                e = e - step_size
                count_calc = 0
            print("Energy: ", e)
            print("Pressure: ", p)

            m.fs.energy_out.unfix()
            m.fs.energy_out.fix(e)

            if options['var'] == 'P':

                m.fs.C101.outlet.pressure[0].setub(p*1e5 + 0.1e5)
                m.fs.C101.outlet.pressure[0].setlb(p*1e5 - 0.1e5)
                # m.fs.C102.outlet.pressure[0].fix(P)
                m.fs.S101.split_fraction[0, "recycle_R101"].setlb(0.7)
                m.fs.S101.split_fraction[0, "recycle_R101"].setub(0.75)
            # m.fs.energy_out.setlb(e-1)
            # m.fs.energy_out.setub(e+1)
            try:

                solve_status = opt.solve(m, tee=True, logfile="reaction_test.log")
                # assert solve_status.solver.termination_condition == TerminationCondition.optimal
                # assert solve_status.solver.status == SolverStatus.ok
                # init_file_name = '../initialization/init_fs_{0:03.0f}_{1:0.2g}_TREMP_new_{2}.json.gz'.format(value(m.fs.energy_out),
                #                                                                               100*value(m.fs.CH4_purity), name)

                if solve_status.solver.termination_condition.value == 'optimal':
                    energy_out.append(value(m.fs.energy_out))
                    terminal_cond.append(solve_status.solver.termination_condition.value)
                    energy_feed_in.append(value(m.fs.energy_in))
                    h2_co2_ratio.append(value(m.fs.H2_CO2_ratio))
                    rec.append(value(m.fs.S101.split_fraction[0, "recycle_R101"])*100)
                    cooling_demand.append(value(m.fs.cooling_demand)/1000)
                    hd_101.append(value(m.fs.HEX101.heat_duty[0])/1000)
                    hd_102.append(value(m.fs.HEX102.heat_duty[0])/1000)
                    hd_103.append(value(m.fs.HEX103.heat_duty[0])/1000)
                    hd_104.append(value(m.fs.HEX104.heat_duty[0])/1000)
                    hd_105.append(value(m.fs.HEX105.heat_duty[0])/1000)
                    hd_106.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
                    hd_107.append(value(m.fs.HEX106.heat_duty[0]) / 1000)
                    heating_demand.append(value(m.fs.heating_demand) / 1000)
                    electricity_demand.append(value(m.fs.electricity_demand)/1000)
                    ed_1.append(value(m.fs.C101.work_mechanical[0])/1000)
                    ed_2.append(value(m.fs.C102.work_mechanical[0])/1000)
                    pressure_R101_in.append(value(m.fs.C101.outlet.pressure[0])/1e5)
                    # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
                    T_R101_in.append(value(m.fs.R101.inlet.temperature[0])- 273.15)
                    T_R102_in.append(value(m.fs.R102.inlet.temperature[0])- 273.15)
                    T_R103_in.append(value(m.fs.R103.inlet.temperature[0])- 273.15)
                    T_R104_in.append(value(m.fs.R104.inlet.temperature[0]) - 273.15)
                    T_R101_out.append(value(m.fs.R101.outlet.temperature[0]) - 273.15)
                    T_R102_out.append(value(m.fs.R102.outlet.temperature[0]) - 273.15)
                    T_R103_out.append(value(m.fs.R103.outlet.temperature[0]) - 273.15)
                    T_R104_out.append(value(m.fs.R104.outlet.temperature[0]) - 273.15)
                    T_HEX103_out.append(value(m.fs.HEX103.outlet.temperature[0]) - 273.15)
                    T_HEX105_out.append(value(m.fs.HEX105.outlet.temperature[0]) - 273.15)
                    T_HEX107_out.append(value(m.fs.HEX107.outlet.temperature[0]) - 273.15)
                    etha_chem.append(value(m.fs.etha_chem))
                    etha_ener.append(value(m.fs.etha_energy))
                    oper_cost.append(value(m.fs.operating_cost))
                    sng_cost.append(value(m.fs.sng_cost) * 100)

                if solve_status.solver.termination_condition.value != 'optimal' and count_calc==1:
                    energy_out.append(-1)
                    terminal_cond.append(-1)
                    energy_feed_in.append(-1)
                    h2_co2_ratio.append(-1)
                    rec.append(-1)
                    cooling_demand.append(-1)
                    hd_101.append(-1)
                    hd_102.append(-1)
                    hd_103.append(-1)
                    hd_104.append(-1)
                    hd_105.append(-1)
                    hd_106.append(-1)
                    hd_107.append(-1)
                    heating_demand.append(-1)
                    electricity_demand.append(-1)
                    ed_1.append(-1)
                    ed_2.append(-1)
                    pressure_R101_in.append(-1)
                    # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
                    T_R101_in.append(-1)
                    T_R102_in.append(-1)
                    T_R103_in.append(-1)
                    T_R104_in.append(-1)
                    T_R101_out.append(-1)
                    T_R102_out.append(-1)
                    T_R103_out.append(-1)
                    T_R104_out.append(-1)
                    T_HEX103_out.append(-1)
                    T_HEX105_out.append(-1)
                    T_HEX107_out.append(-1)
                    etha_chem.append(-1)
                    etha_ener.append(-1)
                    oper_cost.append(-1)
                    sng_cost.append(-1)

                if solve_status.solver.termination_condition.value != 'optimal':
                    ms.from_json(m,fname=f'{options["init_folder"]}\\init_fs_500_95_TREMP_{options["title"]}.json.gz')
                    count_calc = count_calc + 1
                    continue
                else:
                    # ms.to_json(m, fname=init_file_name)
                    e = e - step_size
                    count_calc = 0
            except ValueError:
                ms.from_json(m,fname=f'{options["init_folder"]}\\init_fs_500_95_TREMP_{options["title"]}.json.gz')

                if count_calc==1:
                    energy_out.append(-1)
                    terminal_cond.append(-1)
                    energy_feed_in.append(-1)
                    h2_co2_ratio.append(-1)
                    rec.append(-1)
                    cooling_demand.append(-1)
                    hd_101.append(-1)
                    hd_102.append(-1)
                    hd_103.append(-1)
                    hd_104.append(-1)
                    hd_105.append(-1)
                    hd_106.append(-1)
                    hd_107.append(-1)
                    heating_demand.append(-1)
                    electricity_demand.append(-1)
                    ed_1.append(-1)
                    ed_2.append(-1)
                    pressure_R101_in.append(-1)
                    # pressure_R102_in.append(value(m.fs.C102.outlet.pressure[0])/1e5)
                    T_R101_in.append(-1)
                    T_R102_in.append(-1)
                    T_R103_in.append(-1)
                    T_R104_in.append(-1)
                    T_R101_out.append(-1)
                    T_R102_out.append(-1)
                    T_R103_out.append(-1)
                    T_R104_out.append(-1)
                    T_HEX103_out.append(-1)
                    T_HEX105_out.append(-1)
                    T_HEX107_out.append(-1)
                    etha_chem.append(-1)
                    etha_ener.append(-1)
                    oper_cost.append(-1)
                    sng_cost.append(-1)
                count_calc = count_calc + 1
                continue
        name_list = [

            'energy_feed_out',
            'opt',
            'energy_feed_in',
            'H2/CO2',
            'rec',
            'cooling_demand',
            'hd_101',
            'hd_102',
            'hd_103',
            'hd_104',
            'hd_105',
            'hd_106',
            'hd_107',
            'heating_demand',
            'electricity_demand',
            'ed_1',
            'ed_2',
            'pressure_R101_in',
            # 'pressure_R102_in',
            'T_R101_in',
            'T_R102_in',
            'T_R103_in',
            'T_R104_in',
            'T_R101_out',
            'T_R102_out',
            'T_R103_out',
            'T_R104_out',
            'T_HEX103_out',
            'T_HEX105_out',
            'T_HEX107_out',
            'etha_chem',
            'etha_ener',
            'oper_cost',
            'sng_cost'
            # 'r_1',
            # 'r_2',
            # 'r_3'
        ]
        df = pd.DataFrame(list(zip(
            energy_out,
            terminal_cond,
            energy_feed_in,
            h2_co2_ratio,
            rec,
            cooling_demand,
            hd_101,
            hd_102,
            hd_103,
            hd_104,
            hd_105,
            hd_106,
            hd_107,
            heating_demand,
            electricity_demand,
            ed_1,
            ed_2,
            pressure_R101_in,
            # pressure_R102_in,
            T_R101_in,
            T_R102_in,
            T_R103_in,
            T_R104_in,
            T_R101_out,
            T_R102_out,
            T_R103_out,
            T_R104_out,
            T_HEX103_out,
            T_HEX105_out,
            T_HEX107_out,
            etha_chem,
            etha_ener,
            oper_cost,
            sng_cost
            # r_1,
            # r_2,
            # r_3
        )), columns=name_list)
        # df.to_csv("../export/part_load_purity_{0:0.2g}.csv".format(100 * value(m.fs.CH4_purity)))
        if options['var'] == 'P':
            df.to_excel(f'{options["output_folder"]}\\multi_pressure_TREMP_{options["title"]}_P_{p}.xlsx')
            print("saved xlsx")
    return

def calc_gradient_sheet(filename, data_points, k_el=0.2, k_heat=10/1000, k_cool=5/1000):
    # k_el = 0.2
    # k_cool = 5/1000 # €/kWh 10°C cooling water
    # k_heat = 2*k_cool

    df = pd.read_excel(filename)
    df.to_dict()
    sng_costs = df['sng_cost'].iloc[::-1].to_numpy()
    energy_out = df['energy_feed_out'].iloc[::-1].to_numpy()
    CD = (df['cooling_demand'].iloc[::-1].to_numpy() - df['heating_demand'].iloc[::-1].to_numpy())*k_cool/energy_out
    HD = df['heating_demand'].iloc[::-1].to_numpy()*k_heat/energy_out
    ED = df['electricity_demand'].iloc[::-1].to_numpy()*k_el/energy_out
    # CD = df['cooling_demand'].iloc[::-1].to_numpy()
    # HD = df['heating_demand'].iloc[::-1].to_numpy()
    # ED = df['electricity_demand'].iloc[::-1].to_numpy()

    grad = np.gradient(ED,energy_out)
    grad_CD = [0]
    grad_HD = [0]
    grad_ED = [0]

    for i in range(1,len(CD)):
        grad_i_CD = ((CD[i] - CD[i-1])/(energy_out[i] - energy_out[i-1]))
        grad_i_HD = ((HD[i] - HD[i-1]) / (energy_out[i] - energy_out[i-1]))
        grad_i_ED = ((ED[i] - ED[i-1]) / (energy_out[i] - energy_out[i-1]))
        grad_CD.append(grad_i_CD)
        grad_HD.append(grad_i_HD)
        grad_ED.append(grad_i_ED)
    grad_CD_df = pd.Series(grad_CD)
    grad_HD_df = pd.Series(grad_HD)
    grad_ED_df = pd.Series(grad_ED)

    grad_CD_data = {}
    grad_HD_data = {}
    grad_ED_data = {}
    for t in range(0, len(data_points)):
        nearest_point_idx, nearest_point_val = min(enumerate(energy_out), key=lambda x: abs(x[1] - (data_points[t])))
        if nearest_point_val >= data_points[t]:
            if nearest_point_val == energy_out[0]:
                grad_CD_data[t] = 0
                grad_HD_data[t] = 0
                grad_ED_data[t] = 0
            else:
                grad_CD_data[t] = \
                    (grad_CD_df[nearest_point_idx]-grad_CD_df[nearest_point_idx-1])/(energy_out[nearest_point_idx]-energy_out[nearest_point_idx-1])* \
                (data_points[t]-energy_out[nearest_point_idx-1]) + grad_CD_df[nearest_point_idx-1]

                grad_HD_data[t] = \
                    (grad_HD_df[nearest_point_idx]-grad_HD_df[nearest_point_idx-1])/(energy_out[nearest_point_idx]-energy_out[nearest_point_idx-1])* \
                (data_points[t]-energy_out[nearest_point_idx-1]) + grad_HD_df[nearest_point_idx-1]

                grad_ED_data[t] = \
                    (grad_ED_df[nearest_point_idx]-grad_ED_df[nearest_point_idx-1])/(energy_out[nearest_point_idx]-energy_out[nearest_point_idx-1])* \
                (data_points[t]-energy_out[nearest_point_idx-1]) + grad_ED_df[nearest_point_idx-1]
        else:
            nearest_point_idx = nearest_point_idx + 1
            grad_CD_data[t] = \
                (grad_CD_df[nearest_point_idx] - grad_CD_df[nearest_point_idx - 1]) / (
                            energy_out[nearest_point_idx] - energy_out[nearest_point_idx - 1]) * \
                (data_points[t] - energy_out[nearest_point_idx - 1]) + grad_CD_df[nearest_point_idx - 1]

            grad_HD_data[t] = \
                (grad_HD_df[nearest_point_idx]-grad_HD_df[nearest_point_idx-1])/(energy_out[nearest_point_idx]-energy_out[nearest_point_idx-1])* \
                (data_points[t]-energy_out[nearest_point_idx-1]) + grad_HD_df[nearest_point_idx-1]

            grad_ED_data[t] = \
                (grad_ED_df[nearest_point_idx]-grad_ED_df[nearest_point_idx-1])/(energy_out[nearest_point_idx]-energy_out[nearest_point_idx-1])* \
                (data_points[t]-energy_out[nearest_point_idx-1]) + grad_ED_df[nearest_point_idx-1]

        # if nearest_point_idx == 0:
        #     grad_CD_data[data_points[t]] = grad_CD_df[0]
        #     grad_CD_data[data_points[t]] = grad_CD_df[0]
        #     grad_CD_data[data_points[t]] = grad_CD_df[0]
        # elif nearest_point_idx == len(energy_out):
        #     grad_CD_data[data_points[t]] = grad_CD_df[-1]
        #     grad_CD_data[data_points[t]] = grad_CD_df[-1]
        #     grad_CD_data[data_points[t]] = grad_CD_df[-1]
        # elif nearest_point_val == data_points[t]:
        #     grad_CD_data[data_points[t]] = 0
        #     grad_CD_data[data_points[t]] = 0
        #     grad_CD_data[data_points[t]] = 0
        # elif nearest_point_val > data_points[t]:
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx]
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx]
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx]
        # else:
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx-1]
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx - 1]
        #     grad_CD_data[data_points[t]] = grad_CD_df[nearest_point_idx - 1]
        # nearest_point2 = take_closest(energy_out, data_points[t])
    return grad_CD_data, grad_HD_data, grad_ED_data

