"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang

import logging

from pyomo.environ import units as pyunits

# Import IDAES cores
from idaes.core import LiquidPhase, VaporPhase, Component, MaterialFlowBasis
from idaes.core.base.phases import PhaseType as PT

from idaes.models.properties.modular_properties.state_definitions import FTPx, FpcTP, FcPh, FPhx
from idaes.models.properties.modular_properties.eos.ceos import Cubic, CubicType
from idaes.models.properties.modular_properties.eos.ideal import Ideal
from idaes.models.properties.modular_properties.phase_equil import SmoothVLE
from idaes.models.properties.modular_properties.phase_equil.bubble_dew import \
        LogBubbleDew, IdealBubbleDew
from idaes.models.properties.modular_properties.phase_equil.forms import fugacity, log_fugacity

from idaes.models.properties.modular_properties.pure.Perrys import Perrys

from idaes.models.properties.modular_properties.pure.RPP5 import RPP5
from idaes.models.properties.modular_properties.pure.RPP4 import RPP4
from idaes.models.properties.modular_properties.pure.NIST import NIST

from idaes.models.properties.modular_properties.base.generic_reaction import (
        ConcentrationForm)
from idaes.models.properties.modular_properties.reactions.dh_rxn import \
    constant_dh_rxn
from idaes.models.properties.modular_properties.reactions.rate_constant import arrhenius
from idaes.models.properties.modular_properties.reactions.rate_forms import power_law_rate
from idaes.models.properties.modular_properties.reactions.equilibrium_constant import van_t_hoff, gibbs_energy
from idaes.models.properties.modular_properties.reactions.equilibrium_forms import power_law_equil, log_power_law_equil


# [1] - RPP5: The Properties of Gases & Liquids, 5th Edition Reid, Prausnitz and Polling, 2001, McGraw-Hill
# [2] - Perrys: Perry’s Chemical Engineers’ Handbook, 7th Edition Perry, Green, Maloney, 1997, McGraw-Hill
# [3] - RPP4: The Properties of Gases & Liquids, 4th Edition Reid, Prausnitz and Polling
#TODO: Heat of vaporization
#TODO: Allg. Gastkonstante R


# Set up logger
_log = logging.getLogger(__name__)

thermo_configuration_vapor = {
    "base_units": {"time": pyunits.s,
                   "length": pyunits.m,
                   "mass": pyunits.kg,
                   "amount": pyunits.mol,
                   "temperature": pyunits.K},

    "components": {
        'CH4': {"type": Component,
                "elemental_composition": {"C": 1, "H": 4, "O": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": RPP5,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (16.043E-3, pyunits.kg/pyunits.mol),  # [1] A.5, pg. 723
                    "pressure_crit": (45.99E5, pyunits.Pa),  # [1] A.5, pg. 723
                    "temperature_crit": (190.56, pyunits.K),  # [1] A.5, pg. 723
                    "temperature_boil": (111.66, pyunits.K),  # [1] A.5, pg. 723
                    "omega": 0.011, # [1] A.5, pg. 723
                    "dens_mol_liq_comp_coeff": {
                        '1': (2.9214, pyunits.kmol*pyunits.m**-3),  # [2] pg. 135, table 2-30
                        '2': (0.28976, None),
                        '3': (190.56, pyunits.K),
                        '4': (0.28881, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #     '1': (0, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 211, table 2-196, eqn2:6.5708E1
                    #     '2': (0, pyunits.J / pyunits.kmol / pyunits.K ** 2), #eqn2:3.8883E4
                    #     '3': (0, pyunits.J / pyunits.kmol / pyunits.K ** 3),#eqn2:-2.5795E2
                    #     '4': (0, pyunits.J / pyunits.kmol / pyunits.K ** 4),#eqn2:6.1407E2
                    #     '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},#eqn2:0
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J/pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J/pyunits.kmol/pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "cp_mol_ig_comp_coeff": {
                        "A": (1.925e1, pyunits.J/pyunits.mol/pyunits.K), # [3] p. 671
                        "B": (5.213e-2, pyunits.J/pyunits.mol/pyunits.K**2),
                        "C": (1.197e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                        "D": (-1.132e-8, pyunits.J/pyunits.mol/pyunits.K**4)},
                    "enth_mol_form_vap_comp_ref": (
                        -7.4520E7, pyunits.J/pyunits.kmol),  # [2] pg.2-195 p.236, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        1.8627E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-195 p.236, Table 2-221


                    "pressure_sat_comp_coeff": {'A': (3.76870, None),  # [1] A.47, pg.765, eqn1
                                                'B': (395.7440, pyunits.K),
                                                'C': (266.681, pyunits.K)}}},
        'CO2': {"type": Component,
                "elemental_composition": {"C": 1, "O": 2, "H": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": NIST,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (44.01E-3, pyunits.kg / pyunits.mol),  # [1] A.6, pg. 724
                    "pressure_crit": (73.74E5, pyunits.Pa),  # [1] A.6, pg. 724
                    "temperature_crit": (304.12, pyunits.K),  # [1] A.6, pg. 724
                    "omega": 0.225,  # [1] A.6, pg. 724
                    "dens_mol_liq_comp_coeff": {
                            '1': (2.768, pyunits.kmol*pyunits.m**-3),  # [2] pg. 139 2-98, table 2-30
                            '2': (0.26212, None),
                            '3': (304.21, pyunits.K),
                            '4': (0.2908, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #         '1': (-8.3043E6, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 215, table 2-196, Range: 220K-290K
                    #         '2': (1.0437E5, pyunits.J / pyunits.kmol / pyunits.K ** 2),
                    #         '3': (-4.3333E2, pyunits.J / pyunits.kmol / pyunits.K ** 3),
                    #         '4': (6.0052E-1, pyunits.J / pyunits.kmol / pyunits.K ** 4),
                    #         '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.kmol / pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "enth_mol_form_vap_comp_ref": (
                        -39.351E7, pyunits.J/pyunits.kmol),  # [2] pg.2-198 p.239, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        2.1368E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-198 p.239, Table 2-221
                    "cp_mol_ig_comp_coeff": {
                          "A": (1.980E1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.679
                          "B": (7.344E-2, pyunits.J/pyunits.mol/pyunits.K**2),
                          "C": (-5.602E-5, pyunits.J/pyunits.mol/pyunits.K**3),
                          "D": (1.715E-8, pyunits.J/pyunits.mol/pyunits.K**4)},
                    #https://webbook.nist.gov/cgi/cbook.cgi?ID=C124389&Mask=4&Type=ANTOINE&Plot=on#ANTOINE
                    "pressure_sat_comp_coeff": {'A': (6.81228, None),  # From NIST Webbook
                                                'B': (1301.679, pyunits.K),
                                                'C': (-3.494, pyunits.K)}}},

        'H2': {"type": Component,
                "elemental_composition": {"H": 2, "C": 0, "O": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": RPP5,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (2.016E-3, pyunits.kg / pyunits.mol),  # [1] A.19, pg. 737
                    "pressure_crit": (12.93E5, pyunits.Pa),  # [1] A.19, pg. 737
                    "temperature_crit": (32.98, pyunits.K),  # [1] A.19, pg. 737
                    "temperature_boil": (20.27, pyunits.K),  # [1] A.19, pg. 737
                    "omega": -0.217,  # [1] A.6, pg. 724
                    # "dens_mol_liq_comp_coeff": {
                    #     '1': (5.414, pyunits.kmol*pyunits.m**-3),  # [2] pg. 138 2-98, table 2-30
                    #     '2': (0.34893, None),
                    #     '3': (33.19, pyunits.K),
                    #     '4': (0.2706, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #     '1': (0, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 215, table 2-196, #eqn2: 6.6653E1
                    #     '2': (0, pyunits.J / pyunits.kmol / pyunits.K ** 2),#eqn2:6.7659E3
                    #     '3': (0, pyunits.J / pyunits.kmol / pyunits.K ** 3),#eqn2:-1.2363E2
                    #     '4': (0, pyunits.J / pyunits.kmol / pyunits.K ** 4),#eqn2:4.7827E2
                    #     '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},#eqn2:0
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.kmol / pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "enth_mol_form_vap_comp_ref": (
                        0, pyunits.J/pyunits.kmol),  # [2] pg.2-198 p.239, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        1.3057E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-198 p.239, Table 2-221
                    "cp_mol_ig_comp_coeff": {
                            "A": (2.714e1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.665
                            "B": (9.274e-3, pyunits.J/pyunits.mol/pyunits.K**2),
                            "C": (-1.381e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                            "D": (7.645e-9, pyunits.J/pyunits.mol/pyunits.K**4)},

                    "pressure_sat_comp_coeff": {'A': (2.93954, None),  # [1] A.47, pg.765, eqn1
                                                'B': (66.79540, pyunits.K),
                                                'C': (275.65, pyunits.K)}}},

        'H2O': {"type": Component,
                "elemental_composition": {"H": 2, "O": 1, "C": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": NIST,
                "enth_mol_ig_comp": NIST,
                "entr_mol_ig_comp": NIST,
                "pressure_sat_comp": NIST,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (18.015E-3, pyunits.kg / pyunits.mol),  # [1] A.19, pg. 737
                    "pressure_crit": (220.64E5, pyunits.Pa),  # [1] A.19, pg. 737
                    "temperature_crit": (647.14, pyunits.K),  # [1] A.19, pg. 737
                    "temperature_boil": (373.15, pyunits.K),  # [1] A.19, pg. 737
                    "omega": 0.344,  # [1] A.6, pg. 724
                "enth_mol_form_vap_comp_ref": (
                    # 0, pyunits.J / pyunits.mol),
                    -241.8264, pyunits.kJ/pyunits.mol),  # [2] pg.2-199 p.240, Table 2-221
                "entr_mol_form_vap_comp_ref": (
                   # 0, pyunits.J/pyunits.kmol/pyunits.K),  # [2]1.8872E5 pg.2-199 p.240, Table 2-221
                    188.835, pyunits.J/pyunits.mol/pyunits.K),  # NIST Webbook
                "cp_mol_ig_comp_coeff": {
                        'A': (30.09200, pyunits.J/pyunits.mol/pyunits.K),  # [1] temperature range 500 K- 1700 K
                        'B': (6.832514, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-1),
                        'C': (6.793435, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-2),
                        'D': (-2.534480, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-3),
                        'E': (0.082139, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**2),
                        'F': (-250.8810, pyunits.kJ/pyunits.mol),
                        'G': (223.3967, pyunits.J/pyunits.mol/pyunits.K),
                        'H': (-241.8264, pyunits.kJ/pyunits.mol)},
                            # "A": (3.194e1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.668
                            # "B": (1.436e-3, pyunits.J/pyunits.mol/pyunits.K**2),
                            # "C": (2.432e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                            # "D": (-1.176e-8, pyunits.J/pyunits.mol/pyunits.K**4)},
                "pressure_sat_comp_coeff": {
                        'A': (4.6543, None),  # [1], temperature range 255.9 K - 373 K
                        'B': (1435.264, pyunits.K),
                        'C': (-64.848, pyunits.K)}}}},

#4-21, 13-7


    # Specifying phases
    "phases":  {
                "Vap": {"type": VaporPhase,
                        "equation_of_state": Cubic,
                        "component_list": ["CH4", "H2O", "H2", "CO2"],
                        "equation_of_state_options": {
                            "type": CubicType.PR}}},

    # Specifying state definition
    "state_definition": FpcTP,
    "state_bounds": {#"flow_mol": (0, 100, 1000, pyunits.mol/pyunits.s),
                    "flow_mol_phase_comp": (1e-20, 30, 200, pyunits.mol/pyunits.s),
                     "temperature": (283.15, 570, 1300, pyunits.K),
                     "pressure": (5e4, 5e5, 31e5, pyunits.Pa)},
    "pressure_ref": (101325, pyunits.Pa),
    "temperature_ref": (298.15, pyunits.K),

    "bubble_dew_method": LogBubbleDew,
    "parameter_data": {"PR_kappa": {("CH4", "CH4"): 0.000, #DATA from AspenPlus Simulation
                                    ("CH4", "CO2"): 0.0919,
                                    ("CH4", "H2O"): 0.000,
                                    ("CH4", "H2"): 0.0156,
                                    ("CO2", "CO2"): 0.000,
                                    ("CO2", "CH4"): 0.0919,
                                    ("CO2", "H2O"): 0.12,
                                    ("CO2", "H2"): -0.1622,
                                    ("H2", "H2"): 0.000,
                                    ("H2", "CH4"): 0.0156,
                                    ("H2", "CO2"): -0.1622,
                                    ("H2", "H2O"): 0.000,
                                    ("H2O", "H2O"): 0.000,
                                    ("H2O", "CH4"): 0.000,
                                    ("H2O", "CO2"): 0.12,
                                    ("H2O", "H2"): 0.000},
                        "gas_const":(8.314, pyunits.J/pyunits.mol/pyunits.K)}}



thermo_configuration_VLE = {
    "base_units": {"time": pyunits.s,
                   "length": pyunits.m,
                   "mass": pyunits.kg,
                   "amount": pyunits.mol,
                   "temperature": pyunits.K},

    "components": {
        'CH4': {"type": Component,
                "elemental_composition": {"C": 1, "H": 4, "O": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": RPP5,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (16.043E-3, pyunits.kg/pyunits.mol),  # [1] A.5, pg. 723
                    "pressure_crit": (45.99E5, pyunits.Pa),  # [1] A.5, pg. 723
                    "temperature_crit": (190.56, pyunits.K),  # [1] A.5, pg. 723
                    "temperature_boil": (111.66, pyunits.K),  # [1] A.5, pg. 723
                    "omega": 0.011, # [1] A.5, pg. 723
                    "dens_mol_liq_comp_coeff": {
                        '1': (2.9214, pyunits.kmol*pyunits.m**-3),  # [2] pg. 135, table 2-30
                        '2': (0.28976, None),
                        '3': (190.56, pyunits.K),
                        '4': (0.28881, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #     '1': (0, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 211, table 2-196, eqn2:6.5708E1
                    #     '2': (0, pyunits.J / pyunits.kmol / pyunits.K ** 2), #eqn2:3.8883E4
                    #     '3': (0, pyunits.J / pyunits.kmol / pyunits.K ** 3),#eqn2:-2.5795E2
                    #     '4': (0, pyunits.J / pyunits.kmol / pyunits.K ** 4),#eqn2:6.1407E2
                    #     '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},#eqn2:0
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J/pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J/pyunits.kmol/pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "cp_mol_ig_comp_coeff": {
                        "A": (1.925e1, pyunits.J/pyunits.mol/pyunits.K), # [3] p. 671
                        "B": (5.213e-2, pyunits.J/pyunits.mol/pyunits.K**2),
                        "C": (1.197e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                        "D": (-1.132e-8, pyunits.J/pyunits.mol/pyunits.K**4)},
                    "enth_mol_form_vap_comp_ref": (
                        -7.4520E7, pyunits.J/pyunits.kmol),  # [2] pg.2-195 p.236, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        1.8627E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-195 p.236, Table 2-221


                    "pressure_sat_comp_coeff": {'A': (3.76870, None),  # [1] A.47, pg.765, eqn1
                                                'B': (395.7440, pyunits.K),
                                                'C': (266.681, pyunits.K)}}},
        'CO2': {"type": Component,
                "elemental_composition": {"C": 1, "O": 2, "H": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": NIST,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (44.01E-3, pyunits.kg / pyunits.mol),  # [1] A.6, pg. 724
                    "pressure_crit": (73.74E5, pyunits.Pa),  # [1] A.6, pg. 724
                    "temperature_crit": (304.12, pyunits.K),  # [1] A.6, pg. 724
                    "omega": 0.225,  # [1] A.6, pg. 724
                    "dens_mol_liq_comp_coeff": {
                            '1': (2.768, pyunits.kmol*pyunits.m**-3),  # [2] pg. 139 2-98, table 2-30
                            '2': (0.26212, None),
                            '3': (304.21, pyunits.K),
                            '4': (0.2908, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #         '1': (-8.3043E6, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 215, table 2-196, Range: 220K-290K
                    #         '2': (1.0437E5, pyunits.J / pyunits.kmol / pyunits.K ** 2),
                    #         '3': (-4.3333E2, pyunits.J / pyunits.kmol / pyunits.K ** 3),
                    #         '4': (6.0052E-1, pyunits.J / pyunits.kmol / pyunits.K ** 4),
                    #         '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.kmol / pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "enth_mol_form_vap_comp_ref": (
                        -39.351E7, pyunits.J/pyunits.kmol),  # [2] pg.2-198 p.239, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        2.1368E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-198 p.239, Table 2-221
                    "cp_mol_ig_comp_coeff": {
                          "A": (1.980E1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.679
                          "B": (7.344E-2, pyunits.J/pyunits.mol/pyunits.K**2),
                          "C": (-5.602E-5, pyunits.J/pyunits.mol/pyunits.K**3),
                          "D": (1.715E-8, pyunits.J/pyunits.mol/pyunits.K**4)},
                    #https://webbook.nist.gov/cgi/cbook.cgi?ID=C124389&Mask=4&Type=ANTOINE&Plot=on#ANTOINE
                    "pressure_sat_comp_coeff": {'A': (6.81228, None),  # From NIST Webbook
                                                'B': (1301.679, pyunits.K),
                                                'C': (-3.494, pyunits.K)}}},

        'H2': {"type": Component,
                "elemental_composition": {"H": 2, "C": 0, "O": 0},
                # "cp_mol_liq_comp": Perrys,
                # "dens_mol_liq_comp": Perrys,
                # "enth_mol_liq_comp": Perrys,
                # "entr_mol_liq_comp": Perrys,
                "cp_mol_ig_comp": RPP4,
                "enth_mol_ig_comp": RPP4,
                "entr_mol_ig_comp": RPP4,
                "pressure_sat_comp": RPP5,
                "valid_phase_types": PT.vaporPhase,
                # "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (2.016E-3, pyunits.kg / pyunits.mol),  # [1] A.19, pg. 737
                    "pressure_crit": (12.93E5, pyunits.Pa),  # [1] A.19, pg. 737
                    "temperature_crit": (32.98, pyunits.K),  # [1] A.19, pg. 737
                    "temperature_boil": (20.27, pyunits.K),  # [1] A.19, pg. 737
                    "omega": -0.217,  # [1] A.6, pg. 724
                    # "dens_mol_liq_comp_coeff": {
                    #     '1': (5.414, pyunits.kmol*pyunits.m**-3),  # [2] pg. 138 2-98, table 2-30
                    #     '2': (0.34893, None),
                    #     '3': (33.19, pyunits.K),
                    #     '4': (0.2706, None)},
                    # "cp_mol_liq_comp_coeff": {
                    #     '1': (0, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 215, table 2-196, #eqn2: 6.6653E1
                    #     '2': (0, pyunits.J / pyunits.kmol / pyunits.K ** 2),#eqn2:6.7659E3
                    #     '3': (0, pyunits.J / pyunits.kmol / pyunits.K ** 3),#eqn2:-1.2363E2
                    #     '4': (0, pyunits.J / pyunits.kmol / pyunits.K ** 4),#eqn2:4.7827E2
                    #     '5': (0, pyunits.J / pyunits.kmol / pyunits.K ** 5)},#eqn2:0
                    # "enth_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.mol),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    # "entr_mol_form_liq_comp_ref": (
                    #     0, pyunits.J / pyunits.kmol / pyunits.K),  # kein Wert zum flüssigen Zustand bei Standardbedingungen
                    "enth_mol_form_vap_comp_ref": (
                        0, pyunits.J/pyunits.kmol),  # [2] pg.2-198 p.239, Table 2-221
                    "entr_mol_form_vap_comp_ref": (
                        1.3057E5, pyunits.J/pyunits.kmol/pyunits.K),  # [2] pg.2-198 p.239, Table 2-221
                    "cp_mol_ig_comp_coeff": {
                            "A": (2.714e1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.665
                            "B": (9.274e-3, pyunits.J/pyunits.mol/pyunits.K**2),
                            "C": (-1.381e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                            "D": (7.645e-9, pyunits.J/pyunits.mol/pyunits.K**4)},

                    "pressure_sat_comp_coeff": {'A': (2.93954, None),  # [1] A.47, pg.765, eqn1
                                                'B': (66.79540, pyunits.K),
                                                'C': (275.65, pyunits.K)}}},

        'H2O': {"type": Component,
                "elemental_composition": {"H": 2, "O": 1, "C": 0},
                # "cp_mol_liq_comp": Perrys,
                "dens_mol_liq_comp": Perrys,
                "enth_mol_liq_comp": Perrys,
                "entr_mol_liq_comp": Perrys,
                "enth_mol_ig_comp": NIST,
                "entr_mol_ig_comp": NIST,
                "pressure_sat_comp": NIST,
                "valid_phase_types": [PT.vaporPhase, PT.liquidPhase],
                "phase_equilibrium_form": {("Vap", "Liq"): fugacity},
                "parameter_data": {
                    "mw": (18.015E-3, pyunits.kg / pyunits.mol),  # [1] A.19, pg. 737
                    "pressure_crit": (220.64E5, pyunits.Pa),  # [1] A.19, pg. 737
                    "temperature_crit": (647.14, pyunits.K),  # [1] A.19, pg. 737
                    "temperature_boil": (373.15, pyunits.K),  # [1] A.19, pg. 737
                    "omega": 0.344,  # [1] A.6, pg. 724
                "dens_mol_liq_comp_coeff": {
                    '1': (4.3910, pyunits.kmol*pyunits.m**-3),  # [2] pg. 138 2-98, table 2-30, Temperature range 403.15-647.13K; 333.15-403.15K:4.9669
                    '2': (2.487E-1, pyunits.kmol*pyunits.m**-3*pyunits.K**-1),#333.15-403.15K: 2.7788E-1
                    '3': (6.4713E2, pyunits.kmol*pyunits.m**-3*pyunits.K**-2),#333.15-403.15K: 6.4713E2
                    '4': (2.534E1, pyunits.kmol*pyunits.m**-3*pyunits.K**-3),#333.15-403.15K: 1.874E-1
                    'eqn_type': 2},
                "cp_mol_liq_comp_coeff": {
                    '1': (2.7637E5, pyunits.J / pyunits.kmol / pyunits.K),  # [2] pg. 215, table 2-196, Range: 272.16-533.15K
                    '2': (-2.0901E3, pyunits.J / pyunits.kmol / pyunits.K ** 2),
                    '3': (8.125, pyunits.J / pyunits.kmol / pyunits.K ** 3),
                    '4': (-1.4116E-2, pyunits.J / pyunits.kmol / pyunits.K ** 4),
                    '5': (9.3701E-6, pyunits.J / pyunits.kmol / pyunits.K ** 5)},
                "enth_mol_form_liq_comp_ref": (
                    -285.83E3, pyunits.J / pyunits.mol),  # von NIST Webbook
                "entr_mol_form_liq_comp_ref": (
                    69.95, pyunits.J / pyunits.mol / pyunits.K),  # von NIST Webbook
                "enth_mol_form_vap_comp_ref": (
                    -241.8264E3, pyunits.J / pyunits.mol),
                    # -24.1814E7, pyunits.J/pyunits.kmol),  # [2] pg.2-199 p.240, Table 2-221
                "entr_mol_form_vap_comp_ref": (
                   188.835, pyunits.J/pyunits.mol/pyunits.K),  # [2]1.8872E5 pg.2-199 p.240, Table 2-221
                "cp_mol_ig_comp_coeff": {
                        'A': (30.09200, pyunits.J/pyunits.mol/pyunits.K),  # [1] temperature range 500 K- 1700 K
                        'B': (6.832514, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-1),
                        'C': (6.793435, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-2),
                        'D': (-2.534480, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**-3),
                        'E': (0.082139, pyunits.J*pyunits.mol**-1*pyunits.K**-1*pyunits.kiloK**2),
                        'F': (-250.8810E3, pyunits.J/pyunits.mol),
                        'G': (223.3967, pyunits.J/pyunits.mol/pyunits.K),
                        'H': (-241.8264E3, pyunits.J/pyunits.mol)},
                            # "A": (3.194e1, pyunits.J/pyunits.mol/pyunits.K),  # [3] p.668
                            # "B": (1.436e-3, pyunits.J/pyunits.mol/pyunits.K**2),
                            # "C": (2.432e-5, pyunits.J/pyunits.mol/pyunits.K**3),
                            # "D": (-1.176e-8, pyunits.J/pyunits.mol/pyunits.K**4)},

                "pressure_sat_comp_coeff": {
                        'A': (4.6543, None),  # [1], temperature range 255.9 K - 373 K
                        'B': (1435.264, pyunits.K),
                        'C': (-64.848, pyunits.K)}}}},

#4-21, 13-7

    # Specifying phases
    "phases":  {
                "Liq": {"type": LiquidPhase,
                        "equation_of_state": Cubic,
                        # "component_list": ["H2O"]},
                        "equation_of_state_options": {
                            "type": CubicType.PR}},
                "Vap": {"type": VaporPhase,
                        "equation_of_state": Cubic,
                        # "component_list": ["CH4", "H2O", "H2", "CO2"],
                        "equation_of_state_options": {
                            "type": CubicType.PR}}},

    # Specifying state definition
    "state_definition": FpcTP, #FpcTP,
    "state_bounds": {#"flow_mol": (0, 100, 1000, pyunits.mol/pyunits.s),
                     "enth_mol": (-1e10, 100, 1e10, pyunits.J/pyunits.mol),
                     "flow_mol_phase_comp": (0, 30, 200, pyunits.mol/pyunits.s),
                     "temperature": (283.15, 500, 1300, pyunits.K),
                     "pressure": (5e4, 5e5, 31e5, pyunits.Pa)},
                    #"mole_frac_comp": {"H2O":(0,0.5,1),"CO2":(0,0.5,1),
                    #                   "CH4":(0,0.5,1),"H2":(0,0.5,1)}},
    "pressure_ref": (101325, pyunits.Pa),
    "temperature_ref": (298.15, pyunits.K),

    # Defining phase equilibria
    "phases_in_equilibrium": [("Vap", "Liq")],
    "phase_equilibrium_state": {("Vap", "Liq"): SmoothVLE},
    "bubble_dew_method": IdealBubbleDew,
    "parameter_data": {"PR_kappa": {("CH4", "CH4"): 0.000, #DATA from AspenPlus Simulation
                                    ("CH4", "CO2"): 0.0919,
                                    ("CH4", "H2O"): 0.000,
                                    ("CH4", "H2"): 0.0156,
                                    ("CO2", "CO2"): 0.000,
                                    ("CO2", "CH4"): 0.0919,
                                    ("CO2", "H2O"): 0.12,
                                    ("CO2", "H2"): -0.1622,
                                    ("H2", "H2"): 0.000,
                                    ("H2", "CH4"): 0.0156,
                                    ("H2", "CO2"): -0.1622,
                                    ("H2", "H2O"): 0.000,
                                    ("H2O", "H2O"): 0.000,
                                    ("H2O", "CH4"): 0.000,
                                    ("H2O", "CO2"): 0.12,
                                    ("H2O", "H2"): 0.000},
                        "gas_const":(8.314, pyunits.J/pyunits.mol/pyunits.K)}}
