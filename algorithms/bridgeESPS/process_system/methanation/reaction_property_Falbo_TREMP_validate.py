##############################################################################
# Institute for the Design of Advanced Energy Systems Process Systems
# Engineering Framework (IDAES PSE Framework) Copyright (c) 2018-2020, by the
# software owners: The Regents of the University of California, through
# Lawrence Berkeley National Laboratory,  National Technology & Engineering
# Solutions of Sandia, LLC, Carnegie Mellon University, West Virginia
# University Research Corporation, et al. All rights reserved.
#
# Please see the files COPYRIGHT.txt and LICENSE.txt for full copyright and
# license information, respectively. Both files are also available online
# at the URL "https://github.com/IDAES/idaes-pse".
##############################################################################

"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang
"""
Reaction properties of Sabatier reaction CO2 + 4 H2 -> CH4 + 2 H2O
Kinetic approach based on Falbo, 2018
"""

# Import Pyomo libraries
from pyomo.environ import (Constraint,
                           exp,
                           Param,
                           Set,
                           units as pyunits,
                           Var,
                           log, Expression
                           )

# Import IDAES cores
from idaes.core import (declare_process_block_class,
                        MaterialFlowBasis,
                        ReactionParameterBlock,
                        ReactionBlockDataBase,
                        ReactionBlockBase)
from idaes.core.util.constants import Constants as const
import idaes.logger as idaeslog
from idaes.core.util.misc import add_object_reference
import math

# Set up logger
_log = idaeslog.getLogger(__name__)


@declare_process_block_class("MethanationReactionParameterBlock")
class MethanationReactionParameterData(ReactionParameterBlock):
    """
    Reaction Parameter Block Class
    """

    def build(self):
        '''
        Callable method for Block construction.
        '''
        super(MethanationReactionParameterData, self).build()

        self._reaction_block_class = MethanationReactionBlock

        # Rate Reaction Index
        self.rate_reaction_idx = Set(initialize=["R1"])

        # Rate Reaction Stoichiometry
        self.rate_reaction_stoichiometry = {("R1", "Vap", "CO2"): -1,
                                            ("R1", "Vap", "H2"): -4,
                                            ("R1", "Vap", "CH4"): 1,
                                            ("R1", "Vap", "H2O"): 2}

        # Paper: GHSV = 5 L(STP)/h/g_cat ; STP: 273.15K, 1,013 bar
        # 500kW: nH2=2.25 mol/s, nCO2=0.56 mol/s --> Vges = 226560 L/h
        # gCat = 45312.084409966956
        rho_norm_CO2 = 1.977 # kg/m³
        rho_norm_H2 = 0.08988 # kg/m³

        self.alpha = Param(default=0.91, units=1/pyunits.Pa)
        self.gKat = Param(default=146.63E3, doc="gKat")
        self.bulk_den = Param(default=5E3) # 5E3 850 kg/m³; Paper: V=2.186e-5 m³, gCat = 0.375g -> 17.186kg/m³ 17.186E3
        self.n = Param(default=0.152, doc="Fit exponent")

        # Arrhenius Constant
        self.ko_ref = Param(
            default=95.43* self.bulk_den,
            doc="Rate constant ref",
            units=pyunits.mol/pyunits.Pa**(5*self.n)/pyunits.s/pyunits.m**3) #mol*bar^(-0.54)*s^(-1)*gcat*(-1)
        # TODO: Does not convert kJ in J, but 93600 leads to error
        # Activation Energy
        self.energy_activation = Param(default=75.3e3,
                                       doc="Activation energy",
                                       units=pyunits.J/pyunits.mol)

        # Reference temperature
        self.T_ref = Param(default=(310+273.15),
                           doc="Reference temperature",
                           units=pyunits.K)

        # Heat of Reaction
        dh_rxn_dict = {"R1": -165e3}
        self.dh_rxn = Param(self.rate_reaction_idx,
                            initialize=dh_rxn_dict,
                            doc="Heat of reaction [J/mol]")

    @classmethod
    def define_metadata(cls, obj):
        obj.add_properties({
                # 'k_rxn': {'method': '_rate_constant', 'units': 'mol/Pa^0.54.s'}
                'reaction_rate': {'method':'_rxn_rate', 'units': 'mol/s'}
                # 'k_eq': {'method': '_keq_constant', 'units': '1/Pa^(2*0.076)'}
                })
        obj.add_default_units({'time': pyunits.s,
                               'length': pyunits.m,
                               'mass': pyunits.kg,
                               'amount': pyunits.mol,
                               'temperature': pyunits.K})


class _MethanationReactionBlock(ReactionBlockBase):
    """
    This Class contains methods which should be applied to Reaction Blocks as a
    whole, rather than individual elements of indexed Reaction Blocks.
    """
    def initialize(blk, outlvl=idaeslog.NOTSET, **kwargs):
        '''
        Initialization routine for reaction package.

        Keyword Arguments:
            outlvl : sets output level of initialization routine

        Returns:
            None
        '''
        init_log = idaeslog.getInitLogger(blk.name, outlvl, tag="properties")
        init_log.info('Initialization Complete.')


@declare_process_block_class("MethanationReactionBlock", block_class=_MethanationReactionBlock)
class MethanationReactionBlockData(ReactionBlockDataBase):
    """
    An example reaction package for saponification of ethyl acetate
    """

    def build(self):
        """
        Callable method for Block construction
        """
        super(MethanationReactionBlockData, self).build()
    #TODO: Normale Arrhenius Gleichung
    #TODO: Masse Katalysator bestimmen
        # Heat of reaction - no _ref as this is the actual property
        add_object_reference(
                self,
                "dh_rxn",
                self.config.parameters.dh_rxn)
    # def _rate_constant(self):
    #     self.k_rxn = Var(
    #         initialize=1.4569982759109257, #700K: 194.9602453830113, 650K: 1.4569982759109257, 750K: 9.3392599474385
    #         doc="Rate constant",
    #         bounds=(0, self.params.ko_ref),
    #         units=pyunits.mol/pyunits.Pa**(5*self.params.n)/pyunits.s/pyunits.m**3)
    #     try:
    #         self.arrhenius_eqn = Constraint(
    #             expr=self.k_rxn == self.params.ko_ref * exp(
    #                 (-self.params.energy_activation / const.gas_constant / self.state_ref.temperature)))
    #         # self.arrhenius_eqn = Constraint(
    #         #     expr=self.k_rxn == self.params.ko_ref * exp(
    #         #         (-self.params.energy_activation / const.gas_constant) * (
    #         #                     1 / self.state_ref.temperature - 1 / self.params.T_ref)))
    #     except AttributeError:
    #         # If constraint fails, clean up so that DAE can try again later
    #         self.del_component(self.k_rxn)
    #         self.del_component(self.arrhenius_eqn)
    #         raise
    # def _keq_constant(self):
    #     self.k_eq = Var(initialize=4796.478041072786, # 700 K: 439.0524028716858, 650K: 4796.478041072786, 750K:54.21599604376753
    #                       doc="Equilibrium constant",
    #                     bounds=(0,943406686.5306958), ## 200°C
    #                       units=pyunits.Pa**(-2*self.params.n))     ### ????
    #     try:
    #         self.keq_equation = Constraint(
    #             expr=self.k_eq == exp(
    #                 (1 / 1.987) * (56000 / (self.state_ref.temperature ** 2) + (34633 / self.state_ref.temperature)
    #                              - 16.4 * log(self.state_ref.temperature)
    #                              + 0.00577 * self.state_ref.temperature) + 33.165))
    #     except AttributeError:
    #         self.del_component(self.k_eq)
    #         self.del_component(self.keq_equation)
    #         raise

    def _rxn_rate(self):
        self.reaction_rate = Var(self.params.rate_reaction_idx,
                                 initialize=0.1,
                                 bounds=(1e-8, 50),
                                 doc="Rate of reaction",
                                 units=pyunits.mol/pyunits.s/pyunits.m**3) ### ????
        try:
            def rate_rule(b, r):
                self.k_rxn2 = Expression(expr=self.params.ko_ref * exp(
                    (-self.params.energy_activation / const.gas_constant / self.state_ref.temperature)))
                self.k_eq2 = Expression(expr=exp(
                    (1 / 1.987) * (56000 / (self.state_ref.temperature ** 2) + (34633 / self.state_ref.temperature)
                                   - 16.4 * log(self.state_ref.temperature)
                                   + 0.00577 * self.state_ref.temperature) + 33.165))
                self.pH2O = Expression(expr=self.state_ref.pressure*1e-5/1.01325*(
                    self.state_ref.flow_mol_phase_comp["Vap", "H2O"] / (
                    self.state_ref.flow_mol_phase_comp["Vap", "H2O"] + self.state_ref.flow_mol_phase_comp["Vap", "CH4"] +
                    self.state_ref.flow_mol_phase_comp["Vap", "CO2"] + self.state_ref.flow_mol_phase_comp["Vap", "H2"])))
                self.pH2 = Expression(expr=self.state_ref.pressure*1e-5/1.01325*(
                    self.state_ref.flow_mol_phase_comp["Vap", "H2"] / (
                    self.state_ref.flow_mol_phase_comp["Vap", "H2O"] + self.state_ref.flow_mol_phase_comp["Vap", "CH4"] +
                    self.state_ref.flow_mol_phase_comp["Vap", "CO2"] + self.state_ref.flow_mol_phase_comp["Vap", "H2"])))
                self.pCH4 = Expression(expr=self.state_ref.pressure*1e-5/1.01325*(
                    self.state_ref.flow_mol_phase_comp["Vap", "CH4"] / (
                    self.state_ref.flow_mol_phase_comp["Vap", "H2O"] + self.state_ref.flow_mol_phase_comp["Vap", "CH4"] +
                    self.state_ref.flow_mol_phase_comp["Vap", "CO2"] + self.state_ref.flow_mol_phase_comp["Vap", "H2"])))
                self.pCO2 = Expression(expr=self.state_ref.pressure*1e-5/1.01325*(
                    self.state_ref.flow_mol_phase_comp["Vap", "CO2"] / (
                    self.state_ref.flow_mol_phase_comp["Vap", "H2O"] + self.state_ref.flow_mol_phase_comp["Vap", "CH4"] +
                    self.state_ref.flow_mol_phase_comp["Vap", "CO2"] + self.state_ref.flow_mol_phase_comp["Vap", "H2"]))
                )
                # return b.reaction_rate[r] == (
                #             b.k_rxn *
                #             (b.state_ref.mole_frac_comp["H2"] * b.state_ref.pressure*1e-5)**(4*self.params.n) *
                #             (b.state_ref.mole_frac_comp["CO2"] * b.state_ref.pressure*1e-5)**self.params.n *
                #             (1 - ((b.state_ref.mole_frac_comp["CH4"]**self.params.n*b.state_ref.mole_frac_comp["H2O"]**(2*self.params.n)
                #                    *(b.state_ref.pressure*1e-5)**(3*self.params.n))/
                #                   (b.state_ref.mole_frac_comp["H2"]**(4*self.params.n)*b.state_ref.mole_frac_comp["CO2"]**self.params.n
                #                    *b.k_eq**self.params.n*(b.state_ref.pressure*1e-5)**(5*self.params.n)))))
                s = 0.001


                #
                #         k_rxn         (                       pCH4**n * pH2O**n       )
                # r = ______________ *  | pCO2**n * pH2**4n - ________________________  |
                #       1+alpha*pH20    |                               K_eq**n         |
                #                       (                                               )

                return b.reaction_rate[r] == (
                            self.k_rxn2/(1+self.params.alpha*self.pH2O) *(
                            (self.pH2)**(4*self.params.n) *
                            (self.pCO2)**self.params.n -
                            (((self.pCH4)**(self.params.n)*
                              (self.pH2O)**(2*self.params.n)
                                  )/self.k_eq2**self.params.n)))
            self.rate_expression = Constraint(self.params.rate_reaction_idx,
                                              rule=rate_rule)
        except AttributeError:
            # If constraint fails, clean up so that DAE can try again later
            self.del_component(self.reaction_rate)
            self.del_component(self.rate_expression)
            raise



        # self.rate_expression = Constraint(
        #     expr=self.reaction_rate["R1"] ==
        #     self.k_rxn*(self.state_ref.mole_frac_comp["CO2"]**(self.params.n)*self.state_ref.mole_frac_comp["H2"]**(4*self.params.n)*
        #                 self.state_ref.pressure**(5*self.params.n) -
        #                 (self.state_ref.mole_frac_comp["CH4"]**(self.params.n)*self.state_ref.mole_frac_comp["H2O"]**(2*self.params.n)
        #                  *self.state_ref.pressure**(3*self.params.n))/self.k_eq**self.params.n))





    def get_reaction_rate_basis(b):
        return MaterialFlowBasis.molar
    def model_check(blk):
        pass
