"""Case study for bridging energy and process system optimization"""
#
# This file is part of the PtG-ES4 project which is released under the MIT
# license. See file LICENSE for full license details.
#
#
# AUTHORS: Luka Bornemann, Yifan Wang

from pathlib import Path

def set_size(use=True, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to sit nicely in our document.

    Parameters
    ----------
    width_pt: float
            Document width in points
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    width_pt = 422.52347
    height_pt = 635.5
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    if use==True:
        # Figure height in inches
        fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])
    else:
        fig_height_in =fig_width_in * golden_ratio*1.3 * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)


def plot_part_load(options=None, titel=None, relative=True):
    import numpy as np
    import pandas as pd

    import matplotlib as mpl
    import matplotlib.pyplot as plt

    mpl.rcParams.update({
            'text.usetex': True,
            'text.latex.preamble': "\n".join([
            r'\usepackage{amsmath}',
            r'\usepackage{amssymb}',
            r'\usepackage{units}',
            r'\usepackage[version=4]{mhchem}',
            # r'\usepackage{siunitx},'
            ]),
             # 'font.size': 11,
             'font.family': 'lmodern',
    })
    mpl.rcParams['lines.markersize'] = 3

    ###
    ### 1. read data from excel
    ###

    input_file = f'{options["input_folder"]}\\{options["filename"]}'

    df = pd.read_excel(input_file)
    df.to_dict()

    energy_out = df['energy_feed_out']
    if relative == True:
        Q_N = energy_out[0]
    else:
        Q_N = 1
    x_ticks = np.arange(0, 600, step=100)

    ###
    ###  define which plot
    ###

    ### Fig. 1
    plot_processEnergyDemand = True

    ### Fig. 2
    plot_reactor_T_P = True
    plot_condenser_T = True
    plot_Recycling = True
    plot_lambdaH2CO2 = True

    ### Fig. 3
    plot_specificCost = True
    plot_specificCost_pConst = True

    ### Fig. 4
    plot_efficiency = True

    ########################################## Ploting ##########################################
    ###
    ### Fig. 1
    ###
    if plot_processEnergyDemand:

        ###
        ### calculate cooling demand
        cooling_demand = {
            'HE1': df['hd_101'] * (-1) / Q_N,
            'HE2': df['hd_102'] * (-1) / Q_N,
            'HE3': df['hd_103'] * (-1) / Q_N,
            'HE5': df['hd_105'] * (-1) / Q_N,
            'HE7': df['hd_107'] * (-1) / Q_N,
            # 'R101': df['hd_R101']*(-1)/Q_N,
            # 'R102': df['hd_R102']*(-1)/Q_N,
        }

        ###
        ### calculate heating demand
        heating_demand = {
            # 'Pre heater': df['hd_101'] * (1) / Q_N,
            'HE4': df['hd_104'] * (1) / Q_N,
            'HE6': df['hd_106'] * (1) / Q_N,

        }

        ###
        ### calculate electricity demand
        el_demand = {
            # 'Pre heater': df['hd_101'] * (1) / Q_N,
            'C1': df['ed_1'] * (1) / Q_N,
            'C2': df['ed_2'] * (1) / Q_N,

        }

        ###
        ### plot cooling demand
        ###
        cooling_demand_fit = np.polyfit(energy_out / Q_N, sum(cooling_demand.values()), deg=1)

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

        ax.stackplot(energy_out / Q_N, cooling_demand.values(),
                     labels=cooling_demand.keys(), alpha=0.8)
        # ax2.plot(energy_out/Q_N, cooling_demand_fit[1] + cooling_demand_fit[0]*energy_out/Q_N, 'r-')
        ax.legend(loc='upper left', ncol=2, prop={'size': 8}, frameon=False)
        # ax2.set_title('Kühlbedarf')

        ax.set_xticks(x_ticks)
        ax.set_xlim([0, 500])

        ax.set_ylim([0, 200])
        ax.set_ylabel(r'$\dot{E}^\mathrm{buy, PS}_\mathrm{MP, cooling}$ in kW')
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_4}$ in kW')
        # ax.set_title('Teillast: SNG Kosten')
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_cooling_demand.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_cooling_demand.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

        ###
        ### plot heating demand
        ###
        heating_demand_fit = np.polyfit(energy_out / Q_N, sum(heating_demand.values()), deg=1)
        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        ax.stackplot(energy_out / Q_N, heating_demand.values(),
                     labels=heating_demand.keys(), alpha=0.8)
        # ax2.plot(energy_out/Q_N, heating_demand_fit[1] + heating_demand_fit[0] * energy_out/Q_N, 'r-')
        ax.legend(loc='upper left', prop={'size': 8}, frameon=False)
        # ax2.set_title('Heizbedarf')
        ax.set_xlim([0, 500])
        ax.set_ylim([0, 20])
        ax.set_xticks(x_ticks)

        ax.set_ylabel(r'$\dot{E}^\mathrm{buy, PS}_\mathrm{MP, heating}$ in kW')
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_4}$ in kW')
        # ax.set_title('Teillast: SNG Kosten')
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_heating_demand.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_heating_demand.pdf', format='pdf', bbox_inches='tight')
        #plt.show()

        ###
        ### plot electricity demand
        ###
        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

        ax.stackplot(energy_out / Q_N, el_demand.values(),
                     labels=el_demand.keys(), alpha=0.8)
        # ax2.set_title('Heizbedarf')
        ax.legend(loc='upper left', prop={'size': 8}, frameon=False)
        ax.set_xlim([0, 500])
        ax.set_ylim([0, 40])
        ax.set_xticks(x_ticks)

        ax.set_ylabel(r'$\dot{E}^\mathrm{buy, PS}_\mathrm{MP, electricity}$ in kW')
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_4}$ in kW')
        # ax.set_title('Teillast: SNG Kosten')
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_electricity_demand.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_electricity_demand.pdf', format='pdf', bbox_inches='tight')
        #plt.show()

    ###
    ### Fig. 2
    ###
    if plot_reactor_T_P:
        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

        ax.plot(energy_out / Q_N, 'T_R101_in', 'o-', data=df, label='R1')
        ax.plot(energy_out / Q_N, 'T_R102_in', 's-', data=df, label='R2')
        ax.plot(energy_out / Q_N, 'T_R103_in', '^-', data=df, label='R3')
        ax.plot(energy_out / Q_N, 'T_R104_in', 'D-', data=df, label='R4')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([240, 400])
        ax.set_yticks([240, 280, 320, 360, 400])
        ax.set_ylabel(r'Temperature in °C')

        # ax.set_title('Reaktortemperaturen - Eingang')
        ax.legend(loc='upper left', ncol=2, prop={'size': 8}, frameon=False)
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_temp_in.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_temp_in.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        ax.plot(energy_out / Q_N, 'T_R101_out', 'o-', data=df, label='R1')
        ax.plot(energy_out / Q_N, 'T_R102_out', 's-', data=df, label='R2')
        ax.plot(energy_out / Q_N, 'T_R103_out', '^-', data=df, label='R3')
        ax.plot(energy_out / Q_N, 'T_R104_out', 'D-', data=df, label='R4')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([300, 650])
        ax.set_yticks([300, 450, 600])
        ax.set_ylabel(r'Temperature in °C')
        # ax.set_title('Reaktortemperaturen - Ausgang')
        ax.legend(loc='upper left', ncol=2, prop={'size': 8}, frameon=False)
        # ax.grid()
        # fig.tight_layout()  # otherwise the right y-label is slightly clipped
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_temp_out.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_temp_out.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        ax.plot(energy_out / Q_N, 'pressure_R101_in', 'o-', data=df, label='R101')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([1, 20])
        ax.set_yticks([1, 5, 10, 15, 20])
        ax.set_ylabel(r'Pressure in bar')
        # ax.set_title('Reaktortemperaturen - Ausgang')
        # ax.legend(loc='upper left',  ncol=4, prop={'size': 8} )
        # ax.grid()
        # fig.tight_layout()  # otherwise the right y-label is slightly clipped
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_pressure.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_pressure.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

    if plot_condenser_T:
        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        # fig.suptitle('Kondensatortemperaturen')

        ax.plot(energy_out / Q_N, 'T_HEX103_out', 'o-', data=df, label='HE3')
        ax.plot(energy_out / Q_N, 'T_HEX105_out', 's-', data=df, label='HE5')
        ax.plot(energy_out / Q_N, 'T_HEX107_out', '^-', data=df, label='HE7')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([25, 40])
        ax.set_ylabel(r'Temperature in °C')
        ax.legend(loc='upper left', ncol=1, prop={'size': 8}, frameon=False)
        # ax.grid()

        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_con.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_con.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

    if plot_Recycling:
        sng_cost_fit = np.polyfit(energy_out / Q_N, df['sng_cost'], deg=1)

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        ax.plot(df['energy_feed_out'] / Q_N, df['rec'], 'o-')
        # ax.plot(energy_out/Q_N, sng_cost_fit[1] + sng_cost_fit[0]*energy_out/Q_N)

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylabel(r'$Rec$ in $\%$')

        # ax.set_title('Recycling Rate')
        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_rec.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_rec.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

    if plot_lambdaH2CO2:

        sng_cost_fit = np.polyfit(energy_out / Q_N, df['sng_cost'], deg=1)

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))
        ax.plot(df['energy_feed_out'] / Q_N, df['H2/CO2'], 'o-')
        # ax.plot(energy_out/Q_N, sng_cost_fit[1] + sng_cost_fit[0]*energy_out/Q_N)

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([3.8, 4.2])
        ax.set_ylabel(r'$\lambda_{\ce{H2}/\ce{CO2}}$')
        # ax.set_title('Recycling Rate')

        fig.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_lambda_h2co2.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_lambda_h2co2.pdf', format='pdf', bbox_inches='tight')
        # plt.show()

    ###
    ### Fig. 3
    ###
    if plot_specificCost:

        sng_cost_fit = np.polyfit(energy_out / Q_N, df['sng_cost'], deg=1)
        k_cool = 5 / 1000
        k_el = 0.2

        cool_cost = (df['cooling_demand'] - df['heating_demand']) * k_cool / energy_out * 100
        el_cost = df['electricity_demand'] * k_el / energy_out * 100
        ges_cost = df['sng_cost']

        ymin = min(cool_cost.min(), el_cost.min(), ges_cost.min())
        ymax = max(cool_cost.max(), el_cost.max(), ges_cost.max())

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

        ax.plot(df['energy_feed_out'] / Q_N, cool_cost, 'o-', label='cooling')
        ax.plot(df['energy_feed_out'] / Q_N, el_cost, 's-', label='electricity')
        ax.plot(df['energy_feed_out'] / Q_N, ges_cost, '^-', label='sum')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_4}$ in kW')

        ax.set_ylim([0, 2])
        ax.set_ylabel( r'$\tilde{c}^{\tilde{e}}_\mathrm{MP, CH_4}$ in $\unitfrac{\mathrm{ct}}{\mathrm{kWh}_\mathrm{SNG}}$')

        ax.legend(loc='best', ncol=2, prop={'size': 8}, frameon=False)
        # ax.set_title('Teillast: SNG Kosten')
        fig.tight_layout()
        #plt.savefig(f'{options["output_folder"]}\\partA_specificCost.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_specificCost.pdf', format='pdf', bbox_inches='tight')
        # plt.savefig(f'./pictures/kuehlungs_kosten_kalibrierung.eps', format='eps', dpi=1200, bbox_inches='tight')
        # plt.show()

    if plot_specificCost_pConst:
        import matplotlib.pyplot as plt
        import matplotlib.colors as col

        x = np.arange(20, 520, 20)

        P = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

        def f(x, y):
            return np.sin(x) ** 10 + np.cos(10 + y * x) * np.cos(x)

        X, Y = np.meshgrid(x, P)
        Z = f(X, Y)
        z = np.asarray(X) * 0

        # par_list = ['etha_chem', 'heating_demand', 'cooling_demand', 'sng_cost', ]
        par_list = ['sng_cost']

        for par in par_list:
            for p in P:
                filename = f'{options["input_folder"]}\\multi_pressure_TREMP_{titel}_P_{p}.xlsx'

                df = pd.read_excel(filename)

                e = df[par]
                e = e.to_numpy()
                e = np.flip(e)
                # idx = (e == -1)
                # e[idx] = 0
                if p == P[0]:
                    zz = e

                if p != P[0]:
                    zz = np.vstack((zz,e))
            idxx = (zz != -1)
            minZ = min(zz[idxx])
            maxZ = max(zz[idxx])
            idx = (zz == -1)
            # if par == 'heating_demand' or par == 'cooling_demand':
            zz[idx] = -10
            fig, ax = plt.subplots(1, 1, figsize=set_size(use=False, fraction=0.5, subplots=(1,1)))
            # plt.set_cmap('hot')
            # plt.pcolormesh(X, Y, zz, vmin=(minZ-0.1*minZ), shading='auto')
            norm = col.Normalize(minZ-0.1*minZ, maxZ, clip=True)


            cs = plt.contourf(X, Y, zz, levels=np.arange(0.99*minZ, 1.01*maxZ, (1.01*maxZ-0.99*minZ)/30), cmap='viridis', antialiased=False, extend='both')

            cs.cmap.set_under('k')#'#929591')


            #cs.set_clim(0.99*minZ,1.01*maxZ)
            cs.set_clim(0.5, 2.0)
            from matplotlib.cm import ScalarMappable

            quadcontourset = ax.contourf(
                X, Y, zz,
                levels=np.arange(0.99*minZ, 1.01*maxZ, (1.01*maxZ-0.99*minZ)/30),  # change this to `levels` to get the result that you want
                cmap='viridis', antialiased=False, extend='both',
                vmin=0.8, #YW 0.99*minZ,
                vmax=1.6 #YW1.01*maxZ
            )
            #level_boundaries = np.linspace(0.99*minZ, 1.01*maxZ, (1.01*maxZ-0.99*minZ)/30)

            fig.colorbar(
                ScalarMappable(norm=quadcontourset.norm, cmap=quadcontourset.cmap),
                ticks=[0.8, 1.2, 1.6]
                # boundaries=level_boundaries,
                # values=(level_boundaries[:-1] + level_boundaries[1:]) / 2,
            )

            ax.set_ylabel(r'Pressure in bar')
            x_ticks = [20,100,200,300,400,500]
            y_ticks = [1, 5, 10, 15, 20]
            ax.set_xlim([20, 500])
            ax.set_xticks(x_ticks)
            ax.set_yticks(y_ticks)
            ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_4}$ in kW')
            if par == 'etha_chem':
                ax.set_title(r'$\eta_\mathrm{chem}$ in $\%$')
            elif par == 'sng_cost':
                ax.set_title(r'$\tilde{c}^{\mathrm{sum}}_\mathrm{MP, CH_4}$ in $\unitfrac{\mathrm{ct}}{\mathrm{kWh}_\mathrm{SNG}}$')
            elif par == 'heating_demand':
                ax.set_title(r'$\dot{Q}^\mathrm{W\ddot{a}rme}$ in kW')
            elif par == 'cooling_demand':
                ax.set_title(r'$\dot{Q}^\mathrm{K\ddot{a}lte}$ in kW')
            fig.set_tight_layout(True)
            plt.tight_layout()
            # plt.savefig(f'{options["output_folder"]}/partA_{par}.svg', format='svg', dpi=1200, bbox_inches='tight')
            plt.savefig(f'{options["output_folder"]}/partA_{par}.pdf', format='pdf', bbox_inches='tight')

            plt.show()

    ##
    ### Fig. 4
    ###
    if plot_efficiency:
        etha_chem_fit = np.polyfit(energy_out / Q_N, df['etha_chem'], deg=1)
        etha_ener_fit = np.polyfit(energy_out / Q_N, df['etha_ener'], deg=1)

        fig, ax = plt.subplots(1, 1, figsize=set_size(use=True, fraction=0.5, subplots=(1, 1)))

        ax.plot(df['energy_feed_out'] / Q_N, df['etha_chem'] * 100, 'o-')

        ax.set_xlim([0, 500])
        ax.set_xticks(x_ticks)
        ax.set_xlabel(r'$ \dot{E}^\mathrm{out, PS}_\mathrm{MP, CH_{4}}$ in kW')

        ax.set_ylim([0, 100])
        ax.set_ylabel(r'$\eta_\mathrm{MP,CH_4}$ in $\%$')

        fig.set_tight_layout(True)
        plt.tight_layout()
        # plt.savefig(f'{options["output_folder"]}\\partA_efficiency.svg', format='svg', dpi=1200, bbox_inches='tight')
        plt.savefig(f'{options["output_folder"]}\\partA_efficiency.pdf', format='pdf', bbox_inches='tight')
        # plt.savefig(f'./pictures/wirkungsgrad_kalibrierung.eps', format='eps', dpi=1200, bbox_inches='tight')

    plt.show()

if __name__ == "__main__":

    ###
    ### plot results of part A
    ###

    ### define file paths
    options = {
        'filename': 'part_load_purity_95_TREMP_500_20_paper.xlsx',
        'input_folder': Path('./results_step2'),
        'output_folder': Path('./pictures_step2')
    }



    plot_part_load(options=options, titel='paper', relative=False)