<!-- This file is part of the PtG-ES4 project which is released under the MIT 
license. See file LICENSE.txt for full license details.

AUTHOR: Yifan Wang -->
# PtG-ES4 Algorithms: PtGES

## Description
This directory is under construction. In case of emergency, please contact the author directly.
## Structure 
```
ptg-es4/algorithms:
|-- PtGES
    |-- components models
    |-- method
    
    |-- input
    |-- output

    |-- case_study_w_gGrid.py
    |-- case_study_wo_gGrid.py
    |-- env_PtGES_py38.yml
    |-- postProcessing.py
```

## Referencing
```bibtex
@incollection{wang2023power,
  title={A Power-to-Gas energy system: modeling and operational optimization for seasonal energy supply and storage},
  author={Wang, Yifan and Bornemann, Luka and Reinert, Christiane and von der Assen, Niklas},
  booktitle={Computer Aided Chemical Engineering},
  volume={52},
  pages={2867--2872},
  year={2023},
  publisher={Elsevier}
}
```

## Getting started

