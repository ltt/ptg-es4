<!-- This file is part of the PtG-ES4 project which is released under the MIT 
license. See file LICENSE.txt for full license details.

AUTHOR: Yifan Wang -->
# PtG-ES4: Power-to-gas energy systems for seasonal energy supply and storage

## Project Description
This ongoing project provides the source code for our models and methods developed for the mathematical optimization of
power-to-gas energy systems, which involves Mixed-Integer Linear Programming (MILP) and Nonlinear Programming (NLP) 
problems. 
 
Based on a power-to-gas test field located in Duisburg, Germany (Research Project [PtG-MSE](https://www.zbt.de/aktuell/presse/pressemitteilungen/pressemitteillungen-anzeigeseite/detail/News/power-to-gas-modellvorhaben-fuer-sektorenuebergreifende-energiesysteme-abgeschlossen/),
 as of June 2023), the general structure of a power-to-gas energy system can be illustrated as follows:  


![](ptg_es.png)


## Project Structure
The entire project is structured generally as follows:
````
PtG-ES4 project
|-- algorithms
    |-- bridgeESPS
        |-- method
        |-- case_study
        |-- env_bridgeESPS_py38.yml
        |-- README_bridgeESPS.md
    |-- RiNSES4
        |-- method
        |-- case_study
        |-- env_bridgeESPS_py38.yml
        |-- README_bridgeESPS.md
    |-- PtGES
        |-- method
        |-- case_study
        |-- env_PtGES_py38.yml
        |-- README_PtGES.md
    |-- other ongoing algorithms  
|-- comando
|-- components
|-- docs
|-- examples
|-- README.md
|-- .gitignore
````

## Referencing
When referencing the PtG-ES4 project in an academic context, please include proper citations.

[[1] A Power-to-Gas energy system: modeling and operational optimization for seasonal energy supply and storage](https://www.sciencedirect.com/science/article/pii/B978044315274050456X)

- The source code can be found under [/algorithms/PtGES/](./algorithms/PtGES/README_PtGES.md) directory.

```bibtex
@incollection{Wang2023,
  title={A Power-to-Gas energy system: modeling and operational optimization for seasonal energy supply and storage},
  author={Wang, Yifan and Bornemann, Luka and Reinert, Christiane and von der Assen, Niklas},
  booktitle={Computer Aided Chemical Engineering},
  volume={52},
  pages={2867--2872},
  year={2023},
  doi = {https://doi.org/10.1016/j.compchemeng.2023.108582},
  url ={https://www.sciencedirect.com/science/article/pii/B978044315274050456X}
  publisher={Elsevier}
}
```

[[2] A method to bridge energy and process system optimization: Identifying the feasible operating space
for a methantation process in power-to-gas energy systems](https://www.sciencedirect.com/science/article/pii/S0098135423004520?dgcid=coauthor).

- The source code can be found under [/algorithms/bridgeESPS/](./algorithms/bridgeESPS/README_bridgeESPS.md) directory.

```bibtex
@article{Wang2024a,
title = {A method to bridge energy and process system optimization: Identifying the feasible operating space for a methanation process in power-to-gas energy systems},
journal = {Computers & Chemical Engineering},
volume = {182},
pages = {108582},
year = {2024},
issn = {0098-1354},
doi = {https://doi.org/10.1016/j.compchemeng.2023.108582},
url = {https://www.sciencedirect.com/science/article/pii/S0098135423004520},
author = {Yifan Wang and Luka Bornemann and Christiane Reinert and Niklas {von der Assen}},
}
```

[[3] RiNSES4: Rigorous Nonlinear Synthesis of Energy Systems for Seasonal Energy Supply and Storage](https://psecommunity.org/LAPSE:2024.1583).

- The source code can be found under [/algorithms/RiNSES4/](./algorithms/RiNSES4/README_RiNSES4.md) directory.

```bibtex
@article{Wang2024b,
title = {RiNSES4: Rigorous Nonlinear Synthesis of Energy Systems for Seasonal Energy Supply and Storage},
journal = {Systems and Control Transactions},
volume = {3},
pages = {604--611},
year = {2024},
doi = {https://doi.org/10.69997/sct.105466},
url = {https://psecommunity.org/LAPSE:2024.1583},
author = {Yifan Wang, Marvin Volkmer, Dörthe Franzisca Hagedorn, Christiane Reinert and Niklas {von der Assen}},
}
```
## License
This project is licensed under the MIT License, for more information please refer to the [LICENSE](LICENSE.txt) file.

## Documentation and Support
The PtG-ES4 project is developed using the COMANDO and IDAES plateforms, employing Python programming language 
versions 3.7 and 3.8. The necessary Python environments vary according to the specific algorithms used. 
For installation instructions, please refer to the README file located in each respective algorithm's directory.

Comprehensive background information and documentation for the COMANDO and IDAES platforms are readily available on 
their respective websites. You can explore detailed resources at:

- COMANDO
    - [COMANDO's Documentation](https://comando.readthedocs.io/en/latest)
    - [COMANDO's Source Code](https://jugit.fz-juelich.de/iek-10/public/optimization/comando)

- IDAES
    - [IDAES's Documentation](https://idaes-pse.readthedocs.io/en/stable/)
    - [IDAES's Source Code](https://github.com/IDAES/idaes-pse)

For models and methods developed in our GitLab repository, you are welcome to initiate discussions, report issues or bugs, 
and contribute to ongoing projects. Should you have any further questions or need assistance, please feel free to 
reach out via email at [yifan.wang@ltt.rwth-aachen.de](yifan.wang@ltt.rwth-aachen.de).
